<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function resetPasword()
    {
        return view('templates/resetpassword');
    }

    public function Paswordreset(Request $request)
    {

        $this->validate($request, [
            'password' => 'required|min:3|max:20',
            'confirmpassword' => 'required|min:3|max:20|same:password'
        ]);
        $id = decrypt($request->id);

        $user = Users::where('id', '=', $id)->first();

        $password = Hash::make($request->password);
        $updateUser = $user->update(['password' => $password]);
        if ($updateUser) {
            return Redirect::to('/');
//    return Redirect::back()->with('status', 'Password Updated Successfully.');

        }
//        return Redirect::back()->with('status', 'Password Updated Successfully.');
    }
}
