<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\General\UtilityHelper;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Users;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function customerLogin()
    {
        $rules = array(
            'username' => 'required|string|max:255', // make sure the email is an actual email
            'password' => 'required|string|min:6',
            );

        $validator = Validator::make(Input::all() , $rules);

        // if the validator fails, redirect back to the form

        if ($validator->fails())
        {
            return response()->json($validator->errors()->toArray());
        }
        else
        {
            // create our user data for the authentication
            $userdata = array(
                'username' => Input::get('username'),
                'password' => Input::get('password')
            );
            $userdataemail = array(
                'email' => Input::get('username'),
                'password' => Input::get('password')
            );
            $userdatacellno = array(
                'cell_no' => Input::get('username'),
                'password' => Input::get('password')
            );
            //--------------------------------------------------
            $userInfo = Users::where([['username',Input::get('username')],['user_status','1']])->first();
            $useremail = Users::where([['email',Input::get('username')],['user_status','1']])->first();
            $usercellno = Users::where([['cell_no',Input::get('username')],['user_status','1']])->first();

            //--------------------------------------------------
            if($userInfo)
            {
                // attempt to do the login
                if (Auth::attempt($userdata))
                {
                    return response()->json(['success'=>'success']);;
                }
                else
                {
                    return response()->json(['error'=>'Something Wrong']);
                }
            }
            elseif ($useremail)
            {
                if (Auth::attempt($userdataemail))
                {
                    return response()->json(['success'=>'success']);
                }
                else
                {
                    return response()->json(['error'=>'Something Wrong']);
                }
            }
            elseif ($usercellno)
            {
                if (Auth::attempt($userdatacellno))
                {
                    return response()->json(['success'=>'success']);
                }
                else
                {
                    return response()->json(['error'=>'Something Wrong']);
                }
               }
               else{
                   $msg = 'Something wrong! Username or Password is wrong or Inactive user.';
                   return response()->json(['error'=>$msg]);

               }
        }
    }

    public function doLogin()
    {
        $rules = array(
            'username' => 'required|string', // make sure the email is an actual email
            'password' => 'required|string');

        // password has to be greater than 3 characters and can only be alphanumeric and);
        // checking all field

        $validator = Validator::make(Input::all() , $rules);

        // if the validator fails, redirect back to the form

        if ($validator->fails())
        {
            return Redirect::to('/admin/login')->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        }
        else
        {
            // create our user data for the authentication
            $userdata = array(
                'username' => Input::get('username'),
                'password' => Input::get('password')
            );
            //--------------------------------------------------
            $userInfo = Users::where([['username',Input::get('username')],['user_status','1']])->first();
            //--------------------------------------------------
            if($userInfo)
            {
                // attempt to do the login
                if (Auth::attempt($userdata))
                {
                    if(UtilityHelper::getUserRole() == 'Vendor'){
                       return Redirect::to('/admin/profile-management/profile');
                    }elseif (UtilityHelper::getUserRole() == ''){

                        return Redirect::to('/admin/login');
                    }else{
                        return Redirect::to('/admin/dashboard');
                    }

                }
                else
                {
                    //validation not successful, send back to form
                    return Redirect::to('/admin/login');
                    exit();
                }
            }
            else
            {

                $msg = 'Something wrong! Username or Password is wrong or Inactive user.';
                return Redirect::to('/admin/login')->with('status',$msg);
                exit();
            }
        }
    }

    function doLogout(Request $request)
    {
        return redirect('/admin/login')->with('status',Auth::logout());
        exit;
    }
}