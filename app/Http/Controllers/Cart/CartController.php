<?php

namespace App\Http\Controllers\Cart;

use App\Helpers\General\UtilityHelper;
use App\Helpers\Products\ProductsHelper;
use App\Http\Controllers\BaseController;
use App\Models\Admin\Log;
use App\Models\Admin\Reviews;
use App\Models\Admin\Units;
use App\Models\Customers;
use App\Models\OrderItems;
use App\Models\Orders;
use App\Models\Products;
use App\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Mockery\Exception;

class CartController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $cart = $this->getCart($request);


        $price = isset($cart['total']['price']) ? isset($cart['total']['price']) : '';

        $content = view('layouts.cart.cart-page')->with(['items' => $cart['items'], 'total' => $cart['total']['qty'], 'price' => $price]);
        return view('templates.cart')->with('content', $content);
//        $content = view('layouts.cart.cart-page')->with(['items' => $cart['items'], 'total' => $cart['total']['qty']]);
//        $category = view('layouts.home.products-block')->with(['items' => $cart['items'], 'total' => $cart['total']['qty']]);
//        return view('templates.cart')->with('content', $content, 'category',$category  );
    }

    public function checkOutSuccess(Request $request)
    {
        $orderNumber = Input::get('order_number');
//        UtilityHelper::sendOrderMail($orderNumber);
        if (!$orderNumber) {
            return redirect('/');
        } else {
            try {
                $order_details = Orders::where('orderNumber', $orderNumber)->first();
                if (count($order_details) < 1) {
                    return redirect('/');
                } else {
                    $customer_details = Users::where('id', $order_details->user_id)->first();
                    if ($customer_details == '') {
                        $customer_details = Customers::where('id', $order_details->customer_id)->first();
                    }
                    return view('layouts.cart.place-order')->with(['order' => $order_details, 'customer' => $customer_details]);
                }
            } catch (Exception $ex) {
                return redirect('/');
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function placeOrder(Request $request)
    {

        $email = $request->email;
        $ordernote = $request->ordernote;
        $storeLocation = $request->storeLocation;

        $customer = new Customers();
        $order = new Orders();
        $orderItems = new OrderItems();
        $userdata = Users::where('email', '=', $email)->first();
        $cartTotal = $this->getTotalItemsAndPriceArray($request);
        if ($userdata) {

            $updateuser = $userdata->where('id', '=', $userdata->id)->update(['address' => $request->address, 'cell_no' => $request->phone]);
            $customer_id = $userdata->id;
            $order = $order->placeuserOrder($customer_id, $cartTotal, $ordernote, $storeLocation,$request);
        } else {
            $cutomerDetail = Customers::where('email', '=', $email)->first();
//        $cutomerDetail=Customers::where('email','=',$email)->pluck('id')->first();
            if ($cutomerDetail == null) {
                $customer_id = $customer->addCustomer($request);
            } else {
                $cutomerDetail->where('id', '=', $cutomerDetail->id)->update(['address' => $request->address, 'phoneno' => $request->phone]);
                $customer_id = $cutomerDetail->id;
            }
            $order = $order->placeOrder($customer_id, $cartTotal, $ordernote, $storeLocation,$request);
        }


        if ($order) {


            foreach ($this->getCart($request)['items'] as $key => $value) {
                $item_details = Products::select('product_title', 'sku','shop_id', 'salePriceValue')->where('id', $key)->first();
//                $removeProduct = Products::where('product_title', '=', $item_details->product_title)->pluck('quantity_in_stock')->first();
//                $newQuantity = $removeProduct - $value['qty'];
//                Products::where('product_title', '=', $item_details->product_title)->update(['quantity_in_stock' => $newQuantity]);

                if (isset($value['price']) ? isset($value['price']) : '') {
                    $priceUnit = $value['price'];
                    $exode = explode("/", $priceUnit);
                    $price = $exode[0];
                    $unit = $exode[1];
                } else {
                    $price = $item_details->salePriceValue;
                    $unit = Units::where('product_id', '=', $key)->pluck('unit')->first();

                }
                $shopId = $item_details->shop_id;
                $values = array(
                    'object_id' => $key,
                    'order_id' => $order->id,
                    'name' => $item_details->product_title,
                    'sku' => isset($item_details->sku) ? $item_details->sku : '',
                    'price' => $price,
                    'shop_id' => $shopId,
                    'itemNetPrice' => $price,
                    'quantity_unit_id' => $unit,
                    'item_quantity' => $value['qty'],
                    'total' => $value['qty'] * $price,
                    'subtotal' => $value['qty'] * $price

                );
//                    dd($values);
                $detail = $orderItems->addOrderItems($values);
//
//                    $orderNumber=Orders::where('id','=',$orderId)->first();
//                    $request->session()->flush();
//                    $request->session()->flush();

            }
            UtilityHelper::sendOrderMail($order->orderNumber);
            $request->session()->forget('cart');

            Log::addLog('Order is Placed - Order Source : Website', $order->id);
            return response()->json(['success' => 'success', 'order_number' => $order->orderNumber]);
        }
    }

    public function getTotalItemsAndPriceArray($request)
    {
        $cartData = $this->getTotalItemsAndPrice($request);

        preg_match("/\{(.*)\}/s", $cartData, $cartData);
        $cartData = json_decode($cartData[0], true);
        return $cartData;
    }

    /**
     * @return $this
     */
    public function goToCheckout(Request $request)
    {
        $cartData = $this->getTotalItemsAndPrice($request);
        preg_match("/\{(.*)\}/s", $cartData, $cartData);
        $cartData = json_decode($cartData[0], true);
        $content = view('layouts.cart.checkout')->with('content', $cartData);
        return view('templates.checkout')->with('content', $content);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    //
//    public function addToCart(Request $request)
//    {
//        $id = $request->input('product_id');
//        $quantity = $request->input('quantity');
//        $session = $request->session();
//        $cartData = ($session->get('cart')) ? $session->get('cart') : array();
//        if (isset($cartData['total'])) {
//            $cartData['total']['qty'] += $quantity;
//        } else {
//            $cartData['total'] = array(
//                'qty' => $quantity
//            );
//            $cartData['items'] = array();
//        }
//        if (array_key_exists($id, $cartData['items'])) {
//            $cartData['items'][$id]['qty']++;
//        } else {
//            $cartData['items'][$id] = array(
//                'qty' => $quantity
//            );
//        }
//        $request->session()->put('cart', $cartData);
//        return response()->json(['success' => "Product is added Successfully", 'total' => $cartData['total']['qty']]);
//    }


    public function addToCart(Request $request)
    {
        $id = $request->input('product_id');
        $price = $request->input('price');
//dd($price);
        $quantity = $request->input('quantity');

        $session = $request->session();
        $cartData = ($session->get('cart')) ? $session->get('cart') : array();
        if ($price != 'undefined') {
            if (isset($cartData['total'])) {
                $cartData['total']['qty'] += $quantity;
            } else {
                $cartData['total'] = array(
                    'qty' => $quantity,
                    'price' => $price
                );
                $cartData['items'] = array();
            }
            if (array_key_exists($id, $cartData['items'])) {
                $cartData['items'][$id]['qty']++;
            } else {
                $cartData['items'][$id] = array(
                    'qty' => $quantity,
                    'price' => $price
                );
            }
            $request->session()->put('cart', $cartData);
            return response()->json(['success' => "Product is added Successfully", 'total' => $cartData['total']['qty']]);
        } else {
            if (isset($cartData['total'])) {
                $cartData['total']['qty'] += $quantity;
            } else {
                $cartData['total'] = array(
                    'qty' => $quantity
                );
                $cartData['items'] = array();
            }
            if (array_key_exists($id, $cartData['items'])) {
                $cartData['items'][$id]['qty']++;
            } else {
                $cartData['items'][$id] = array(
                    'qty' => $quantity
                );
            }
            $request->session()->put('cart', $cartData);
            return response()->json(['success' => "Product is added Successfully", 'total' => $cartData['total']['qty']]);

        }
    }

    /**
     * Update Cart
     */
    public function updateCart(Request $request)
    {
        $id = $request->key;
        $new_quantity = $request->quantity;
        $new_price = $request->price;

        $session = $request->session();
        $cartData = ($session->get('cart')) ? $session->get('cart') : array();

        if (array_key_exists($id, $cartData['items'])) {

            $old_quantity = $cartData['items'][$id]['qty'];
            $cartData['items'][$id]['qty'] = $new_quantity;
            $cartData['total']['qty'] += $new_quantity - $old_quantity;

        }
        $request->session()->put('cart', $cartData);
        return response()->json(['success' => 'Updated']);
    }

    /**
     * Delete Cart
     */
    public function deleteCart(Request $request)
    {
        $id = $request->key;
        $session = $request->session();
        $cartData = ($session->get('cart')) ? $session->get('cart') : array();
        if (array_key_exists($id, $cartData['items'])) {
            $qty = $cartData['items'][$id]['qty'];
            $cartData['total']['qty'] -= $qty;
            unset($cartData['items'][$id]);
        }
        $request->session()->put('cart', $cartData);
        return response()->json(['success' => 'Deleted']);
    }

    /**
     * Get Cart Items and Count
     */
    public function getCart($request)
    {
//        if (Auth::check()) {
//            return true;
//        } else {
        $session = $request->session();
        $cartData = ($session->get('cart')) ? $session->get('cart') : false;
        if ($cartData) {
            return $cartData;
            return response()->json(['success' => 'success', 'cartData' => $cartData]);
        }
//        }
        return false;
    }

    /**
     * make cart and return to render
     */

    public function makeCart(Request $request)
    {
        $cart = $this->getCart($request);
        if ($cart == false) {
            return response()->json(['success' => 'success', 'total' => 0]);
        } else {
            return response()->json(['success' => 'success', 'total' => $cart['total']['qty'], 'items' => $cart['items']]);
        }
    }

    /**
     * make Cart Drop Down
     */
    public function makeCartDropDown(Request $request)
    {
//        $request->session()->forget('cart');
//
//        $request->session()->flush();
        $items = json_decode($request->items, true);
        return view('layouts.cart.cart_drop_down')->with('items', $items);
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */

//    public function getTotalItemsAndPrice(Request $request)
//    {
////        if (Auth::check()) {
////            return true;
////        } else {
//        $session = $request->session();
//        $cartData = ($session->get('cart')) ? $session->get('cart') : false;
//        if ($cartData) {
//            $totalItems = $cartData['total']['qty'];
//            $total = 0;
//
//            foreach ($cartData['items'] as $key => $value) {
//                $select = [
//                    'salePriceValue',
//                ];
//                $details = \App\Helpers\Products\ProductsHelper::getSingleProduct("", $key, $select);
//                $price = $details->salePriceValue;
//                $total += $value['qty'] * $price;
//            }
//            return response()->json(["success" => "successs", "totalItems" => "$totalItems", "total" => "$total"]);
//        }
////        }
//        return false;
//    }

    public function getTotalItemsAndPrice(Request $request)
    {
//        if (Auth::check()) {
//            return true;
//        } else {
        $session = $request->session();
        $cartData = ($session->get('cart')) ? $session->get('cart') : false;
//        dd($cartData);
        if ($cartData) {
            $totalItems = $cartData['total']['qty'];
            $total = 0;
//dd($cartData);
            foreach ($cartData['items'] as $key => $value) {
                $select = [
                    'salePriceValue',
                ];
//                dd();
                $totalPrice = isset($value['price']) ? isset($value['price']) : '';

                $details = \App\Helpers\Products\ProductsHelper::getSingleProduct("", $key, $select);
                if (isset($value['price']) ? isset($value['price']) : '') {

                    $priceUnit = $value['price'];
                    $exode = explode("/", $priceUnit);
                    $price = $exode[0];
                } else {
                    $price = $details->salePriceValue;
                }

                $total += $value['qty'] * $price;
            }
//            dd($total);
            return response()->json(["success" => "successs", "totalItems" => "$totalItems", "total" => "$total"]);
        }
//        }
        return false;
    }

    public function addreviews(Request $request){


        $values=array(

            'product_id'=>$request->productId,
            'reviewerName'=>$request->name,
            'review'=>$request->review,
            'additionDate' => date('Y-m-d H:i:s'),
            'rating'=>$request->rating,
        );

   $review= new Reviews();
   $data=$review->addReviews($values);
        return response()->json($data);
    }
}
