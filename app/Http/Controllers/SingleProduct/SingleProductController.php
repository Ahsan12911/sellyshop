<?php

namespace App\Http\Controllers\SingleProduct;

use App\Helpers\Products\ProductsHelper;
use App\Models\Admin\Reviews;
use App\Http\Controllers\BaseController;
use App\Models\Categories;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class SingleProductController extends BaseController
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getCategoryBySlug($slug)
    {
        $product  = Products::where('cleanURL',$slug)->where('status',1)->select('category_id')->first();

        if($product)
        {
            return Categories::where('id',$product->category_id)->first()->cleanURL;
        }
        else{

            return '/home/';
        }

    }

    public function indexWithCategory($slug)
    {

        $cleanURL = $this->getCategoryBySlug($slug);
        return redirect(url($cleanURL.'/'.$slug.'.html'));

    }

    /**
     *Get Products Page
     * @param string
     * @return View
     */

    public function index($category = null,$slug = null)
    {
        if(!$category || !$slug )
        {
            return Redirect::to('/');
        }
        $productDetails = $this->getProductDetail($slug);
        if(!$productDetails)
        if(!$productDetails)
        {
            return Redirect::to('/');
        }
        $meta = ProductsHelper::getProductTitle($slug);
        $title = $meta->product_page_title;
        $meta_keyword = $meta->meta_keywords;
        $meta_descriptions = $meta->meta_descriptions;

        return view('templates.single-product')->with(['content' => $productDetails, 'title' => $title, 'meta_keyword' => $meta_keyword, 'meta_descriptions' => $meta_descriptions]);
    }

    /**
     * Get Single Product View
     * @param string
     * @return View
     */
    public function getProductDetail($slug)
    {

        $item = ProductsHelper::getSingleProduct($slug);
        if ($item) {

           $relatedProduct = ProductsHelper::getRelatedProduct($item->category_id,$item->id);
            $categoryTitle= Categories::where('id',$item->category_id)->pluck('category_title')->first();
            $reviews = Reviews::where('status', '=', 'Published')->where('product_id', '=', $item->id)->paginate(7);
            return view('layouts.single-product.product-detail')->with(['item' => $item, 'reviews' => $reviews, 'relatedProduct' => $relatedProduct, 'categoryTitle' => $categoryTitle]);
        }
        else{
            return false;
        }
    }
}
