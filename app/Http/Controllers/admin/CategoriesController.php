<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;


use App\Http\Requests\admin\CategoriesRequest;
use App\Models\Admin\CategoryLog;
use App\Models\Admin\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Categories;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
    */

    public function listings()
    {
        $categories = Categories::paginate(10);
        return view('admin/categories-management/categories',['categoriesData' => $categories]);
    }

    public function index()
    {
        $categories=Categories::all();
        return view('admin/categories-management/add-categories',['categoriesData' => $categories]);
    }

    public function addCategories(CategoriesRequest $request)
    {
        $this->validate($request,[
            'category_icon' => 'required',
        ]);
        $file = $request->file('category_icon');
        $name = time() . $file->getClientOriginalName();
        $destinationPath = public_path('images/category');

        $file->move($destinationPath,$name);

        $productValues = array(
            'parent_id' => $request->parent_category,
            'category_title' => $request->categoryTitle,
            'category_description' => $request->category_description,
            'category_icon' => $name,
            'category_page_title' => $request->category_page_title,
            'meta_keywords' => $request->meta_keywords,
            'meta_descriptions' => $request->meta_descriptions,
            'cleanURL' => $request->cleanURL,
            'status' => $request->status,
            'CreatedDate' => date('Y-m-d H:i:s')
        );

//dd($productValues);
        $categories = new Categories();

        if($lastInsertId = $categories->addCategory($productValues))
        {
           $parentCat= Categories::where('id',$request->parent_category)->pluck('category_title')->first();
            CategoryLog::addLog($lastInsertId,$request->categoryTitle,null,$parentCat);
            $msg = 'Product added successfully.';
            return Redirect::to('/admin/categories-management/categories')->with('status',$msg);
            exit();
        }
        else
        {
            $msg = 'Error: Product not added.';
            return Redirect::to('/admin/categories-management/add-category')->withErrors([$msg, 'this message']);
            exit();
        }
    }

    public function updatelisting ($id)
    {
        $category=Categories::find($id);
        $categoriesData=Categories::all();

        return view('admin/categories-management/updatecategories',compact('category','categoriesData'));
    }





    public function updateCategory(CategoriesRequest $request,$id )
    {

        $categoryId = $request->id;

        $file = $request->file('category_icon');
        if($file !='') {
            $name = time() . $file->getClientOriginalName();
            $destinationPath = public_path('images/category');

            $file->move($destinationPath, $name);
        }else {
            $name=Categories::where('id', $categoryId)->pluck('category_icon')->first();
        }
        $categoryValues = array(
            'parent_id' => $request->parent_category,
            'category_title' => $request->categoryTitle,
            'category_description' => $request->category_description,
            'category_icon' => $name,
            'category_page_title' => $request->category_page_title,
            'meta_keywords' => $request->meta_keywords,
            'meta_descriptions' => $request->meta_descriptions,
            'cleanURL' => $request->cleanURL,
            'status' => $request->status,
            'ModifiedDate' => date('Y-m-d H:i:s')
        );
$category=Categories::where('id',$categoryId)->update($categoryValues);
        $parentCat= Categories::where('id',$request->parent_category)->pluck('category_title')->first();
        CategoryLog::updateLog($categoryId,$request->categoryTitle,null,$parentCat);
            $msg = 'Category updated successfully.';
            return Redirect::to('/admin/categories-management/categories')->with('status',$msg);



    }
public function updatestatus($id){
$upd=Categories::where('id', $id)->update(array('status'=>'0'));
    $categories= Categories::where('id',$id)->select('parent_id','category_title')->first();
    $parentCat= Categories::where('id',$categories->parent_id)->pluck('category_title')->first();
    CategoryLog::disablestatus($id,$categories->category_title,null,$parentCat);
    return redirect()->back();
}
    public function updatestatusone($id){
$upd=Categories::where('id', $id)->update(array('status'=>'1'));
        $categories= Categories::where('id',$id)->select('parent_id','category_title')->first();
        $parentCat= Categories::where('id',$categories->parent_id)->pluck('category_title')->first();
        CategoryLog::enablestatus($id,$categories->category_title,null,$parentCat);
    return redirect()->back();
}
    public function deleteCategories($id)
    {
        $categories= Categories::where('id',$id)->select('parent_id','category_title')->first();
        $parentCat= Categories::where('id',$categories->parent_id)->pluck('category_title')->first();
        CategoryLog::deleteCategory($id,$categories->category_title,null,$parentCat);

        Categories::where('id', '=', $id)->delete();


        $msg="Records Delete";
        return Redirect::back()->withErrors([$msg, 'this message']);
    }

    public function deleteMultipleCategories(Request $request)
    {
        $delete = $request->input('delete');
        if ($delete) {

            foreach ($delete as $del) {
                Categories::where('id', '=', $del)->delete();


                $msg="Records Deleted ";
                return Redirect::back()->withErrors([$msg, 'this message']);
            }
        }
        else {
            $msg="please select at least one record ";
            return Redirect::back()->withErrors([$msg, 'this message']);
        }
        return redirect()->back();
    }
}