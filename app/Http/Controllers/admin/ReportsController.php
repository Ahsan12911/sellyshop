<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\ProductsRequest;
use App\Models\Admin\InventoryRecord;
use App\Models\Admin\Orders;

use App\Models\Admin\Units;
use App\Models\Admin\Users;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Categories;
use App\Models\Admin\Products;
use App\Models\Admin\Inventory;
use App\Models\Admin\InventoryItems;
use App\Models\Admin\InventoryWastage;
use App\Models\Admin\InventoryWastageItems;
use App\Models\Admin\OrdersItems;
use Auth;

use Maatwebsite\Excel\Facades\Excel;


class ReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function stockReport($pid)
    {
        $InventoryItems = new InventoryItems();
        $OrdersItems = new OrdersItems();
        $Products = new Products();
        $productInfo = $Products->getProductInfo($pid);
        $productTitle = $productInfo[0]->product_title;
        $ItemsInStock = $productInfo[0]->quantity_in_stock;
        $TotalItemsPurchased = $InventoryItems->sumItemsPurchased($pid);
        $TotalItemsSold = $OrdersItems->sumItemsSold($pid);
        $TotalExpenseOnPurchase = $InventoryItems->ExpenseOnPurchase($pid);
        $TotalIncomeFromSale = $OrdersItems->IncomeFromSale($pid);;
        $NetProfit = $TotalIncomeFromSale - $TotalExpenseOnPurchase;
        $purchaseHistory = inventory
            ::join('inventory_items', 'inventory.id', '=', 'inventory_items.inventory_id')
            ->join('products', 'inventory_items.product_id', '=', 'products.id')
            ->select('products.id', 'products.product_title', 'inventory.purchase_date', 'inventory.invoice_no', 'inventory_items.quantity', 'inventory_items.unit', 'inventory_items.unit_price')
            ->where('product_id', '=', $pid)
            ->get();
        $saleHistory = orders
            ::join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('products', 'order_items.object_id', '=', 'products.id')
            ->select('orders.created_at', 'orders.orderNumber', 'order_items.item_quantity', 'order_items.quantity_unit_id', 'order_items.price')
            ->where('order_items.object_id', '=', $pid)
            ->get();
        return view('admin/reports/stock-report', compact('purchaseHistory', 'saleHistory'), ['productTitle' => $productTitle, 'TotalItemsPurchased' => $TotalItemsPurchased, 'TotalItemsSold' => $TotalItemsSold, 'TotalExpenseOnPurchase' => $TotalExpenseOnPurchase, 'TotalIncomeFromSale' => $TotalIncomeFromSale, 'NetProfit' => $NetProfit, 'ItemsInStock' => $ItemsInStock]);
    }

    public function ordersStats()
    {
        $now = Carbon::now();
        $weekStart = Carbon::now()->subDays($now->dayOfWeek)->setTime(0, 0);
        $yearStart = Carbon::now()->startOfYear();
        $monthStart = Carbon::now()->startOfMonth();

        $itemquantity = OrdersItems::where('created_at', '>', $yearStart)
            ->select(array('object_id', DB::raw('COUNT(item_quantity) AS count')))->groupBy('object_id')->get();
        $ordersweek = Orders::where('created_at', '>', $weekStart)->get();
        $orderemonthly = Orders::where('created_at', '>', $monthStart)->get();
        $ordersyearly = Orders::where('created_at', '>', $yearStart)->get();
        $ordersday = Orders::where('created_at', 'like', '%' . date('Y-m-d', strtotime($now)) . '%')->get();
        $dayordernew = Orders::where('created_at', 'like', '%' . date('Y-m-d', strtotime($now)) . '%')->get();
        $weekorders = Orders::where('created_at', '>', $weekStart)->get();
        $monthlyOrders = Orders::where('created_at', '>', $monthStart)->get();
        $yearlyOrders = Orders::where('created_at', '>', $yearStart)->get();
        return view('/admin/reports/orders-stats-report', ['orders' => $ordersday, 'ordersweek' => $ordersweek, 'orderemonthly' => $orderemonthly,
            'ordersyearly' => $ordersyearly,
            'dayordernew' => $dayordernew, 'weekorders' => $weekorders,
            'monthlyOrders' => $monthlyOrders, 'yearlyOrders' => $yearlyOrders,]);
    }

    public function itemReport(Request $request)
    {
        $products = Products::all();
        return view('admin/reports/itemized-report', ['productsData' => $products]);

    }

    public function newitemReport(Request $request)
    {
        $products = Products::all();
        return view('admin/reports/newitemized-report', ['productsData' => $products]);

    }

    public function newitemizedReport(Request $request)
    {

        $productid = $request->pid;
        $daterange = $request->daterange;
        if ($daterange != '') {
            $dates = explode(' - ', $daterange);
            $start_date = $dates[0];
            $end_date = $dates[1];
            if ($start_date == $end_date) {
                $single = explode('/', $start_date);
                $single_date = $single[2] . '-' . $single[0] . '-' . $single[1];
                $saleItem = Orders::where('created_at', 'LIKE', '%' . $single_date . '%');
                $inventory = InventoryRecord::where('created_at', 'LIKE', '%' . $single_date . '%');
            } else {
                $datesnew = explode(' - ', $daterange);
                $first_date = Carbon::parse($datesnew[0]);
                $last_date = Carbon::parse($datesnew[1]);
                $saleItem = Orders::whereBetween('created_at', array($first_date, $last_date));
                $inventory = InventoryRecord::whereBetween('created_at', array($first_date, $last_date));

            }

            $inventryRecord = $inventory->where('product_id', $productid)->get();
            $sales = $saleItem->where('shipping_status_id', '=', 4)->get();

            $inventoryItemsArray = array();
            foreach ($sales as $item) {
                $inventoryItems = OrdersItems::where('order_id', $item->id)->where('object_id', $productid)->get()->toArray();
                foreach ($inventoryItems as $inventoryItem) {
                    array_push($inventoryItem, $item->created_at);
                    array_push($inventoryItemsArray, $inventoryItem);
                }
            }

            return view('admin/reports/newitemReport', ['inventory' => $inventryRecord, 'saleItem' => $inventoryItemsArray,]);

        }
    }

    public function itemizedReport(Request $request)
    {
        $productid = $request->pid;
        $daterange = $request->daterange;

        if ($daterange != '') {
            $dates = explode(' - ', $daterange);
            $start_date = $dates[0];
            $end_date = $dates[1];
            if ($start_date == $end_date) {
                $single = explode('/', $start_date);
                $single_date = $single[2] . '-' . $single[0] . '-' . $single[1];
                $saleItem = OrdersItems::where('created_at', 'LIKE', '%' . $single_date . '%')->get();
                $inventory = Inventory::where('purchase_date', 'LIKE', '%' . $single_date . '%')->get();
                $waste = InventoryWastage::where('purchase_date', 'LIKE', '%' . $single_date . '%')->get();
            } else {
                $datesnew = explode(' - ', $daterange);
                $first_date = Carbon::parse($datesnew[0]);
                $last = Carbon::parse($datesnew[1]);
                $last_date = $last->addDay(1);
                $saleItem = OrdersItems::whereBetween('created_at', array($first_date, $last_date))->get();
                $inventory = Inventory::whereBetween('purchase_date', array($first_date, $last_date))->get();
                $waste = InventoryWastage::whereBetween('purchase_date', array($first_date, $last_date))->get();

            }
            $inventoryItemsArray = array();
            $wasteItemsArray = array();
            foreach ($inventory as $item) {
                $inventoryItems = InventoryItems::where('inventory_id', $item->id)->where('product_id', $productid)->get()->toArray();
                foreach ($inventoryItems as $inventoryItem) {
                    array_push($inventoryItem, $item->purchase_date);
                    array_push($inventoryItemsArray, $inventoryItem);
                }
            }
            foreach ($waste as $item) {
                $wasteItems = InventoryWastageItems::where('inventory_id', $item->id)->where('product_id', $productid)->get()->toArray();
                foreach ($wasteItems as $wasteItem) {
                    array_push($wasteItem, $item->purchase_date);
                    array_push($inventoryItemsArray, $wasteItem);
                }
            }

            return view('admin/reports/itemReport', ['inventory' => $inventoryItemsArray, 'waste' => $wasteItemsArray, 'saleItem' => $saleItem]);

        }
    }

    public function ordersource(Request $request)
    {
        $users = Users::all();
        $weborders = Orders::all();
        $processByweb = Orders::with('ordersprocess')->where('ordersource', '=', 1)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->get();
        $processBymobil = Orders::with('ordersprocess')->where('ordersource', '=', 0)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->get();
        $processByCSr = Orders::with('ordersprocess')->where('ordersource', '=', 2)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->get();

        return view('/admin/reports/ordersource', ['users' => $users, 'weborders' => $weborders, 'processBy' => $processByCSr, 'processByweb' => $processByweb, 'processBymobil' => $processBymobil,]);
    }

    public function ordersourceSearch(Request $request)
    {

        $sourceDate = $request->ordersource_date;
        if ($sourceDate != '') {
            $dates = explode(' - ', $sourceDate);

//           $start_date = $dates[0];
            $start_date = $dates[0];

            $end_date = $dates[1];
            if ($start_date == $end_date) {

                $single = explode('/', $start_date);


                $single_date = $single[2] . '-' . $single[0] . '-' . $single[1];

                $searchquert = Orders::where('orders.created_at', 'LIKE', '%' . $single_date . '%');
                $processByweb = Orders::with('ordersprocess')->where('ordersource', '=', 1)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->where('orders.created_at', 'LIKE', '%' . $single_date . '%')->get();
                $processBymobil = Orders::with('ordersprocess')->where('ordersource', '=', 0)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->where('orders.created_at', 'LIKE', '%' . $single_date . '%')->get();
                $processByCSr = Orders::with('ordersprocess')->where('ordersource', '=', 2)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->where('orders.created_at', 'LIKE', '%' . $single_date . '%')->get();

            } else {
                $datesnew = explode(' - ', $sourceDate);
                $first_date = Carbon::parse($datesnew[0]);
                $last_date = Carbon::parse($datesnew[1]);
                $searchquert = Orders::whereBetween('orders.created_at', array($first_date, $last_date));
                $processByweb = Orders::with('ordersprocess')->where('ordersource', '=', 1)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->whereBetween('orders.created_at', array($first_date, $last_date))->get();
                $processBymobil = Orders::with('ordersprocess')->where('ordersource', '=', 0)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->whereBetween('orders.created_at', array($first_date, $last_date))->get();
                $processByCSr = Orders::with('ordersprocess')->where('ordersource', '=', 2)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->whereBetween('orders.created_at', array($first_date, $last_date))->get();

            }

            $weborders = $searchquert->get();
            $processBy = $searchquert->where('ordersource', '=', 2)->select(array('processed_by', DB::raw('COUNT(processed_by) AS count')))->groupBy('processed_by')->get();

        } else {
            $weborders = Orders::all();
            $processBy = Orders::where('ordersource', '=', 2)->select(array('processed_by', DB::raw('COUNT(processed_by) AS count')))->groupBy('processed_by')->get();
            $processByweb = Orders::with('ordersprocess')->where('ordersource', '=', 1)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->get();
            $processBymobil = Orders::with('ordersprocess')->where('ordersource', '=', 0)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->get();
            $processByCSr = Orders::with('ordersprocess')->where('ordersource', '=', 2)->select(['processed_by', DB::raw('COUNT(processed_by) AS count')])->groupBy('processed_by')->get();
        }
        return view('/admin/reports/ordersource', ['processBy' => $processBy, 'weborders' => $weborders, 'processByweb' => $processByweb, 'processBymobil' => $processBymobil]);
    }

    public function ordersourceReportExcel(Request $request)
    {
        $userId = $request->username;

        $orderItem = array();
        foreach ($userId as $id) {
            $orderItem[] = Orders::where('processed_by', '=', $id)->get();

        }


        Excel::create('OrdersDetail', function ($excel) use ($orderItem) {

            $excel->setTitle('Order Source');
            $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
            $excel->setDescription('Stck Detail file');


            $excel->sheet('sheet1', function ($sheet) use ($orderItem) {
                $sheet->loadView('orderSourcesExcel')->with(['orderData' => $orderItem]);
            });

        })->download('csv');

    }


    public function wasteReport()
    {
        $products = Products::all();
        return view('admin/reports/waste-report', ['productsData' => $products]);

    }

    public function getwasteReport(Request $request)
    {
        $value = $request->value;

        if($value == 'waste'){
            $productid = $request->pid;
            $productUnit = $request->units;
            $daterange = $request->daterange;
            $products = Products::where('id' ,'>',0)->pluck('id')->toArray();

            if ($daterange != '') {
                $dates = explode(' - ', $daterange);
                $start_date = $dates[0];
                $end_date = $dates[1];
                if ($start_date == $end_date) {
                    $single = explode('/', $start_date);
                    $single_date = $single[2] . '-' . $single[0] . '-' . $single[1];
                    if ($productid != ''){

                        $inventoryRecord = InventoryRecord::where('created_at', 'LIKE', '%' . $single_date . '%')
                            ->where('product_id', $productid)
                            ->get();
                    }
                    else{
                        $inventoryRecord = InventoryRecord::where('created_at', 'LIKE', '%' . $single_date . '%')
                            ->whereIn('product_id',$products)
                            ->orderBy('product_id','ASC')
                            ->get();
                    }
                }
                else {
                    $datesnew = explode(' - ', $daterange);
                    $first_date = Carbon::parse($datesnew[0]);
                    $last = Carbon::parse($datesnew[1]);
                    $last_date = $last->addDay(1);
                    if ($productid != ''){

                        $inventoryRecord = InventoryRecord::whereBetween('created_at', array($first_date, $last_date))
                            ->where('product_id', $productid)
                            ->get();
                    }
                    else{
                        $inventoryRecord = InventoryRecord::whereBetween('created_at', array($first_date, $last_date))
                            ->whereIn('product_id',$products)
                            ->orderBy('product_id','ASC')
                            ->get();
                    }
                }


                return view('admin/reports/imports/waste', ['inventoryRecord' => $inventoryRecord]);
            }

        }

        elseif ($value == 'csv'){
            $productid = $request->products;
            $daterange = $request->ordersource_date;
            $products = Products::where('id' ,'>',0)->pluck('id')->toArray();

            if ($daterange != '') {
                $dates = explode(' - ', $daterange);
                $start_date = $dates[0];
                $end_date = $dates[1];
                if ($start_date == $end_date) {
                    $single = explode('/', $start_date);
                    $single_date = $single[2] . '-' . $single[0] . '-' . $single[1];
                    if ($productid != ''){

                        $inventoryRecord[] = InventoryRecord::where('created_at', 'LIKE', '%' . $single_date . '%')
                            ->where('product_id', $productid)
                            ->get();
                    }
                    else{
                        $inventoryRecord[] = InventoryRecord::where('created_at', 'LIKE', '%' . $single_date . '%')
                            ->whereIn('product_id',$products)
                            ->orderBy('product_id','ASC')
                            ->get();
                    }
                }
                else {
                    $datesnew = explode(' - ', $daterange);
                    $first_date = Carbon::parse($datesnew[0]);
                    $last = Carbon::parse($datesnew[1]);
                    $last_date = $last->addDay(1);
                    if ($productid != ''){

                        $inventoryRecord[] = InventoryRecord::whereBetween('created_at', array($first_date, $last_date))
                            ->where('product_id', $productid)
                            ->get();
                    }
                    else{
                        $inventoryRecord[] = InventoryRecord::whereBetween('created_at', array($first_date, $last_date))
                            ->whereIn('product_id',$products)
                            ->orderBy('product_id','ASC')
                            ->get();
                    }
                }

                Excel::create('OrdersDetail', function ($excel) use ($inventoryRecord) {

                    $excel->setTitle('Order Detail');
                    $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
                    $excel->setDescription('Order Detail file');
                    $excel->sheet('sheet1', function ($sheet) use ($inventoryRecord) {
                        $sheet->loadView('admin/reports/imports/csv-waste-report')->with('inventoryRecord', $inventoryRecord);
                    });

                })->download('csv');
                         }


        }
    }

    public function getProductunitInfo(Request $request)
    {
        $product_id = $request->pid;
        $divid = $request->divid;

        try {
            $productsData = Products::where(['id' => $product_id])->get();
            $market_price = $productsData[0]->salePriceValue;
            $product_quantity = $productsData[0]->quantity_in_stock;

            $units = Units::where(['product_id' => $product_id])->get();
            $unitData = '';
            $unitData .= '<select class="e1 form-control" name="units"  id="unit" required>';


            $unitData .= '<option value="">Select Unit</option>';
            foreach ($units as $unit) {
                $unitData .= '<option value="' . $unit->unit . '">' . $unit->unit . '</option>';


            }
            $unitData .= '</select>';

            return response()->json(['status' => 'success', 'openingStock' => $product_quantity, 'market_price' => $market_price, 'unitData' => $unitData]);
        } catch (QueryException $ex) {
            return response()->json(['status' => 'error', 'message' => 'Error Occurred. Data not retrieved.']);
        }

    }
    public function csrSupervioserReport(){
        return view('admin/reports/csr-report');
    }
    public function csvSupervioserReport(Request $request){

            $orderItem = array();
        $order_date=$request->date;

        if ($order_date != '') {
            $dates = explode(' - ', $order_date);

//           $start_date = $dates[0];
            $start_date = $dates[0];

            $end_date = $dates[1];
            if ($start_date == $end_date) {

                $single = explode('/', $start_date);


                $single_date = $single[2] . '-' . $single[0] . '-' . $single[1];

                    $orderItem[] =Orders::where('created_at', 'LIKE', '%' . $single_date . '%')->get();
            } else {
                $datesnew = explode(' - ', $order_date);
                $first_date = Carbon::parse($datesnew[0]);
                $date = Carbon::parse($datesnew[1]);
                $last_date  =$date->addDay(1);
                $orderItem[] = Orders::
                with('orderUser','orderCustomer','ordersprocess','orderDelieverdBy','orderShippingStatus','orderPaymentStatus')
                    ->whereBetween('created_at', array($first_date, $last_date))->get();
            }
        }

        Excel::create('OrdersDetail', function ($excel) use ($orderItem) {

                $excel->setTitle('Order Detail');
                $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
                $excel->setDescription('Order Detail file');
                $excel->sheet('sheet1', function ($sheet) use ($orderItem) {
                    $sheet->loadView('CsrReport')->with('data', $orderItem);
                });

            })->download('csv');

    }
    public function bulckWasteReport(){
        return view('admin/reports/bulk-waste-report');
    }
    public function WasteReportdata(Request $request){

        $order_date=$request->daterange;

        if ($order_date != '') {
            $dates = explode(' - ', $order_date);

//           $start_date = $dates[0];
            $start_date = $dates[0];

            $end_date = $dates[1];
            if ($start_date == $end_date) {

                $single = explode('/', $start_date);


                $single_date = $single[2] . '-' . $single[0] . '-' . $single[1];

                $orders =Orders::where('created_at', 'LIKE', '%' . $single_date . '%')->where('shipping_status_id','=','4')->get();
                $purchases =InventoryRecord::where('created_at', 'LIKE', '%' . $single_date . '%')->where('record_type','=','p')->get();
                $waste =InventoryRecord::where('created_at', 'LIKE', '%' . $single_date . '%')->where('record_type','=','w')->get();
            } else {
                $datesnew = explode(' - ', $order_date);
                $first_date = Carbon::parse($datesnew[0]);
                $date = Carbon::parse($datesnew[1]);
                $last_date  =$date->addDay(1);
                $orders = Orders::whereBetween('created_at', array($first_date, $last_date))->where('shipping_status_id','=','4')->get();
                $purchases =InventoryRecord::whereBetween('created_at', array($first_date, $last_date))->where('record_type','=','p')->get();
                $waste =InventoryRecord::whereBetween('created_at', array($first_date, $last_date))->where('record_type','=','w')->get();
            }
    }

    return view('admin/reports/imports/waste-report',['orders'=>$orders,'purchases'=>$purchases,'waste'=>$waste]);
    }

    public function totalSaleReport(){
        return view('admin/reports/total-sale-report');
    }

    public function totalSaleReportdata(Request $request){

        $order_date=$request->daterange;

        if ($order_date != '') {
            $dates = explode(' - ', $order_date);

//           $start_date = $dates[0];
            $start_date = $dates[0];

            $end_date = $dates[1];
            if ($start_date == $end_date) {

                $single = explode('/', $start_date);


                $single_date = $single[2] . '-' . $single[0] . '-' . $single[1];

                $totalOrders =Orders::where('created_at', 'LIKE', '%' . $single_date . '%')->count();
                $deliverOrders =Orders::where('created_at', 'LIKE', '%' . $single_date . '%')->where('shipping_status_id','=','4')->get();
            } else {
                $datesnew = explode(' - ', $order_date);
                $first_date = Carbon::parse($datesnew[0]);
                $date = Carbon::parse($datesnew[1]);
                $last_date  =$date->addDay(1);
                $totalOrders = Orders::whereBetween('created_at', array($first_date, $last_date))->count();
                $deliverOrders = Orders::whereBetween('created_at', array($first_date, $last_date))->where('shipping_status_id','=','4')->get();
            }
        }
        return view('admin/reports/imports/sale-report',['orders'=>$totalOrders,'deliverOrders'=>$deliverOrders]);
    }
}