<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\admin\ReviewsRequest;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Reviews;

class ReviewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $reviews = Reviews::paginate(10);
        $products = Products::all();

        return view('admin/reviews-management/reviews', ['reviews' => $reviews, 'products' => $products]);
    }

    public function add()
    {

        $productsData = Products::all();
        return view('admin/reviews-management/add-reviews', ['productsData' => $productsData]);
    }

    public function update($id)
    {
        $reviews = Reviews::find($id);
        $productid = $reviews->product_id;
        if ($productid) {

            $productname = Products::find($productid);
        } else {
            $productname = '';
        }
        if ($productname) {

            $productsData = Products::all();
        } else {
            $productsData = '';

        }
        $allProducts = Products::all();
        return view('admin/reviews-management/update-reviews', ['reviews' => $reviews, 'productname' => $productname, 'productsData' => $productsData, 'allProducts' => $allProducts]);
    }

    public function addReviews(ReviewsRequest $request)
    {
        $ip = '';
        $product = $request->input('product');
        if (is_array($product)) {
            foreach ($product as $prod) {

                $Values = array(
                    'product_id' => $prod,
                    'review' => $request->review,
                    'response' => $request->response,
                    'rating' => $request->rating,
                    'additionDate' => date('Y-m-d H:i:s'),
                    'reviewerName' => $request->reviewerName,
                    'status' => $request->status,
                    'ip' => $ip,
                    'isNew' => '1'
                );

                $reviews = new Reviews();
                $lastInsertId = $reviews->addReviews($Values);
            }


        } else {

            $Values = array(
                'product_id' => $request->product,
                'review' => $request->review,
                'response' => $request->response,
                'rating' => $request->rating,
                'additionDate' => date('Y-m-d H:i:s'),
                'reviewerName' => $request->reviewerName,
                'status' => $request->status,
                'ip' => $ip,
                'isNew' => '1'
            );

            $reviews = new Reviews();
            $lastInsertId = $reviews->addReviews($Values);
        }
        $msg = 'Review added successfully.';

        return Redirect::to('/admin/reviews-management/reviews')->with('status', $msg);


    }

    public function updateReviews(ReviewsRequest $request)
    {
        $reviewId = $request->id;
        $productId = $request->product;
        if($request->status == null){
            $status='Pending';
        }
        else{
            $status=$request->status;
        }
        $ip = '';

        $Values = array(
            'product_id' => $productId,
            'review' => $request->review,
            'response' => $request->response,
            'rating' => $request->rating,
            'additionDate' => date('Y-m-d H:i:s'),
            'reviewerName' => $request->reviewerName,
            'status' => $request->status,
            'ip' => $ip,
            'isNew' => $request->isNew
        );
        $reviews = Reviews::where('id', $reviewId)->update($Values);

        if ($reviews) {
            $msg = 'Reviews updated successfully.';

            return Redirect::to('/admin/reviews-management/reviews')->with('status', $msg);
            exit();
        } else {
            $msg = 'Something wrong: Unable to update Review.';

            return Redirect::to('/admin/reviews-management/reviews')->withErrors([$msg, 'this message']);
            exit();
        }
    }

    public function deleteStatus($id)
    {
        DB::table('reviews')->where('id', '=', $id)->delete();
        $msg = "Record is deleted ";
        return Redirect::back()->withErrors([$msg, 'this message']);
    }

    public function deleteMutilpleReviews(Request $request)
    {
        $ids = $request->delete;
        foreach ($ids as $id) {
            Reviews::where('id', $id)->delete();
        }
        $msg = "Record is deleted ";
        return Redirect::back()->withErrors([$msg, 'this message']);
    }
    public function enableStatus($id)
    {
        $enable=Reviews::where('id',$id)->update(['status'=>'Published']);
        $msg = 'Status updated successfully.';
        return Redirect::to('/admin/reviews-management/reviews')->with('status', $msg);
    }

    public function disableStatus($id)
    {
        Reviews::where('id',$id)->update(['status'=>'Pending']);
        $msg = 'Status updated successfully.';
        return Redirect::to('/admin/reviews-management/reviews')->with('status', $msg);
    }
}