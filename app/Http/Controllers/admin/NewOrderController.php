<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Models\Admin\Countries;
use App\Models\Admin\InventoryRecord;
use App\Models\Admin\InventoryWastage;
use App\Models\Admin\InventoryWastageItems;
use App\Models\Admin\Location;
use App\Models\Admin\Log;
use App\Models\Admin\OrderPaymentStatusTranslations;
use App\Models\Admin\OrderShippingStatusTranslations;
use App\Models\Admin\OrdersItems;
use App\Models\Admin\OrderSubShippingStatusTranslations;
use App\Models\Admin\Products;
use App\Models\admin\StockLog;
use App\Models\Admin\Units;
use App\Models\Admin\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Admin\Orders;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Customers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\One\User;
use Spatie\Permission\Models\Role;

class NewOrderController extends Controller
{
    private $customerID;
    private $orderID;
    private $setNewOrderToProcessing;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->setNewOrderToProcessing = false;
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function placeOrder(Request $request)
    {
        $this->customerID = $this->addEditCustomer($request);
        if(!$this->checkItemsAvailability($request))
        {
            return Redirect::to('/admin/order-form')->with('status', 'Order Cannot Be placed,Item Quantity exceeds the available Quantity.');
        }
        $this->orderID = $this->createOrder($request);
        $this->addOrderItems($request);
        $this->changeNewOrderStatus($request);
        return Redirect::to('/admin/order-form-success')->with('status', 'Order placed successfully.');
    }


    public function changeNewOrderStatus($request)
    {
        if ($this->setNewOrderToProcessing) {
            $orderSource = isset($request->orderSource) ? $request->orderSource : false;
            if ($orderSource && $orderSource == 'csr') {
                $statusId = DB::table('order_shipping_status_translations')->select('id')->where('code', 'pro')->first()->id;
                $this->changeFulfillmentStatus($this->orderID, $statusId);
            }
        }

    }

    /**
     * @param $request
     * @return bool|mixed
     */

    public function addEditCustomer($request)
    {
        $update = $request->update;

        if ($update == '') {
            $customerDtail = array(
                'username' => $request->customer_name,
                'email' => $request->customer_email,
                'phoneno' => $request->customer_number,
                'address' => $request->Address,
            );
            $customer = new Customers();
            $customerId = $customer->addCustomer($customerDtail);

        } else {
            $customerupdate = array(
                'username' => $request->customer_name,
                'email' => $request->customer_email,
                'phoneno' => $request->customer_number,
                'address' => $request->Address,
            );
            Customers::where('id', '=', $request->customer_id)->update($customerupdate);
            $customerId = $request->customer_id;
        }
        return $customerId;
    }

    /**
     * @param $request
     * @return bool|mixed
     */

    public function createOrder($request)
    {
        $orderTotal = $request->OrderTotalPrice;
        $products_array = $request->products;
        $orderId = Orders::orderBy('id', 'DESC')->pluck('id')->first();
        $orderNumber = $orderId + 1;
        $foc = false;
        foreach ($products_array as $pkey => $pval) {
            if (isset($_POST['focs'][$pkey])) {
                $foc = true;
            }
        }
        $orderValues = array(
            'customer_id' => $this->customerID,
            'processed_by' => Auth::User()->id,
            'shipping_status_id' => '1',
            'payment_status_id' => '1',
            'calltype' => $request->calltype,
            'orderNumber' => 'S0' . $orderNumber,
            'total' => $orderTotal,
            'ordersource' => 2,
            'location_id' => $request->location,
            'isPosOrder' => '0',
            'is_foc' => $foc == true ? '1' : '0',
            'username' => $request->customer_name,
            'address' => $request->Address,
            'phoneno' => $request->customer_number,
            'date' => date('Y-m-d'),
            'created_at' => date('Y-m-d H:i:s')
        );
        $orders = new Orders();
        $orderID = $orders->addOrder($orderValues);
        Log::addLog('Order is Placed - Order Source : Phone', $orderID);
        return $orderID;
    }

    public function checkItemsAvailability($request)
    {
        foreach ($request->products as $pkey => $pval) {
            $quantity = isset($_POST['orderQuantity'][$pkey]) ? $_POST['orderQuantity'][$pkey] : '';
            $unit = isset($_POST['units'][$pkey]) ? $_POST['units'][$pkey] : '';

            $quantityInStock = Units::where('product_id',$pval)->where('unit',$unit)->select('product_quantity')->first()->product_quantity;
            if($quantityInStock<$quantity)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $request
     * @return bool
     */
    public function addOrderItems($request)
    {
        foreach ($request->products as $pkey => $pval) {
            $product = isset($_POST['products'][$pkey]) ? $_POST['products'][$pkey] : '';
            $unit = isset($_POST['units'][$pkey]) ? $_POST['units'][$pkey] : '';
            $costPrice = isset($_POST['costPrices'][$pkey]) ? $_POST['costPrices'][$pkey] : '';
            $quantity = isset($_POST['orderQuantity'][$pkey]) ? $_POST['orderQuantity'][$pkey] : '';
            $totalPrices = isset($_POST['totalPrices'][$pkey]) ? $_POST['totalPrices'][$pkey] : '';
            $is_foc = isset($_POST['focs'][$pkey]) ? '1' : '0';
            $focReason = isset($_POST['reason'][$pkey]) ? $_POST['reason'][$pkey] : '';
            $reference = isset($_POST['orderReference'][$pkey]) ? $_POST['orderReference'][$pkey] : '';

            $products = new Products();
            $productsData = $products->getProductInfo($product);
            $productName = $productsData[0]->product_title;

            $orderItemsValues = array(
                'order_id' => $this->orderID,
                'object_id' => $product,
                'name' => $productName,
                'quantity_unit_id' => $unit,
                'price' => $costPrice,
                'item_quantity' => $quantity,
                'total' => $totalPrices,
                'is_foc' => $is_foc,
                'reason' => $focReason,
                'reference' => $reference,
                'created_at' => date('Y-m-d H:i:s')
            );
            $ordersItems = new OrdersItems();
            $ordersItems->addOrderItems($orderItemsValues);
        }
        return true;
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fulfillmentSubStatusChangeRequest(Request $request)
    {
        $sub_shipping_status = $request->status;
        $orderId = $request->orderId;
        $statusMessage = $this->changeSubFulfillmentStatus($orderId, $sub_shipping_status);
        return response()->json(['status' => 'ok', 'statusMessage' => $statusMessage]);
    }

    /**
     * @param $orderId
     * @param $statusId
     * @return bool
     */
    public function changeSubFulfillmentStatus($orderId, $statusId)
    {
        Orders::where('id', '=', $orderId)->update(['sub_shipping_status_id' => $statusId]);
        if ($statusId) {
            $statusnName = DB::table('order_shipping_sub_status_translations')->select('name')->where('status_number', $statusId)->first()->name;
            Log::addLog('Sub Fulfilment Status Changed to ' . $statusnName, $orderId);
            return $statusnName;
        }
        return true;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fulfillmentStatusChangeRequest(Request $request)
    {
        $shipping_status = $request->value;
        $orderId = $request->orderId;
        $status=OrderShippingStatusTranslations::where('id',$shipping_status)->first();

        if($status->name =='Processing'){
        $checkStock = $this->checkItemQuantity($orderId);
        if(!$checkStock){
            return false;
        }else{
        $statusCode = $this->changeFulfillmentStatus($orderId, $shipping_status);
        return response()->json(['status' => 'ok', 'statusCode' => $statusCode]);
    }
    }
    else{
        $statusCode = $this->changeFulfillmentStatus($orderId, $shipping_status);
        return response()->json(['status' => 'ok', 'statusCode' => $statusCode]);
    }
    }

    /**
     * @param $orderId
     * @param $statusId
     * @return bool
     */
    public function changeFulfillmentStatus($orderId, $statusId)
    {
        Orders::where('id', '=', $orderId)->update(['shipping_status_id' => $statusId]);
        $statusCode = DB::table('order_shipping_status_translations')->select('code')->where('id', $statusId)->first()->code;
        $statusnName = DB::table('order_shipping_status_translations')->select('name')->where('id', $statusId)->first()->name;
        Log::addLog('Fulfilment Status Changed to ' . $statusnName, $orderId);
        if ($statusCode == "pro") {
            Orders::removeStock($orderId,'Order Processed');
            $this->setProcessingTime($orderId);
            $this->setProcessedBy($orderId);
        }
        if ($statusCode == "del") {
            $this->setOrderDeliveryTime($orderId);
        }
        if ($statusCode != "ond") {
            $this->changeSubFulfillmentStatus($orderId, null);
        }

        return $statusCode;
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function setProcessedBy($orderId)
    {
        $addProcessedBy = Orders::where('id', '=', $orderId)->update(['processed_by' => \Illuminate\Support\Facades\Auth::user()->id]);
        if ($addProcessedBy) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function setPaymentStatus(Request $request)
    {
        $addPayStatus = Orders::where('id', '=', $request->orderId)->update(['payment_status_id' => $request->value]);
        $statusCode = DB::table('order_payment_status_translations')->select('code')->where('id', $request->value)->first()->code;
        $statusnName = DB::table('order_payment_status_translations')->select('name')->where('id', $request->value)->first()->name;
        Log::addLog('Payment Status Changed to ' . $statusnName, $request->orderId);
        if ($statusCode == "dec") {
            Orders::addStockBack($request->orderId , 'Order Declined');
        } else if ($statusCode == "rej" || $statusCode == "ref") {
            $stockIn = Orders::where('id', $request->orderId)->first()->stock_in;
            $stockOut = Orders::where('id', $request->orderId)->first()->stock_out;
            if ($stockIn == 0 && $stockOut == 1)  {
            $this->addStockToWaste($request->orderId);
            }
        } else if ($statusCode == "foc") {
//            $this->setOrderToFoc($request->orderId);
        }
        if ($addPayStatus) {
            return response()->json($addPayStatus);
        } else {


            return false;
        }
    }

    public function setOrderToFoc($orderId)
    {
        $orderAmount = Orders::where('id', $orderId)->first()->total;
        Orders::where('id', $orderId)
            ->update(['total' => 0, 'foc_amount' => $orderAmount]);
        $orderItems = OrdersItems::where('order_id', $orderId)->get();
        foreach ($orderItems as $item) {
            $total = $item->total;
            $item->total = 0;
            $item->foc_amount = $total;
            $item->save();
        }
        return true;
    }


    /**
     * @param $orderId
     */
    public function addStockToWaste($orderId)
    {
        $products_array = OrdersItems::where('order_id', $orderId)->get();
        Orders::where('id', $orderId)->update(['stock_in' => '1']);
        foreach ($products_array as $item) {

                $openingStock =  Units::where('unit',$item->quantity_unit_id)->where('product_id',$item->object_id)->pluck('product_quantity')->first();
            $inventoryItemsValues = array(
                'added_by' => Auth::User()->id,
                'product_id' => $item->object_id,
                'invoice' =>  rand(10000, 99999),
                'unit' => $item->quantity_unit_id,
                'opening_stock' => $openingStock + $item->item_quantity,
                'quantity' => $item->item_quantity,
                'total_stock' => $openingStock,
                'unit_price' => 0,
                'profit' => 0,
                'sale_price' => 0,
                'record_type' => 'w',
                'record_date' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s'),


            );

            $inventoryItems = new InventoryRecord();
            $lastInventoryItemsId = $inventoryItems->addInventoryRecords($inventoryItemsValues);


            $message = 'Stock Added to Waste Source : '.'Order Rejected'.' - Quantity : '.$item->item_quantity;
                StockLog::addLog($message,null,$item->object_id,null);

            }

        }

    /**
     * @param $orderId
     * @return bool
     */
    public function setOrderDeliveryTime($orderId)
    {
        $orderDate = Orders::where('id', $orderId)->first()->created_at;
        $order_created_date = $orderDate;
        $current_date = date('Y-m-d H:i:s');

        $datetime1 = strtotime($order_created_date);
        $datetime2 = strtotime($current_date);
        $interval = abs($datetime2 - $datetime1);
        $minutess = round($interval / 60);

        $format = '%02d:%02d';
        $hours = floor($minutess / 60);
        $minutes = ($minutess % 60);
        $deliveryTime = sprintf($format, $hours, $minutes);

        Orders::where('id', '=', $orderId)->update(['delivery_time' => $deliveryTime]);
        return true;
    }

    /**
     * @param $orderId
     * @return bool
     */
    public function setProcessingTime($orderId)
    {
        $orderDate = Orders::where('id', $orderId)->first()->created_at;
        $order_created_date = $orderDate;
        $current_date = date('Y-m-d H:i:s');
        $datetime1 = strtotime($order_created_date);
        $datetime2 = strtotime($current_date);
        $interval = abs($datetime2 - $datetime1);
        $minutess = round($interval / 60);

        $format = '%02d:%02d';
        $hours = floor($minutess / 60);
        $minutes = ($minutess % 60);
        $processingTime = sprintf($format, $hours, $minutes);

        Orders::where('id', '=', $orderId)->update(['processing_time' => $processingTime]);
        return true;
    }

    public function addLogData($id, $orderId)
    {
        $ordernumber = Orders::where('id', '=', $orderId)->pluck('orderNumber')->first();
        $value = array(
            'users_id' => $id,
            'changing' => 'Order placed by',
            'orderNumber' => $ordernumber,
            'created_at' => date('Y-m-d H:i:s'),
        );
        $log = Log::insert($value);
    }
    public function checkItemQuantity($orderId){
    $orderItem=OrdersItems::where('order_id',$orderId)->get();
    foreach ($orderItem as $item ){
        $productQuantity=Units::where('unit',$item->quantity_unit_id)->where('product_id',$item->object_id)->select('product_quantity')->first()->product_quantity;
        if($productQuantity < $item->item_quantity){
            return false;
        }
    }
    return true;

    }
    public function checkneworder(){
       $shipmentId= OrderShippingStatusTranslations::where('name','=','New')->pluck('id')->first();
        $data=Orders::where('shipping_status_id','=',$shipmentId)->where('schedule_order_date','=',null)->count();
        if ($data > 0){
        return response($data);
    }
    }
    public function scheduleorder(){

        $currentDate=Carbon::now();

        $scheduleOrder = Orders
            ::join('order_payment_status_translations', 'orders.payment_status_id', '=', 'order_payment_status_translations.id')
            ->join('order_shipping_status_translations', 'orders.shipping_status_id', '=', 'order_shipping_status_translations.id')
            ->leftjoin('payment_methods', 'orders.payment_method', '=', 'payment_methods.id')
            ->select('orders.*', 'order_payment_status_translations.name AS order_payment_status','order_shipping_status_translations.name AS order_shipment_status','payment_methods.title AS order_payment_method')
            ->where('orders.schedule_order_date','!=','' )
            ->whereIn('orders.shipping_status_id',array(1,2))
          ->orderBy('orders.schedule_order_date','ASC')
            ->paginate(20);
        $location=Location::all();
        $csrrole = Role::where('name', '=', 'CSR')->first();
        $csrusers = Users::join('user_has_roles', 'users.id', '=', 'user_has_roles.users_id')
            ->leftJoin('roles', 'user_has_roles.role_id', '=', 'roles.id')->where('roles.id', '=', $csrrole->id)
            ->select('users.username', 'users.id')->get();

        $riderrole = Role::where('name', '=', 'Rider')->first();
        $riderusers = Users::join('user_has_roles', 'users.id', '=', 'user_has_roles.users_id')
            ->leftJoin('roles', 'user_has_roles.role_id', '=', 'roles.id')->where('roles.id', '=', $riderrole->id)
            ->select('users.username', 'users.id')->get();
        $lowinvtproducts = Products::where('quantity_in_stock','<','1' )->get();
        $lowinvtproductsCount = Products::where('quantity_in_stock','<','1' )->count();
        $bestsellersproducts = Products::where('best_sellers','=','1' )->get();
        $orderPaymentStatusesValues = OrderPaymentStatusTranslations::all();
        $fulfilmentStatus = OrderShippingStatusTranslations::all();
        $subFulfilmentStatus = OrderSubShippingStatusTranslations::all();


        $links = $scheduleOrder->links();
        return view('admin/orders-management/schedule',['location'=>$location,'scheduleOrder'=>$scheduleOrder,'lowinvproducts'=>$lowinvtproducts,
            'lowinvtproductsCount'=>$lowinvtproductsCount,'bestsellersproducts'=>$bestsellersproducts,
            'orderPaymentStatusesValues' => $orderPaymentStatusesValues,
            'fulfilmentStatus' => $fulfilmentStatus,
            'subFulfilmentStatus' => $subFulfilmentStatus,
            'csrusers' => $csrusers, 'riderusers' => $riderusers,'links' => $links,]);

    }
}