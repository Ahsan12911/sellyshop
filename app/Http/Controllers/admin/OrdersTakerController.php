<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Models\Admin\Countries;
use App\Models\Admin\Location;
use App\Models\Admin\OrderShippingStatusTranslations;
use App\Models\Admin\OrdersItems;
use App\Models\Admin\Products;
use App\Models\Admin\StockLog;
use App\Models\Admin\Units;
use App\Models\Admin\Users;
use Illuminate\Http\Request;
use App\Models\Admin\Orders;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Customers;
use Auth;

class OrdersTakerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $productsData = Products::where('status', '=', 1)->get();
        $countries = Countries::all();
        $location = Location::all();
        return view('admin/orders-management/order-taker', compact('productsData', 'countries', 'location'));
    }

    public function getProductInfo(Request $request)
    {
        $product_id = $request->pid;
        $divid = $request->divid;

        try {
            $productsData = Products::where(['id' => $product_id])->get();
            $market_price = $productsData[0]->salePriceValue;
            $product_quantity = $productsData[0]->quantity_in_stock;

            $units = Units::where(['product_id' => $product_id])->get();
            $unitData = '';
            $unitData .= '<select class="e1 form-control" name="units[]" onchange="setunitPrice(' . $divid . ',' . $product_id . ')" id="unit_' . $divid . '" required>';


            $unitData .= '<option value="">Select Unit</option>';
            foreach ($units as $unit) {
                $unitData .= '<option value="' . $unit->unit . '">' . $unit->unit . '</option>';


            }
            $unitData .= '</select>';

            return response()->json(['status' => 'success', 'openingStock' => $product_quantity, 'market_price' => $market_price, 'unitData' => $unitData]);
        } catch (QueryException $ex) {
            return response()->json(['status' => 'error', 'message' => 'Error Occurred. Data not retrieved.']);
        }
    }

    public function getunitInfo(Request $request)
    {
        $product_id = $request->pid;
        $unit = $request->unit;
        $data = Units::where('unit', $unit)->where('product_id', $product_id)->select('product_price', 'product_quantity')->first();
//        $data= Products::join('products_unit','products.id','=','products_unit.product_id')->where('products_unit.unit','=',$unit)
//            ->where('products_unit.product_id','=',$product_id)->select('products.salePriceValue as salePriceValue','products.quantity_in_stock as quantity_in_stock', 'products_unit.value as value')->first();

        return response()->json($data);
    }


    /*public function setCategory(Request $request)
    {
        $id=$request->id;
        $data=Products::where('category_id','=',$id)->get();

        return response()->json($data);
    }

    public function setproduct(Request $request)
    {
        $order_item=DB::table('order_items')->where('object_id', '=',$request->id)->get();

        if(count($order_item) == 0)
        {
            $data= Products::where('id','=',$request->id)->get();
            return response($data);
        }
        else
        {
            $data=DB::table('products')->join('order_items', 'products.id', '=', 'order_items.object_id')->where('id', '=',$request->id)->get();
            return response($data);
        }
    }

    public  function addCustomorder(Request $request)
    {
        //dd($request->user_product[0]);die;
        if($request->user_product[0]=="Select Product")
        {
            return redirect('/admin/orders-management/order-tacker')->with('order_select_product', 'First Select Products. ');
        }

        $cat_id = $request->input('user_cate');
        $product_id = $request->input('user_product');
        $singleItemPrice = $request->input('singleItemPrice');
        $quantity = $request->input('quantity');
        $con = $request->customer_number;

        $users = new Customers();
        $userDetails = $users->getCustomerDetails($request->customer_email);

        if(!isset($userDetails[0]['id'])){

            $uservalue = array(
                'username' => $request->customer_name,
                'email' => $request->customer_email,
                'phoneno' => $request->customer_number,
                'house_no' => $request->house_no,
                'street' => $request->street_no,
                'town' => $request->town,
                'block' => $request->block,
                'city' => $request->city,
                'state' => $request->state,
                'zip' => $request->zip,
                'country_code' => $request->country,
                'created_at' => date('Y-m-d H:i:s')
            );

            $lastInsertId = $users->addCustomer($uservalue);
            //dd($lastInsertId);die;

        }elseif(isset($userDetails[0]['id'])){

            $lastInsertId = $userDetails[0]['id'];
        }

        $order=new Orders();
        $order->user_id = $lastInsertId;
        $order->save();
        $orderId=$order->id;
        $quantity = $request->input('quantity');
        $totalPrice = $request->input('totalPrice');
        $product = $request->input('user_product');

        $addValue = array();
        $addValue['user_product'] = $request->input('user_product');

        $addValue['quantity'] = $request->input('quantity');
        $addValue['totalPrice'] = $request->input('totalPrice');

        $totalOrderPrice = 0;

        for($counter = 0; $counter < count($addValue['quantity']); $counter++){

            $dbUpdate = array(
                'object_id' => $addValue['user_product'][$counter],
                'order_id' => $orderId,
                'item_quantity' => $addValue['quantity'][$counter],
                'subtotal' => $addValue['totalPrice'][$counter],
            );
            $totalOrderPrice = $totalOrderPrice + $addValue['totalPrice'][$counter];

            $result= DB::table('order_items')->insert($dbUpdate);
        }
        $userSession = session()->get('user_session_id');
        $currentOrderId = DB::table('orders')->orderBy('id', 'desc')->first()->id;
        $result = DB::table('orders')->where('id','=',$currentOrderId)->update(['total'=>$totalOrderPrice,'processor_id'=>$userSession]);

        return redirect('/admin/orders-management/order-tacker')->with('order_status', 'Order Placed Successfully. ');
    }*/

    public function searchCustomer(Request $request)
    {
        $search_keyword = isset($request->search_keyword) ? $request->search_keyword : '';

        if ($search_keyword != '') {
            //------------------------------ Search From Customers ------------------
            $searchQuery_cus = Customers::where(function ($query) use ($search_keyword) {
                $query->Where('email', 'LIKE', '%' . $search_keyword . '%')
                    ->orWhere('full_name', 'LIKE', '%' . $search_keyword . '%')
                    ->orWhere('username', 'LIKE', '%' . $search_keyword . '%')
                    ->orWhere('phoneno', 'LIKE', '%' . $search_keyword . '%');
            });
            $searchResultsCus = $searchQuery_cus->get();

            if ($searchResultsCus) {
//                return response()->json($searchResultsCus);
                return view('admin/orders-management/customer-data', ['searchResultsCus' => $searchResultsCus]);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }

    public function customerdetail(Request $request)
    {
        $id = $request->id;
        $data = Customers::find($id);
        if ($data) {
            return response()->json($data);
        } else {
            return response()->json(false);
        }
    }

    public function setNewUnit(Request $request)
    {
        $orderitemId = $request->orderitemId;
        $exquantity = $request->newquantity;

        $cquantity = explode('/', $exquantity);
        $newquantity = $cquantity[0];
        $unit= $cquantity[1];

        $orderitem = OrdersItems::where('id', '=', $orderitemId)->first();
        $quantity = $orderitem->item_quantity;
        $total = $orderitem->price;
        $productId = $orderitem->object_id;
        $shipingId = Orders::where('id', $orderitem->order_id)->select('shipping_status_id')->first()->shipping_status_id;
        $orderStatus = OrderShippingStatusTranslations::where('id', $shipingId)->select('name')->first()->name;
        $quantityproduct = Units::where('unit', '=', $unit)->where('product_id', '=', $productId)->pluck('product_quantity')->first();
//        $quantityproduct = Products::where('id', '=', $productId)->pluck('quantity_in_stock')->first();
        if ($orderStatus == 'Processing') {
            if ($newquantity > $quantity) {
                $newtotal = $total * $newquantity;
                $quantityupdate = $newquantity - $quantity;
                $updateQuantity = $quantityproduct - $quantityupdate;
                if ($quantityproduct >= $quantityupdate) {
                    Units::where('unit', '=', $unit)->where('product_id', '=', $productId)->update(['product_quantity' => $updateQuantity]);
                    $itemUpdate = OrdersItems::where('id', '=', $orderitemId)->update(['item_quantity' => $newquantity, 'total' => $newtotal]);
                    $data = OrdersItems::where('id', '=', $orderitemId)->select('total', 'order_id')->first();
                    $message = 'Stock Removed Source : ' . 'Item Quantity Changed' . ' - Opening Stock : ' . $quantityproduct . ' Change : ' . $quantityupdate . ' New Quantity : ' . $updateQuantity;
                    StockLog::addLog($message, $data->order_id, $productId, null);
                    $totalamount = OrdersItems::where('order_id', '=', $data->order_id)->sum('total');
                    Orders::where('id', '=', $data->order_id)->update(['total' => $totalamount]);

                    return response($data);
                } else {
                    return false;
                }

            } else {
                $newtotal = $total * $newquantity;

                $quantityupdate = $quantity - $newquantity;
                $updateQuantity = $quantityproduct + $quantityupdate;
                Units::where('unit', '=', $unit)->where('product_id', '=', $productId)->update(['product_quantity' => $updateQuantity]);

                $itemUpdate = OrdersItems::where('id', '=', $orderitemId)->update(['item_quantity' => $newquantity, 'total' => $newtotal]);
                $data = OrdersItems::where('id', '=', $orderitemId)->select('total', 'order_id')->first();
                $totalamount = OrdersItems::where('order_id', '=', $data->order_id)->sum('total');

                $message = 'Stock Added Source : ' . 'Item Quantity Changed' . ' - Opening Stock : ' . $quantityproduct . ' Change : ' . $quantityupdate . ' New Quantity : ' . $updateQuantity;
                StockLog::addLog($message, $data->order_id, $productId, null);
                Orders::where('id', '=', $data->order_id)->update(['total' => $totalamount]);

                return response($data);


            }
        } else {
            if ($newquantity <= $quantityproduct) {
                $newtotal = $total * $newquantity;
                $itemUpdate = OrdersItems::where('id', '=', $orderitemId)->update(['item_quantity' => $newquantity, 'total' => $newtotal]);
                $data = OrdersItems::where('id', '=', $orderitemId)->select('total', 'order_id')->first();
                $totalamount = OrdersItems::where('order_id', '=', $data->order_id)->sum('total');
                Orders::where('id', '=', $data->order_id)->update(['total' => $totalamount]);
            } else {
                return false;
            }
        }
    }

}