<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\PagesRequest;
use App\Models\Admin\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Pages;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function listings()
    {
        $pages = Pages::all();
        return view('admin/pages/listings', ['pagesData' => $pages]);
    }

    public function addPage()
    {
        return view('admin/pages/add-page');
    }

    public function add(Request $request)
    {
        $pageValues = array(
            'name' => $request->name,
            'body' => $request->body,
            'metaKeywords' => $request->metaKeywords,
            'metaKeywordsDescriptions' => $request->metaKeywordsDescriptions,
            'metaTitle' => $request->metaTitle,
            'cleanURL' => $request->cleanURL,
            'enabled' => $request->enabled
        );

        $pages = new Pages();

        if ($lastInsertId = $pages->addPage($pageValues))
        {
            $msg = 'Page added successfully.';
            return Redirect::to('/admin/pages/listings')->with('status', $msg);
            exit();
        }
        else
        {
            $msg = 'Error: Page not added.';
            return Redirect::to('/admin/pages/listings')->withErrors([$msg, 'this message']);
            exit();
        }
    }

    public function updatePage($id){

        $currentPageData = Pages::find($id);
        return view('admin/pages/update-page',compact('currentPageData'));
    }

    public function update(Request $request)
    {
        $pid = $request->id;

        $updateValues = array(
            'name'=>$request->name,
            'body'=>$request->body,
            'metaKeywords'=>$request->metaKeywords,
            'metaDescriptions'=>$request->metaDescriptions,
            'metaTitle'=>$request->metaTitle,
            'cleanURL'=>$request->cleanURL,
            'enabled'=>$request->enabled
        );

        /*$page = Pages::find($pid);
        if($page->updatePage($pid,$updateValues))
        {
            $msg = 'Page updated';
            return Redirect::to('admin/pages/listings')->with('status',$msg);
        }
        else
        {
            return redirect()->back();
        }*/

        $page = Pages::find($pid);
        $page->name       = $request->name;
        $page->body      = $request->body;
        $page->metaKeywords = $request->metaKeywords;
        $page->metaDescriptions = $request->metaDescriptions;
        $page->metaTitle = $request->metaTitle;
        $page->cleanURL = $request->cleanURL;
        $page->enabled = $request->enabled;
        $pageUpdated = $page->save();

        if($pageUpdated)
        {
            $msg = 'Page updated';
            return Redirect::to('admin/pages/listings')->with('status',$msg);
        }
        else
        {
            return redirect()->back();
        }
    }

    public function updatestatustoenb($id)
    {
        $upd = Pages::where('id', $id)->update(array('enabled' => '1'));
        return redirect()->back();
    }

    public function updatestatustodis($id)
    {
        $upd = Pages::where('id', $id)->update(array('enabled' => '0'));
        return redirect()->back();
    }

    function deletePages(Request $request)
    {
        $pid = $request->id;

        $pages = new Pages();

        if($pages->deletePage($pid))
        {
            $msg = 'Page deleted successfully';
            return Redirect::to('admin/pages/listings')->with('status',$msg);
        }
        else
        {
            $msg = 'Unable to delete page';
            return Redirect::to('admin/pages/listings')->with('status',$msg);
        }
    }


}