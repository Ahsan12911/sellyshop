<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Models\Admin\Orders;
use App\Http\Requests\admin\UsersRequest;
use App\Models\Shops;
use Illuminate\Http\Request;
use App\Models\Admin\UserHasRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Models\Admin\Users;
use Spatie\Permission\Models\Role;
use Illuminate\Contracts\Encryption\DecryptException;


class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function listings(Request $request)
    {


        $orders = Orders::all();


        $users = Users::where('admin', '!=', '1')->paginate(15);


        return view('admin/users-management/user', ['usersData' => $users, 'orders' => $orders,]);
    }

//    }
    public function index()
    {
        $roles = Role::all();

        return view('admin/users-management/add-users', ['roles' => $roles]);
    }

    public function addUsers(UsersRequest $request)
    {
        $user_role = $request->user_role;


        $pas = Hash::make($request->password);

        $usersValues = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'username' => $request->user_name,
            'email' => $request->email,
            'password' => $pas,
            'user_status' => $request->user_status,
            'created_at' => date('Y-m-d H:i:s')
        );

        $user = new Users();
        $lastInsertId = $user->addUser($usersValues);

        $user = Users::find($lastInsertId);
        $role = Role::find($request->user_role);

        $user->assignRole($role);

        return Redirect::to('/admin/users-management/users')->with('status', 'Users added successfully.');
    }

    public function updateUser(Request $request, $id)
    {
        $user = Users::find($id);
        $userrole = Users::join('user_has_roles', 'users.id', '=', 'user_has_roles.users_id')
            ->where('id', '=', $id)->first();

        $roles = Role::all();
        return view('admin/users-management/updateuser', ['usersData' => $user, 'roles' => $roles, 'userrole' => $userrole,]);

    }

    public function updateUsers(UsersRequest $request, $id)
    {

        $status = $request->user_status;

        $userId = $request->id;
        $userValues = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'username' => $request->user_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'user_status' => $status,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $users = Users::where('id', $userId)->update($userValues);

        $user = Users::find($userId);

        $role = Role::find($request->user_role);

        $user->removeRole(Role::all());
        $user->assignRole($role);

        $msg = "Record updated ";

        return Redirect::to('/admin/users-management/users')->with('status', $msg);

    }


    public function deleteUsers($id)
    {
        Users::where('id', '=', $id)->delete();
        return Redirect::back()->withErrors(['Record Deleted', 'this message']);

    }

    public function deleteMultiple(Request $request)
    {
        $delete = $request->input('delete');
        if ($delete) {
            foreach ($delete as $del) {
                Users::where('id', '=', $del)->delete();

            }
        } else {
            $msg = "please select at least one record ";
            return Redirect::back()->withErrors([$msg, 'this message']);
        }
        return Redirect::back()->withErrors(['Record Deleted', 'this message']);

    }

    function usersvalidation(Request $request)
    {
        $email = $request->email;

        $data = Users::where('email', '=', $email)->get();
        if (!$data) {
            return response();
        } else {

            return response($data);

        }

    }


    function usersnamevalidation(Request $request)
    {
        $username = $request->name;

        $data = Users::where('username', '=', $username)->get();
        if (!$data) {
            return response();
        } else {

            return response($data);

        }
    }

    function validateupdat(Request $request)
    {

        $email = $request->email;
        $id = $request->id;
        $data = Users::where('email', '=', $email)->where('id', '!=', $id)->value('email');
        if (!$data) {
            return response();
        } else {

            return response($data);

        }
    }

    function validateupdatename(Request $request)
    {

        $name = $request->name;
        $id = $request->id;

        $data = Users::where('username', '=', $name)->where('id', '!=', $id)->value('username');
        if (!$data) {
            return response();
        } else {

            return response($data);

        }

    }


    function searchUsers(Request $request)
    {

        $keyword = $request->keyword;
        $status = $request->status;
        $searchQuery = Users::where('admin', '!=', '1');

        if ($keyword != '') {
            $searchQuery->where(function ($query) use ($keyword) {
                $query
                    ->orWhere('first_name', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('last_name', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('username', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('email', 'LIKE', '%' . $keyword . '%')->get();
            });

        }

        if ($status != '') {
            $searchQuery->where('user_status', '=', $status);

        }


        $users = $searchQuery->paginate(15);
        $orders = Orders::all();


        return view('admin/users-management/user', ['usersData' => $users, 'orders' => $orders,]);

    }


    public function assignUserRole(Request $request)
    {
        $users = Users::orderBy('username', 'asc')->get();
        $roles = Role::all();
        return view('admin/users-access-module/assignUserRole', ['data' => $users, 'roles' => $roles,]);
    }

    public function loadUserRole(Request $request)
    {
        $id = $request->user;

        $user_role = Users::join('user_has_roles', 'users.id', '=', 'user_has_roles.users_id')
            ->where('users.id', '=', $id)->first();
        $roles = Role::all();

        return view('admin/users-access-module/users-roles', ['user_role' => $user_role, 'roles' => $roles, 'user_id' => $request->user]);
    }

    public function assignUserPermission(Request $request)
    {
        $users = Users::orderBy('username', 'asc')->get();

        return view('admin/users-access-module/userHasPermission', ['data' => $users]);
    }

    public function assignRoleToUser(Request $request)
    {
        $user = $request->userId;
        $role = $request->roleid;
        $usersId = Users::where('id', '=', $user)->firstOrFail();
        $roleId = Role::find($role);

        $usersId->removeRole(Role::all());
        $usersId->assignRole($roleId);


    }

    public function statusDisable(Request $request, $id)
    {

        $upd = Users::where('id', $id)->update(array('user_status' => '0'));
        return redirect()->back();
    }

    public function statusEnable(Request $request, $id)
    {

        $upd = Users::where('id', $id)->update(array('user_status' => '1'));
        return redirect()->back();
    }

    public function resetpassword(Request $request)
    {
        $id = Auth::user()->id;
        $users = Users::where('id', $id)->first();
        return view('/admin/users-management/resetPassword', ['usersData' => $users]);
    }

    public function updatepassword(Request $request)
    {

        $pass = Users::where('id', $request->id)->pluck('password')->first();
        if (Hash::check($request->oldpassword, $pass)) {
            $this->validate($request, [
                'password' => 'required|min:3|max:20',
                'confirm_password' => 'required|min:3|max:20|same:password'
            ]);
            $passChange = Hash::make($request->password);
            $upd = Users::where('id', $request->id)->update(['password' => $passChange]);

            if($upd){
            return Redirect::back()->with('status', 'Password Successfully Updated');
        }
        else{
            return Redirect::back()->withErrors(['Password not  Updated', 'this message']);

        }
        }
        else {

            return Redirect::back()->withErrors(['Old Password is not correct', 'this message']);
        }

    }

    public function profile(){
        $user = Users::with('shops')->where('id','=',Auth::id())->first();
        return view('admin/users-management/profile',compact('user'));
    }


    public function addprofile(Request $request){
        Users::where('id','=',Auth::id())->update([
            'cnic'=>$request->cnic,
            'country'=>$request->country,
            'city'=>$request->city,
            ]);
       $shop = Shops::where('owner','=',Auth::id())->update([
            'name'=>$request->shop_name,
            'shop_detail'=>$request->description,
        ]);
   if(!$shop){
       Shops::create([
           'name'=>$request->shop_name,
           'owner'=>Auth::id(),
           'shop_detail'=>$request->description,
       ]);
   }
        $user = Users::with('shops')->where('id','=',Auth::id())->first();
        return Redirect::to('/admin/profile-management/profile')->with(['success'=>'Profile Updated']);
    }

}
