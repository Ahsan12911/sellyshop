<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderPlaced extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $order;
    private $customer;
    private $order_items;
    public function __construct($order_details = null , $customer_details = null , $order_items=null)
    {
        //
        $this->order=$order_details;
        $this->customer = $customer_details;
        $this->order_items = $order_items;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.orderConfirmation')->with(['order'=>$this->order , 'customer'=>$this->customer, 'order_items'=>$this->order_items]);
    }
}
