<?php

namespace App\Helpers\Products;

use App\Models\Admin\Units;
use App\Models\Categories;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ProductsHelper
{

//    /**
//     * @param $param
//     *
//     * @return \App\Models\Products
//     */
    public static function getProductsBySlug($params = null , $slug = null ,$category = null)
    {


        if($params == null){
            $params =
                [
                    'select' => [
                        'product_title',
                        'product_description'
                    ],
                    'where' => [['best_sellers', '1']],
                    'order_by' => 'product_title',
                    'order' => 'desc',
                    'limit' => '',
                    'paginate' => '',
                ];
        }

        //Get Category Detail From Given Category
        if($category == null && $slug!=null)
        {
            $categories = Categories::findBySlug($slug);
            $model = $categories->products();
        }
        elseif($category!=null)
        {
            $categories = $category;
            $model = $categories->products();
        }


        if($category==null && $slug==null)
        {
            $model = new Products();
        }

        $products = $model->select($params['select'])->where(function ($query) use ($params) {

            //Refine Query If Where Clause is present in the params
            if ((isset($params['where']) && $params['where'] != "") || (isset($params['where_or']) && $params['where_or'] != "")) {
                if (isset($params['where']) && $params['where'] != "") {
                    $query->where($params['where']);
                }
                if (isset($params['where_or']) && $params['where_or'] != "") {
                    $query->orWhere($params['where_or']);
                }
            }

            //Refine Query If order_by Clause is present in the params

        });


        if ((isset($params['order_by']) && $params['order_by'] != "") && (isset($params['order']) && $params['order'] != "")) {
            $products->orderby($params['order_by'], $params['order']);
        }

        //Refine Query If order_by paginate is present in the params

        if (isset($params['paginate']) && $params['paginate'] != "") {
            $products = $products->paginate($params['paginate']);
        }

        //Refine Query If order_by limit is present in the params

        elseif (isset($params['limit']) && $params['limit'] != "") {
            $products = $products->limit($params['limit'])->get();
        }

        //Otherwise Simply Fetch The Data
        else {
            $products = $products->get();
        }

        return $products;
    }

    /**
     * Get Single Product by Slug
     */

    public static function getSingleProduct($slug="",$id="",$slct=""){
        if($slct!="")
        {
            $select = $slct;
        }
        else
        {
            $select = [
                'id',
                'product_title',
                'category_id',
                'product_description',
                'health_benifits',
                'product_detail',
                'available_for_sales',
                'quantity_in_stock',
                'salePriceValue',
                'product_description',
                'meta_keywords',
                'product_page_title',
                'meta_descriptions',
                'product_image'
            ];
        }

        $product = new Products();
        try
        {
            if($slug!="")
            {
                $item = $product->select($select)->where('cleanURL',$slug)->where('status',1)->first();
            }
            else
            {
                $item = $product->select($select)->where('id',$id)->where('status',1)->first();
            }

            return $item;
        }
        catch(Exception $exception)
        {
            return false;
        }

    }

    public static function getProductUnits($productId, $unit) {
        $unit = Units::where('product_id','=',$productId)->where('unit','=',$unit)->first();
   return $unit;
    }
 public static function addToCart(Request $request) {

        return true;
    }

    public static function getProductTitle($slug){

       $title= Products::where('cleanURL','=',$slug)->select('product_page_title','meta_keywords','meta_descriptions')->first();

        return $title;
    }
    public static function getRelatedProduct($catId,$prodID){

       $relatedProduct= Products::where('category_id','=',$catId)->where('status','=',1)->where('id','!=',$prodID)->get();

        return $relatedProduct;
    }

}