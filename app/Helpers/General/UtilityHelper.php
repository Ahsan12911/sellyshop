<?php
namespace App\Helpers\General;


use App\Mail\OrderPlaced;
use App\Models\Admin\Location;
use App\Models\Admin\OrderPaymentStatusTranslations;
use App\Models\Admin\OrderShippingStatusTranslations;
use App\Models\Categories;
use App\Models\Customers;
use App\Models\OrderItems;
use App\Models\Orders;
use App\Users;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;
use Mockery\Exception;
use App\Http\Controllers\Auth\LoginController;
use App\Models\Admin\Products;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Mail\forgotpassword;


class UtilityHelper
{

    /**
     * @param string
     *
     * @return string
     */


    public static function getImage($path)
    {
        return asset(Config::get('constants.public_path') . 'images/' . $path);
    }

    /**
     * @param string
     *
     * @return string
     */

    public static function getCategories()
    {
        $categories = new Categories();
        try {
            $data = $categories->select('id','category_title','cleanURL','category_icon')->where('status','=','1')->where('parent_id','=',null)->get();
            return $data;
        } catch (Exception $ex) {
            return false;
        }

    }

    /**
     * @param string
     *
     * @return View
     */
//
//    public static function getCategoriesHtml()
//    {
//        $content = self::getCategories();
//        if($content)
//        {
//            return view('layouts.home._category_home_page')->with('content',$content);
//        }
//        else
//        {
//        }
//    }

    public static function getCategoriesApi()
    {
        $categories = new Categories();
        try {
            $data = $categories->select('id','category_title', 'cleanURL')->where('status','=',1)->get();
            if (!$data) {
                return response()->json(['STATUS' => false, 'MESSAGE' => 'Not Found', 'CODE' => 400], 200);
            } else {
                return response()->json([
                    'STATUS' => 'true',
                    'MESSAGE' => 'record found',
                    'DATA' => $data
                ], 200);
            }
            return response()->json($data);
        } catch (Exception $ex) {
            return response()->json(['STATUS' => false, 'MESSAGE' => 'Error', 'CODE' => 402], 200);
        }

    }

    public static function getProductssApi()
    {
        $product = new Products();

        try {
            $data = $product->join('products_unit','products.id','products_unit.product_id')
               ->select('products.id As id','products.category_id As category_id','products.product_title As product_title','products.product_description As product_description',
                   'products.health_benifits As health_benifits', 'products.product_detail As product_detail','products.discountType As discountType',
                   'products.marketPrice As marketPrice','products.on_sales As on_sales','products.premiumProducts As premiumProducts',
                   'products.popular_items As popular_items','products.best_sellers As best_sellers','products.featured_product As featured_product','products.sku As sku'
                   ,'products.upc_isbn As upc_isbn',
                   'products.mnf_vendor As mnf_vendor','products.product_image As product_image','products.available_for_sales As available_for_sales'
                   ,'products.product_length_fractional_part As product_length_fractional_part'
                   ,'products.freight As freight','products.status As status','products.weight As weight'
                   ,'products.require_shipping As require_shipping','products.free_shipping As free_shipping','products.arrivalDate As arrivalDate','products.expire_date As expire_date'
                   ,'products.inventoryEnabled As inventoryEnabled','products.lowLimitEnabledCustomer As lowLimitEnabledCustomer','products.lowLimitEnabled As lowLimitEnabled',
                   'products.lowLimitAmount As lowLimitAmount'
                   ,'products.product_page_title As product_page_title','products.meta_keywords As meta_keywords','products.meta_descriptions As meta_descriptions'
                   ,'products.cleanURL As cleanURL','products.CreatedDate As CreatedDate','products.ModifiedDate As ModifiedDate'
                   ,'products_unit.product_price As salePriceValue','products_unit.product_quantity As quantity_in_stock','products_unit.unit As unit'
               )->where('products.status','=',1)->get();

            if (!$data) {
                return response()->json(['STATUS' => false, 'MESSAGE' => 'Not Found', 'CODE' => 400], 200);
            } else {
                return response()->json([
                    'STATUS' => 'true',
                    'MESSAGE' => 'record found',
                    'DATA' => $data
                ], 200);
            }
            return response()->json($data);
        }
        catch (Exception $ex) {
            return response()->json(['STATUS' => false, 'MESSAGE' => 'Error', 'CODE' => 402], 200);
        }

    }


    public static function sendOrderMail($orderNumber)
    {
        $order_details = Orders::where('orderNumber', $orderNumber)->select('id','orderNumber','total','user_id','customer_id','created_at')->first();
        $customer_details = Customers::where('id', $order_details->customer_id)->first();
        if($customer_details==''){
            $customer_details=\App\Users::where('id', $order_details->user_id)->first();
        }
        $order_items = OrderItems::with('item')->where('order_id',$order_details->id)->select('object_id','price','amount','total')->get();
        Mail::to($customer_details->email)->send(new OrderPlaced($order_details,$customer_details,$order_items));
    }

    public static function getRegisterApi($name, $email, $password)
    {
        $validate=Users::select('username','email')->where('email', '=', $email)->first();
        if($validate){
            $customer_id = Users::select('id')->where('email', $email)->first();
            return response()->json(['STATUS' => false, 'MESSAGE' => 'Exist', 'CODE' => 402 , "ID" => $customer_id], 200);
        }
        else{
        $hasPasswor = Hash::make($password);
        $user = Users::create(['username' => $name, 'email' => $email, 'password' => $hasPasswor]);

        $customer_id = Users::select('id')->where('email', $email)->first();
        if ($user&&$customer_id) {
            return response()->json(['STATUS' => true, 'MESSAGE' => 'success', "Id" => $customer_id], 200);
        } else {
            return response()->json(['STATUS' => false, 'MESSAGE' => 'Error', 'CODE' => 402], 200);
        }
        }
    }


    public static function addinOrder($user_id, $cart_total, $cart_subtotal,$storeLocation)
    {


        $order = new Orders();

        return $order->apiplaceOrder($user_id, $cart_total, $cart_subtotal,$storeLocation);

    }

    public static function apiaddinOrder($order_id, $product_id, $qty)
    {
        $order_item = new OrderItems();

        return $order_item->apiaddItems($order_id, $product_id, $qty);

    }

    public static function apiUpdateUser($id,$name,$phone,$address,$orderNumber){
        $userdetails = Users::where("id",$id) -> first();

        $userdetails->username = $name;

        $userdetails -> cell_no = $phone;

        $userdetails -> address = $address;

        $userdetails -> save();

        self::sendOrderMail($orderNumber);

    }
    public static function getLoginApi($name, $password)
    {
      //  $pass=  Hash::make($password);

        $userdata = array(
            'email' => $name,
            'password' => $password,
        );
//        dd($userdata);
//        $login = new LoginController();
//        $log=$login->customerLogin($userdata);
        $userInfo = Users::where([['email', $name], ['user_status', '1']])->first();
        if ($userInfo) {
            if (Auth::attempt($userdata)) {
//            dd($userdata);
                // attempt to do the login
                return response()->json(['success' => 'success', "Id" => $userInfo->id]);;

            } else {

                $msg = 'Something wrong! Password is wrong or Inactive user.';
                return response()->json(['error' => $msg]);
            }

        }
        else {

            $msg = 'Something wrong! Email is wrong or Inactive user.';
            return response()->json(['error' => $msg]);
        }

    }
    public static function apiforgotPasword($email){

        $user=Users::where('email','=',$email)->first();
//   dd($user);
        if($user){
            $message="set your pasword";
            $userid= $user->id;
            Mail::to($email)->send(new forgotpassword($message,$userid));
            return response()->json(['success'=>'You will shortly recieve msg to reset password.']);;

        }
        else
        {
            return response()->json(['error'=>'Something Wrong']);

        }
    }

//    public static function getLocationApi()
//    {
//        $location=Location::all();
//        try {;
//            if (!$location) {
//                return response()->json(['STATUS' => false, 'MESSAGE' => 'Not Found', 'CODE' => 400], 200);
//            } else {
//                return response()->json([
//                    'STATUS' => 'true',
//                    'MESSAGE' => 'record found',
//                    'DATA' => $location
//                ], 200);
//            }
//            return response()->json($location);
//        } catch (Exception $ex) {
//            return response()->json(['STATUS' => false, 'MESSAGE' => 'Error', 'CODE' => 402], 200);
//        }
//
//    }

    public static function getLocationApi()
    {
        $location=Location::all();
        try {;
            if (!$location) {
                return response()->json(['STATUS' => false, 'MESSAGE' => 'Not Found', 'CODE' => 400], 200);
            } else {
                return response()->json([
                    'STATUS' => 'true',
                    'MESSAGE' => 'record found',
                    'DATA' => $location
                ], 200);
            }
            return response()->json($location);
        } catch (Exception $ex) {
            return response()->json(['STATUS' => false, 'MESSAGE' => 'Error', 'CODE' => 402], 200);
        }

    }
 public static function userProcessingOrder($userId)
    {
        $user=Orders::join('users','orders.user_id','=','users.id')
            ->join('order_items','orders.id','=','order_items.order_id')
            ->where('shipping_status_id','=','1')
            ->where('users.id','=',$userId)->get();

        try {;
            if (!$user) {
                return response()->json(['STATUS' => false, 'MESSAGE' => 'Not Found', 'CODE' => 400], 200);
            } else {
                return response()->json([
                    'STATUS' => 'true',
                    'MESSAGE' => 'record found',
                    'DATA' => $user
                ], 200);
            }
            return response()->json($user);
        } catch (Exception $ex) {
            return response()->json(['STATUS' => false, 'MESSAGE' => 'Error', 'CODE' => 402], 200);
        }

    }
        public static function userOrderList($userId){
            $orderlist=Orders::where('user_id','=',$userId)->get();

            $paymentStatus=OrderPaymentStatusTranslations::all();
            $fulfilmentStatus=OrderShippingStatusTranslations::all();
            try {;
                if (!$orderlist) {
                    return response()->json(['STATUS' => false, 'MESSAGE' => 'Not Found', 'CODE' => 400], 200);
                } else {
                    return response()->json([
                        'STATUS' => 'true',
                        'MESSAGE' => 'record found',
                        'DATA' => $orderlist,$paymentStatus,$fulfilmentStatus,
                    ], 200);
                }
                return response()->json($orderlist);
            } catch (Exception $ex) {
                return response()->json(['STATUS' => false, 'MESSAGE' => 'Error', 'CODE' => 402], 200);
            }

        }
        public static function userOrderItemList($oredrid){
            $order= OrderItems::join('orders','order_items.order_id','=','orders.id')
                ->where('order_items.order_id','=',$oredrid)->get();
            try {;
                if (!$order) {
                    return response()->json(['STATUS' => false, 'MESSAGE' => 'Not Found', 'CODE' => 400], 200);
                } else {
                    return response()->json([
                        'STATUS' => 'true',
                        'MESSAGE' => 'record found',
                        'DATA' => $order
                    ], 200);
                }
                return response()->json($order);
            } catch (Exception $ex) {
                return response()->json(['STATUS' => false, 'MESSAGE' => 'Error', 'CODE' => 402], 200);
            }

        }

        public static function putjsonOrder($string)
        {


            $area = json_decode($string, true);

            // dd($area);
            $user = $area['updateModel'];
            $address = $user['Address'];
            $cnumber = $user['Cnumber'];
            $fullname= $user['fullName'];

            $order = $area['Order'];
            $sunTotal = $order['subTotal'];
            $total = $order['total'];
            $user_id = $order['user_id'];
//            Users::where('id','=',$user_id)->first();
//            $storeLOcation = 1;
            $storeLOcation = $order['store_location'];

            $userdetails = Users::where("id",$user_id)->first();


            if ($userdetails !=null && count($area['OrderItems']) > 0){
            $order = new Orders();


            $orderId = $order->apiplaceOrder($user_id, $total, $sunTotal, $storeLOcation, $address, $cnumber, $fullname);


            if ($orderId) {

                $str = $orderId;
                $order_id = explode(" ",$str)[0];
                $order_number = explode(" ",$str)[1];


                foreach($area['OrderItems'] as $i => $v)
                {
                    $itemId =  $v['item_id'];
                    $qty = $v['item_qty'];

//                echo $itemId;
//                echo $qty;
                    $order_item = new OrderItems();

                    //$order_item->apiaddItems($order_id, $itemId, $qty);


                    $itemPlaced = $order_item->apiaddItems($order_id, $itemId, $qty);

                    if ($itemPlaced){
//                        $quantity=Products::where('id','=',$itemId)->pluck('quantity_in_stock')->first();
//                    $newquantity=$quantity - $qty;
//                    Products::where('id','=',$itemId)->update(['quantity_in_stock'=>$newquantity]);
                    }
                    else{
                        return  response()->json(['error'=>'item','item_id'=>$itemId]);
                    }
                }

                    $userdetails->username = $fullname;

                    $userdetails -> cell_no = $cnumber;

                    $userdetails -> address = $address;

                    $userdetails -> save();

                }
                else{
                    return  response()->json(['error'=>'user']);
                }


                self::sendOrderMail($order_number);


            }

             else {
                return response()->json(['error' => 'Login','userId'=>$user_id,'itemCount'=>count($area['OrderItems'])]);
            }
            return response()->json(['succes'=>'success','ordernumber'=>$order_number]);

        }

    public static function putjsonOrder2($user_id,$string)
    {


        $area = json_decode($string, true);

        $user = $area['updateModel'];
        $address = $user['Address'];
        $cnumber = $user['Cnumber'];
        $fullname= $user['fullName'];
        // dd($area);
        $order = $area['Order'];
        $sunTotal = $order['subTotal'];
        $total = $order['total'];

        Users::where('id','=',$user_id)->first();
        $storeLOcation = $order['store_location'];

        $userdetails = Users::where("id",$user_id)->first();


        if ($userdetails !=null && count($area['OrderItems']) > 0){
            $order = new Orders();


            $orderId = $order->apiplaceOrder($user_id, $total, $sunTotal, $storeLOcation, $address, $cnumber, $fullname);


            if ($orderId) {

                $str = $orderId;
                $order_id = explode(" ",$str)[0];
                $order_number = explode(" ",$str)[1];


                foreach($area['OrderItems'] as $i => $v)
                {
                    $itemId =  $v['item_id'];
                    $qty = $v['item_qty'];

//                echo $itemId;
//                echo $qty;
                    $order_item = new OrderItems();

                    //$order_item->apiaddItems($order_id, $itemId, $qty);


                    $itemPlaced = $order_item->apiaddItems($order_id, $itemId, $qty);

                    if ($itemPlaced){
//                        $quantity=Products::where('id','=',$itemId)->pluck('quantity_in_stock')->first();
//                    $newquantity=$quantity - $qty;
//                    Products::where('id','=',$itemId)->update(['quantity_in_stock'=>$newquantity]);
                    }
                    else{
                        return  response()->json(['error'=>'item','item_id'=>$itemId]);
                    }
                }

                $userdetails->username = $fullname;

                $userdetails -> cell_no = $cnumber;

                $userdetails -> address = $address;

                $userdetails -> save();

            }
            else{
                return  response()->json(['error'=>'user']);
            }


            self::sendOrderMail($order_number);


        }

        else {
            return response()->json(['error' => 'Login','userId'=>$user_id,'itemCount'=>count($area['OrderItems'])]);
        }
        return response()->json(['succes'=>'success','ordernumber'=>$order_number]);

    }
    public static function putjsonOrder3($user_id,$string)
    {


        $area = json_decode($string, true);

        $user = $area['updateModel'];
        $address = $user['Address'];
        $cnumber = $user['Cnumber'];
        $fullname= $user['fullName'];
        // dd($area);
        $order = $area['Order'];
        $sunTotal = $order['subTotal'];
        $total = $order['total'];

        Users::where('id','=',$user_id)->first();
        $storeLOcation = $order['store_location'];

        $userdetails = Users::where("id",$user_id)->first();


        if ($userdetails !=null && count($area['OrderItems']) > 0){
            $order = new Orders();


            $orderId = $order->apiplaceOrder($user_id, $total, $sunTotal, $storeLOcation, $address, $cnumber, $fullname);


            if ($orderId) {

                $str = $orderId;
                $order_id = explode(" ",$str)[0];
                $order_number = explode(" ",$str)[1];


                foreach($area['OrderItems'] as $i => $v)
                {
                    $itemId =  $v['item_id'];
                    $qty = $v['item_qty'];
                    $unit = $v['unit'];
                    $price = $v['price'];


//                echo $itemId;
//                echo $qty;
                    $order_item = new OrderItems();

                    //$order_item->apiaddItems($order_id, $itemId, $qty);


                    $itemPlaced = $order_item->apiaddItemstwo($order_id, $itemId, $qty,$unit,$price);

                    if ($itemPlaced){
//                        $quantity=Products::where('id','=',$itemId)->pluck('quantity_in_stock')->first();
//                    $newquantity=$quantity - $qty;
//                    Products::where('id','=',$itemId)->update(['quantity_in_stock'=>$newquantity]);
                    }
                    else{
                        return  response()->json(['error'=>'item','item_id'=>$itemId]);
                    }
                }

                $userdetails->username = $fullname;

                $userdetails -> cell_no = $cnumber;

                $userdetails -> address = $address;

                $userdetails -> save();

            }
            else{
                return  response()->json(['error'=>'user']);
            }


//            self::sendOrderMail($order_number);

        }

        else {
            return response()->json(['error' => 'Login','userId'=>$user_id,'itemCount'=>count($area['OrderItems'])]);
        }

        return response()->json(['succes'=>'success','ordernumber'=>$order_number]);
    }

    // IOS application api's




        public static function getProductsIos()
    {
        $product = new Products();

        try {
            $data = $product->with('productUnits')->where('products.status','=',1)->get();

            if (!$data) {
                return response()->json(['STATUS' => false, 'MESSAGE' => 'Not Found', 'CODE' => 400], 200);
            } else {
                return response()->json([
                    'STATUS' => 'true',
                    'MESSAGE' => 'record found',
                    'DATA' => $data
                ], 200);
            }
            return response()->json($data);
        }
        catch (Exception $ex) {
            return response()->json(['STATUS' => false, 'MESSAGE' => 'Error', 'CODE' => 402], 200);
        }

    }
    public static function getUserRole(){
            return Auth::user() ->roles ->pluck('name')->first();
    }
}


