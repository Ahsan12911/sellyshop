<?php

namespace App\Models;


use Illuminate\Support\Facades\Config;

class Customers extends Model
{
    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(Config::get('selly.table_names.customers'));
    }

    public function addCustomer($request)
    {
        $this->username = $request->full_name;
        $this->email = $request->email;
        $this->phoneno = $request->phone;
        $this->address = $request->address;
        $this->save();
        return $this->id;
    }
}
