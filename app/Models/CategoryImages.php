<?php

namespace App\Models;

use Illuminate\Support\Facades\Config;

class CategoryImages extends Model
{
    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(Config::get('selly.table_names.CategoryImages'));
    }
}
