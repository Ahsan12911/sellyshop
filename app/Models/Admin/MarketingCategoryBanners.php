<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class MarketingCategoryBanners extends Model
{
    protected $table = 'marketing_category_banners';

    public $timestamps = false;

    protected $fillable = [
        'horizontal_banner',
        'vertical_banner',
        'vertical_banner_position_left',
        'vertical_banner_position_right',
        'added_on',
        'updated_on'
    ];

    function addBanners($Values)
    {
        $created = MarketingCategoryBanners::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateBanners($iid,$Values)
    {
        $updated = MarketingCategoryBanners::update($Values);
    }
}