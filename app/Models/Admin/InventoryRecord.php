<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class InventoryRecord extends Model
{
    protected $table = 'inventory_record';

    public $timestamps = false;

    protected $fillable = [
        'added_by',
        'product_id',
        'invoice',
        'unit',
        'opening_stock',
        'quantity',
        'total_stock',
        'waste_opening_price',
        'total_waste_price',
        'unit_price',
        'profit',
        'sale_price',
        'record_type',
        'record_date',
        'created_at'
    ];
    function addInventoryRecords($Values)
    {
        $created = InventoryRecord::create($Values);
        $lastInsertId = $created->id;
        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }
}
