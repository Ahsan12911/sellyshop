<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class OrdersItems extends Model
{
    protected $table = 'order_items';

    public function orders()
    {
        return $this->hasOne('App\Models\Admin\Orders','id','order_id');
    }

    protected $fillable = [
        'object_id',
        'order_id',
        'categoryAdded',
        'name',
        'sku',
        'price',
        'itemNetPrice',
        'discountedSubtotal',
        'amount',
        'total',
        'subtotal',
        'object_type',
        'quantity_unit_id',
        'item_quantity',
        'selected_amount',
        'is_foc',
        'created_at',
        'updated_at'
    ];

    public static function getOrderItemsCount($oid)
    {
        $itemsquantity = OrdersItems::where('order_id','=',$oid)->count();

        return $itemsquantity;
    }

    public function sumItemsSold($pid)
    {
        $noofItemsSold =  OrdersItems::where('object_id','=',$pid)->count('id');

        return $noofItemsSold;
    }

    public function IncomeFromSale($pid)
    {
        $totalIncome =  OrdersItems::where('object_id','=',$pid)->count('price');

        return $totalIncome;
    }

    function addOrderItems($Values)
    {
        $created = OrdersItems::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }
}