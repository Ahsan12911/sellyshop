<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'country_translations';

    public $timestamps = false;

    protected $fillable = [
        'label_id',
        'id',
        'country',
        'code'
    ];
}