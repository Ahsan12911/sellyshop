<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Categories extends Model
{
    public $timestamps = false;
    use SoftDeletes;

    protected $fillable = [
        'parent_id',
        'category_title',
        'category_description',
        'category_icon',
        'category_page_title',
        'meta_keywords',
        'meta_descriptions',
        'cleanURL',
        'status',
        'CreatedDate',
        'ModifiedDate'
    ];

    function addCategory($Values)
    {
        $created = Categories::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateCategory($pid,$Values)
    {
        $created = Categories::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }
}