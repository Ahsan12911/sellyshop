<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class StockLog extends Model
{
   protected $table = 'stock_log';

   protected $hidden = [];

   public static function addLog($message,$orderId,$productId,$userId = null)
   {
       if($userId == null)
       {
           $userId = isset(Auth::user()->id)&&Auth::user()->id!=null?Auth::user()->id:null;
       }
       $log = new StockLog();
       $log->message = $message;
       $log->order_id = $orderId;
       $log->product_id = $productId;
       $log->user_id = $userId;
       $log->save();
   }
   public static function addLogDelete($message,$orderId,$productId,$userId = null,$productName)
   {
       if($userId == null)
       {
           $userId = isset(Auth::user()->id)&&Auth::user()->id!=null?Auth::user()->id:null;
       }
       $log = new StockLog();
       $log->message = $message;
       $log->order_id = $orderId;
       $log->product_id = $productId;
       $log->product_name = $productName;
       $log->user_id = $userId;
       $log->save();
   }
}
