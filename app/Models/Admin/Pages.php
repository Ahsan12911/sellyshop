<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'body',
        'metaKeywords',
        'metaDescriptions',
        'metaTitle',
        'cleanURL',
        'enabled'
    ];

    function addPage($Values)
    {
        $created = Pages::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updatePage($pid,$updatedValues)
    {
        $updated = Pages::where('id','=',$pid)->update($updatedValues);
        if($updated)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function deletePage($id)
    {
        $deleted = Pages::where('id','=',$id)->delete();
        if($deleted)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
