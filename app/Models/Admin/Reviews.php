<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'product_id',
        'review',
        'response',
        'rating',
        'additionDate',
        'responseDate',
        'reviewerName',
        'status',
        'ip',
        'isNew',

    ];

    function  addReviews($values){
        $created=Reviews::create($values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }


    }
}

