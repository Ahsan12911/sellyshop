<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ProductsImages extends Model
{
    protected $table ='products_images_multiple';
    protected $timestamp = false;
}
