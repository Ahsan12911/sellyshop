<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class MarketingNewLaunchSlider extends Model
{
    public $timestamps = false;
    protected $table = 'marketing_new_launch_slider';
    protected $fillable = [
        'image',
        'title',
        'date_added',
        'date_modified',
        'status',

    ];

    function addSlider($Values)
    {
        $created = MarketingNewLaunchSlider::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }
    function updateSlider($id,$Values)
    {
        $updated = MarketingNewLaunchSlider::update($Values);
    }
}
