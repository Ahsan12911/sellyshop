<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PaymentMethods extends Model
{
    public $timestamps = false;

    protected $table = 'payment_methods';

}

