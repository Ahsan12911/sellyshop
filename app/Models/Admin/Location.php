<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'storelocation';

    public $timestamps = false;


    protected $fillable = [
        'store_name',
        'location_name',
        'timefrom',
        'timeto',

    ];
    function addLocation($Values)
    {
        $created = Location::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }
}
