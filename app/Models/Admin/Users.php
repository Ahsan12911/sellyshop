<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Users extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    public $timestamps = false;
    protected $softDelete = true;
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
        'user_status',
        'created_at',
        'updated_at',
        'cnic',
        'country',
        'city',
    ];

    function addUser($Values)
    {
        $created = Users::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateUser($pid,$Values)
    {
        $created = Users::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function getUserInfo($userId){
        $userInfo = Users::where('id','=',$userId)->first();
        return $userInfo;
    }
  public function usersOrders(){
        return $this->hasOne('App\Models\Admin\Orders','user_id','id');
  }
  public function shops(){
      return $this->belongsTo('App\Models\Shops','id','owner');
  }

}