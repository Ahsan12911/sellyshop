<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class InventoryWastageItems extends Model
{
    protected $table = 'inventory_wastage_items';

    public $timestamps = false;

    protected $fillable = [
        'inventory_id',
        'product_id',
        'unit',
        'opening_stock',
        'quantity_waste',
        'total_stock'
    ];

    function addWastageInventoryItems($Values)
    {
        $created = InventoryWastageItems::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    function updateWastageInventoryItems($iid,$Values)
    {
        $updated = InventoryWastageItems::where('id',$iid)->update($Values);
    }
}