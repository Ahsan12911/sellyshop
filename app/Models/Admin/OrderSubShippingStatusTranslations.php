<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class OrderSubShippingStatusTranslations extends Model
{
    //
    protected $table = 'order_shipping_sub_status_translations';
    public $timestamps = false;
    protected $hidden = [];
}
