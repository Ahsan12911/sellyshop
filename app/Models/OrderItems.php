<?php

namespace App\Models;

use App\Models\Admin\Units;
use Illuminate\Support\Facades\Config;

class OrderItems extends Model
{
    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     *
     */
    protected $table = 'order_items';


    protected $fillable = [
        'object_id',
        'order_id',
        'name',
        'sku',
        'price',
        'shop_id',
        'quantity_unit_id',
        'itemNetPrice',
        'total',
        'subtotal',
        'item_quantity'
    ];

    public function item()
    {
        return $this->hasOne('App\Models\Products','id','object_id');
    }
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable('order_items');
    }
    function addOrderItems($Values)
    {
        $created = OrderItems::create($Values);
        $lastInsertId = $created->id;

        if ($created)
        {
            return $lastInsertId;
        }
        else
        {
            return false;
        }
    }

    public function apiaddItems($order_id,$item,$qty)
    {


        $item_details = Products::select('product_title','sku','salePriceValue','unit')->where('id',$item)->first();
        $unit=Units::where('product_id','=',$item)->orderBy('id','DESC')->first();
        if ($item_details && $unit) {
            $this->object_id = $item;
            $this->order_id = $order_id;
            $this->name = $item_details->product_title;
            $this->sku = isset($item_details->sku) ? $item_details->sku : '';
            $this->price = $unit->product_price;
            $this->itemNetPrice = $unit->product_price;
            $this->item_quantity = $qty;
            $this->quantity_unit_id = $unit->unit;
            $this->total = $qty * $unit->product_price;
            $this->subtotal = $qty * $unit->product_price;
            $this->save();


            return response()->json(['success'=>'success']);
        }
        else
        {
            return response()->json(['error'=>'something wrong']);
        }


    }

    public function apiaddItemstwo($order_id,$item,$qty,$unit,$price)
    {


        $item_details = Products::select('product_title','sku','salePriceValue','unit')->where('id',$item)->first();
//        $unit=Units::where('product_id','=',$item)->orderBy('id','DESC')->first();
        if ($item_details ) {
            $this->object_id = $item;
            $this->order_id = $order_id;
            $this->name = $item_details->product_title;
            $this->sku = isset($item_details->sku) ? $item_details->sku : '';
            $this->price = $price;
            $this->itemNetPrice = $price;
            $this->item_quantity = $qty;
            $this->quantity_unit_id = $unit;
            $this->total = $qty * $price;
            $this->subtotal = $qty * $price;
            $this->save();


            return response()->json(['success'=>'success']);
        }
        else
        {
            return response()->json(['error'=>'something wrong']);
        }


    }
}