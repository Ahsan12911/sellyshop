<?php

namespace App\Models;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\QueryException;
use Psy\Util\Json;

class Products extends Model
{
    public $timestamps = false;
    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    // protected $table= 'Products';
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(Config::get('selly.table_names.products'));
    }

    /**
     * The category that belong to the user.
     */
    public function categories()
    {
        return $this->hasOne(Config::get('selly.models.categories'));
    }

    /**
     * Get the images for the Product.
     */
    public function productImages()
    {
        return $this->hasMany(Config::get('selly.models.ProductImages'), 'product_id');
    }
    public function categoryProducts()
    {
        return $this->hasMany('App\Models\Categories','id','category_id');
    }

    /**
     * Return Products
     * @param
     * @return Json
     */

    public static function getProducts()
    {
//        if(count($products->get())==0){
//            return response()->json(['Status'=>true],['Message' => __('messages.no_data_found')]);
//        }
        $params =
            [
                'where' => [['on_sales', '<', '0']],
                'where_or' => [['on_sales', '0']],
                'order_by' => 'id',
                'order' => 'desc',
                'paginate' => 1,
            ];
        try {
            $products = new Products();
            if (isset($params['where']) && $params['where'] != "") {
                $products->where($params['where']);
            }
            if (isset($params['where_or']) && $params['where_or'] != "") {
                $products->orwhere($params['where_or']);
            }
            if (isset($params['order_by']) && $params['order_by'] != "" && isset($params['order']) && $params['order'] != "") {
                $products->orderBy($params['order_by'], $params['order']);
            }
            if (isset($params['paginate']) && $params['paginate'] != "") {
                dd($products->where($params['where'])->orwhere($params['where_or'])->orderBy($params['order_by'], $params['order'])->toSql());
                dd($products->paginate($params['paginate']));
            } else {
                $products->get();
            }
        } catch (QueryException $ex) {
            return response()->json(['Status' => false], ['Error' => $ex->getMessage()]);
        }
    }
    public function productsUnit(){
        return $this->hasMany('App\Models\Admin\Units', 'product_id', 'id');
    }
    public function productsCategory(){
        return $this->hasOne('App\Models\Categories', 'id', 'category_id');
    }
}
