<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Users extends Model
{
    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
    */

    protected $guarded = [];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(Config::get('selly.table_names.products'));
    }

    /**
     * The categories that belong to the user.
     */
    public function categories()
    {
        return $this->belongsToMany(Config::get('selly.models.categories'),Config::get('selly.table_names.ProductCategories'),'product_id', 'category_id');
    }

    /**
     * Get the images for the Product.
    */
    public function productImages()
    {
        return $this->hasMany(Config::get('selly.models.ProductImages'),'product_id');
    }
}