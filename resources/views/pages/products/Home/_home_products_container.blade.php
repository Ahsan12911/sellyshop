@php($count=0)
<div class="img-prd">
    <div class="hometab box">
        <div class="tab-head">
            <div class="hometab-heading box-heading">{{strtoupper($title)}}</div>
            <div id="{{$title}}" class="htabs tabs">
                <ul class="nav nav-tabs">
                    @foreach(Config::get('constants.sub_categories') as $key=>$value)
                        @php($count++)
                        <li class='tab {{$count==1?'active':''}}'>
                            <a href="#tab-{{$value.$title}}" data-toggle="tab">{{$key}}</a>
                        </li>
                        @endforeach
                </ul>
            </div>
        </div>
@php($count=0)
        <div class="tab-content">
            @foreach(Config::get('constants.sub_categories') as $key=>$value)
                @php($count++)
                <div id="tab-{{$value.$title}}" class="tab-pane fade in {{$count==1?'active':''}}">
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">
                                <a class="fa prev"></a>
                                <a class="fa next"></a>
                            </div>
                            <div class="owl-carousel custom_carousel box-product" id="products_{{$value.$title}}">

                            </div>
                        </div>
                    </div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>
            @endforeach
            <div class="pre_loader">
                {!! $content or '<div class="loader"></div>'!!}
            </div>

        </div>
    </div>
</div>