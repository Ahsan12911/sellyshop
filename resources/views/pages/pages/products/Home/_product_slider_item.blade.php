<div class="slider-item">
    <div class="product-block product-thumb transition">
        <div class="product-block-inner">
            <div class="image">
                <a href="#">
                    <img src="{{asset('public/images/catalog/16-242x297.jpg')}}" title="Aliquam Ac Neque" alt="Aliquam Ac Neque" class="img-responsive reg-image"/>
                    <img class="img-responsive hover-image" src="{{asset('public/images/catalog/18-242x297.jpg')}}" title="Aliquam Ac Neque" alt="Aliquam Ac Neque"/>
                </a>
                <div class="button-gr lists">
                    <div class="quickview-button" ><a class="quickview-button" href="#">Quick View</a></div>
                </div>
                <div class="rating">
                    <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                    <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                    <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                    <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                    <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                </div>
            </div>
            <div class="product-details">
                <div class="caption">
                    <h4 class="name"><a href="#" title="Aliquam Ac Neque">Capsicum</a></h4>
                    <p class="price">
                        $241.99                                                                                                             <span class="price-tax">Ex Tax: $199.99</span>
                    </p>
                    <div class="button-group">
                        <button type="button" class="addtocart" onclick="cart.add('49');"><i class="fa fa-shopping-cart"></i><a href="add-products.html" class="hidden-xs hidden-sm hidden-md">Add to Cart</a></button>
                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('49');"><i class="fa fa-heart"></i></button>
                        <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('49');"><i class="fa fa-exchange"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>