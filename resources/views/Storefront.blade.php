<!doctype html>
<html>
<head>
    <title>Selly</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">


    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
    <style>

        html {
            position: relative;
            min-height: 100%;
        }

        body {
            background-color: #fff;
            font-family: "Roboto", sans-serif;
            font-size: 14px;

            margin:0;padding:0
        }
        .bg-clr{background:#2caae1;height:100px;text-align:center;position:relative;}
        .circle{background:#fff;text-align:center;height:120px;width:120px;border-radius:100%;display:inline-block;border:1px solid #ddd;margin-top:50px;}
        .progress-txt{margin-top:100px;}
        h3{text-align:center;color:gray;}
        h4{text-align:center;color:gray;margin-top:52px;font-size:20px;}
        p{text-align:center;color:gray;margin-top:-10px;}
        img{width: 50px;
        }
        .animation{
            position: absolute;
            left: 0;
            right: 0;
            margin: 33px auto;
        }

        @keyframes rotate{
            from{
                transform: rotate(0deg)
            }
            to{
                transform: rotate(360deg)
            }

        }
    </style>
</head>

<body>
<div class="bg-clr">
    <div class="circle">
        <img class="animation" src="{{url('public/images/catalog/gear.png')}}" alt="" class="" style="animation: rotate 5s ease-in-out infinite">
    </div>
</div>
<h3 class="progress-txt">WORK IN PROGRESS</h3>
<h4>Storefront is closed due to maintenance</h4>
<p>Inconvenience is regretted. Visit again after short time.</p>

</body>
</html>
