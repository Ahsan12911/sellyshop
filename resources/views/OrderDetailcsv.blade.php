<tr>
    <th>OrderNumber</th>
    <th>customerName</th>
    <th>customerEmail</th>
    <th>customerAddress</th>
    <th>customerCellno</th>
    {{--<th>User Name</th>--}}
    {{--<th>User Email</th>--}}
    {{--<th>User Address</th>--}}

    <th>itemName</th>
    <th>itemPrice</th>
    <th>itemQuantity</th>
    <th>subtotal</th>
    <th>Discount on Product</th>
    <th>Product Discount Price</th>
    <th>Order Total</th>
    <th>Discount on Order</th>
    <th>Order Discount Price</th>
    <th>shipping status</th>
    <th>payment status</th>
    <th>payment method</th>
    <th>orders date</th>
    <th>Staff note</th>
    <th>Process By</th>
    <th>Delivered By</th>
</tr>
@php
    $grandTatal=0;

@endphp
@foreach($data as $order)

    @foreach($order as $orderitem)
        @php
            $user= \App\Users::all();
            $username=$user->where('id','=',$orderitem->userID)->pluck('username')->first();
            $email=$user->where('id','=',$orderitem->userID)->pluck('email')->first();
            $address=$user->where('id','=',$orderitem->userID)->pluck('address')->first();
            $cellNo=$user->where('id','=',$orderitem->userID)->pluck('cell_no')->first();
        @endphp
        <tr>
            <td>{{$orderitem->orderNumber}}</td>
            @if($orderitem->orderUsername != '')
                <td>{{$orderitem->orderUsername}}</td>
                <td>@if($orderitem->customers_email !=''){{$orderitem->customers_email}}@else{{$email}}@endif</td>
                <td>{{$orderitem->orderAddress}}</td>
                <td>{{$orderitem->orderPhoneno}}</td>
            @elseif($orderitem->customers_username != '')
                <td>{{$orderitem->customers_username}}</td>
                <td>{{$orderitem->customers_email}}</td>
                <td>{{$orderitem->customers_address}}</td>
                <td>{{$orderitem->phoneno}}</td>
            @else
                <td>{{$username}}</td>
                <td>{{$email}}</td>
                <td>{{$address}}</td>
                <td>{{$cellNo}}</td>
            @endif
            <td>{{$orderitem->item_name}}</td>
            <td>{{$orderitem->item_price}}</td>
            <td>{{$orderitem->item_quantity}}</td>
            <td>{{$orderitem->order_item_total}}</td>
            <td>{{$orderitem->order_item_discount}}@if($orderitem->order_item_discount > 0)%@endif</td>
            <td>{{$orderitem->order_item_discount_price}}</td>
            <td>@if($orderitem->orderDiscountTotal > 0)
                    {{$orderitem->orderDiscountTotal}}
                @else
                    {{$orderitem->ordertotal}}
                @endif</td>
            <td>{{$orderitem->orderDiscount}}@if($orderitem->orderDiscount > 0)%@endif</td>
            <td>{{$orderitem->orderDiscountTotal}}</td>
            <td>{{$orderitem->shipping_status_name}}</td>
            <td>{{$orderitem->payment_name}}</td>
            <td>COD</td>
            <td>{{$orderitem->orderdate}}</td>
            <td>{{$orderitem->staff_note}}</td>
            @php
                if($orderitem->order_item_discount_price)
                {
                 $grandTatal += $orderitem->order_item_discount_price;

                }
                else{
               $grandTatal += $orderitem->order_item_total;
                }

                        $deliverdBy= $user->where('id','=',$orderitem->delivered_by)->pluck('username')->first();
                        $processBy= $user->where('id','=',$orderitem->processed_by)->pluck('username')->first();

            @endphp


            <th>{{$processBy}}</th>
            <th>{{$deliverdBy}}</th>
        </tr>
    @endforeach
@endforeach
<br>
<tr>
    <th>
    </th>
    <th>
    </th>
    <th>
    </th>
    <th>
        Grand Total
    </th>
    <th>
        {{$grandTatal}}
    </th>
</tr>
