<div class="hidden-xs img-prd">
    <div class="hometab box">

        <div class="our-store our-store-small-img">
            <!-- <img src="image/cache/catalog/featured.png" title="" alt="" class="featured-img">-->
            <h3 class="module-title"><span>Featured</span></h3>

            <div class="row mt-20">
                <div class="col-md-6 our-store-small-img">
                    <div class="row">
                        <div class="col-sm-6 pr-5">
                            <img src="{{Utility::getImage('catalog/pizza-shop1.jpg')}}" title="" alt="" class="img-responsive">
                        </div>
                        <div class="col-sm-6 pr-5">
                            <img src="{{Utility::getImage('catalog/pizza-shop2.jpg')}}" title="" alt="" class="img-responsive">
                        </div>

                    </div>
                    <div class="row mt-20">
                        <div class="col-sm-6 pr-5">
                            <img src="{{Utility::getImage('catalog/Chicken-Burger2.jpg')}}" title="" alt="" class="img-responsive">
                        </div>
                        <div class="col-sm-6 pr-5">
                            <div>
                                <img src="{{Utility::getImage('catalog/shawarma.jpg')}}" title="" alt="" class="img-responsive">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="owl-carousel launched-featured-carousel owl-theme">

                        <div class="slider-item">
                            <img src="{{Utility::getImage('catalog/launched-item2.jpg')}}" title="" alt="" class="img-responsive our-store-large-img">
                        </div>
                        <div class="slider-item">
                            <img src="{{Utility::getImage('catalog/launched-slider3.jpg')}}" title="" alt="" class="img-responsive our-store-large-img">
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<style>
    .our-store
    {
        background:url("public/images/catalog/our-store-bg.png");
    }

</style>
<script>
    $('.launched-featured-carousel').owlCarousel({
        items: 1,
        singleItem: false,
        navigation: true,
        autoPlay: true,
        navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
        pagination: true,
        itemsDesktop : [1200,4],
        itemsDesktopSmall :	[979,3],
        itemsTablet :	[767,2],
        itemsMobile : [479,1]
    });
</script>