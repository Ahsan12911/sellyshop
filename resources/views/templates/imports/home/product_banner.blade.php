<div class="clearfix"></div>

<div class="home-seo-text">
    <h1 class="text-center">Quality with Convenience</h1>
    <p>
        Selly is on a mission to fight against food adulteration. The selling of export quality products within Lahore gives us an
        opportunity to declare a war against the status quo. With a wise leadership and an able workforce, we plan on making
        Pakistan a healthier place to live in. By striving to support everything you need in the kitchen, we bring unmatchable
        grocery products, tempting fruits, and the greenest vegetables. Online grocery shopping is not the same when you
        are at Selly because the products have a class of their own and the services compliment them unequivocally.
    </p>
    <p class="mt-20 mb-0">
        <a href="https://www.selly.pk/vegetables">Fresh vegetables</a> at our online grocery store speak of nature in its true sense. While the fruits in Pakistan
        generally face the corrupt dilemma, here at Selly.pk, we have turned it around for the sake of commonizing better
        health.
        <a href="https://www.selly.pk/grocery">Buy grocery online</a> with complete trust & confidence in the functioning elements of Selly.pk. Let’s prioritize
        health with our export quality products!
    </p>
    <a href="https://selly.pk/about-us" class="home-read-more-btn">Read More</a>
</div>
{{--<div class="home-seo-text">--}}
    {{--<p>--}}
        {{--Selly is on a mission to fight against food adulteration. The selling of export quality products within Lahore gives us an opportunity to declare a war against the status quo. With a wise leadership and an able workforce, we plan on making Pakistan a healthier place to live in.  By striving to support everything you need in the kitchen, we bring unmatchable grocery products, tempting fruits, and the greenest vegetables. Online grocery shopping is not the same when you are at Selly because the products have a class of their own and the services compliment them unequivocally.--}}
    {{--</p>--}}
    {{--<p>--}}
        {{--<a href="https://www.selly.pk/vegetables">Fresh vegetables</a> at our online grocery store speak of nature in its true sense. While the fruits in Pakistan generally face the corrupt dilemma, here at Selly.pk, we have turned it around for the sake of commonizing better health. <a href="https://www.selly.pk/grocery">Buy grocery online</a> with complete trust & confidence in the functioning elements of Selly.pk. Let’s prioritize health with our export quality products!--}}
    {{--</p>--}}

{{--</div>--}}
<div class="product_banner hidden-xs">
    <div class="product_img">
        <a href="#" title="pbanner"><img class="lazy prd_image" src="{{asset(Config::get('constants.public_path').'images/slider/banner-img.jpg')}}" alt="banner-img"></a>
    </div>
</div>
