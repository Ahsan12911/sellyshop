{{--Content Top Banner Images and Slider--}}
<div class="content-top">
    <div class="main-slider">
        <div id="spinner"></div>
        <div class="banner mt-20">
            <div id="kb" class="carousel kb_elastic animate_text kb_wrapper" data-ride="carousel" data-interval="6000" data-pause="hover">
                <!-- Wrapper-for-Slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <!-- First-Slide -->
                        <img src="{{asset(Config::get('constants.public_path').'images/slider/rice-slider-2.jpg')}}" alt="rice-slider" class="img-responsive" />
                        <div class="carousel-caption kb_caption kb_caption_right">
                            <h3 data-animation="animated flipInX">Selly. <span></span> pk</h3>
                            <h4 data-animation="animated flipInX">Currently Serving in Lahore</h4>
                        </div>
                    </div>
                    <div class="item">
                        <!-- Second-Slide -->
                        <img src="{{asset(Config::get('constants.public_path').'images/slider/bg-slider-new.jpg')}}" alt="bg-slider-new" class="img-responsive" />
                        <div class="carousel-caption kb_caption kb_caption_center">
                            <h3 data-animation="animated fadeInDown">Fresh Fruits & Vegetables</h3>
                            <h4 data-animation="animated fadeInUp">10:00 AM - 09:00 PM </h4>
                        </div>
                    </div>
                                  <div class="item">
                        <!-- fourth-Slide -->
                        <img src="{{asset(Config::get('constants.public_path').'images/slider/bg-slider4.jpg')}}" alt="banner-img" class="img-responsive" />
                        <div class="carousel-caption kb_caption kb_caption_center">
                            <h3 data-animation="animated fadeInLeft">Fresh Fruits, Vegetables and Groceries</h3>
                            <h4 data-animation="animated flipInX">Complete Household and Grocery Store</h4>
                        </div>
                    </div>
                </div>
                <!-- Left-Button -->
                <a class="left carousel-control kb_control_left" href="#kb" role="button" data-slide="prev">
                    <span class="fa fa-angle-left kb_icons" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <!-- Right-Button -->
                <a class="right carousel-control kb_control_right" href="#kb" role="button" data-slide="next">
                    <span class="fa fa-angle-right kb_icons" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#slideshow0').owlCarousel({
            items: 6,
            autoPlay: 5000,
            singleItem: true,
            navigation: true,
            navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
            pagination: true,
            transitionStyle : "fade"
        });
    </script>

    <script type="text/javascript">
        // Can also be used with $(document).ready()
        $(window).load(function () {
            $("#spinner").fadeOut("slow");
        });
    </script>
    <div class="subslider hidden-xs ">
        <div class="subbanner1_img">
            <div class="slidebanner1 subbanner">
                <a href="#" title="Banner_CMS1">
                    <img class="banner_image" src="{{asset(Config::get('constants.public_path').'images/catalog/slider-coming-soon.jpg')}}"
                         alt="strawberry">
                </a>
            </div>
            <div class="slidebanner2 subbanner">
                <a href="#" title="Banner_CMS1">
                    <img class="banner_image" src="{{asset(Config::get('constants.public_path').'images/catalog/fruits1.jpg')}}"
                         alt="fruits">
                </a>
            </div>
        </div>
        <div class="subbanner2_img">
            <div class="slidebanner3 subbanner">
                <a href="#" title="Banner_CMS1">
                    <img class="banner_image" src="{{asset(Config::get('constants.public_path').'images/catalog/vegetables-category.jpg')}}"
                         alt="vegetables">
                </a>
            </div>
            <div class="slidebanner4 subbanner">
                <a href="#" title="Banner_CMS1">
                    <img class="banner_image" src="{{asset(Config::get('constants.public_path').'images/catalog/banner-image.jpg')}}"
                         alt="banner">
                </a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="text-center">
        <div id="servicecmsblock" class="clearfix">
            <div class="col-md-2 text-center">
                <div class="second-content-two">
                    <div class="inner-content">
                        <div class="service-content">
                            <div class="icon-left2"></div>
                            <div class="service-right">
                                <div class="title">Free Shipping</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-center">
                <div class="first-content-one">
                    <div class="inner-content">
                        <div class="service-content">
                            <div class="icon-left1"></div>
                            <div class="service-right">
                                <div class="title">Premium Quality</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-center">
                <div class="fifth-content-five">
                    <div class="inner-content">
                        <div class="service-content">
                            <div class="icon-left5"></div>
                            <div class="service-right">
                                <div class="title">10am to 9pm Customer Support</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-center">
                <div class="sixth-content-six">
                    <div class="inner-content">
                        <div class="service-content">
                            <div class="icon-left6"></div>
                            <div class="service-right">
                                <div class="title">Competitive Prices</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-center">
                <div class="four-content-four">
                    <div class="inner-content">
                        <div class="service-content">
                            <div class="icon-left4"></div>
                            <div class="service-right">
                                <div class="title">Money back Guarantee</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-center">
                <div class="third-content-three">
                    <div class="inner-content">
                        <div class="service-content">
                            <div class="icon-left3"></div>
                            <div class="service-right">
                                <div class="title">No Hidden Charges</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>