<div class="col-md-12 filter-pd-rl">
    <div class="category_filter hidden-xs">
        <div class="col-md-4 btn-list-grid">
            <div class="btn-group">
                <button type="button" id="grid-view" class="btn btn-default grid active" data-toggle="tooltip" title="" data-original-title="Grid"><i class="fa fa-th"></i></button>
                <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="" data-original-title="List"><i class="fa fa-th-list"></i></button>
            </div>
        </div>
        <div class="compare-total">
            <a href="#" id="compare-total">Product Compare (0)</a>
        </div>
        <div class="pagination-right">
            <div class="sort-by-wrapper">
                <div class="col-md-2 text-right sort-by">
                    <label class="control-label" for="input-sort">Sort By:</label>
                </div>
                <div class="col-md-3 text-right sort">
                    <select id="input-sort" class="form-control" >
                        <option value="" selected="selected">Default</option>
                        <option value="">Name (A - Z)</option>
                        <option value="">Name (Z - A)</option>
                        <option value="">Price (Low &gt; High)</option>
                        <option value="">Price (High &gt; Low)</option>
                        <option value="">Rating (Highest)</option>
                        <option value="">Rating (Lowest)</option>
                        <option value="">Model (A - Z)</option>
                        <option value="">Model (Z - A)</option>}
                    </select>

                </div>
            </div>
            <div class="show-wrapper">
                <div class="col-md-1 text-right show">
                    <label class="control-label" for="input-limit">Show:</label>
                </div>
                <div class="col-md-2 text-right limit">
                    <select id="input-limit" class="form-control">
                        <option value="#" selected="selected">12</option>
                        <option value="#">25</option>
                        <option value="#">50</option>
                        <option value="#">75</option>
                        <option value="#">100</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
