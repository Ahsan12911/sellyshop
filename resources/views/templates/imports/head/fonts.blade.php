<link rel="preload" href="{{ url('public')  }}/front/assets/vendor/fontawesome-free/webfonts/fa-regular-400.woff2" as="font" type="font/woff2"
      crossorigin="anonymous">
<link rel="preload" href="{{ url('public')  }}/front/assets/vendor/fontawesome-free/webfonts/fa-solid-900.woff2" as="font" type="font/woff2"
      crossorigin="anonymous">
<link rel="preload" href="front/{{ url('public')  }}/front/assets/vendor/fontawesome-free/webfonts/fa-brands-400.woff2" as="font" type="font/woff2"
      crossorigin="anonymous">
<link rel="preload" href="{{ url('public')  }}/front/assets/fonts/wolmart87d5.woff?png09e" as="font" type="font/woff" crossorigin="anonymous">
<!-- Vendor CSS -->
<link rel="stylesheet" type="text/css" href="{{ url('public')  }}/front/assets/vendor/fontawesome-free/css/all.min.css">
