@php($logged_in = \Illuminate\Support\Facades\Auth::check())
<div class="header-bottom sticky-content fix-top sticky-header has-dropdown">
    <div class="container">
        <div class="inner-wrap">
            <div class="header-left">
                <div class="dropdown category-dropdown has-border" data-visible="true">
                    <a href="#" class="category-toggle text-dark" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="true" data-display="static"
                       title="Browse Categories">
                        <i class="w-icon-category"></i>
                        <span>Browse Categories</span>
                    </a>

                    <div class="dropdown-box">
                        <ul class="menu vertical-menu category-menu">
                            @foreach($categories as $item)
                                <li class="top_level"><a href="{{url($item->cleanURL)}}">{{$item->category_title}}</a>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <nav class="main-nav">
                    <ul class="menu active-underline">
                        <li class="active">
                            <a href="demo1.html">Home</a>
                        </li>
                        <li>
                            <a href="shop-banner-sidebar.html">Shop</a>
                        </li>
                        <li>
                            <a href="vendor-dokan-store.html">Vendor</a>
                        </li>
                        <li>
                            <a href="blog.html">Blog</a>
                        </li>
                        <li>
                            <a href="about-us.html">Pages</a>
                        </li>
                        <li>
                            <a href="elements.html">Elements</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="header-right">
                <a href="#" class="d-xl-show"><i class="w-icon-map-marker mr-1"></i>Track Order</a>
                <a href="#"><i class="w-icon-sale"></i>Daily Deals</a>
            </div>
        </div>
    </div>
</div>


