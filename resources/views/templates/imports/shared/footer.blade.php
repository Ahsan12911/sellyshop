<!-- Start of Footer -->
<footer class="footer appear-animate" data-animation-options="{
            'name': 'fadeIn'
        }">
    <div class="footer-newsletter bg-primary">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-xl-5 col-lg-6">
                    <div class="icon-box icon-box-side text-white">
                        <div class="icon-box-icon d-inline-flex">
                            <i class="w-icon-envelop3"></i>
                        </div>
                        <div class="icon-box-content">
                            <h4 class="icon-box-title text-white text-uppercase font-weight-bold">Subscribe To
                                Our Newsletter</h4>
                            <p class="text-white">Get all the latest information on Events, Sales and Offers.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-6 col-md-9 mt-4 mt-lg-0 ">
                    <form action="#" method="get"
                          class="input-wrapper input-wrapper-inline input-wrapper-rounded">
                        <input type="email" class="form-control mr-2 bg-white" name="email" id="email"
                               placeholder="Your E-mail Address" />
                        <button class="btn btn-dark btn-rounded" type="submit">Subscribe<i
                                    class="w-icon-long-arrow-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="widget widget-about">
                        <a href="demo1.html" class="logo-footer">
                            <img src="{{ url('public')  }}/front/assets/images/logo_footer.png" alt="logo-footer" width="144"
                                 height="45" />
                        </a>
                        <div class="widget-body">
                            <p class="widget-about-title">Got Question? Call us 24/7</p>
                            <a href="tel:18005707777" class="widget-about-call">1-800-570-7777</a>
                            <p class="widget-about-desc">Register now to get updates on pronot get up icons
                                & coupons ster now toon.
                            </p>

                            <div class="social-icons social-icons-colored">
                                <a href="#" class="social-icon social-facebook w-icon-facebook"></a>
                                <a href="#" class="social-icon social-twitter w-icon-twitter"></a>
                                <a href="#" class="social-icon social-instagram w-icon-instagram"></a>
                                <a href="#" class="social-icon social-youtube w-icon-youtube"></a>
                                <a href="#" class="social-icon social-pinterest w-icon-pinterest"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget">
                        <h3 class="widget-title">Company</h3>
                        <ul class="widget-body">
                            <li><a href="about-us.html">About Us</a></li>
                            <li><a href="#">Team Member</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="contact-us.html">Contact Us</a></li>
                            <li><a href="#">Affilate</a></li>
                            <li><a href="#">Order History</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget">
                        <h4 class="widget-title">My Account</h4>
                        <ul class="widget-body">
                            <li><a href="#">Track My Order</a></li>
                            <li><a href="cart.html">View Cart</a></li>
                            <li><a href="login.html">Sign In</a></li>
                            <li><a href="#">Help</a></li>
                            <li><a href="wishlist.html">My Wishlist</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget">
                        <h4 class="widget-title">Customer Service</h4>
                        <ul class="widget-body">
                            <li><a href="#">Payment Methods</a></li>
                            <li><a href="#">Money-back guarantee!</a></li>
                            <li><a href="#">Product Returns</a></li>
                            <li><a href="#">Support Center</a></li>
                            <li><a href="#">Shipping</a></li>
                            <li><a href="#">Term and Conditions</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle">
            <div class="widget widget-category">
                <div class="category-box">
                    <h6 class="category-name">Consumer Electric:</h6>
                    <a href="#">TV Television</a>
                    <a href="#">Air Condition</a>
                    <a href="#">Refrigerator</a>
                    <a href="#">Washing Machine</a>
                    <a href="#">Audio Speaker</a>
                    <a href="#">Security Camera</a>
                    <a href="#">View All</a>
                </div>
                <div class="category-box">
                    <h6 class="category-name">Clothing & Apparel:</h6>
                    <a href="#">Men's T-shirt</a>
                    <a href="#">Dresses</a>
                    <a href="#">Men's Sneacker</a>
                    <a href="#">Leather Backpack</a>
                    <a href="#">Watches</a>
                    <a href="#">Jeans</a>
                    <a href="#">Sunglasses</a>
                    <a href="#">Boots</a>
                    <a href="#">Rayban</a>
                    <a href="#">Acccessories</a>
                </div>
                <div class="category-box">
                    <h6 class="category-name">Home, Garden & Kitchen:</h6>
                    <a href="#">Sofa</a>
                    <a href="#">Chair</a>
                    <a href="#">Bed Room</a>
                    <a href="#">Living Room</a>
                    <a href="#">Cookware</a>
                    <a href="#">Utensil</a>
                    <a href="#">Blender</a>
                    <a href="#">Garden Equipments</a>
                    <a href="#">Decor</a>
                    <a href="#">Library</a>
                </div>
                <div class="category-box">
                    <h6 class="category-name">Health & Beauty:</h6>
                    <a href="#">Skin Care</a>
                    <a href="#">Body Shower</a>
                    <a href="#">Makeup</a>
                    <a href="#">Hair Care</a>
                    <a href="#">Lipstick</a>
                    <a href="#">Perfume</a>
                    <a href="#">View all</a>
                </div>
                <div class="category-box">
                    <h6 class="category-name">Jewelry & Watches:</h6>
                    <a href="#">Necklace</a>
                    <a href="#">Pendant</a>
                    <a href="#">Diamond Ring</a>
                    <a href="#">Silver Earing</a>
                    <a href="#">Leather Watcher</a>
                    <a href="#">Rolex</a>
                    <a href="#">Gucci</a>
                    <a href="#">Australian Opal</a>
                    <a href="#">Ammolite</a>
                    <a href="#">Sun Pyrite</a>
                </div>
                <div class="category-box">
                    <h6 class="category-name">Computer & Technologies:</h6>
                    <a href="#">Laptop</a>
                    <a href="#">iMac</a>
                    <a href="#">Smartphone</a>
                    <a href="#">Tablet</a>
                    <a href="#">Apple</a>
                    <a href="#">Asus</a>
                    <a href="#">Drone</a>
                    <a href="#">Wireless Speaker</a>
                    <a href="#">Game Controller</a>
                    <a href="#">View all</a>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="footer-left">
                <p class="copyright">Copyright © 2021 Wolmart Store. All Rights Reserved.</p>
            </div>
            <div class="footer-right">
                <span class="payment-label mr-lg-8">We're using safe payment for</span>
                <figure class="payment">
                    <img src="{{ url('public')  }}/front/assets/images/payment.png" alt="payment" width="159" height="25" />
                </figure>
            </div>
        </div>
    </div>
</footer>
<!-- End of Footer -->
<script>
    $('#registrationPopup').on('shown.bs.modal', function () {
        $('#registrationPopup').trigger('focus')
    });
</script>




<!-- Registration modal Start Here -->
<div class="modal fade" id="registrationPopup" tabindex="-1" role="dialog" aria-labelledby="registrationLabel">
    <div class="modal-dialog" role="document">
        <div class="login-form">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="register_error">
                <form id="form_register" name="form_register" method="POST" action="{{ route('registerCustomer') }}">
                    {{ csrf_field() }}
                    <h3 class="text-center">New Account</h3>
                    <div class="text-center social-btn">
                        <a href="{{url('/redirect/facebook')}}" class="btn btn-primary btn-lg"><i class="fa fa-facebook"></i> Sign in with <b>Facebook</b></a>
                        {{--<a href="#" class="btn btn-info btn-lg"><i class="fa fa-twitter"></i> Sign in with <b>Twitter</b></a>--}}
                        <a href="{{url('/redirect/google')}}" class="btn btn-danger btn-lg"><i class="fa fa-google"></i> Sign in with <b>Google</b></a>
                    </div>
                    <div class="or-seperator"><b>or</b></div>
                    <h4 style="color: red;" class="register_error_msg text-center"></h4>
                    <div class="form-group">
                        <input type="text" value="" class="form-control input-lg" id="username" name="username" placeholder="Username" required>
                    </div>
                    <div class="form-group">
                        <input type="email" value="" class="form-control input-lg" id="registeremail" name="registeremail" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input type="text" value="" class="form-control input-lg" id="address" name="address" placeholder="Address" required>
                    </div>
                    <div class="form-group">
                        <input type="text" value="" class="form-control input-lg" id="phone" name="phone" placeholder="Cell No." required>
                    </div>
                    <div class="form-group">
                        <input type="password" value="" class="form-control input-lg" id="password" name="password" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <input type="password" value="" class="form-control input-lg" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password" required>
                    </div>
                    <div class="text-center mb-15">
                        <label><input type="checkbox">Sign up or Selly.pk news, sales and deals</label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg login-btn">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <!-- Registration modal End Here -->
{{--<!-- Login modal Start Here -->--}}
<div class="modal fade" id="loginPopup" tabindex="-1" role="dialog" aria-labelledby="loginPopupLabel">
    <div class="modal-dialog" role="document">
        <div class="login-form">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <form id="form_login" name="form_login" method="POST" action="{{ route('loginCustomer') }}">
                {{csrf_field()}}
                <h3 class="text-center">Sign in</h3>
                <div class="text-center social-btn">
                    {{--<a href="{{url('/redirect/facebook')}}" class="btn btn-primary btn-lg"><i class="fa fa-facebook"></i> Sign in with <b>Facebook</b></a>--}}
                    {{--<a href="#" class="btn btn-info btn-lg"><i class="fa fa-twitter"></i> Sign in with <b>Twitter</b></a>--}}
                    <a href="{{url('/redirect/google')}}" class="btn btn-danger btn-lg"><i class="fa fa-google"></i> Sign in with <b>Google</b></a
                </div>
                <div class="or-seperator"><b>or</b></div>
                <h4 style="color: red;" class="login_error_msg text-center"></h4>
                <div class="form-group">
                    <input type="text" class="form-control input-lg" id="login-username" name="username" placeholder="Username" required>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control input-lg" id="login-password" name="password" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-lg login-btn">Sign in</button>
                </div>
                <div class="text-center dont-have-account">
                    <a data-toggle="modal" data-target="#Sellyregistration" class="pull-left">Create New Account</a>
                    {{--<a type="button" data-toggle="modal" data-target="#Sellyregistration" class="pull-left">Create New Account</a>--}}
                    {{--<a href="#" class="pull-left">Create New Account</a>--}}
                    <a type="button" data-toggle="modal" data-target="#forgot" class="pull-right">Forgot Password</a>
                </div>
            </form>
        </div>
    </div>
</div>