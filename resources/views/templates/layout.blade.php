<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
{{--    <meta name="description" content="{{ $meta_descriptions }}" />--}}
{{--    <meta name="keywords" content="{{$meta_keyword}}">--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">   <!-- CSRF Token -->
    <title>{{$title or 'Best Online Shopping Store'}}</title>
    <link href="{{asset(Config::get('constants.public_path').'images/icons/icossn.png')}}" rel="icon" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    @php
        $public_path=Config::get('constants.public_path');
    @endphp
    @include('templates.imports.head.fonts')
    @include('templates.imports.head.style')
    @include('templates.imports.head.scripts')
</head>
<body class="home">
<div class="page-wrapper">
 <!-- Modal -->
{{--  <div class="modal fade did-you-know-txt" id="did-you-know-txt" role="dialog">--}}
{{--    <div class="modal-dialog">--}}

{{--      <!-- Modal content-->--}}
{{--      <div class="modal-content">--}}
{{--        <div class="modal-header">--}}
{{--          <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--          <h3 class="modal-title">Did You Know</h3>--}}
{{--        </div>--}}
{{--        <div class="modal-body">--}}
{{--          <p>Word Selly is from Middle English selli, sellich, which means rare, strange, wondrous, extraordinary, wonderful; having unusually good qualities, excellent, admirable; select, better, superior, choice”), equivalent to seld +‎ -ly. Cognate with Scots selly, silly (“approved, good, worthy”), Old Saxon seldlīk (“rare, wonderful”)</p>--}}
{{--        </div>--}}
{{--      </div>--}}

{{--    </div>--}}
{{--  </div>--}}

<!--Sticky Button-->
{{--      <div class="text-center">--}}
{{--            <button data-toggle="modal" data-target="#did-you-know-txt" class="toggle_btn absolute-position toggle-nav toggle-position">--}}
{{--                    <img width="20" src="{{Utility::getImage('/catalog/did-you-know.png')}}" alt="did you know">--}}
{{--                    <span>Did you know</span>--}}
{{--             </button>--}}
{{--      </div>--}}

<header class="header">
    @include('templates.imports.shared.header')
    @include('templates.imports.shared.main-nav')
</header>


<main class="main">
    @yield('content')
</main>

<!-- Footer -->
@include('templates.imports.shared.footer')
</div>
<!--Modals-->
@include('layouts.modals.newsletter')
@include('layouts.modals.login')
@include('layouts.modals.forgotpassword')
@include('layouts.modals.resetpassword')
@include('layouts.modals.register')
<!-- Script -->
@include('templates.imports.shared.script')

@yield('script')


{{--endscript--}}
{{--endfooter--}}
<script>
    function hideFunction() {
        $('.app-banner').hide()
    }
</script>

</body>
</html>

