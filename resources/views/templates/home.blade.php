@extends('templates.layout')
@section('content')
    <div class="row home_row">
        <div id="content" class="col-sm-12">
            @include('templates.imports.home.content-top')
            <div class="">
                {!! $content or 'No Data Found'!!}
            </div>
        </div>
    </div>
@stop

