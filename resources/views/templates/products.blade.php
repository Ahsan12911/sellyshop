
@extends('templates.layout')
@section('content')

    <div class="mt-40"></div>
        <div class="row">
            <div class="col-md-3 products_leftside">
                @include('templates.imports.products.left-pane')
            </div>
            <div class="col-md-9 products_rightside">
                <h1>{{$category_title or ""}}</h1>
                <div class="row">
                    <div id="content">
                        @include('templates.imports.products.content-top-breadcrumb')
                        @include('templates.imports.products.category_filter')
                        <br>
                        {!! $content !!}
                        {{--@include('templates.imports.products.pagination-wrapper')--}}
                        <div hidden >


                        </div>
                    </div>
                </div>
            </div>
        </div>

@stop