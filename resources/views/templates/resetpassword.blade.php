<!DOCTYPE html>
<html dir="ltr" lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="My Store"/>
    <title>Selly.pk</title>
    <link href="image/catalog/cart.png" rel="icon"/>
    <!-- Fonts - Start -->
    <!-- Fonts - End -->
    <!-- CSS - Start -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- CSS - End -->
    <!-- JavaScript - Start -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- JavaScript - End -->
    <style>
        body {
            color: #808080;
            font-family: "Roboto", sans-serif;
            font-size: 14px;
            font-weight: 400;
            letter-spacing: 0.7px;
            line-height: 24px;
            margin: 0;
        }

        .container h1 {
            color: #000;
            font-weight: 700;
            text-transform: uppercase;
        }

        .checkout_payment .p-0 {
            padding: 0 !important;
        }

        .checkout_form_invoice {
            padding: 40px 0 0 0;
        }

        .invoice_box h2 {
            font-size: 20px;
            margin-bottom: 15px;
            text-transform: capitalize;
            color: #000;
        }

        .mb-0 {
            margin-bottom: 0 !important;
        }

        .mt-20 {
            margin-top: 20px;
        }

        .billing_address_box h3 {
            color: #000;
            font-size: 18px;
            margin-top: 40px;
        }

        .checkout_payment .billing_address_box span {
            display: inline-block;
            margin-bottom: 0;
        }

        .payment_method_box h3 {
            color: #000;
            font-size: 18px;
            margin-top: 40px;
        }

        .invoice_box_right {
            padding: 40px 0 0 0;
        }

        .invoice_box_right img {
            width: 200px;
        }

        .place_order_table {
            margin-top: 50px;
        }

        .place_order_table .table > thead > tr > th {
            border-bottom: 1px solid #ddd;
            padding: 10px 0;
        }

        .place_order_table .table > thead > tr > th {
            width: 30% !important;
        }

        .place_order_table .table > tbody > tr > td {
            border-top: 1px solid #ddd;
            padding: 5px;
            width: 0;
        }

        .grand-total {
            font-weight: 600;
            margin-top: 10px;
        }

        .content-top {
            margin-top: 50px;
        }

        .page-title {
            font-size: 20px;
        }

        .place_order_table .grand-total {
            color: #000;
            font-size: 20px;
        }
    </style>
</head>
<body>

<div class="container checkout_payment">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="clearfix"></div>
            <div class="col-md-5 col-sm-6 col-xs-6">
                <form method="post" action="{{route('Paswordreset')}}" onsubmit="">
                    {{csrf_field()}}
                    @if(count($errors))

                        <div >

                            {{--<strong>Whoops!</strong> There were some problems with your input.--}}

                            <br/>

                            <ul class="alert alert-danger">

                                @foreach($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach

                            </ul>
                        </div>

                    @endif

                    @if (session('status'))
                        <div id="hideaftermoment" class="alert alert-success">

                            <a href="{{url('/')}}"> <h2>Go To Home Page</h2></a>
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="checkout_form_invoice invoice_box">
                    <h2 class="mb-10">Please Enter Password</h2>
                    <input type="password" name="password" id="password"  onkeyup="validatepasswor()" class="form-control">

                    <input type="hidden" name="url" id="url" value="{{$_SERVER['REQUEST_URI']}}">
                    <input type="hidden" name="id" id="id" >
                    <h2 class="mb-10">Confirm  Password</h2>
                    <input type="password" name="confirmpassword" id="confirmpassword" onkeyup="validatepasswor()" class="form-control" >
                    <br>
                    <span id='message'></span>
                    <br>
                    <input type="submit"   value="Reset Password" class="btn btn-success" >
                    </div>
                </form>

            </div>
            </div>
            <div class="clearfix"></div>

        </div>
    </div>
<script>
    $(document).ready(function () {
    var url=$('#url').val();

        var id = url.substring(url.lastIndexOf('/') + 1);
        $('#id').val(id);
         });

    function validatepasswor() {
        var password= $('#password').val()
        var confirmpassword =$('#confirmpassword').val()
    if(password == confirmpassword){
        $('#message').html('Matching').css('color', 'green');
    } else
        $('#message').html('Not Matching').css('color', 'red');

    }
</script>
</body>
</html>