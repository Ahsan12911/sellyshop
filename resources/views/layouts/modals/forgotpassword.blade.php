

<div class="modal fade" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="login-form">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <form class="form-horizontal" role="form" action="{{url('forgotPasword')}}" method="post">
                {{csrf_field()}}
                <div id="idFormGroup" class="form-group">


                    <p class="fname_error error text-center alert alert-danger hidden"></p>
                    <div class="form-group">
                        <div class="row"><span class="error-class" id="name"></span></div>
                        <label class="control-label col-sm-4" for="Name">Email:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="email" id="RoleName" placeholder="Example@example.com">
                        </div>
                    </div>

                    <p class="email_error error text-center alert alert-danger hidden"></p>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success "  style="height: 35px;">
                            <span id="footer_action_button" class='glyphicon ' style="margin-bottom: 15px;"> Send</span>
                        </button>
                        <span>
                            You will shortly receive an email.
                        </span>


                    </div>


                </div>
            </form>

        </div>
    </div>
</div>


<script>
    $('#forgot').on('shown.bs.modal', function () {
        $('#forgot').trigger('focus')
    });

</script>