@foreach($orderlist as $orderitem)
<div class="collapsed-items">
    <div class="shipping-method-box">
        <div class="shipping-method">
            <span>Shipping Method :</span>
            <span> Free Delivery</span>
        </div>
        <div class="payment-mode">
            <span>Payment Mode :</span>
            <span> Cash on Delivery</span>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="items_blocks clearfix">
        {{--<div class="item-img">--}}
            {{--<img src="{{asset($public_path.'images/template/selly.png')}}">--}}
        {{--</div>--}}
        <div class="item-details">
            <div class="item-name">
                {{$orderitem->name}}
            </div>
            <div class="item-quantity">
                <span>Qty : </span> <span>{{$orderitem->item_quantity}} /{{$orderitem->quantity_unit_id}}  </span>
            </div>
            <div class="item-price">
                <span>Price :</span> <b>{{$orderitem->total}} Rs</b>
            </div>
        </div>
    </div>
</div>
    @endforeach