<div class="img-prd hidden-xs">
    <div class="hometab box">
        <div class="col-md-12">
            <div class="tab-head">
                <div class="hometab-heading box-heading">Best Sellers</div>
            </div>
            <div class="tab-content">
                <div id="tab-bestsellers">
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">
                                <a class="fa prev"></a>
                                <a class="fa next"></a>
                            </div>
                            <div class="owl-carousel custom_carousel box-product">
                                @foreach($products as $product)
                                    @php
                                        $product_id = $product->id;
                                        $product_title = $product->product_title;
                                        $on_sale = $product->on_sales;
                                        $salePriceValue = $product->salePriceValue;
                                        $marketPrice = $product->marketPrice;
                                        $product_description = $product->product_description;
                                        $image = $product->product_image;
                                        $cleanURL = url($product->cleanURL).'.html';
                                    @endphp
                                    <div class="slider-item">
                                        <div class="product-block product-thumb transition">
                                            <div class="product-block-inner">
                                                <div class="image">
                                                    <a href="{{url($cleanURL)}}">
                                                        <img src="{{Utility::getImage('catalog/'.$image)}}"
                                                             title="{{$product_title}}" alt="{{$product_title}}"
                                                             class="img-responsive reg-image"/>
                                                        <img class="img-responsive hover-image"
                                                             src="{{Utility::getImage('catalog/'.$image)}}"
                                                             title="{{$product_title}}" alt="{{$product_title}}"/>
                                                    </a>
                                                    <div class="button-gr lists">
                                                        <div class="quickview-button"><a class="quickview-button"
                                                                                         href="{{url('quickview/'.$product->cleanURL)}}">Quick
                                                                View</a></div>
                                                    </div>
                                                    <div class="rating">
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i
                                                                    class="fa fa-star off fa-stack-2x"></i></span>
                                                    </div>
                                                </div>
                                                <div class="product-details">
                                                    <div class="caption">
                                                        <h4 class="name"><a href="#"
                                                                            title="Aliquam Ac Neque">{{$product_title}}</a></h4>
                                                        <p class="price rupees">
                                                            {{$marketPrice}}
                                                        </p>
                                                        <div class="button-group">
                                                            <button type="button" class="addtocart"
                                                                    onclick="cart.add({{$product_id}});"><i
                                                                        class="fa fa-shopping-cart"></i>Add to Cart</button>
                                                            {{--<button class="wishlist" type="button" data-toggle="tooltip"--}}
                                                                    {{--title="Add to Wish List"--}}
                                                                    {{--onclick="wishlist.add({{$product_id}});"><i--}}
                                                                        {{--class="fa fa-heart"></i></button>--}}
                                                            {{--<button class="compare" type="button" data-toggle="tooltip"--}}
                                                                    {{--title="Add to Compare" onclick="compare.add('49');">--}}
                                                                {{--<i class="fa fa-exchange"></i></button>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>
            </div>
        </div>
    </div>
</div>