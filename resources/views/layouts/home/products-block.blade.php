<div class="img-prd">
    <div class="hometab box">
        <div class="col-md-12">
            <div class="tab-head fruits-section">
                <h1 class="hometab-heading box-heading">Premium Products</h1>
            </div>
            <div class="tab-content">
                <div id="latestItemGroceries" class="tab-pane fade in active mt-0">
                    <div class="box">
                        <div class="box-content">
                            <div class="customNavigation">
                                <a class="fa prev"></a>
                                <a class="fa next"></a>
                            </div>
                            <div class="owl-carousel custom_carousel box-product" id="carousel_premium">
                                @php
                                    $products=\App\Models\Products::with('productsUnit','productsCategory')->where('status','=','1')->where('premiumProducts','=','1')->orderBy('product_title','DESC')->get();
                                @endphp
                                @foreach($products as $prod)
                                @if($prod->productsUnit[0]['product_quantity'] > 0 && $prod->productsUnit[0]['unit'] != '')
                                <div class="slider-item">

                                    <div class="product-block product-thumb transition">

                                        <div class="product-block-inner">

                                            <div class="Product-Box clearfix">
                                                <div class="mt-15">

                                                    <h4 class="name">
                                                        {{$prod->product_title}}
                                                    </h4>
                                                    <p class="price">
                                                        @php
                                                         $on_sale=$prod->on_sales;
                                                        @endphp
                                                        @if(isset($on_sale) && $on_sale=="1")
                                                            <span class="price-new">Rs {{number_format($prod->productsUnit[0]['product_price'],2)}} /{{$prod->productsUnit[0]['unit']}}
                                                        </span>
                                                            <span class="price-old">Rs {{number_format($prod->productsUnit[0]['product_price_discount'],2)}} /{{$prod->productsUnit[0]['unit']}}
                                                        </span>
                                                        @else
                                                            <span class="price-new">Rs {{number_format($prod->productsUnit[0]['product_price'],2)}}/{{$prod->productsUnit[0]['unit']}}</span>
                                                        @endif
                                                        <input type="hidden"  value="{{$prod->productsUnit[0]['product_price']}}/{{$prod->productsUnit[0]['unit']}}" id="product-price_{{$prod->id}}" >

                                                        {{--<span class="price-tax">Ex Tax: $100.00</span>--}}
                                                    </p>
                                                </div>
                                                <div class="button-group wishlist-new-btn">
                                                    <a href="{{url('')}}/{{$prod->productsCategory->cleanURL.'/'.$prod->cleanURL}}.html">


                                                        <button class="" type="button" data-toggle="tooltip" title="Quick view" data-placement="right" >
                                                        <i class="fa fa-search"></i>
                                                        </button></a>
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" data-placement="right" onclick="wishlist.add({{$prod->id}});">
                                                        <i class="fa fa-heart"></i>
                                                    </button>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare" data-placement="right" onclick="compare.add({{$prod->id}});">
                                                        <i class="fa fa-share-square-o"></i>
                                                    </button>

                                                </div>
                                                <div class="image ">
                                                    <a href="{{url('')}}/{{$prod->productsCategory->cleanURL.'/'.$prod->cleanURL}}.html">
                                                     <img src="{{Utility::getImage('products/')}}/{{$prod->product_image}}" title="{{$prod->product_title}}" alt="{{$prod->product_title}}" class="img-responsive "
                                                        />
                                                        </a>
                                                    <div class="rating">
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="product-details">
                                                    <div class="caption">
                                                        <div class="button-group cart-btn-new">
                                                            @if(count($prod->productsUnit) > 1)
                                                                <a class="view-product-btn btn" href="{{url('')}}/{{$prod->productsCategory->cleanURL.'/'.$prod->cleanURL}}.html">
                                                                    View Product
                                                                </a>
                                                                @else
                                                                <button type="button" class="addtocart"  onclick="cart.add('{{$prod->id}}',1);">
                                                                    Add to Cart
                                                                </button>
                                                                @endif
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                </div>
            </div>
        </div>
    </div>
</div>

@php
    $count=1;
@endphp
@foreach($categories as $category)
    @if($category->category_title == 'Vegetables' || $category->category_title == 'Fruits')
        <div class="img-prd">
            <div class="hometab box">
                <div class="col-md-12">
                    <div class="tab-head fruits-section">
                        <h1 class="hometab-heading box-heading">{{$category->category_title}}</h1>

                    </div>
                    <div class="tab-content">
                        <div id="latestItemGroceries" class="tab-pane fade in active mt-0">
                            <div class="box">
                                <div class="box-content">
                                    <div class="customNavigation">
                                        <a class="fa prev"></a>
                                        <a class="fa next"></a>
                                    </div>
                                    <div class="owl-carousel custom_carousel box-product" id="carousel_category_{{$count}}">
                                        @php
                                            $products=\App\Models\Products::with('productsUnit')->where('status','=',1)
                                                         ->where('category_id','=',$category->id)->get();

                                             @endphp

                                        @foreach($products as $prod)
                                            @if(count($prod->productsUnit) && $prod->productsUnit[0]['product_quantity'] > 0 )

                                            @php $categoryTitle = $category->cleanURL;
                                            @endphp
                                            <div class="slider-item">

                                                <div class="product-block product-thumb  transition">

                                                    <div class="product-block-inner">

                                                        <div class="Product-Box clearfix">
                                                            <div class="mt-15">
                                                                <h4 class="name">
                                                                    {{$prod->product_title}}
                                                                </h4>
                                                                <p class="price">
                                                                    @php
                                                                        $on_sale=$prod->on_sales;
                                                                    @endphp

                                                                    @if(isset($on_sale) && $on_sale=="1")
                                                                        <span class="price-new">Rs {{number_format($prod->productsUnit[0]['product_price'],2)}} /{{$prod->productsUnit[0]['unit']}}
                                                        </span>
                                                                        <span class="price-old">Rs {{number_format($prod->productsUnit[0]['product_price_discount'],2)}} /{{$prod->productsUnit[0]['unit']}}
                                                        </span>
                                                                    @else
                                                                        <span class="price-new">Rs {{number_format($prod->productsUnit[0]['product_price'],2)}}/{{$prod->productsUnit[0]['unit']}}</span>
                                                                    @endif


                                                                    <input type="hidden" value="{{$prod->productsUnit[0]['product_price']}}/{{$prod->productsUnit[0]['unit']}}" id="product-price_{{$prod->id}}" >

                                                                    <span class="price-tax">Ex Tax: $100.00</span>
                                                                </p>
                                                            </div>
                                                            <div class="button-group wishlist-new-btn">
                                                                <a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html">
                                                                <button class="" type="button" data-toggle="tooltip" title="Quick view" data-placement="right" onclick="compare.add('48');">
                                                                    <i class="fa fa-search"></i>
                                                                </button>
                                                                </a>
                                                                <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" data-placement="right" onclick="wishlist.add({{$prod->id}});">
                                                                    <i class="fa fa-heart"></i>
                                                                </button>
                                                                <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare" data-placement="right" onclick="compare.add('48');">
                                                                    <i class="fa fa-share-square-o"></i>
                                                                </button>
                                                            </div>
                                                            <div class="image ">
                                                                <a href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html">
                                                                    <img src="{{Utility::getImage('products/')}}/{{$prod->product_image}}" title="{{$prod->product_title}}" alt="{{$prod->product_title}}" class="img-responsive "
                                                                    />
                                                                     </a>
                                                                <div class="rating">
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                    <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                    <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                    <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                    <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                </div>
                                                            </div>
                                                            <div class="product-details">
                                                                <div class="caption">
                                                                    <div class="button-group cart-btn-new">
                                                                        @if(count($prod->productsUnit) > 1)

                                                                            <a class="view-product-btn btn" href="{{url('')}}/{{$categoryTitle.'/'.$prod->cleanURL}}.html">
                                                                                View Product
                                                                            </a>
                                                                        @else   <button type="button" class="addtocart"  onclick="cart.add('{{$prod->id}}',1);">
                                                                            Add to Cart
                                                                        </button>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @php
            $count++
        @endphp
    @endif
@endforeach
    </div>
</div>
<div class="hometab box"></div>


<script>
    $(document).ready(function () {
        $('#carousel_premium').owlCarousel({
            items: 5,
            singleItem: false,
            navigation: false,
            navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
            pagination: false,
            itemsDesktop: [1200, 5],
            itemsDesktopSmall: [979, 3],
            itemsTablet: [767, 2],
            itemsMobile: [479, 1]
        });

        $("#carousel_premium").prev(".customNavigation").children("a.next").click(function () {
            $("#carousel_premium").trigger('owl.next');
        })
        $("#carousel_premium").prev(".customNavigation").children("a.prev").click(function () {
            $("#carousel_premium").trigger('owl.prev');
        })
    });
</script>
<script>

            $('#carousel_category_1').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $('#carousel_category_1').prev(".customNavigation").children("a.next").click(function(){
                $('#carousel_category_1').trigger('owl.next');
            })
            $('#carousel_category_1').prev(".customNavigation").children("a.prev").click(function(){
                $('#carousel_category_1').trigger('owl.prev');
            })

            $('#carousel_category_2').owlCarousel({
                items: 5,
                singleItem: false,
                navigation: false,
                navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                pagination: false,
                itemsDesktop : [1200,5],
                itemsDesktopSmall :	[979,3],
                itemsTablet :	[767,2],
                itemsMobile : [479,1]
            });

            $('#carousel_category_2').prev(".customNavigation").children("a.next").click(function(){
                $('#carousel_category_2').trigger('owl.next');
            })
            $('#carousel_category_2').prev(".customNavigation").children("a.prev").click(function(){
                $('#carousel_category_2').trigger('owl.prev');
            })


</script>


