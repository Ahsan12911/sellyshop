{{--Not in Use--}}
<script>
    $(document).ready(function () {
        $('.blogcarousel').owlCarousel({
            items: 5,
            singleItem: false,
            navigation: true,
            navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
            pagination: true,
            itemsDesktop: [1200, 5],
            itemsDesktopSmall: [979, 4],
            itemsTablet: [767, 2],
            itemsMobile: [479, 1]
        });

        $('.custom_carousel').owlCarousel({
            items: 6,
            singleItem: false,
            navigation: true,
            navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
            pagination: true,
            itemsDesktop: [1200, 6],
            itemsDesktopSmall: [979, 5],
            itemsTablet: [767, 2],
            itemsMobile: [479, 1]
        });

    });

</script>