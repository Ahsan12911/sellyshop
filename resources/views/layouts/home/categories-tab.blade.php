<div class="container">
    <h2 class="title justify-content-center ls-normal mb-4 mt-10 pt-1 appear-animate">Popular Departments
    </h2>
    <div class="tab tab-nav-boxed tab-nav-outline appear-animate">
        <ul class="nav nav-tabs justify-content-center" role="tablist">
            @foreach($categories as $item)
            <li class="nav-item mr-2 mb-2">
                <a class="nav-link active br-sm font-size-md ls-normal" href="#tab1-1">{{$item->category_title}}</a>
            </li>
            @endforeach
        </ul>
    </div>
    <!-- End of Tab -->
    <div class="tab-content product-wrapper appear-animate">
        <div class="tab-pane active pt-4" id="tab1-1">
            <div class="row cols-xl-5 cols-md-4 cols-sm-3 cols-2">
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="#">
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/3-1-1.jpg" alt="Product"
                                     width="300" height="338" />
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/3-1-2.jpg" alt="Product"
                                     width="300" height="338" />
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart"
                                   title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="#">Classic Hat</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 60%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="#" class="rating-reviews">(1 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$53.00</ins>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Tab Pane -->
        <div class="tab-pane pt-4" id="tab1-2">
            <div class="row cols-xl-5 cols-md-4 cols-sm-3 cols-2">
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="#">
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/3-4-1.jpg" alt="Product"
                                     width="300" height="338" />
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/3-4-2.jpg" alt="Product"
                                     width="300" height="338" />
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart"
                                   title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="#">Fashion Blue Towel</a>
                            </h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 100%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="#" class="rating-reviews">(8 reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$26.55 - $29.99</ins>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Tab Pane -->
        <div class="tab-pane pt-4" id="tab1-3">
            <div class="row cols-xl-5 cols-md-4 cols-sm-3 cols-2">
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="#">
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/3-9.jpg" alt="Product"
                                     width="300" height="338" />
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart"
                                   title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="#">Data Transformer Tool
                                </a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 60%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="#" class="rating-reviews">(3 reviews)</a>
                            </div>
                            <div class="product-price">
                                <span class="price">$64.47</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Tab Pane -->
        <div class="tab-pane pt-4" id="tab1-4">
            <div class="row cols-xl-5 cols-md-4 cols-sm-3 cols-2">
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="#">
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/3-4-1.jpg" alt="Product"
                                     width="300" height="338" />
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/3-4-2.jpg" alt="Product"
                                     width="300" height="338" />
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart"
                                   title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="#">Fashion Blue Towel</a>
                            </h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 100%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="#" class="rating-reviews">(8 reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$26.55 - $29.99</ins>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Tab Pane -->
    </div>
    <!-- End of Tab Content -->

    <div class="row category-cosmetic-lifestyle appear-animate mb-5">
        <div class="col-md-6 mb-4">
            <div class="banner banner-fixed category-banner-1 br-xs">
                <figure>
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/categories/3-1.jpg" alt="Category Banner"
                         width="610" height="200" style="background-color: #3B4B48;" />
                </figure>
                <div class="banner-content y-50 pt-1">
                    <h5 class="banner-subtitle font-weight-bold text-uppercase">Natural Process</h5>
                    <h3 class="banner-title font-weight-bolder text-capitalize text-white">Cosmetic
                        Makeup<br>Professional</h3>
                    <a href="shop-banner-sidebar.html"
                       class="btn btn-white btn-link btn-underline btn-icon-right">Shop Now<i
                                class="w-icon-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 mb-4">
            <div class="banner banner-fixed category-banner-2 br-xs">
                <figure>
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/categories/3-2.jpg" alt="Category Banner"
                         width="610" height="200" style="background-color: #E5E5E5;" />
                </figure>
                <div class="banner-content y-50 pt-1">
                    <h5 class="banner-subtitle font-weight-bold text-uppercase">Trending Now</h5>
                    <h3 class="banner-title font-weight-bolder text-capitalize">Women's
                        Lifestyle<br>Collection</h3>
                    <a href="shop-banner-sidebar.html"
                       class="btn btn-dark btn-link btn-underline btn-icon-right">Shop Now<i
                                class="w-icon-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Category Cosmetic Lifestyle -->

    <div class="product-wrapper-1 appear-animate mb-5">
        <div class="title-link-wrapper pb-1 mb-4">
            <h2 class="title ls-normal mb-0">Clothing & Apparel</h2>
            <a href="shop-boxed-banner.html" class="font-size-normal font-weight-bold ls-25 mb-0">More
                Products<i class="w-icon-long-arrow-right"></i></a>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-4 mb-4">
                <div class="banner h-100 br-sm" style="background-image: url({{ url('public')  }}/front/assets/images/demos/demo1/banners/2.jpg);
                        background-color: #ebeced;">
                    <div class="banner-content content-top">
                        <h5 class="banner-subtitle font-weight-normal mb-2">Weekend Sale</h5>
                        <hr class="banner-divider bg-dark mb-2">
                        <h3 class="banner-title font-weight-bolder ls-25 text-uppercase">
                            New Arrivals<br> <span
                                    class="font-weight-normal text-capitalize">Collection</span>
                        </h3>
                        <a href="shop-banner-sidebar.html"
                           class="btn btn-dark btn-outline btn-rounded btn-sm">shop Now</a>
                    </div>
                </div>
            </div>
            <!-- End of Banner -->
            <div class="col-lg-9 col-sm-8">
                <div class="swiper-container swiper-theme" data-swiper-options="{
                                'spaceBetween': 20,
                                'slidesPerView': 2,
                                'breakpoints': {
                                    '992': {
                                        'slidesPerView': 3
                                    },
                                    '1200': {
                                        'slidesPerView': 4
                                    }
                                }
                            }">
                    <div class="swiper-wrapper row cols-xl-4 cols-lg-3 cols-2">
                        <div class="swiper-slide product-col">
                            <div class="product-wrap product text-center">
                                <figure class="product-media">
                                    <a href="#">
                                        <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/4-1.jpg" alt="Product"
                                             width="216" height="243" />
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart w-icon-cart"
                                           title="Add to cart"></a>
                                        <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                           title="Add to wishlist"></a>
                                        <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                           title="Quickview"></a>
                                        <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                           title="Add to Compare"></a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <h4 class="product-name"><a href="#">Men’s
                                            Clothing</a>
                                    </h4>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 60%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="#" class="rating-reviews">(3
                                            reviews)</a>
                                    </div>
                                    <div class="product-price">
                                        <ins class="new-price">$23.99</ins><del
                                                class="old-price">$25.68</del>
                                    </div>
                                </div>
                            </div>
                            <div class="product-wrap product text-center">
                                <figure class="product-media">
                                    <a href="#">
                                        <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/4-5-1.jpg"
                                             alt="Product" width="216" height="243" />
                                        <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/4-5-2.jpg"
                                             alt="Product" width="216" height="243" />
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart w-icon-cart"
                                           title="Add to cart"></a>
                                        <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                           title="Add to wishlist"></a>
                                        <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                           title="Quickview"></a>
                                        <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                           title="Add to Compare"></a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <h4 class="product-name"><a href="#">Best Travel
                                            Bag</a>
                                    </h4>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 60%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="#" class="rating-reviews">(3
                                            reviews)</a>
                                    </div>
                                    <div class="product-price">
                                        <ins class="new-price">$25.68</ins><del
                                                class="old-price">$28.99</del>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Product Wrapper 1 -->
    <div class="banner banner-fashion appear-animate br-sm mb-9" style="background-image: url({{ url('public')  }}/front/assets/images/demos/demo1/banners/4.jpg);
            background-color: #383839;">
        <div class="banner-content align-items-center">
            <div class="content-left d-flex align-items-center mb-3">
                <div class="banner-price-info font-weight-bolder text-secondary text-uppercase lh-1 ls-25">
                    25
                    <sup class="font-weight-bold">%</sup><sub class="font-weight-bold ls-25">Off</sub>
                </div>
                <hr class="banner-divider bg-white mt-0 mb-0 mr-8">
            </div>
            <div class="content-right d-flex align-items-center flex-1 flex-wrap">
                <div class="banner-info mb-0 mr-auto pr-4 mb-3">
                    <h3 class="banner-title text-white font-weight-bolder text-uppercase ls-25">For Today's
                        Fashion</h3>
                    <p class="text-white mb-0">Use code
                        <span
                                class="text-dark bg-white font-weight-bold ls-50 pl-1 pr-1 d-inline-block">Black
                                        <strong>12345</strong></span> to get best offer.</p>
                </div>
                <a href="shop-banner-sidebar.html"
                   class="btn btn-white btn-outline btn-rounded btn-icon-right mb-3">Shop Now<i
                            class="w-icon-long-arrow-right"></i></a>
            </div>
        </div>
    </div>
    <!-- End of Banner Fashion -->

    <div class="product-wrapper-1 appear-animate mb-7">
        <div class="title-link-wrapper pb-1 mb-4">
            <h2 class="title ls-normal mb-0">Home Garden & Kitchen</h2>
            <a href="shop-boxed-banner.html" class="font-size-normal font-weight-bold ls-25 mb-0">More
                Products<i class="w-icon-long-arrow-right"></i></a>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-4 mb-4">
                <div class="banner h-100 br-sm" style="background-image: url({{ url('public')  }}/front/assets/images/demos/demo1/banners/5.jpg);
                        background-color: #EAEFF3;">
                    <div class="banner-content content-top">
                        <h5 class="banner-subtitle font-weight-normal mb-2">Deals And Promotions</h5>
                        <hr class="banner-divider bg-dark mb-2">
                        <h3 class="banner-title font-weight-bolder text-uppercase ls-25">
                            Trending <br> <span class="font-weight-normal text-capitalize">House
                                            Utensil</span>
                        </h3>
                        <a href="shop-banner-sidebar.html"
                           class="btn btn-dark btn-outline btn-rounded btn-sm">shop now</a>
                    </div>
                </div>
            </div>
            <!-- End of Banner -->
            <div class="col-lg-9 col-sm-8">
                <div class="swiper-container swiper-theme" data-swiper-options="{
                                'spaceBetween': 20,
                                'slidesPerView': 2,
                                'breakpoints': {
                                    '992': {
                                        'slidesPerView': 3
                                    },
                                    '1200': {
                                        'slidesPerView': 4
                                    }
                                }
                            }">
                    <div class="swiper-wrapper row cols-xl-4 cols-lg-3 cols-2">
                        <div class="swiper-slide product-col">
                            <div class="product-wrap product text-center">
                                <figure class="product-media">
                                    <a href="#">
                                        <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/6-1.jpg" alt="Product"
                                             width="216" height="243" />
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart w-icon-cart"
                                           title="Add to cart"></a>
                                        <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                           title="Add to wishlist"></a>
                                        <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                           title="Quickview"></a>
                                        <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                           title="Add to Compare"></a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <h4 class="product-name"><a href="#">Home Sofa</a>
                                    </h4>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="#" class="rating-reviews">(5
                                            reviews)</a>
                                    </div>
                                    <div class="product-price">
                                        <ins class="new-price">$75.99</ins>
                                    </div>
                                </div>
                            </div>
                            <div class="product-wrap product text-center">
                                <figure class="product-media">
                                    <a href="#">
                                        <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/6-5.jpg" alt="Product"
                                             width="216" height="243" />
                                    </a>
                                    <div class="product-action-vertical">
                                        <a href="#" class="btn-product-icon btn-cart w-icon-cart"
                                           title="Add to cart"></a>
                                        <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                           title="Add to wishlist"></a>
                                        <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                           title="Quickview"></a>
                                        <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                           title="Add to Compare"></a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <h4 class="product-name"><a href="#">Electric
                                            Rice-Cooker</a></h4>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 60%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="#" class="rating-reviews">(3
                                            reviews)</a>
                                    </div>
                                    <div class="product-price">
                                        <ins class="new-price">$215.00</ins>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
                <!-- End of Produts -->
            </div>
        </div>
    </div>
    <!-- End of Product Wrapper 1 -->

    <h2 class="title title-underline mb-4 ls-normal appear-animate">Our Clients</h2>
    <div class="swiper-container swiper-theme brands-wrapper mb-9 appear-animate" data-swiper-options="{
                    'spaceBetween': 0,
                    'slidesPerView': 2,
                    'breakpoints': {
                        '576': {
                            'slidesPerView': 3
                        },
                        '768': {
                            'slidesPerView': 4
                        },
                        '992': {
                            'slidesPerView': 5
                        },
                        '1200': {
                            'slidesPerView': 6
                        }
                    }
                }">
        <div class="swiper-wrapper row gutter-no cols-xl-6 cols-lg-5 cols-md-4 cols-sm-3 cols-2">
            <div class="swiper-slide brand-col">
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/1.png" alt="Brand" width="410"
                         height="186" />
                </figure>
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/2.png" alt="Brand" width="410"
                         height="186" />
                </figure>
            </div>
            <div class="swiper-slide brand-col">
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/3.png" alt="Brand" width="410"
                         height="186" />
                </figure>
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/4.png" alt="Brand" width="410"
                         height="186" />
                </figure>
            </div>
            <div class="swiper-slide brand-col">
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/5.png" alt="Brand" width="410"
                         height="186" />
                </figure>
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/6.png" alt="Brand" width="410"
                         height="186" />
                </figure>
            </div>
            <div class="swiper-slide brand-col">
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/7.png" alt="Brand" width="410"
                         height="186" />
                </figure>
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/8.png" alt="Brand" width="410"
                         height="186" />
                </figure>
            </div>
            <div class="swiper-slide brand-col">
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/9.png" alt="Brand" width="410"
                         height="186" />
                </figure>
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/10.png" alt="Brand" width="410"
                         height="186" />
                </figure>
            </div>
            <div class="swiper-slide brand-col">
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/11.png" alt="Brand" width="410"
                         height="186" />
                </figure>
                <figure class="brand-wrapper">
                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/brands/12.png" alt="Brand" width="410"
                         height="186" />
                </figure>
            </div>
        </div>
    </div>
    <!-- End of Brands Wrapper -->

    <div class="post-wrapper appear-animate mb-4">
        <div class="title-link-wrapper pb-1 mb-4">
            <h2 class="title ls-normal mb-0">From Our Blog</h2>
            <a href="blog-listing.html" class="font-weight-bold font-size-normal">View All Articles</a>
        </div>
        <div class="swiper">
            <div class="swiper-container swiper-theme" data-swiper-options="{
                            'slidesPerView': 1,
                            'spaceBetween': 20,
                            'breakpoints': {
                                '576': {
                                    'slidesPerView': 2
                                },
                                '768': {
                                    'slidesPerView': 3
                                },
                                '992': {
                                    'slidesPerView': 4
                                }
                            }
                        }">
                <div class="swiper-wrapper row cols-lg-4 cols-md-3 cols-sm-2 cols-1">
                    <div class="swiper-slide post text-center overlay-zoom">
                        <figure class="post-media br-sm">
                            <a href="post-single.html">
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/blogs/1.jpg" alt="Post" width="280"
                                     height="180" style="background-color: #4b6e91;" />
                            </a>
                        </figure>
                        <div class="post-details">
                            <div class="post-meta">
                                by <a href="#" class="post-author">John Doe</a>
                                - <a href="#" class="post-date mr-0">03.05.2021</a>
                            </div>
                            <h4 class="post-title"><a href="post-single.html">Aliquam tincidunt mauris
                                    eurisus</a>
                            </h4>
                            <a href="post-single.html" class="btn btn-link btn-dark btn-underline">Read
                                More<i class="w-icon-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="swiper-slide post text-center overlay-zoom">
                        <figure class="post-media br-sm">
                            <a href="post-single.html">
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/blogs/2.jpg" alt="Post" width="280"
                                     height="180" style="background-color: #cec9cf;" />
                            </a>
                        </figure>
                        <div class="post-details">
                            <div class="post-meta">
                                by <a href="#" class="post-author">John Doe</a>
                                - <a href="#" class="post-date mr-0">03.05.2021</a>
                            </div>
                            <h4 class="post-title"><a href="post-single.html">Cras ornare tristique elit</a>
                            </h4>
                            <a href="post-single.html" class="btn btn-link btn-dark btn-underline">Read
                                More<i class="w-icon-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="swiper-slide post text-center overlay-zoom">
                        <figure class="post-media br-sm">
                            <a href="post-single.html">
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/blogs/3.jpg" alt="Post" width="280"
                                     height="180" style="background-color: #c9c7bb;" />
                            </a>
                        </figure>
                        <div class="post-details">
                            <div class="post-meta">
                                by <a href="#" class="post-author">John Doe</a>
                                - <a href="#" class="post-date mr-0">03.05.2021</a>
                            </div>
                            <h4 class="post-title"><a href="post-single.html">Vivamus vestibulum ntulla nec
                                    ante</a>
                            </h4>
                            <a href="post-single.html" class="btn btn-link btn-dark btn-underline">Read
                                More<i class="w-icon-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="swiper-slide post text-center overlay-zoom">
                        <figure class="post-media br-sm">
                            <a href="post-single.html">
                                <img src="{{ url('public')  }}/front/assets/images/demos/demo1/blogs/4.jpg" alt="Post" width="280"
                                     height="180" style="background-color: #d8dce0;" />
                            </a>
                        </figure>
                        <div class="post-details">
                            <div class="post-meta">
                                by <a href="#" class="post-author">John Doe</a>
                                - <a href="#" class="post-date mr-0">03.05.2021</a>
                            </div>
                            <h4 class="post-title"><a href="post-single.html">Fusce lacinia arcuet nulla</a>
                            </h4>
                            <a href="post-single.html" class="btn btn-link btn-dark btn-underline">Read
                                More<i class="w-icon-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
    <!-- Post Wrapper -->

    <h2 class="title title-underline mb-4 ls-normal appear-animate">Your Recent Views</h2>
    <div class="swiper-container swiper-theme shadow-swiper appear-animate pb-4 mb-8" data-swiper-options="{
                    'spaceBetween': 20,
                    'slidesPerView': 2,
                    'breakpoints': {
                        '576': {
                            'slidesPerView': 3
                        },
                        '768': {
                            'slidesPerView': 5
                        },
                        '992': {
                            'slidesPerView': 6
                        },
                        '1200': {
                            'slidesPerView': 8
                        }
                    }
                }">
        <div class="swiper-wrapper row cols-xl-8 cols-lg-6 cols-md-4 cols-2">
            <div class="swiper-slide product-wrap mb-0">
                <div class="product text-center product-absolute">
                    <figure class="product-media">
                        <a href="https://www.portotheme.com/html/wolmart/product-defa#">
                            <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/7-1.jpg" alt="Category image"
                                 width="130" height="146" style="background-color: #fff" />
                        </a>
                    </figure>
                    <h4 class="product-name">
                        <a href="#">Women's Fashion Handbag</a>
                    </h4>
                </div>
            </div>
            <!-- End of Product Wrap -->
            <div class="swiper-slide product-wrap mb-0">
                <div class="product text-center product-absolute">
                    <figure class="product-media">
                        <a href="https://www.portotheme.com/html/wolmart/product-defa#">
                            <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/7-2.jpg" alt="Category image"
                                 width="130" height="146" style="background-color: #fff" />
                        </a>
                    </figure>
                    <h4 class="product-name">
                        <a href="#">Electric Frying Pan</a>
                    </h4>
                </div>
            </div>
            <!-- End of Product Wrap -->
            <div class="swiper-slide product-wrap mb-0">
                <div class="product text-center product-absolute">
                    <figure class="product-media">
                        <a href="https://www.portotheme.com/html/wolmart/product-defa#">
                            <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/7-3.jpg" alt="Category image"
                                 width="130" height="146" style="background-color: #fff" />
                        </a>
                    </figure>
                    <h4 class="product-name">
                        <a href="#">Black Winter Skating</a>
                    </h4>
                </div>
            </div>
            <!-- End of Product Wrap -->
            <div class="swiper-slide product-wrap mb-0">
                <div class="product text-center product-absolute">
                    <figure class="product-media">
                        <a href="https://www.portotheme.com/html/wolmart/product-defa#">
                            <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/7-4.jpg" alt="Category image"
                                 width="130" height="146" style="background-color: #fff" />
                        </a>
                    </figure>
                    <h4 class="product-name">
                        <a href="#">HD Television</a>
                    </h4>
                </div>
            </div>
            <!-- End of Product Wrap -->
            <div class="swiper-slide product-wrap mb-0">
                <div class="product text-center product-absolute">
                    <figure class="product-media">
                        <a href="https://www.portotheme.com/html/wolmart/product-defa#">
                            <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/7-5.jpg" alt="Category image"
                                 width="130" height="146" style="background-color: #fff" />
                        </a>
                    </figure>
                    <h4 class="product-name">
                        <a href="#">Home Sofa</a>
                    </h4>
                </div>
            </div>
            <!-- End of Product Wrap -->
            <div class="swiper-slide product-wrap mb-0">
                <div class="product text-center product-absolute">
                    <figure class="product-media">
                        <a href="https://www.portotheme.com/html/wolmart/product-defa#">
                            <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/7-6.jpg" alt="Category image"
                                 width="130" height="146" style="background-color: #fff" />
                        </a>
                    </figure>
                    <h4 class="product-name">
                        <a href="#">USB Receipt</a>
                    </h4>
                </div>
            </div>
            <!-- End of Product Wrap -->
            <div class="swiper-slide product-wrap mb-0">
                <div class="product text-center product-absolute">
                    <figure class="product-media">
                        <a href="https://www.portotheme.com/html/wolmart/product-defa#">
                            <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/7-7.jpg" alt="Category image"
                                 width="130" height="146" style="background-color: #fff" />
                        </a>
                    </figure>
                    <h4 class="product-name">
                        <a href="#">Electric Rice-Cooker</a>
                    </h4>
                </div>
            </div>
            <!-- End of Product Wrap -->
            <div class="swiper-slide product-wrap mb-0">
                <div class="product text-center product-absolute">
                    <figure class="product-media">
                        <a href="https://www.portotheme.com/html/wolmart/product-defa#">
                            <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/7-8.jpg" alt="Category image"
                                 width="130" height="146" style="background-color: #fff" />
                        </a>
                    </figure>
                    <h4 class="product-name">
                        <a href="#">Table Lamp</a>
                    </h4>
                </div>
            </div>
            <!-- End of Product Wrap -->
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <!-- End of Reviewed Producs -->
</div>
<div class="home-appliances clearfix">
    <div class="row category-block">
        <div class="text-center">



            @foreach($categories as $item)
                @php
                    $title = $item->category_title;
                    $image = $item->category_icon;

                @endphp


                <div class="col-md-3 col-sm-4 col-xs-12 gallery_img_grids mb-20">
                    <a href="{{url('/'.strtolower($item->cleanURL))}}">
                        <div class="box-border">
                            <div class="image-grid">
                                <img src="{{Utility::getImage('category/'.$image)}}" title="{{$title}}" alt="{{$title}}" class="img-responsive reg-image">
                            <!-- <div class="image-grid-effect">
                                        <div class="wthree_text">
                                            <h3>{{$title}}</h3>
                                        </div>
                                    </div> -->
                            </div>
                            <p>{{$title}}</p>
                        </div>
                    </a>
                </div>

            @endforeach



        </div>
    </div>
</div>