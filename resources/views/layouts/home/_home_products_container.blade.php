<div class="product-wrapper-1 appear-animate mb-8">
    <div class="title-link-wrapper pb-1 mb-4">
        <h2 class="title ls-normal mb-0">Consumer Electric</h2>
        <a href="shop-boxed-banner.html" class="font-size-normal font-weight-bold ls-25 mb-0">More
            Products<i class="w-icon-long-arrow-right"></i></a>
    </div>
    <div class="row">
        <div class="col-lg-3 col-sm-4 mb-4">
            <div class="banner h-100 br-sm" style="background-image: url({{ url('public')  }}/front/assets/images/demos/demo1/banners/3.jpg);
                    background-color: #252525;">
                <div class="banner-content content-top">
                    <h5 class="banner-subtitle text-white font-weight-normal mb-2">New Collection</h5>
                    <hr class="banner-divider bg-white mb-2">
                    <h3 class="banner-title text-white font-weight-bolder text-uppercase ls-25">
                        Top Camera <br> <span
                                class="font-weight-normal text-capitalize">Mirrorless</span>
                    </h3>
                    <a href="shop-banner-sidebar.html"
                       class="btn btn-white btn-outline btn-rounded btn-sm">shop now</a>
                </div>
            </div>
        </div>
        <!-- End of Banner -->
        <div class="col-lg-9 col-sm-8">
            <div class="swiper-container swiper-theme" data-swiper-options="{
                                'spaceBetween': 20,
                                'slidesPerView': 2,
                                'breakpoints': {
                                    '992': {
                                        'slidesPerView': 3
                                    },
                                    '1200': {
                                        'slidesPerView': 4
                                    }
                                }
                            }">
                <div class="swiper-wrapper row cols-xl-4 cols-lg-3 cols-2">
                    <div class="swiper-slide product-col">
                        <div class="product-wrap product text-center">
                            <figure class="product-media">
                                <a href="#">
                                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/5-1-1.jpg"
                                         alt="Product" width="216" height="243" />
                                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/5-1-2.jpg"
                                         alt="Product" width="216" height="243" />
                                </a>
                                <div class="product-action-vertical">
                                    <a href="#" class="btn-product-icon btn-cart w-icon-cart"
                                       title="Add to cart"></a>
                                    <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                       title="Add to wishlist"></a>
                                    <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                       title="Quickview"></a>
                                    <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                       title="Add to Compare"></a>
                                </div>
                                <div class="product-label-group">
                                    <label class="product-label label-discount">6% Off</label>
                                </div>
                            </figure>
                            <div class="product-details">
                                <h4 class="product-name"><a href="#">Professional
                                        Pixel
                                        Camera</a></h4>
                                <div class="ratings-container">
                                    <div class="ratings-full">
                                        <span class="ratings" style="width: 100%;"></span>
                                        <span class="tooltiptext tooltip-top"></span>
                                    </div>
                                    <a href="#" class="rating-reviews">(5
                                        reviews)</a>
                                </div>
                                <div class="product-price">
                                    <ins class="new-price">$215.68</ins><del
                                            class="old-price">$230.45</del>
                                </div>
                            </div>
                        </div>
                        <div class="product-wrap product text-center">
                            <figure class="product-media">
                                <a href="#">
                                    <img src="{{ url('public')  }}/front/assets/images/demos/demo1/products/5-5.jpg" alt="Product"
                                         width="216" height="243" />
                                </a>
                                <div class="product-action-vertical">
                                    <a href="#" class="btn-product-icon btn-cart w-icon-cart"
                                       title="Add to cart"></a>
                                    <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                       title="Add to wishlist"></a>
                                    <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                       title="Quickview"></a>
                                    <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                       title="Add to Compare"></a>
                                </div>
                            </figure>
                            <div class="product-details">
                                <h4 class="product-name"><a href="#">Latest
                                        Speaker</a>
                                </h4>
                                <div class="ratings-container">
                                    <div class="ratings-full">
                                        <span class="ratings" style="width: 60%;"></span>
                                        <span class="tooltiptext tooltip-top"></span>
                                    </div>
                                    <a href="#" class="rating-reviews">(3
                                        reviews)</a>
                                </div>
                                <div class="product-price">
                                    <ins class="new-price">$250.68</ins>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
            <!-- End of Produts -->
        </div>
    </div>
</div>
<!-- End of Product Wrapper 1 -->
@php($count=0)
<div class="img-prd">
    <div class="hometab box">
        <div class="col-md-3 pr-0">
            <div class="banner-img">
                <div class="">
                    <img src="{{Utility::getImage('catalog/banner.png')}}" title="Aliquam Ac Neque"
                         alt="Aliquam Ac Neque"
                         class="img-responsive reg-image">
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-head">
                <div class="hometab-heading box-heading">{{strtoupper($title)}}</div>
                <div id="tab-{{$title}}" class="htabs tabs-catagory">
                    <ul class='etabs'>
                        @foreach(Config::get('constants.sub_categories') as $key=>$value)
                            @php($count++)
                            <li class='tab {{$count==1?'active':''}}'>
                                <a href="#tab-{{$value.$title}}" data-toggle="tab">{{$key}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @php($count=0)
            <div class="tab-content">
                @foreach(Config::get('constants.sub_categories') as $key=>$value)
                    @php($count++)
                    <div id="tab-{{$value.$title}}">
                        <div class="box">
                            <div class="box-content">
                                <div class="customNavigation">
                                    <a class="fa prev"></a>
                                    <a class="fa next"></a>
                                </div>
                                <div class="owl-carousel custom_carousel box-product" id="products_{{$value.$title}}">
                                </div>
                            </div>
                        </div>
                        <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('#tab-{{$title}} a').tabs();

    {{--$( document ).ready(function() {--}}
        {{--url = '{{url(\Illuminate\Support\Facades\Config::get('constants.urls.get-products'))}}';--}}
        {{--$('products_{{$value.$title}}').html(getProducts(url));--}}
    {{--});--}}

</script>