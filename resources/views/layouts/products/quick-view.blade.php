@extends('templates.layout')
@php
        $product_id = $item->id;
        $product_title = $item->product_title;
        $product_description = $item->product_description;
        $available_for_sales = $item->available_for_sales;
        $quantity_in_stock = $item->quantity_in_stock;
        $marketPrice = $item->marketPrice;
        $product_description = $item->product_description;
@endphp
    <div class="quickview">
        <div class="quickview-container">
            <div id="content" class="productpage-quickview">
                <div class="">
                    <h2 class="page-title">Capsicum</h2>
                    <div class="col-sm-6 product-left">
                        <div class="product-info">
                            <div class="left product-image thumbnails">
                                <!-- Megnor Cloud-Zoom Image Effect Start -->
                                <div class="image"><a class="thumbnail" title="Aliquam Ac Neque"><img id="tmzoom"
                                      src="{{Utility::getImage('catalog/16-813x1000.jpg')}}"
                                      data-zoom-image="image/cache/catalog/16-813x1000.jpg"
                                      title="Aliquam Ac Neque"
                                      alt="Aliquam Ac Neque"/></a>
                                </div>
                                <!-- Megnor Cloud-Zoom Image Effect End-->
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 product-right">
                        <h3 class="product-title">Capsicum</h3>
                        <div class="rating-wrapper">
                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-1x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-1x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-1x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-1x"></i></span>
                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-1x"></i></span>
                        </div>
                        <ul class="list-unstyled attr">
                            @if($available_for_sales==1 && $quantity_in_stock>0)
                                <li class="green">In stock ({{$quantity_in_stock}} items available)</li>
                            @elseif($available_for_sales==0)
                                <li class="red">Not Available For Sale</li>
                            @elseif($quantity_in_stock<=0)
                                <li class="red">Out of Stock</li>
                            @endif
                        </ul>
                        <ul class="list-unstyled price">
                            <li>
                                <h3 class="product-price">{{$marketPrice}} Rs / KG</h3>
                            </li>
                        </ul>


                        <div id="product2">
                            <div class="form-group qty">
                                <label class="control-label" for="input-quantity">Qty</label>
                                <input type="text" name="quantity" value="1" size="2" id="input-quantity"
                                       class="form-control"/>
                                <input type="hidden" name="product_id" value="49"/>
                                <!--<br />-->
                                <button type="button" onclick="cart.add('{{$product_id}}',1)" id="button-cart2" data-loading-text="Loading..."
                                        class="btn btn-primary btn-lg btn-block">Add to Cart
                                </button>
                                <div class="button-group prd_page">
                                    <button type="button" data-toggle="tooltip" class="btn btn-default wishlist"
                                            title="Add to Wish List" onclick="wishlist.add('49');">Add to Wish List
                                    </button>
                                    <button type="button" data-toggle="tooltip" class="btn btn-default compare"
                                            title="Add to Compare" onclick="compare.add('49');">Add to Compare
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style" data-url=""><a
                                    class="addthis_button_facebook_like"></a> <a class="addthis_button_tweet"></a> <a
                                    class="addthis_button_pinterest_pinit"></a> <a
                                    class="addthis_counter addthis_pill_style"></a></div>
                        <!-- AddThis Button END -->
                    </div>
                </div>
            </div>
        </div>
    </div>
<style>
    nav, header {
        display: none;
    }

    .content_product_block {
        display: none;
    }

    .content_header_top, .page-title {
        display: none;
    }

    #footer {
        display: none;
    }
    .bottomfooter {
        display: none;
    }

    .content-top-breadcum {
        display: none;
    }

    .header-container {
        display: none;
    }

    #powered {
        display: none;
    }

    #container {
        width: 850px;
    }

    .top_button {
        display: none !important;
    }
</style>