@php
    use App\Models\Admin\Units;
    use App\Models\Categories;
    use App\Models\Products;

@endphp
    <div class="row cat_prod">
        @foreach ($products->chunk(4) as $chnuk)
        @foreach ($chnuk as $product)
            @php
                $product_id = $product->id;
                $product_title = $product->product_title;
                $on_sale = $product->on_sales;
                $salePriceValue = $product->salePriceValue;
                $marketPrice = $product->marketPrice;
                $product_description = $product->product_description;
                $image = $product->product_image;
                $category = Categories::where('id',$product->category_id)->select('cleanURL')->first();
                $cleanURL = $product->cleanURL.'.html';

            $units=\App\Models\Admin\Units::where('product_id','=',$product->id)->where('product_quantity','>',0)->first();
            $unitCount=\App\Models\Admin\Units::where('product_id','=',$product->id)->count();

            @endphp
            @if($units['product_quantity'] > 0)
            <div class="product-layout product-grid  col-lg-3 col-md-4 col-sm-4 col-xs-6 ">
                <div class="product-block product-thumb ">
                    <div class="product-block-inner">
                            <div class="Product-Box clearfix">
                                {{--<div class="ribbon ribbon-green">--}}
                                    {{--<span class="save-text">Save</span>--}}
                                    {{--<span class="disc-per">40%</span>--}}
                                {{--</div>--}}
                                {{--<div class="corner-ribbon top-right sticky green">New</div>--}}
                                {{--<div class="corner-ribbon-special top-right sticky gray">Last Item</div>--}}
                                <div class="mt-15 mb-15">
                                    <h4 class="name">{{$product->product_title}}</h4>
                                    <p class="price">
                                        @php $on_sale=$product->on_sales;
                                        @endphp @if(isset($on_sale) && $on_sale=="1")
                                            <span class="price-new">Rs {{number_format($units->product_price,2)}} /{{$units->unit}}</span>
                                            <span class="price-old">Rs {{number_format($units->product_price_discount,2)}}/{{$units->unit}}</span>
                                        @else
                                            <span class="price-new">Rs {{number_format($units->product_price,2)}}/{{$units->unit}}</span>
                                        @endif
                                        <input type="hidden" value="{{$units->product_price}}/{{$units->unit}}" id="product-price_{{$product->id}}">
                                        <span class="price-tax">Ex Tax: $100.00</span>
                                    </p>
                                </div>
                                <div class="button-group wishlist-new-btn">
                                    <button data-toggle="tooltip" title="Quick view" data-placement="right" >
                                        <i class="fa fa-search"></i>
                                    </button>
                                    <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List" data-placement="right" onclick="wishlist.add({{$product->id}});">
                                        <i class="fa fa-heart"></i>
                                    </button>
                                    <button class="compare" type="button" data-toggle="tooltip" title="Add to Compare" data-placement="right" onclick="compare.add({{$product->id}});">
                                        <i class="fa fa-share-square-o"></i>
                                    </button>
                                </div>
                                <div class="image ">
                                    <a href="{{url('')}}/{{$category->cleanURL.'/'.$product->cleanURL}}.html">
                                        <img src="{{Utility::getImage('products/')}}/{{$product->product_image}}" title="{{$product->product_title}}" alt="{{$product->product_title}}" class="img-responsive "/>
                                        </a>
                                    <div class="rating">
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                    </div>
                                </div>
                                <div class="product-details">
                                    <div class="caption">
                                        <div class="button-group cart-btn-new">
                                            @if($unitCount > 1)
                                                <a class="view-product-btn btn" href="{{url('')}}/{{$category->cleanURL.'/'.$product->cleanURL}}.html">
                                                    View Product
                                                </a>
                                            @else
                                                <button type="button" class="addtocart"  onclick="cart.add('{{$product->id}}',1);">
                                                    Add to Cart
                                                </button>
                                            @endif
                                        </div>
                                         <div class="additional-caption-box">
                                            {{--<div class="additional-caption">Available on Demand</div>--}}
                                            {{--<div class="additional-caption-one">Available on Demand</div>--}}
                                        </div>
                                    </div>

                                </div>
                            </div>


                    </div>

                </div>

            </div>
@endif
        @endforeach
       @endforeach
    </div>

<div class="pagination-wrapper">
    <div class="col-sm-6 text-left page-link">{{$products->links()}}</div>
    <div class="col-sm-6 text-right page-result">Showing 1 of {{$products->lastPage()}} ({{$products->lastPage()}} Pages)</div>
</div>

@if($category_title != 'On Demand')
    @if($outOfStockProduct->count() > 0)
        <div class="img-prd">
            <div class="hometab box">

                <div class="col-md-12">
                    <div class="tab-head fruits-section">

                        <div class="hometab-heading box-heading out-of-stock-heading">{{$category_title or ""}} (Out of Stock Today)</div>


                    </div>
                    <div class="tab-content">
                        <div id="latestItemGroceries" class="tab-pane fade in active mt-0">
                            <div class="box">
                                <div class="box-content">
                                    <div class="customNavigation">
                                        <a class="fa prev"></a>
                                        <a class="fa next"></a>
                                    </div>
                                    <div class="owl-carousel custom_carousel box-product" id="carousel_6" >

                                        @foreach($outOfStockProduct as $prod)
                                            @php
                                                $outStockUnit=\App\Models\Admin\Units::where('product_id','=',$prod->id)->first();
                                            @endphp
                                        @if($outStockUnit->product_quantity < 1)
                                                <div class="slider-item">

                                                    <div class="product-block product-thumb  transition">

                                                        <div class="product-block-inner">

                                                            <div class="Product-Box clearfix">
                                                                <div class="mt-15">

                                                                    <h4 class="name">
                                                                        {{$prod->product_title}}
                                                                    </h4>
                                                                    <p class="price">

                                                                        @php
                                                                            $on_sale=$prod->on_sales;


                                                                        @endphp

                                                                        @if(isset($on_sale) && $on_sale=="1")
                                                                            <span class="price-new">Rs {{number_format($prod->productsUnit[0]['product_price'],2)}} /{{$prod->productsUnit[0]['unit']}}
                                                        </span>
                                                                            <span class="price-old">Rs {{number_format($prod->productsUnit[0]['product_price_discount'],2)}} /{{$prod->productsUnit[0]['unit']}}
                                                        </span>
                                                                        @else
                                                                            <span class="price-new">Rs {{number_format($prod->productsUnit[0]['product_price'],2)}}/{{$prod->productsUnit[0]['unit']}}</span>
                                                                        @endif


                                                                        <input type="hidden" value="{{$prod->productsUnit[0]['product_price']}}/{{$prod->productsUnit[0]['unit']}}" id="product-price_{{$prod->id}}" >

                                                                        <span class="price-tax">Ex Tax: $100.00</span>
                                                                    </p>
                                                                </div>
                                                                <div class="image ">

                                                                        <img src="{{Utility::getImage('products/')}}/{{$prod->product_image}}" title="{{$prod->product_title}}" alt="{{$prod->product_title}}" class="img-responsive "/>
                                                        <div class="rating">
                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                        <span class="fa fa-stack">
                                                            <i class="fa fa-star off fa-stack-2x"></i>
                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="product-details">
                                                                    <div class="caption">
                                                                        <div class="button-group cart-btn-new">

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif
@endif
<script>
    $(document).ready(function(){
        $('#carousel_6').owlCarousel({
            items: 4,
            singleItem: false,
            navigation: false,
            navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
            pagination: false,
            itemsDesktop : [1200,5],
            itemsDesktopSmall :	[979,3],
            itemsTablet :	[767,2],
            itemsMobile : [479,1]
        });

        $("#carousel_6").prev(".customNavigation").children("a.next").click(function(){
            $("#carousel_6").trigger('owl.next');
        })
        $("#carousel_6").prev(".customNavigation").children("a.prev").click(function(){
            $("#carousel_6").trigger('owl.prev');
        })
    })
</script>
<script>
    $(document).ready(function () {
        $('.box-category').click();
        $('.box-category').click(function () {
            var marginTop = $('.item').css('marginTop');
            if (marginTop == '0px') {
                marginTop = '150px';
            }
            else if (marginTop == '150px') {
                marginTop = '-0px';
            }
            $('.item').animate({marginTop: marginTop}, 1000);
        });    });

</script>