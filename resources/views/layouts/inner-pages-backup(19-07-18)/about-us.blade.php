@extends('templates.layout')
@section('content')

    <div class="">
        <div class="row mt-15">
            <div class="helpful_links_leftside">
                <div class="col-md-3 mt-15 pl-0">
                    <div class="clearfix">
                        <ul>
                            <li>
                                <a href="{{url('about-us')}}">About Us</a>
                            </li>
                            <li>
                                <a href="{{url('privacy-policy')}}">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="{{url('return-policy')}}">Return Policy</a>
                            </li>
                            <li>
                                <a href="{{url('terms-conditions')}}">Terms & Conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9-custom-width80">
                <div class="col-md-9">
                    <div class="content-top-breadcum">
                        <div class="container" style="display:block;">
                            <div class="row">
                                <div id="title-content">
                                    <h1 class="page-title">{{$aboutus->name}}
                                    </h1>
                                    <ul class="breadcrumb">
                                        <li><a href=""><i class="fa fa-home"></i></a></li>
                                        <li><a href="">About Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about_us_page clearfix">
                        {!! $aboutus->body !!}
                        {{--<p><a href="https://selly.pk">Selly.pk</a> is a fruits and vegetables shop online with one single motive, to deliver freshest produce to our customers. The purpose of our existence is to further the idea of health and wellness by offering premium quality products to our customers.</p>--}}
                        {{--<p>When it comes to buying fruits and vegetables, you are not sure of the quality. There are many issues that make it difficult to trust fruit and vegetable vendors. Therefore, <a href="https://selly.pk">Selly.pk</a> plays a collaborative role as an aggregator of quality farm produce. We take great pride in connecting the farms with the customers so that they are able to enjoy fresh seasonal produce right at their doorstep.</p>--}}
                        {{--<h5>How Do We Ensure Quality and Freshness? </h5>--}}
                        {{--<p>At Selly.pk, we understand the challenges in procuring premium quality fresh products. Therefore, we work with our dedicated community of farmers and growers to ensure quality. A stringent quality control process guarantees that each of the fruits and vegetables is checked for wholesomeness before delivery. There is a dedicated team of Quality Controllers that inspects each and every product, ensuring that it meets the quality standards before delivering to the end customers.</p>--}}
                        {{--<h5>Customer Service Excellence</h5>--}}
                        {{--<p>Customer service is an essential pillar of <a href="https://selly.pk">Selly.pk</a>. We have courteous staff that adds to the customer journey from placing the order online to the delivery of the end product in their hands. We take customers’ feedback very seriously. Therefore, we carefully listen to their concerns, which helps us grow as an organization and further our mission of delivering premium quality products.</p>--}}
                    </div>
                </div>
            </div>
            <div hidden>
                {{ $aboutus->metaKeywords }}
                {{ $aboutus->metaDescriptions }}
</div>
<div class="clearfix"></div>
</div>
</div>
@stop