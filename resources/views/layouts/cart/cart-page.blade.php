<div class="row" id="cart-page-reload">
    <div id="content" class="col-sm-8">
        <div class="content-top-breadcum">
            <div class="container" style="display: block">
                <div class="row">
                    <div id="title-content">
                        <h1 class="page-title">Shopping Cart(
                            <span class="total-items">
                            &nbsp;{{isset($total)&&$total>0?$total:0}}
                                </span>
                            {{$total==1?'Item ':'Items '}})
                        </h1>
                        <ul class="breadcrumb">
                            <li><a href=""><i class="fa fa-home"></i></a></li>
                            <li><a href="">Shopping Cart</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered shopping-cart">

                <tbody>
                <tr>
                    <th width="10%" class="text-center">Sr.</th>
                    <th width="30%">Product</th>
                    <th width="10%">Price</th>
                    <th width="40%">Quantity</th>
                    <th width="10%">Total</th>
               </tr>
                @php($subTotal = 0)
                @php
                    $counter=1
                @endphp
                @foreach($items as $key=>$value)
                    @php
                        $select = [
                                    'product_image',
                                    'salePriceValue',
                                    'product_title',
                                    'id',
                                    'quantity_in_stock',
                                ];

                        $details = \App\Helpers\Products\ProductsHelper::getSingleProduct("",$key,$select);

                        $image = $details->product_image;
                         if (isset($value['price'])?isset($value['price']):''){

                             $priceUnit=$value['price'];
                            $exode= explode("/", $priceUnit);
                            $price=$exode[0];
                            $productUnit=$exode[1];
                            }
                          else{
                          $price = $details->salePriceValue;
                                }

                        $product_title = $details->product_title;


                        $product_id = $details->id;
                        $subTotal += $value['qty']*$price;
                  $product_quantity_in_stock = \App\Models\Admin\Units::where('unit',$productUnit)->where('product_id',$product_id)->pluck('product_quantity')->first();

                        $units = \App\Helpers\Products\ProductsHelper::getProductUnits($key,$productUnit);
                    @endphp
                    <tr id="tr_{{$product_id}}">
                        <td class="text-center">{{$counter}}</td>
                        <td>
                            <a href="#"><img src="@if($units->unit == 'Dozen')@if(stripos($details->product_title, 'Eggs') !== FALSE){{Utility::getImage('products/dozen-egg.jpg')}}@endif @else{{Utility::getImage('products/'.$image)}}@endif"
                                             alt="{{$product_title}}"
                                             title="{{$product_title}}" class="cart-width img-thumbnail"></a>
                            <a href="#">{{$product_title}}</a>

                        </td>

                        <td class="">
                            <input type="hidden" value="{{$product_quantity_in_stock}}" id="quantityItem{{$counter}}">



                            <span class="rupees" i>{{$price}}</span><span class="ml-30">×</span></td>
                        <input class="rupees" type="hidden"  id="itemPrice{{$counter}}" value="{{$price}}"></td>

                        <td class="text-left">
                        <div class="input-group btn-block qty-btns">
                            <input class="rupees" type="hidden"  id="ItemPriceUnit_{{$counter}}" value="{{$value['price']}}">
                                    <input type='button' value='-' class='qtyminus' id='qtyminus{{$counter}}' field='quantity' onclick="valueUpdateMinus('{{$product_id}}','{{$value['price']}}','{{$counter}}')" />
                                    <input type='text'  class='qty' onchange="updateCart('{{$product_id}}','{{$value['price']}}','{{$counter}}')" id="quantity_{{$counter}}"
                                           name="quantity_{{$product_id}}" value="{{$value['qty']}}">
                                    <input type='button' value='+'  class='qtyplus' id="qtyplus{{$counter}}" field='quantity' onclick="valueUpdatePlus('{{$product_id}}','{{$value['price']}}','{{$counter}}')" />
                                    <span class="input-group-btn">
                                    <!--<button type="submit" data-toggle="tooltip" title="" class="btn btn-primary update-btn" data-original-title="Update"><i class="fa fa-check"></i></button>-->
                                         <span class="rupees"><span  class="total-{{$counter}}">{{$value['qty']*$price}}</span></span>
                                        <button type="button" data-toggle="tooltip" title=""
                                    class="btn btn-danger remove-btn" onClick="deleteCart('{{$product_id}}','{{$counter}}');"
                                    data-original-title="Remove"><i class="fa fa-trash-o"></i></button>
                                    </span>
                                 </div>
                            <!-- <div class="input-group btn-block">
                                <input min="1" title="quantity" onchange="updateCart('{{$product_id}}','{{$price}}','{{$counter}}')" type="number" id="quantity_{{$product_id}}" name="quantity_{{$product_id}}" value="{{$value['qty']}}" size="1"
                                       class="form-control">
                            </div> -->
                        </td>
                        <td class="text-center">
                            <span class="rupees"><span id="total-{{$counter}}" class="total-{{$product_id}}">{{$value['qty']*$price}}</span></span>
                            <!-- <button type="button" data-toggle="tooltip" title=""
                                    class="btn btn-danger remove-btn" onClick="deleteCart({{$product_id}});"
                                    data-original-title="Remove"><i class="fa fa-trash-o"></i></button> -->
                        </td>

                    </tr>
                    @php($counter++)

                @endforeach
                </tbody>
            </table>
        </div>
        <div class="buttons clearfix mt-20">
        <div class="pull-left" ><a onclick="deleteAllCart()" class="btn empty-cart-btn">Empty Cart</a></div>
            <div class="pull-right"><a href="{{url('/')}}" class="btn continue-shopping-btn">Continue Shopping</a></div>
            @php
                $productsID= isset($product_id)?isset($product_id):'';
            @endphp
            {{--<!-- <input type="hidden" value="{{$productsID}}" id="checkboxHide"> -->--}}



        </div>
    </div>
    <div class="col-sm-4 total-price-detail">
        <div>
            <ul>
                <li>
                    <strong>Subtotal</strong>
                    <span class="rupees pull-right"><span class="subTotal">{{$subTotal}}</span></span>
                </li>

                <li>
                    <hr>
                </li>
                <li class="total-price mt-10">
                <strong>AMOUNT PAYABLE</strong>
                    <strong class="rupees subtotal pull-right">
                        <span class="subTotal">{{$subTotal}}</span></strong>
                </li>
            </ul>
            <input type="hidden" value="{{$productsID}}" id="checkboxHide">
            @if($productsID !='')
            <div class="checkOut-btn" id="checkout">

                    <div class="" ><a href="{{url('checkout')}}"  class="btn btn-primary">Checkout</a></div>

            </div>
            @endif
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<script>

    function updateCart(key,price,couter) {
        showLoader(true);
        var qty = $('#quantity_'+couter).val();
        var rupes = $('#itemPrice'+couter).val();
        var quantityItem=$('#quantityItem'+couter).val();
        if( Number(quantityItem)< Number(qty)){
            alert('This Quantity is not available. Please select another quantity ');
            location.reload();
            $('#checkout').hide();
            document.getElementById("quantity_"+couter).style.color = "#ff0000";
            showLoader(true)

        }else{
            cart.update(key,qty,couter);

            $('#checkout').show();
            document.getElementById("quantity_"+couter).style.color = "black";
            var un =qty*rupes;

            if(un = 'NaN'){
                var tes=rupes.split("/");
                $('#total-'+couter).html(qty*tes[0]);
            }
            else{
                $('#total-'+couter).html(qty*rupes);
            }updateValues();
            showLoader(false);
        }
    }


    function deleteCart(key,counter) {
        cart.remove(key,counter);
        var refresh= $('#tr_'+key).remove();
        if(refresh){ // if true (1)
            setTimeout(function(){// wait for 5 secs(2)
                location.reload(); // then reload the page.(3)
            }, 5000);
        }
        updateValues();

    }
    function deleteAllCart() {
        cart.removeall();
        updateValues();
    }

    function updateValues(key) {

        values = cart.getTotalItemsAndPrice();

        $('.total-items').html(values['totalItems']);
        $('.subTotal').html(values['total']);



    }

</script>
<script>
    function valueUpdatePlus(key,price,couter){
        $('#hideCheck').hide() ;
        // fieldName = $(this).attr('field');
        var currentVal = parseInt($('#quantity_'+couter).val());
       if (!isNaN(currentVal)) {
            $('#quantity_'+couter).val(currentVal + 1);
            updateCart(key,price,couter);
        } else {
            $('#quantity_'+couter).val(0);
        }

    }
    function valueUpdateMinus(key,price,couter) {
        $('#checkout').hide();
            var currentVal = parseInt($('#quantity_'+couter).val());
            if (!isNaN(currentVal) && currentVal > 0) {
                $('#quantity_'+couter).val(currentVal - 1);
                updateCart(key,price,couter);
            } else {
                $('#quantity_'+couter).val(0);
            }
           }
  </script>
  <script>
    $(document).ready(function(){
        $('.optional-data') .hide();
            $(".optional-checkbox input:checkbox").change(function() {
                if ($(this).is(":checked")) {
                $(".optional-data").slideDown()
                } else {
                    $(".optional-data").slideUp ()
                }
            });
    });
</script>
