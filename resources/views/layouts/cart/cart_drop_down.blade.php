@php
    $subTotal = 0;
    $counter = 1;
@endphp
@foreach($items as $key=>$value)
    @php
        $select = [
                    'id',
                    'product_image',
                    'salePriceValue',
                    'product_title',
                ];

        $details = \App\Helpers\Products\ProductsHelper::getSingleProduct("",$key,$select);

    if (isset($value['price'])?isset($value['price']):''){

         $priceUnit=$value['price'];
        $exode= explode("/", $priceUnit);
        $price=$exode[0];
        $unit=$exode[1];
        }
        else{

        $price = $details->salePriceValue;
        }

        $product_title = $details->product_title;
        $product_id = $details->id;

        $units = \App\Helpers\Products\ProductsHelper::getProductUnits($key,$unit);

    $image = $details->product_image;
    @endphp
    <li>
        <table class="table table-striped">
            <tbody>
            <tr id="tr_{{$product_id}}">

                <td class="text-center td-width20"><a href="#"><img src="@if($units->unit == 'Dozen')
                                @if(stripos($details->product_title, 'Eggs') !== FALSE){{Utility::getImage('products/dozen-egg.jpg')}}@endif @else{{Utility::getImage('products/'.$image)}}@endif"
                                                                    alt="{{$product_title}}" title="{{$product_title}}"
                                                                    class="img-thumbnail cart-image"></a>
                </td>

                <td class="text-left td-width50">{{$product_title}}</td>
                <td class="text-right td-width10">{{$value['qty']}}  </td>
                <td class="text-right rupees td-width10">{{$price}}</td>
                <td class="text-right rupees td-width10" >
                    <span onClick="deleteCart('{{$product_id}}','{{$counter}}');">
                                                    <i class="fa fa-times" style="color:red;"></i></span>
                </td>

                @php($subTotal += $value['qty']*$price)
            </tr>

            {{--onClick="deleteCart('{{$product_id}}','{{$counter}}');"--}}
            </tbody>
        </table>
    </li>
    @php($counter ++)
@endforeach

<li>
    <div>
        <table class="table table-bordered total-table">
            <tbody>
            <tr>
                <td class="text-center subTotalText"><strong>Sub-Total</strong></td>
                <td class="text-right rupees">{{$subTotal}}</td>
            </tr>
            </tbody>
        </table>
        <div class="text-right button-container">
            <a href="{{url('cart')}}" class="addtocart"><strong>View Cart</strong></a>
            <a href="{{url('checkout')}}" class="addtocart checkout"><strong>Checkout</strong></a>
        </div>
    </div>
</li>
<script>

    function updateCart(key,price,couter) {
        showLoader(true);
        var qty = $('#quantity_'+couter).val();
        var rupes = $('#itemPrice'+couter).val();
        var quantityItem=$('#quantityItem'+couter).val();
        if( Number(quantityItem)< Number(qty)){
            alert('This Quantity is not available. Please select another quantity ');
            location.reload();
            $('#checkout').hide();
            document.getElementById("quantity_"+couter).style.color = "#ff0000";
            showLoader(true)

        }else{
            cart.update(key,qty,couter);

            $('#checkout').show();
            document.getElementById("quantity_"+couter).style.color = "black";
            var un =qty*rupes;

            if(un = 'NaN'){
                var tes=rupes.split("/");
                $('#total-'+couter).html(qty*tes[0]);
            }
            else{
                $('#total-'+couter).html(qty*rupes);
            }updateValues();
            showLoader(false);
        }
    }


    function deleteCart(key,counter) {
        cart.remove(key,counter);
        var refresh= $('#tr_'+key).remove();
        if(refresh){ // if true (1)
            setTimeout(function(){// wait for 5 secs(2)
                location.reload(); // then reload the page.(3)
            }, 5000);
        }
        updateValues();

    }
    function deleteAllCart() {
        cart.removeall();
        updateValues();
    }

    function updateValues(key) {

        values = cart.getTotalItemsAndPrice();

        $('.total-items').html(values['totalItems']);
        $('.subTotal').html(values['total']);



    }

</script>


<script>
    $(document).ready(function(){
        $('.optional-data') .hide();
        $(".optional-checkbox input:checkbox").change(function() {
            if ($(this).is(":checked")) {
                $(".optional-data").slideDown()
            } else {
                $(".optional-data").slideUp ()
            }
        });
    });
</script>