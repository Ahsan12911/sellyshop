<div class="content-top-breadcum">
    <div class="container">
        <div class="row">
            <div id="title-content">
                <h1 class="page-title">Payment
                </h1>
                <ul class="breadcrumb">
                    <li><a href=""><i class="fa fa-home"></i></a></li>
                    <li><a href="">Shopping Cart</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-4 p-0">


    <div class="checkout_form clearfix">

        <h3>Billing Address</h3>

        <div class="col-md-12">
            <div class="billing_address_box mt-20">
                <span class="billing_firstName"></span>
                <span class="billing_StreetNum"></span>
                <span class="billind_PhoneNum"></span>

            </div>

            <div class="form-group-custom">
                <input type="text" id="" required/>
                <label class="control-label" for="user-name">Order note</label><i class="bar"></i>

            </div>
        </div>

    </div>

</div>
<div class="col-md-4 checkout_cart">

    <div>
        <h3>Payment Method</h3>
        <a href="#">Cash on Deleivery</a>
        <br>
        <p class="mt-10">One of our Customer Care Representatives will shortly get in touch with you. </p>
        <br>

        <p><span class="red-txt">If you do not get a CALL in FIVE (5) MINUTES,</span> please CONTACT on this number <a href="#">0304-111-7355</a> or send us an email at <a href="#">support@selly.pk</a></p>
    </div>
</div>
<div class="col-md-4 checkout_cart">
    <div>
        <ul>
            <li>
                <a  href="#" class="items_in_cart">1 item in Cart</a>
                <span class="pull-right">Rs.10.00</span>
            </li>



            <li>
                <hr>
            </li>
            <li class="total-price">
                <strong>Rs320.68</strong>
            </li>
        </ul>
        <label class="subscribe-sec">
            <div class="subscribe-input"><input type="checkbox"></div>
            <div class="subscribe-text">Subscribe to our news list and be in touch with our latest offers.</div>
        </label>
        <a class="btn btn-default btn-block proceed_payment_btn">Place Order</a>
        <p class="text-center mt-20 mb-0">By clicking the "Place order" button you confirm that you accept the Terms & Conditions</p>
    </div>
</div>
<div class="clearfix"></div>