@extends('templates.layout')
@section('content')
    <div class="">
        <div class="row mt-15">
            <div class="helpful_links_leftside">
                <div class="col-md-3 mt-15 pl-0">
                    <div class="clearfix">
                        <ul>
                            <li>
                                <a href="{{url('about-us')}}">About Us</a>
                            </li>
                            <li>
                                <a href="{{url('privacy-policy')}}">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="{{url('return-policy')}}">Return Policy</a>
                            </li>
                            <li>
                                <a href="{{url('terms-conditions')}}">Terms & Conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9-custom-width80">
                <div class="col-md-9">
                    <div class="content-top-breadcum">
                        <div class="container" style="display: block">
                            <div class="row">
                                <div id="title-content">
                                    <h1 class="page-title">Privacy Policy
                                    </h1>
                                    <ul class="breadcrumb">
                                        <li><a href=""><i class="fa fa-home"></i></a></li>
                                        <li><a href="">Privacy Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="privacy_policy_page clearfix">
                        {!! $PrivacyPolicy->body !!}
                        {{--<h5>Privacy and Confidentiality </h5>--}}
                        {{--<p>The privacy and confidentiality of our site users and potential customers is critical for us. This Privacy Policy page explains how <a href="Selly.pk">Selly.pk</a> collects the user information and uses it. When you visit Selly.pk, you are agreeing to accept this Privacy Policy and any other policies of this website. It is important for you to go through this Privacy Policy page so that you know how we are going to use this information.</p>--}}
                        {{--<h5>Collection of Data </h5>--}}
                        {{--<p>At Selly.pk, we collect the data that we only use for processing your order. Some of the other information that we may collect using our analytical tools include (not limited to) Name, Title, Delivery Address, Phone Number, Email Address, Date of Birth, Gender, and Mobile Phone Number. <a href="Selly.pk">Selly.pk</a> will use this information to provide you with the products that you order online. Some of the data that we collect using analytical tools will help us improve our website’s responsiveness and user experience. We may use this information for providing marketing information including deals, and offers to the potential customers.</p>--}}
                        {{--<h5>Use of Personally identifiable Information  </h5>--}}
                        {{--<p><a href="Selly.pk">Selly.pk</a> can use this data for conducting different types of market researches. Your details will remain anonymous and the recipients of this communication can opt out of it at any time by clicking on the “unsubscribe” link. It may include answering questions in opinions or surveys. We may also, from time to time, send newsletters and promotions to the users.</p>--}}
                        {{--<h5>Links to Third Party Sites </h5>--}}
                        {{--<p><a href="Selly.pk">Selly.pk</a> cannot be held responsible for the links of the third party sites on it. Therefore, if you choose to visit a site with its link on Selly.pk, you should carefully read the respective website’s policy pages. <a href="Selly.pk">Selly.pk</a> or any of its entities or subsidiaries or employees cannot be held responsible for their commitments, promises, or the information that they share. You will be solely responsible for your interactions with all such third party sites.</p>--}}
                        {{--<h5>Use of Cookies on <a href="Selly.pk">Selly.pk</a></h5>--}}
                        {{--<p>A visitor to our site can choose to accept or deny the cookies, it entirely depends on their choice. However, we would strongly recommend that you allow cookies when visiting this site. The addition of “basket” functionality keeps track of the orders that you place with us. You can only place an order when you accept cookies. Cookies are small files through which our server can know about each individual user interacting with our site. Once you have accepted the cookies, you do not need to re-enter the data like email address or any other information necessary for letting the server identify your unique shopping basket.</p>--}}
                        {{--<h5>Site Security </h5>--}}
                        {{--<p>Selly.pk’s expert IT professionals are cognizant of the technological challenges that they face. Therefore, they make every effort to prevent any unlawful or unauthorized access to the site. The firewalls installed on our servers provide an extra layer of security. Therefore, if the system detects any suspicious user activity, it may ask for proof of identity from the user. Please make sure that you do not share your <a href="Selly.pk">Selly.pk</a> username and password with anyone else. You will be responsible for the security and confidentiality of your username and password.</p>--}}
                        {{--<h5>Rights of the End User</h5>--}}
                        {{--<p>If you are worried about the way in which <a href="Selly.pk">Selly.pk</a> uses you data, you can ask us about the process and the way we use your data by sending us an email at <a href="mailto:support@selly.pk">support@selly.pk</a>. Also, you have the right to let us know if there are any inaccuracies on our site that you would like us to correct. Similarly, you can ask us not to use your personal data for marketing purposes.</p>--}}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@stop