@extends('templates.layout')
@section('content')
    <div class="">
        <div class="row mt-15">
            <div class="helpful_links_leftside">
                <div class="col-md-3 mt-15 pl-0">
                    <div class="clearfix">
                        <ul>
                            <li>
                                <a href="{{url('about-us')}}">About Us</a>
                            </li>
                            <li>
                                <a href="{{url('privacy-policy')}}">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="{{url('return-policy')}}">Return Policy</a>
                            </li>
                            <li>
                                <a href="{{url('terms-conditions')}}">Terms & Conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9-custom-width80">
                <div class="col-md-9">
                    <div class="content-top-breadcum">
                        <div class="container" style="display: block;">
                            <div class="row">
                                <div id="title-content">
                                    <h1 class="page-title">Terms & Conditions
                                    </h1>
                                    <ul class="breadcrumb">
                                        <li><a href=""><i class="fa fa-home"></i></a></li>
                                        <li><a href="">Terms & Conditions</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="terms_conditions clearfix">
                        {!! $Termsandconditions->body !!}
                        {{--<h5><a href="Selly.pk">Selly.pk</a> Terms and Conditions</h5>--}}
                        {{--<p>When you access and use Selly.pk, these are the conditions that govern its usage. Please read them carefully so that you do not have any ambiguity or confusion when making a purchase from <a href="Selly.pk">Selly.pk</a> or visiting this site.</p>--}}
                        {{--<h5>Introduction </h5>--}}
                        {{--<p>Thank you for your interest in buying from Selly.pk. <a href="Selly.pk">Selly.pk</a> is an online marketplace for fresh supply of fruits and vegetables. By using our services, you agree to the terms and conditions that are given on this page. You also agree to frequently visit this page to stay updated on any policy changes, modifications, or amendments. This site is owned by IPS USA (Information Process Solutions).</p>--}}
                        {{--<h5>Conditions of Using <a href="Selly.pk">Selly.pk</a> Online Fruits and Vegetables Store </h5>--}}
                        {{--<p>When you decide to use Selly.pk, you may need to provide some personal information including email, name, and other details. <a href="Selly.pk">Selly.pk</a> holds the right to terminate the access of any user to its website without giving any reason. It may mean invalidating any user credentials required to access our site. It may also include blocking social media access or their IP address, if the management decides to sell the fresh vegetables and fruits through its social media accounts, particularly Facebook.</p>--}}
                        {{--<h5>Privacy </h5>--}}
                        {{--<p>We treat all the user submitted details containing personally identifiable information in strictest confidence. Please refer to our privacy policy page to know more about how we use this information.  .</p>--}}
                        {{--<h5>Guarantee Disclaimer about Continuous Availability of the Site </h5>--}}
                        {{--<p><a href="Selly.pk">Selly.pk</a> makes every effort to ensure its online availability at all the times. Our team is striving hard to that there remain no errors or interruptions. However, the unpredictable nature of IT and internet technologies make it difficult to guarantee availability at all times. <a href="Selly.pk">Selly.pk</a> may restrict or deny access to the site due to various reasons including but not limited to maintenance, repairs, or improvement of current facilities. The same applies to the availability of our products and services on any social media site, should we choose to sell our products and provide our services via such sites like Facebook.</p>--}}
                        {{--<h5>Legal Requirements for Accessing <a href="Selly.pk">Selly.pk</a></h5>--}}
                        {{--<ul>--}}
                            {{--<li>You must be 18 years of age to access the website. If not, then you have to access the site under the supervision of a legal guardian or parent.</li>--}}
                            {{--<li>You are only given a non-transferrable right to access the website by strictly abiding to the terms and conditions mentioned on this page or any other policy pages of Selly.pk.&nbsp;</li>--}}
                            {{--<li>We only provide the content on this website for informational purposes. You agree to accept any omissions, commissions, mistakes or misquotation of prices or other details and agree to waive <a href="Selly.pk">Selly.pk</a> and all its related entities of any legal obligations.</li>--}}
                        {{--</ul>--}}
                        {{--<h5>Fair Usage Policy </h5>--}}
                        {{--<p>You hereby agree not to use <a href="Selly.pk">Selly.pk</a> website in any manner which does not fall under fair use policy. This may include but not limited to copying any of its content, using its intellectual assets or any other material without prior written consent. Also, you cannot claim that any of the intellectual property rights of <a href="Selly.pk">Selly.pk</a> or any other site shared here belong to you.</p>--}}
                        {{--<h5>Handling of Objectionable Content </h5>--}}
                        {{--<p><a href="Selly.pk">Selly.pk</a> holds the right to remove any content generated by users that it finds objectionable or offensive due to any reason. It may include the user generated content submitted on the site or any of our social media pages.</p>--}}
                        {{--<h5>Copyrights and Trademarks </h5>--}}
                        {{--<p>All the trademarks and copyrights used on the site belong to Selly.pk. If we have used any of the logos, trademarks or copyright material from any other site, prior permission has been furnished. If you think that any of the text, images, videos, or graphics violate your copyrights, please let us know with proof, and we will take care of it.</p>--}}
                        {{--<h5>Indemnification against Losses</h5>--}}
                        {{--<p>By using Selly.pk, you guarantee that you indemnify the website, its owners, directors, employees, and any of its subsidiaries or entities directly or indirectly related to it against any losses.</p>--}}
                        {{--<h5>Third Party Links </h5>--}}
                        {{--<p>The third party links given on <a href="Selly.pk">Selly.pk</a> are for informational purposes only. <a href="Selly.pk">Selly.pk</a> cannot be held responsible for any loss or bad experience that you may have encountered while visiting any other site. Please read these websites’ respective policy pages to understand their policies and how you need to interact with them.</p>--}}
                        {{--<h5>Updates and Modifications to the Policy Pages </h5>--}}
                        {{--<p><a href="Selly.pk">Selly.pk</a> holds the right to update or modify any of its policy pages at any time without giving any prior notice to anyone. Therefore, we strongly encourage you to keep visiting this page and other relevant pages to remain updated.</p>--}}
                        {{--<h5>Payment of Taxes </h5>--}}
                        {{--<p>You are yourself responsible for paying the taxes against all the purchases from our site. </p>--}}
                        {{--<h5>Right to Change Terms of Service </h5>--}}
                        {{--<p>It is upon Selly.pk’s discretion to make any changes to the terms of the service it is offering to its customers. </p>--}}
                        {{--<h5>Compliance with Applicable Government Regulations </h5>--}}
                        {{--<p>When making purchases from Selly.pk, you agree that you will comply with all applicable government laws, including that of Federal, Provincial, and Local Governments. </p>--}}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

@stop