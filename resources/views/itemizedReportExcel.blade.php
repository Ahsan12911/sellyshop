
<tr>
    <td>product_title</td>
    <td>unit</td>
    <td>waste_date</td>
    <td>waste invoice_no</td>
    <td>waste quantity</td>
    <td>new purchase</td>
    <td>purchase date</td>
    <td>purchase invoice_no</td>
</tr>

    @foreach($productsData as $products)
        @foreach($products as $prod)
            <tr>
            <td>{{$prod->product_title}}</td>
            <td>{{$prod->unit}}</td>
            <td>{{$prod->waste_date}}</td>
            <td>{{$prod->waste_invoice_no}}</td>
            <td>{{$prod->waste_quantity}}</td>
            <td>{{$prod->new_purchase}}</td>
            <td>{{$prod->purchase_date}}</td>
            <td>{{$prod->purchase_invoice_no}}</td>
            </tr>
        @endforeach
    @endforeach
