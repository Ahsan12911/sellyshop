@extends('admin.layouts.master')

<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
{{--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>--}}
@section('content')
    <script>
        function validateForm() {
            var x = document.forms["frmProducts"]["email"].value;
            var pasword = document.forms["frmProducts"]["password"].value;
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {

                $('#error_msg_validateemail').show();
                return false;
            }
            else{
                $('#error_msg_validateemail').hide();

            }

        }
    </script>
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')

        <form name="frmProducts" id="frmProducts" method="post"  action="{{ url('/admin/users-management/addUsers') }}" >
            {{ csrf_field() }}
            <div id="main">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Create profile</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Product name Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="first_name">First name *</label>
                                            <div class="col-md-4 ">
                                                <input id="first_name" name="first_name" class="errors form-control input-md" type="text" value="{{ old('first_name') }}" @if($errors->has('first_name'))style=" border-color:red ;" @endif >

                                            </div>
                                        </div>

                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('first_name') }}</span>--}}

                                        <!-- SKU Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="last_name">Last name</label>
                                            <div class="col-md-4">
                                                <input id="last_name" name="last_name" class="form-control input-md" type="text" >

                                            </div>
                                        </div>

                                        <!-- UPC/ISBN Text input-->
                                        <div class="form-group{{ $errors->has('email') ? 'has-error' : '' }} ">
                                            <label class="col-md-4 control-label" for="email">E-mail *</label>
                                            <div class="col-md-4">
                                                <input id="email" name="email" type="email"  class="form-control input-md email"  value="{{ old('email') }}" @if($errors->has('email'))style=" border-color:red ;" @endif>

                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('email') }}</span>

                                        <div class="form-group{{ $errors->has('user_name') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="username">Username/ Login ID *</label>
                                            <div class="col-md-4">
                                                <input id="username" name="user_name"  class="form-control input-md"   type="text" value="{{ old('user_name') }}" @if($errors->has('user_name'))style=" border-color:red ;" @endif>

                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('user_name') }}</span>


                                        <div class="form-group{{ $errors->has('password') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="password">Password *</label>
                                            <div class="col-md-4">
                                                <input type="password"   name="password" id="password"  class="form-control input-md" @if($errors->has('password'))style=" border-color:red ;" @endif>
                                            </div>
                                            {{--<div class=="alert" id="error_msg_password" style="color: red;margin-left: 35%" hidden>Minimum 8 </div>--}}

                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('password') }}</span>

                                        <div class="form-group{{ $errors->has('confirm_password') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="confirm_password">Confirm password *</label>
                                            <div class="col-md-4">
                                                <input type="password"  name="confirm_password" id="confirm_password" class="form-control input-md" @if($errors->has('confirm_password'))style=" border-color:red ;" @endif>

                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('confirm_password') }}</span>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="user_status">Status</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="hidden" data-toggle="toggle" name="user_status" value="0" >
                                                        <input type="checkbox" data-toggle="toggle" name="user_status" id="user_status" value="1" >
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Product section End-->

                <!-- Marketing section Start-->
                <div class="marketing-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Access information</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Meta description input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="access_level">User Access</label>
                                            <div class="col-md-4">
                                                <select class="form-control" id="access_level" name="user_role" >
                                                    <option value="" selected="selected">Select Access Level</option>
                                                    @foreach($roles as $role)
                                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Marketing section End-->
            </div>
            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="add_inputbtn" id="create_account" type="submit" name="" value="Create account">
                <a href="{{url('admin/users-management/users')}}" class="add_statusbtn" >Cancel </a>

            </div>
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

@endsection