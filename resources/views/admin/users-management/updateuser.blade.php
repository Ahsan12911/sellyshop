@extends('admin.layouts.master')


<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

@section('content')


    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <form name="frmProducts" id="frmProducts" method="post"  action="{{ url('/admin/users-management/updateUsers',[$usersData->id]) }}" >
            {{ csrf_field() }}

            {{--@if(count($errors))--}}
                {{--<div class="alert alert-danger">--}}
                    {{--<strong>Whoops!</strong> There were some problems with your input.--}}
                    {{--<br/>--}}
                    {{--<ul>--}}
                        {{--@foreach($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--@endif--}}

            <input type="hidden" id="hidden_id" value="{{$usersData->id}}">
            <div id="main">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Update Profile</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Product name Text input-->

                                        <div class="form-group{{ $errors->has('first_name') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="first_name">First name *</label>
                                            <div class="col-md-4">
                                                <input id="first_name" name="first_name" value="{{$usersData->first_name}}" class="form-control input-md" type="text" @if($errors->has('first_name'))style=" border-color:red ;" @endif>
                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('first_name') }}</span>

                                        <!-- SKU Text input-->

                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="last_name">Last name</label>
                                            <div class="col-md-4">
                                                <input id="last_name" name="last_name" value="{{$usersData->last_name}}" class="form-control input-md" type="text" @if($errors->has('last_name'))style=" border-color:red ;" @endif>
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('last_name') }}</span>--}}

                                        <!-- UPC/ISBN Text input-->

                                        <div class="form-group{{ $errors->has('email') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="email">E-mail *</label>
                                            <div class="col-md-4">
                                                <input id="email" name="email"  value="{{$usersData->email}}"  class="form-control input-md"  type="email" @if($errors->has('email'))style=" border-color:red ;" @endif>
                                                {{--onkeyup="myFunction()"--}}
                                            </div>
                                        </div>

                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('email') }}</span>


                                        <!-- Mnf#/Vendor# Text input-->

                                        <div class="form-group{{ $errors->has('user_name') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="username">Username/ Login ID *</label>
                                            <div class="col-md-4">
                                                <input id="username" name="user_name" value="{{$usersData->username}}"  class="form-control input-md"  type="text" @if($errors->has('user_name'))style=" border-color:red ;" @endif>
                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('user_name') }}</span>



                                        <div class="form-group{{ $errors->has('password') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="password">Password *</label>
                                            <div class="col-md-4">
                                                <input type="password"  name="password" id="password"  value="{{$usersData->password}}"   class="form-control input-md"  @if($errors->has('password'))style=" border-color:red ;" @endif>

                                            </div>

                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('password') }}</span>


                                        <div class="form-group{{ $errors->has('confirm_password') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="confirm_password">Confirm password *</label>
                                            <div class="col-md-4">
                                                <input type="password"  name="confirm_password" id="confirm_password" value="{{$usersData->password}}" class="form-control input-md" @if($errors->has('confirm_password'))style=" border-color:red ;" @endif>

                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('confirm_password') }}</span>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="user_status">Status</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>

                                                            <input type="checkbox" data-toggle="toggle" name="user_status"  id="user_status" value="1" @if($usersData->user_status == '1') checked @endif >

                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Product section End-->

                <!-- Marketing section Start-->
                <div class="marketing-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Access information</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Meta description input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="access_level">Access level</label>
                                            <div class="col-md-4">
                                                <select class="form-control" id="access_level" name="user_role" >
                                                    @if($userrole != '')
                                                   @foreach($roles as $role)
                                                    <option value="{{$role->id}}" @if($userrole->role_id == $role->id) selected @endif>{{$role->name}}</option>
                                                   @endforeach
                                                        @else
                                                        <option  >Select Role</option>
                                                        @foreach($roles as $role)
                                                            <option value="{{$role->id}}" >{{$role->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Marketing section End-->
            </div>
            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="add_inputbtn" id="create_account" type="submit" name="" value="Update Account">
                <a href="{{url('admin/users-management/users')}}" class="add_statusbtn" >Cancel </a>


            </div>
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

@endsection