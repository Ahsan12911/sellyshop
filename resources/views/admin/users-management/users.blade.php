@extends('admin.layouts.master')

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Users</h3>
                    </div>
                    <div class="search-conditions-box clearfix products_box">
                        <div class="col-md-6">
                            <input type="text" placeholder="Search Keywords">
                        </div>
                        <div class="col-md-2">
                            <select>
                                <option>Any Category</option>
                                <option>No Catogory Assigned</option>
                                <option>Vagetables</option>
                                <option>Fruits</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select>
                                <option>Any Stock Status</option>
                                <option>In Stock</option>
                                <option>Low Stock</option>
                                <option>Out Of Stock</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button class="border_blue search_btn border_radius3 btn-block">Search</button>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="absolute_position angle_downbtn border_radius3"><i class="ti-angle-down"></i></button>
                    </div>
                    <br>
                    <a href="{{url('/admin/users-management/add-users')}}" class="btn add_statusbtn">Add User</a>
                    <br>
                    <br>
                    <div class="search-conditions-hidden search-conditions-box products-hidden-box clearfix">
                        <div class="col-md-3">
                            <div>
                                <p>Search In:</p>
                                <ul>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="" value="">
                                            Name</label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="" value="">
                                            SKU</label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="" value="">
                                            Description</label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="" value="">
                                            Tag</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div>
                                <span>Availability</span>
                                <select>
                                    <option>any availability status</option>
                                    <option>Only Enabled</option>
                                    <option>Only Disabled</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <table class="table order_table" >
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox">
                            </th>
                            <th>Username/ Login ID</th>
                            <th>Email</th>
                            <th>Name</th>
                            <th>Access level</th>
                            <th>Orders</th>
                            <th>Created</th>
                            <th>Last Login</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($usersData as $userInfo)
                        <tr>
                            <td class="right_border_dotted"><input type="checkbox"></td>
                            <td><span>{{$userInfo->username}}</span></td>
                            <td><span>{{$userInfo->email}}</span></td>
                            <td><span>{{$userInfo->first_name.' '.$userInfo->last_name}}</span></td>
                            <td><span>{{$userInfo->admin}}</span></td>
                            <td><span></span></td>
                            <td><span>{{$userInfo->created_at}}</span></td>
                            <td><span>{{$userInfo->last_login}}</span></td>
                            <td class="left_border_dotted text-center"><i class="fa fa-trash-o" aria-hidden="true"></i></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="table_pager">
                        <div class="col-lg-6 col-md-6 text-left">
                            <ul class="pagination">
                                <li><a href="#">&laquo;</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-md-6 text-right">
                            <div class="items-per-page-wrapper">
                                <span><b>10</b> items</span>
                                <select>
                                    <option>10</option>
                                    <option>20</option>
                                    <option>30</option>
                                    <option>40</option>
                                </select>
                                <span>per page</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection