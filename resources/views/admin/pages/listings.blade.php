@extends('admin.layouts.master')

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main" class="target-page">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Pages</h3>
                        <br>

                        <a href="{{url('/admin/pages/add')}}"><button class="add_statusbtn">New Page</button></a>
                    </div>

                    <div class="mt-35">
                        <div >
                            <table class="table tables_ui" id="t_draggable2">
                                <thead>
                                    <tr>
                                        <th width="5%"></th>
                                        <th width="5%"></th>
                                        <th width="30%">Page Name</th>
                                        <th width="30%">Page URL </th>
                                        <th width="5%"></th>
                                    </tr>
                                </thead>
                                <tbody class="t_sortable">
                                @foreach(@$pagesData as $pageInfo)
                                <tr>
                                    <td class="right_border_dotted text-center"><i class="ti-move"></i></td>

                                    @if($pageInfo->enabled == '1')
                                        <td title="Enabled" class="right_border_dotted text-center"><a href="{{url('/admin/pages/disabStatus',[$pageInfo->id])}}"><i class="fa fa-power-off" aria-hidden="true"></i></a></td>
                                    @else
                                        <td title="Disabled" class="right_border_dotted text-center"><a href="{{url('/admin/pages/enbStatus',[$pageInfo->id])}}"><i class="fa fa-power-off" style="color:red" aria-hidden="true"></i></a></td>
                                    @endif

                                    <td><a href="{{url('/admin/pages/update',[$pageInfo->id])}}">{{$pageInfo->name}}</a></td>
                                    <td><a href="#">{{$pageInfo->cleanURL}}.html</a></td>
                                    <td onclick="return confirm('Are You sure to delete this record?');" class="left_border_dotted text-center"><a href="{{url('/admin/pages/delPage' ,[$pageInfo->id]) }}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
        <!-- RIGHT SIDEBAR -->

        <!-- END RIGHT SIDEBAR -->
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/flot.tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-sparkline/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-tour/js/bootstrap-tour.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widget.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/data.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/scroll-parent.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/disable-selection.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/mouse.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/sortable.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-main/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-appear/jquery.appear.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection