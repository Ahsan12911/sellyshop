@extends('admin.layouts.master')

@section('content')
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <form class="form-horizontal" name="frmPages" id="frmPages" method="post" action="{{ url('/admin/pages/add') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-md-9">
                        <div>
                            <h3 class="mt-35 font-normal mb-20 display-ib">New page</h3>
                        </div>
                        <div class="add-product-box">
                            <fieldset>
                                    <!-- Product name Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="product_name">Name</label>
                                        <div class="col-md-4">
                                            <input id="name" name="name" class="form-control input-md" required value="">
                                        </div>
                                    </div>
                                    <!-- Description Text area-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="product_weight">Content</label>
                                        <div class="col-md-8">
                                            <form action="#" method="POST" class='form-wysiwyg'>
                                                <textarea name="body" id="body" class='ckeditor span12' rows="5"></textarea>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="product_name">Page title</label>
                                        <div class="col-md-4">
                                            <input id="metaTitle" name="metaTitle" class="form-control input-md" required placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="product_name">Meta Keywords</label>
                                        <div class="col-md-4">
                                            <input id="metaKeywords" name="metaKeywords" class="form-control input-md" required type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="product_weight">Meta Description</label>
                                        <div class="col-md-8">
                                            <form action="#" method="POST" class='form-wysiwyg'>
                                                <textarea name="metaDescriptions" id="metaDescriptions" class='ckeditor span12' rows="5"></textarea>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- UPC/ISBN Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="available_quantity">CleanURL</label>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <input type="text" name="cleanURL" id="cleanURL" class="form-control" placeholder="" aria-describedby="basic-addon1" value="" required>
                                                <span class="input-group-addon" id="basic-addon1">.html</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- SKU Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="product_name">Enable</label>
                                        <div class="col-md-4">
                                            <input type="checkbox" name="enabled" id="enabled" value="1">
                                        </div>
                                    </div>
                                <div class="add-productbtn-fixed">
                                    <button type="submit" class="add_statusbtn">Add</button>
                                </div>
                                </fieldset>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/flot.tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-sparkline/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-tour/js/bootstrap-tour.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widget.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/data.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/scroll-parent.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/disable-selection.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/mouse.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/sortable.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-main/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-appear/jquery.appear.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

@endsection