@extends('admin.layouts.master')

@section('content')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    {{--<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">--}}

    {{--<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">--}}
    <style>
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #2caae1;;
            border-right: 16px solid white;;
            border-bottom: 16px solid #2caae1;;
            border-left: 16px solid white;    ;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            position: fixed;
            display: none;
            width: 70px;
            height: 70px;
            top: 60%;
            left: 40%;
            right: 20%;
            bottom: 40%;
            cursor: pointer;

        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

    </style>
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row" id="loading-image">
                <div class="col-md-12">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Users</h3>
                    </div>
                    <form method="post" action="{{url('/admin/customer-management/searchcustomer')}}">
                        {{csrf_field()}}
                        <div class="search-conditions-box clearfix products_box">
                            <div class="col-md-6">
                                <input type="text" placeholder="User Name, Email, Cell no , Address " name="keyword" id="search-keywords" >
                            </div>
                            {{--<div class="col-md-4">--}}
                            {{--<select name="status" id="status" >--}}

                            {{--<option value="">Any Status</option>--}}
                            {{--<option value="1">Enabled</option>--}}
                            {{--<option value="0">Disabled</option>--}}

                            {{--</select>--}}
                            {{--</div>--}}

                            <div class="col-md-2">
                                <button type="submit" class="border_blue search_btn border_radius3 btn-block">Search</button>
                            </div>
                        </div>
                    </form>
                    <form   method="post" action="{{url('/admin/customer-management/DeleteMultiplecustomer')}}" onsubmit="return confirm('Are you sure to delete these records?');">
                        {{csrf_field()}}


                        <br>
                        @can('Delete-customer')
                        <input type="submit" name="submit" id="delete-order" class="btn add_inputbtn"     value="Delete Selected Value" >
                        @endcan
                            <br>
                        <div class="table-responsive">
                            <table id="myTable" class="display table" width="100%" >
                                <script type="text/javascript">
                                    $(function() {
                                        $("#hideaftermoment").delay(2000).fadeOut(0);
                                    });
                                </script>
                                @if (session('status'))
                                    <div id="hideaftermoment" class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <script type="text/javascript">
                                    $(function() {
                                        $("#testdiv").delay(2000).fadeOut(0);
                                    });
                                </script>
                                @if($errors->any())
                                    <h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>
                                @endif
                                <thead>
                                <tr>
                                    <script type="text/javascript">
                                        function selectAll(status){
                                            $('.multiplerow').each(function(){
                                                $(this).prop('checked',status);
                                            });

                                        }
                                    </script>

                                    <th>
                                        <input type="checkbox" onclick='selectAll(this.checked)' value="Select All"/>
                                    </th>

                                    <th>Username/ Login ID</th>
                                    <th>Email</th>
                                    <th>Cell no</th>
                                    <th>Address</th>
                                    <th>Customer Order</th>
                                    <th>Created</th>
                                    {{--<th>Wallet</th>--}}
                                    <th></th>
                                </tr>
                                </thead>
                                <div class="loader" id="loader" hidden></div>
                                <tbody id="tableBody">
                                @foreach($usersData as $userInfo)
                                    {{--                                    @php $curent_user_orders=$orders->where('user_id','=',$userInfo->id)->count(); @endphp--}}
                                    <tr id="tableRow">
                                        @php

                                            $customerOrder=$orders->where('customer_id','=',$userInfo->id)->count();
                                        @endphp
                                        <td class="right_border_dotted"><input type="checkbox" class="multiplerow" id="multiplerow" name="delete[]" value="{{$userInfo->id}}"></td>

                                        <td><span>
                                                @if(isset($userInfo->username)?isset($userInfo->username):'')
                                                    {{$userInfo->username}}
                                                @else
                                                    {{$userInfo->full_name}}
                                                @endif

                                                    </span></td>
                                        {{--<input type="hidden" value="{{$userInfo->id}}" id="order">--}}
                                        <td><span>{{$userInfo->email}}</span></td>
                                        <td><span>{{$userInfo->phoneno}}</span></td>
                                        <td><span>
                                                {{$userInfo->address}}
                                            </span></td>
                                        <td><span>{{$customerOrder}}</span></td>

                                        <td><span>{{$userInfo->created_at}}</span></td>
                                        <td><span>{{$userInfo->last_login}}</span></td>


                                        <td ></td>
                                        {{--<td class="left_border_dotted text-center" onclick="return confirm('Are You sure to delete this record?');"><a href="{{url('/admin/customer-management/deletecustomer', [$userInfo->id])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table_pager">
                            <div class="col-lg-4 col-md-4 text-left">


                            </div>
                            <div class="col-lg-4 col-md-offset-4 col-md-4 text-right">
                                <div class="items-per-page-wrapper" id="page-link" style="float: right">

                                    {{$usersData->links()}}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')

    <script>
        $(document).ready(function(){
            $('#myTable').dataTable();
        });
    </script>

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>


    <script  type="text/javascript">



        function  confirmdelete(a){

            var txt=a;
            if (!confirm("Are You sure to delete this record?")) {
                return false;

            } else {
                window.location = "{{url('/admin/users-management/delUsers')}}/"+txt+"!";

            }

        }

    </script>


@endsection
