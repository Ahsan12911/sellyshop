
@extends('admin.layouts.master')
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<style>
    @if($errors->has('category_description'))
        #cke_1_contents{
        border: 1px solid red;
    }
    @endif
        @if($errors->has('meta_descriptions'))
           #cke_2_contents{
        border: 1px solid red;
    }
    @endif

</style>
@section('content')




    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        {{--<script type="text/javascript">--}}
            {{--$(function() {--}}
                {{--$("#testdiv").delay(2000).fadeOut(0);--}}
            {{--});--}}
        {{--</script>--}}
        {{--@if($errors->any())--}}
            {{--<h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>--}}
        {{--@endif--}}

        <form name="updProducts" id="updProducts" method="post" enctype="multipart/form-data" action="{{ url('/admin/categories-management/updCategories',[$category->id]) }}">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$category->id}}">



            {{--@if(count($errors))--}}
                {{--<div class="alert alert-danger">--}}
                    {{--<strong>Whoops!</strong> There were some problems with your input.--}}
                    {{--<br/>--}}
                    {{--<ul>--}}
                        {{--@foreach($errors->all() as $error)--}}
                            {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--@endif--}}
            <div id="updmain">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Add Categories</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name_fr">Parent Category</label>
                                            <div class="col-md-4">
                                                <select class="form-control" id="category_id" name="parent_category" type="text" >
                                                    <option value="" selected="selected">Select Category</option>
                                                    @foreach($categoriesData as $categories)
                                                        <option @if($category->parent_id == $categories->id) selected @endif value="{{$categories->id}}">{{$categories->category_title}}</option>

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!-- Product name Text input-->
                                        <div class="form-group{{ $errors->has('categoryTitle') ? 'has-error' : '' }}">
                                        {{--{{$categoriesData}}--}}

                                            <label class="col-md-4 control-label" for="category_title">Category name *</label>
                                            <div class="col-md-4">
                                                <input id="category_title" name="categoryTitle" value="{{$category->category_title}}" @if($errors->has('categoryTitle'))style=" border-color:red ;" @endif class="form-control input-md"  type="text">
                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('categoryTitle') }}</span>
                                        <!-- SKU Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name_fr">Category icon</label>
                                            <div class="col-md-4">
                                                <input id="category_icon" type="file" name="category_icon"  class="input-md"  >
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('category_icon') }}</span>--}}
                                        <!--Full Description Text area-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="category_description">Description</label>
                                            <div class="col-md-8">
                                                <div action="#" method="POST" class='form-wysiwyg'>
                                                    <textarea name="category_description" id="category_description" class='ckeditor span12' rows="5">{{$category->category_description}}</textarea>
                                                </div>
                                            </div>
                                            {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('category_description') }}</span>--}}
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Product section End-->

                <!-- Marketing section Start-->
                <div class="marketing-sec">
                    <div class="row">
                        <div class="col-md-9">

                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Meta description input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Meta Description</label>
                                            <div class="col-md-8">
                                                <div action="#" method="POST" class='form-wysiwyg'>
                                                    <textarea name="meta_descriptions" id="meta_descriptions"  class='ckeditor span12' rows="5">{{$category->meta_descriptions}}</textarea>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('meta_descriptions') }}</span>--}}
                                        <!-- Meta keywords input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Meta keywords</label>
                                            <div class="col-md-4">
                                                <input id="meta_keywords" name="meta_keywords" value="{{$category->meta_keywords}}" @if($errors->has('meta_keywords'))style=" border-color:red ;" @endif class="form-control input-md"  type="text">
                                            </div>
                                        </div>
{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('meta_keywords') }}</span>--}}
                                        <!-- Product page title input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Category page title</label>
                                            <div class="col-md-4">
                                                <input id="category_page_title" name="category_page_title" @if($errors->has('category_page_title'))style=" border-color:red ;" @endif value="{{$category->category_page_title}}" class="form-control input-md"  type="text">
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('category_page_title') }}</span>--}}
                                        <!-- Clean URL input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Clean URL</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="text" name="cleanURL" value="{{$category->cleanURL}}" @if($errors->has('cleanURL'))style=" border-color:red ;" @endif class="form-control" id="cleanURL" class="form-control" aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">.html</span>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('cleanURL') }}</span>--}}
                                        <!-- Add to Facebook input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="status">Enabled</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="status" id="status" value="1" @if($category->status == 1) checked @endif>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Marketing section End-->
            </div>






            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="add_statusbtn" type="submit" name="" value="Update Category">
            </div>
        </form>

    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>


@endsection