@extends('admin.layouts.master')

@section('content')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    {{--<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">--}}

    {{--<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">--}}
    <style>
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #2caae1;;
            border-right: 16px solid white;;
            border-bottom: 16px solid #2caae1;;
            border-left: 16px solid white;    ;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            position: fixed;
            display: none;
            width: 70px;
            height: 70px;
            top: 60%;
            left: 40%;
            right: 20%;
            bottom: 40%;
            cursor: pointer;

        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

    </style>
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row" id="loading-image">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Users</h3>
                    </div>
                        <a href="{{url('/admin/store-management/StoreLocation')}}" class="btn add_statusbtn">Add Location</a>

                        <br>
                        <br>
                        <div class="table-responsive">
                            <table id="myTable" class="display table" width="100%" >
                                <script type="text/javascript">
                                    $(function() {
                                        $("#hideaftermoment").delay(2000).fadeOut(0);
                                    });
                                </script>
                                @if (session('status'))
                                    <div id="hideaftermoment" class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <script type="text/javascript">
                                    $(function() {
                                        $("#testdiv").delay(2000).fadeOut(0);
                                    });
                                </script>
                                @if($errors->any())
                                    <h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>
                                @endif
                                <thead>
                                <tr>

                                    <th>Store Name</th>
                                    <th>Location Name</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                    <th></th>

                                </tr>
                                </thead>
                                <div class="loader" id="loader" hidden></div>
                                <tbody id="tableBody">
                                @foreach($location as $locationdata)
                                    <tr id="tableRow">

                                        <td><span><a href="{{url('/admin/store-management/updatStoreLocation',[$locationdata->id])}}" >{{$locationdata->store_name}}</a></span></td>
                                        <td><span>{{$locationdata->location_name}}</span></td>
                                        <td><span>{{$locationdata->timefrom}}</span></td>
                                        <td><span>{{$locationdata->timeto}}</span></td>

                                        <td class="left_border_dotted text-center" onclick="return confirm('Are You sure to delete this record?');"><a href="{{url('/admin/store-management/delete',[$locationdata->id])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table_pager">
                            <div class="col-lg-4 col-md-4 text-left">


                            </div>
                            <div class="col-lg-4 col-md-offset-4 col-md-4 text-right">
                                <div class="items-per-page-wrapper" id="page-link" style="float: right">

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')


    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>

    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>





@endsection
