<div class="outer">
    <div class="middle">
        <div class="inner">
            <h1>Access Denied.</h1>
            <h1>You are not authorize to access this page. Please, Contact your Admin for further assistance. </h1>
            <a href={{url('/')}}>Go to Home</a>
        </div>
    </div>
</div>

<style>
    .outer {
        display: table;
        position: absolute;
        height: 100%;
        width: 100%;
    }

    .middle {
        display: table-cell;
        vertical-align: middle;
    }

    .inner {
        margin-left: auto;
        margin-right: auto;
        text-align: center;
    }
</style>