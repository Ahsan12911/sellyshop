@extends('admin.layouts.master')
@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main" class="stock-management-data">
            <div class="row">

            </div>
            <!-- /.col-lg-12 -->
            {{--<form name="frmPurchase" id="frmPurchase" role="form" method="post" action="{{ url('/admin/reports/itemized-report') }}">--}}
            {{--<input type="hidden" name="customer_id" id="customer_id" value="">--}}
            {{csrf_field()}}
            <div class="col-md-12">

                <div class="search-conditions-box clearfix">
                    <div class="col-md-2 pl-0">
                        {{--<button class="border_blue search_btn border_radius3 mr-12 add-record btn add_inputbtn"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Product</button>--}}
                        {{--</div>--}}
                        <select class="e1 form-control" name="products" id="product">
                            <option value="">Select Product</option>
                            @foreach($productsData as $product)
                                <option value="{{$product->id}}">{{$product->product_title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <ul>


                        <li class="col-md-2">

                            <i class="ti-calendar"></i>
                            <input name="ordersource_date" id="daterange"
                                   class="relative_position width_100 input_padding border_blue border_radius3"
                                   type="text" placeholder="Enter Date Range">
                        </li>
                        <li>
                            <button type="submit" class="border_blue search_btn  btn add_inputbtn"
                                    onclick="showProductData()">Itemized Report
                            </button>
                        </li>


                    </ul>

                </div>

                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table" id="tbl_products">
                            <thead>
                            {{--<th width="3%" class="right_border_dotted text-center">Sr</th>--}}
                            <tr>
                                <th>Product Title</th>
                                <th>Unit</th>
                                <th>Date</th>
                                <th>Opening Stock</th>
                                <th>New Stock</th>
                                <th>Total</th>
                                <th>Waste</th>
                                <th>Total Sale</th>
                                <th>Remaining Stock</th>
                                {{--<th>Total Sale</th>--}}
                                {{--<th>Remaining</th>--}}
                                <th>Sale Price</th>
                            </tr>

                            </thead>
                            <tbody id="tbl_products_body">

                            </tbody>
                        </table>

                        <input type="hidden" name="total_products" id="total_products" value="1">
                    </div>
                </div>


                {{--</form>--}}
            </div>
        </div>
    </div>
        <!-- END MAIN CONTENT -->

        @endsection

        @section('footer')
        <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

            <script>


                $(document).ready(function () {

                    $(function () {
                        $('input[name="ordersource_date"]').daterangepicker({
                            autoUpdateInput: false,
                            locale: {
                                cancelLabel: 'Clear'
                            }
                        });

                        $('input[name="ordersource_date"]').on('apply.daterangepicker', function (ev, picker) {
                            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                        });

                        $('input[name="ordersource_date"]').on('cancel.daterangepicker', function (ev, picker) {
                            $(this).val('');
                        });
                        // $('input[name="order_date"]').daterangepicker();

                    });
                })
            </script>

            <script type="text/javascript">


                $(document).ready(function () {

                    jQuery(document).delegate('button.add-record', 'click', function (e) {

                        e.preventDefault();

                        var content = jQuery('#sample_table tr'),
                            size = jQuery('#tbl_products >tbody >tr').length + 1,
                            element = null,
                            element = content.clone();

                        $('#total_products').val(size);

                        /*element.attr('id', 'rec-'+size);
                        element.find('.delete-record').attr('data-id', size);
                        element.appendTo('#tbl_products_body');
                        element.find('.sn').html(size);*/

                        var i = size;

                        //$('#tbl_products_body').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
                        $('#tbl_products_body').append('<tr id="rec-' + i + '">\n' +
                            '                <td>\n' +
                            '                    <select required class="e1 form-control" name="products[]" id="product_' + i + '" onchange="getProductInfo(this.value,' + i + ');">\n' +
                            '                        <option value="">Select Product</option>\n' +
                            '                        @foreach($productsData as $product)\n' +
                            '                            <option value="{{$product->id}}">{{$product->product_title}}</option>\n' +
                            '                        @endforeach\n' +
                            '                    </select>\n' +
                            '                </td>\n' +
                            '                <td width="50" class="left_border_dotted text-center"><a class="btn btn-xs delete-record" data-id="' + i + '"><i class="glyphicon glyphicon-trash"></i></a></td>\n' +
                            '            </tr>');
                    });


                    jQuery(document).delegate('a.delete-record', 'click', function (e) {

                        e.preventDefault();


                        var didConfirm = confirm("Are you sure You want to delete");
                        if (didConfirm == true) {
                            var id = jQuery(this).attr('data-id');
                            var targetDiv = jQuery(this).attr('targetDiv');
                            var quantity = $('#order_quantity_' + id).val();
                            var totalPrice = $('#total_price_' + id).val();

                            var OrderTotalPrice = $('#OrderTotalPrice').val();
                            var OrderTotalQuantity = $('#OrderTotalQuantity').val();
                            jQuery('#rec-' + id).remove();
                            var newTotalPrice = OrderTotalPrice - totalPrice;
                            var newTotalQuantity = OrderTotalQuantity - quantity;
                            $('#OrderTotalQuantity').val(newTotalQuantity);
                            $('#OrderTotalPrice').val(newTotalPrice);


                            size = jQuery('#tbl_products >tbody >tr').length;
                            $('#total_products').val(size);

                            //regnerate index number on table
                            $('#tbl_products_body tr').each(function (index) {
                                $(this).find('span.sn').html(index + 1);
                            });


                            return true;
                        }
                        else {
                            return false;
                        }
                    });
                });

                function changeTotal(id) {
                    var amount = $('#total_price_' + id).val();
                    var total = $('#OrderTotalPrice').val();

                    if ($('#foc_' + id).is(":checked")) {
                        var newAmount = parseInt(total) - parseInt(amount);
                    }
                    else {
                        var newAmount = parseInt(total) + parseInt(amount);
                    }
                    $('#OrderTotalPrice').val(newAmount);
                }

                function showProductData() {
                    var product = $('#product').val();
                    var daterange = $('#daterange').val();


                    $.ajax({

                        type: 'get',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: '{{ url('/admin/reports/itemized-report') }}',
                        data: 'pid=' + product + '&daterange=' + daterange,
                        success: function (data) {
                            $('#tbl_products_body').html(data);
                        },
                        error: function (error) {
//                    alert('error');
                        }
                    });
                }

            </script>
    @endsection
