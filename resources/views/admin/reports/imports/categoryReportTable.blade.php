<table id="stock-log-table" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>Sr. #</th>
        <th>Category Name</th>
        <th>Action Message</th>
        <th>Action By</th>
        <th>Action Time</th>
    </tr>
    </thead>
    <tbody>
    @php
        $counter = 1;

    @endphp
    @foreach($data as $item)
     @php
         $userName = \App\Models\Admin\Users::where('id',$item->user_id)->first()->username;

     @endphp
     <tr>
            <td>{{$counter}}</td>

            <td>{{$item->category_name}}</td>
            <td>{{$item->message}}</td>
            <td>{{$userName}}</td>
            <td>{{date('h:i:s A  d-M-Y',strtotime($item->created_at))}}</td>
        </tr>
        @php

            $counter++;
        @endphp
    @endforeach

    </tbody>
</table>

<script>
    $(document).ready(function () {
        $('#stock-log-table').DataTable({
            paging: false
        });

    });
</script>