@php
    use App\Models\Admin\Products;
    use App\Models\Admin\Orders;
    use App\Models\Admin\OrdersItems;
    use App\Models\Admin\InventoryWastage;
    use App\Models\Admin\InventoryWastageItems;
    use App\Models\Admin\InventoryRecord;
    use Carbon\Carbon;

    $itemarray=[];
    $count=1;
@endphp
@foreach($inventoryRecord as $prod)
    @php
        $title = Products::where('id',$prod['product_id'])->first()->product_title;
    @endphp
    @php
        $totalStock=  InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')->where('product_id', $prod['product_id'])
          ->orderBy('id','DESC')->select('total_stock','record_type')->first();

          $quantity_waste=0;
          $prevousDate=Carbon::parse($prod['created_at'])->subDay(1);
          $prevousDateTwo=Carbon::parse($prod['created_at'])->subDay(2);
          $prevousDateThree=Carbon::parse($prod['created_at'])->subDay(3);
          $openingPrice =InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prevousDate)) . '%')
          ->where('unit',$prod['unit'])->pluck('sale_price')->first();

          $openingPriceTwo =InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prevousDateTwo)) . '%')
          ->where('unit',$prod['unit'])->pluck('sale_price')->first();
          $openingPriceThree =InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prevousDateThree)) . '%')
          ->where('unit',$prod['unit'])->pluck('sale_price')->first();

          $purchase=InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')
          ->where(['product_id'=>$prod['product_id'],'record_type'=>'p'])->get();

          $opening_stock=InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')
          ->where(['product_id'=>$prod['product_id'],'unit'=>$prod['unit']])->first();
          $opening_price=InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')
          ->where(['product_id'=>$prod['product_id'],'unit'=>$prod['unit'],'record_type'=>'p'])->pluck('sale_price')->first();

           $purchaseItem=0;
          foreach ($purchase as $purchas){
          $purchaseItem+=$purchas->quantity;
          }
          $wasteItem=InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')
          ->where(['product_id'=>$prod['product_id'],'record_type'=>'w','unit'=>$prod['unit']])
          ->groupBy('unit') ->selectRaw('sum(quantity) as sum')->first();

    @endphp
@if($prod->record_type == 'w')
    <tr @if( in_array($title.$prod['unit'].date('Y-m-d',strtotime($prod['created_at'])),$itemarray)) hidden  @endif>
        <td>{{$count}}</td>
        <td>{{$title}}</td>
        <td>{{$prod['unit']}}</td>
        <td>{{date('Y-m-d',strtotime($prod['created_at']))}}</td>
        <td>{{$opening_stock->opening_stock}}</td>
        <td>{{ $wasteItem->sum }}</td>
        <td>{{ $opening_price }}</td>
        <td>
            @php
               if($openingPrice != ''){
                $priceonOpening = $opening_stock->opening_stock * $openingPrice;
                  }
               if($openingPriceTwo != '')
                   {
                $priceonOpening =   $opening_stock->opening_stock * $openingPriceTwo;
                   }

               if($openingPriceThree != '')
                   {
                 $priceonOpening =  $opening_stock->opening_stock * $openingPriceThree;
                   }
           $todayPrice =$purchaseItem * $opening_price;
            $totaal=$priceonOpening  + $todayPrice;
            $totalWast=$totaal * $wasteItem->sum;
            $totalQuantity=$opening_stock->opening_stock + $purchaseItem;
            @endphp
            @if($totalQuantity)
                {{$totalWast/$totalQuantity}}
                @else
                {{$totalWast}}
        @endif
                <br>


        </td>

    </tr>
@php

    $itemarray[]=$title.$prod['unit'].date('Y-m-d',strtotime($prod['created_at']));
$count++;
@endphp
    @endif

@endforeach


