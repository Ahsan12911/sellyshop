@php
    use App\Models\Admin\Products;
    use App\Models\Admin\Orders;
    use App\Models\Admin\OrdersItems;
    use App\Models\Admin\InventoryRecord;
    use App\Models\Admin\InventoryWastage;
    use App\Models\Admin\InventoryWastageItems;
  $itemarray=[];
  $saleItemArray=[];
@endphp

@if(count($inventory) > 0)
     @foreach($inventory as $prod)
         @php

             $title = Products::where('id',$prod['product_id'])->first()->product_title;

             $totalStock=  InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')->where('product_id', $prod['product_id'])
             ->orderBy('id','DESC')->select('total_stock','record_type')->first();
              $openingStock=  InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')->where('product_id', $prod['product_id'])
             ->first();
             $purchase=InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')->where(['product_id'=>$prod['product_id'],'record_type'=>'p'])->get();
              $purchaseItem=0;
             foreach ($purchase as $purchas){
             $purchaseItem+=$purchas->quantity;
             }
             $wasteItem=InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')->where(['product_id'=>$prod['product_id'],'record_type'=>'w'])->get();
             $waste=0;
             foreach ($wasteItem as $wasteItm){
                 $waste+=$wasteItm->quantity;
             }
             $dateCount=  InventoryRecord::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')->where('product_id', $prod['product_id'])->count();
                      $sale=0;
                          $itemDetail=OrdersItems::where('created_at', 'LIKE', '%' . date('Y-m-d',strtotime($prod['created_at'])). '%')->where('object_id',$prod['product_id'])
                          ->select('item_quantity','order_id')->get();
                          foreach ($itemDetail as $value){
                         $orderDelivered= \App\Models\Admin\Orders::where('id',$value->order_id)->where('shipping_status_id','=',4)->get();

                      foreach ($orderDelivered as $delivered){
                         if($delivered){
                              $sale += $value->item_quantity;
                             }
                             }
                             }

         @endphp
         @if($prod['record_type']==$totalStock->record_type)
             <tr @if( in_array($title.$prod['unit'].date('Y-m-d',strtotime($prod['created_at'])),$itemarray)) hidden  @endif>
                 <td>{{$title}}</td>
                 <td>{{$prod['unit']}}</td>
                 <td>{{date('Y-m-d',strtotime($prod['created_at']))}}</td>
                 <td>{{$openingStock['opening_stock']}}</td>
                 <td>{{$purchaseItem}}</td>
                 <td>{{$sale}}</td>
                 <td>{{$waste}}</td>
                 <td>@if($prod['record_type']==$totalStock->record_type){{($openingStock['opening_stock'] + $purchaseItem) -($waste + $sale) }}@else{{$prod['total_stock']}}@endif</td>
             </tr>

             @php

                 $itemarray[]=$title.$prod['unit'].date('Y-m-d',strtotime($prod['created_at']));
             @endphp
         @endif
     @endforeach

@else

    @foreach($saleItem as $itemSale)
        @php

            $title = \App\Models\Admin\Products::where('id',$itemSale['object_id'])->first();
            $orderDelivered= \App\Models\Admin\Orders::where('id',$itemSale['order_id'])->where('shipping_status_id','=',4)->get();
              $totalSale=0;

            foreach ($orderDelivered as $delivered){
            if($delivered){
            $totalSale += $itemSale['item_quantity'];
            }
            }


        @endphp
        <tr>
            <td>{{$title->product_title}}</td>
            <td>{{$title->unit}}</td>
            <td>{{date('Y-m-d',strtotime($itemSale['created_at']))}}</td>
            <td></td>
            <td></td>
            <td>{{ $totalSale}}</td>
            <td></td>
            <td></td>
        </tr>
    @endforeach

     @endif


