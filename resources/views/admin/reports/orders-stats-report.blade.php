@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/jquery.dataTables.min.css') }}">

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Statistics</h3>
                        <br>
                        <button class="add_statusbtn">Add Status</button>
                    </div>
                    <div class="mt-35">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#sale-statistics">Payment Statistics</a></li>
                            <li><a data-toggle="tab" href="#home">Shipment Statistics</a></li>
                            {{--<li><a data-toggle="tab" href="#menu1">Best Sellers</a></li>--}}

                        </ul>
                        <div class="tab-content">
                            <div id="sale-statistics" class="tab-pane fade in active">

                                <div>
                                    <!-- bar chart -->
                                    <div class="panel mb-5">
                                        <div class="panel-heading">
                                            <h2 class="panel-title">Sales</h2>
                                            <div class="display-ib order_status">
                                                {{--<a  data-toggle="tab" href="#declined">Declined</a>--}}
                                                {{--<a  data-toggle="tab" href="#delivered">Paid</a>--}}
                                                {{--<a  data-toggle="tab" href="#refund">Refund</a>--}}
                                                {{--<a  data-toggle="tab" href="#">Credit Sales</a>--}}
                                            </div>

                                        </div>
                                        <table class="table table_stats">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Today</th>
                                                <th>This week</th>
                                                <th>This month</th>
                                                <th>This year</th>
                                                <th>All time</th>
                                            </tr>
                                            </thead>
                                            <tbody class="">
                                            <tr>

                                                <td>Awaiting</td>
                                                <td>

                                                @php($awaiting=$orders->where('payment_status_id','=',1)->count())
                                            {{$awaiting}}
                                                </td>
                                                <td>

                                                @php($awaitingweek=$ordersweek->where('payment_status_id','=',1)->count())
                                            {{$awaitingweek}}
                                                </td>
                                                <td>

                                                @php($awaitingmonthly=$orderemonthly->where('payment_status_id','=',1)->count())
                                            {{$awaitingmonthly}}
                                                </td>
                                                <td>

                                                @php($awaitingyearly=$ordersyearly->where('payment_status_id','=',1)->count())
                                            {{$awaitingyearly}}
                                                </td>
                                                <td>

                                                @php($awaiting=$orders->where('payment_status_id','=',1)->count())
                                            {{$awaitingyearly}}
                                                </td>

                                               </tr>

                                            <tr>
                                                <td>Paid</td>
                                                <td>  @php($paid=$orders->where('payment_status_id','=',4)->count())
                                            {{$paid}}       </td>
                                                <td>  @php($paidweekly=$ordersweek->where('payment_status_id','=',4)->count())
                                            {{$paidweekly}}       </td>
                                                <td>  @php($paidmonthly=$orderemonthly->where('payment_status_id','=',4)->count())
                                            {{$paidmonthly}}       </td>
                                                <td>  @php($paidyearly=$ordersyearly->where('payment_status_id','=',4)->count())
                                            {{$paidyearly}}       </td>
                                                <td>
                                            {{$paidyearly}}       </td>

                                               </tr>


                                            <tr>

                                        <td>Declined</td>
                                                <td>  @php($decline=$orders->where('payment_status_id','=',5)->count())
                                            {{$decline}}       </td>
                                                <td>  @php($declineweekly=$ordersweek->where('payment_status_id','=',5)->count())
                                            {{$declineweekly}}       </td>
                                                <td>  @php($declinemonthly=$orderemonthly->where('payment_status_id','=',5)->count())
                                            {{$declinemonthly}}       </td>
                                                <td>  @php($declineyearly=$ordersyearly->where('payment_status_id','=',5)->count())
                                            {{$declineyearly}}       </td>
                                                <td>  {{$declineyearly}}       </td>

                                               </tr>




                                            <tr>
                                                <td>Rejected</td>
                                                <td>  @php($reject=$orders->where('payment_status_id','=',7)->count())
                                            {{$reject}}       </td>
                                          <td>  @php($rejectweek=$ordersweek->where('payment_status_id','=',7)->count())
                                            {{$rejectweek}}       </td>
                                          <td>  @php($rejectmonth=$orderemonthly->where('payment_status_id','=',7)->count())
                                            {{$rejectmonth}}       </td>
                                          <td>  @php($rejectyear=$ordersyearly->where('payment_status_id','=',7)->count())
                                            {{$rejectyear}}       </td>
                                          <td>
                                            {{$rejectyear}}       </td>

                                               </tr>
                                            <tr>
                                                <td>Deduct from salary</td>

                                        <td>       @php($dfs=$orders->where('payment_status_id','=',10)->count())
                                            {{$dfs}}    </td>
                                        <td>       @php($dfsweek=$ordersweek->where('payment_status_id','=',10)->count())
                                            {{$dfsweek}}    </td>
                                        <td>       @php($dfsmonth=$orderemonthly->where('payment_status_id','=',10)->count())
                                            {{$dfsmonth}}    </td>
                                        <td>       @php($dfsyear=$ordersyearly->where('payment_status_id','=',10)->count())
                                            {{$dfsyear}}    </td>
                                        <td>
                                            {{$dfsyear}}    </td>

                                               </tr>
                                            <tr>
                                                <td>FOC</td>

                                        <td>       @php($foc=$orders->where('payment_status_id','=',11)->count())
                                            {{$foc}}    </td>


                                        <td>       @php($focweek=$ordersweek->where('payment_status_id','=',11)->count())
                                            {{$focweek}}    </td>


                                        <td>       @php($focmonth=$orderemonthly->where('payment_status_id','=',11)->count())
                                            {{$focmonth}}    </td>


                                        <td>       @php($focyearly=$ordersyearly->where('payment_status_id','=',11)->count())
                                            {{$focyearly}}    </td>


                                        <td>
                                            {{$focyearly}}    </td>

                                               </tr>

                                            <tr>
                                                <td>Total</td>

                                        <td>       @php($totald=$orders->count())
                                            {{$totald}}    </td>


                                        <td>       @php($totalw=$ordersweek->count())
                                            {{$totalw}}    </td>


                                        <td>       @php($totalm=$orderemonthly->count())
                                            {{$totalm}}    </td>


                                        <td>       @php($tatoly=$ordersyearly->count())
                                            {{$tatoly}}    </td>


                                        <td>
                                            {{$tatoly}}    </td>

                                               </tr>


                                            </tbody>
                                        </table>
                                        <div class="panel-body">

                                                               <span class="fancy-checkbox custom-bgcolor-blue">
                                                                  <label>
                                                                      {{--<input type="checkbox" id="addRemoveBarDataset">--}}
                                                                      {{--<spa  n>Add/Remove Dataset</span>--}}
                                                                  </label>
                                                                  </span>

                                        </div>
                                    </div>
                                    <!-- end bar chart -->
                                </div>





                            </div>




                            <div id="home" class="tab-pane fade">
                                <div class="mb-20">
                                    <img class="mr-5" src="{{url('public/admin/assets/img/analiytics.png')}}">
                                    <a href="#">Configure the Google Analytics module to view the advanced statistics</a>
                                    <p class="mt-20">This section displays order processing statistics</p>
                                </div>


                                <table class="table table_stats">
                                    <thead>
                                    <tr>

                                        <th>
                                            {{--<input type="text" placeholder="Email or Company name">--}}
                                        </th>
                                        <th>Today</th>
                                        <th>This week</th>
                                        <th>This month</th>
                                        <th>This year</th>
                                        <th>All time</th>

                                    </tr>
                                    </thead>
                                    <tbody class="">
                                    <tr>
                                        @php

                                           $dayNewOrders= $dayordernew->where('shipping_status_id','=',1)->count();
                                           $dayorderprocess= $dayordernew->where('shipping_status_id','=',2)->count();
                                           $dayorderdeliverd= $dayordernew->where('shipping_status_id','=',4)->count();
                                           $dayorderdoutOfRadius= $dayordernew->where('shipping_status_id','=',5)->count();
                                           $dayorderrejected= $dayordernew->where('shipping_status_id','=',6)->count();
                                           $dayordernotdeliver= $dayordernew->where('shipping_status_id','=',7)->count();
                                            $totaldayorder=$dayordernew->count();


                                           $weekordernew= $weekorders->where('shipping_status_id','=',1)->count();
                                           $weekorderprocess= $weekorders->where('shipping_status_id','=',2)->count();
                                           $weekorderdeliver= $weekorders->where('shipping_status_id','=',4)->count();
                                           $weekorderoutOfRadius= $weekorders->where('shipping_status_id','=',5)->count();
                                           $weekorderrejected= $weekorders->where('shipping_status_id','=',6)->count();
                                           $weekorderNotdeliver= $weekorders->where('shipping_status_id','=',7)->count();
                                            $totalweekorder=$weekorders->count();

                                           $monthordernew= $monthlyOrders->where('shipping_status_id','=',1)->count();
                                           $monthorderprocess= $monthlyOrders->where('shipping_status_id','=',2)->count();
                                           $monthorderdeliver= $monthlyOrders->where('shipping_status_id','=',4)->count();
                                           $monthorderoutOfRadius= $monthlyOrders->where('shipping_status_id','=',5)->count();
                                           $monthorderrejected= $monthlyOrders->where('shipping_status_id','=',6)->count();
                                           $monthordernotdeliver= $monthlyOrders->where('shipping_status_id','=',7)->count();
                                            $totalmonthorder=$monthlyOrders->count();

                                           $yearorderNew= $yearlyOrders->where('shipping_status_id','=',1)->count();
                                           $yearorderprocess= $yearlyOrders->where('shipping_status_id','=',2)->count();
                                           $yearorderdelivered= $yearlyOrders->where('shipping_status_id','=',4)->count();
                                           $yearorderoutOfRadius= $yearlyOrders->where('shipping_status_id','=',5)->count();
                                           $yearorderrejected= $yearlyOrders->where('shipping_status_id','=',6)->count();
                                           $yearorderNotdelivered= $yearlyOrders->where('shipping_status_id','=',7)->count();

                                         $totalyearorder=$yearlyOrders->count();
                                        @endphp

                                        <td><span>New</span></td>
                                        <td><span>{{$dayNewOrders}}</span></td>
                                        <td><span>{{$weekordernew}}</span></td>
                                        <td><span>{{$monthordernew}}</span></td>
                                        <td><span>{{$yearorderNew}}</span></td>
                                        <td><span>{{$yearorderNew}}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>Processed</span></td>
                                        <td><span>{{$dayorderprocess}}</span></td>
                                        <td><span>{{$weekorderprocess}}</span></td>
                                        <td><span>{{$monthorderprocess}}</span></td>
                                        <td><span>{{$yearorderprocess}}</span></td>
                                        <td><span>{{$yearorderprocess}}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>delivered</span></td>
                                        <td><span>{{$dayorderdeliverd}}</span></td>
                                        <td><span>{{$weekorderdeliver}}</span></td>
                                        <td><span>{{$monthorderdeliver}}</span></td>
                                        <td><span>{{$yearorderdelivered}}</span></td>
                                        <td><span>{{$yearorderdelivered}}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>Will no delivered</span></td>
                                        <td><span>{{$dayordernotdeliver}}</span></td>
                                        <td><span>{{$weekorderNotdeliver}}</span></td>
                                        <td><span>{{$monthordernotdeliver}}</span></td>
                                        <td><span>{{$yearorderNotdelivered}}</span></td>
                                        <td><span>{{$yearorderNotdelivered}}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>rejected</span></td>
                                        <td><span>{{$dayorderrejected}}</span></td>
                                        <td><span>{{$weekorderrejected}}</span></td>
                                        <td><span>{{$monthorderrejected}}</span></td>
                                        <td><span>{{$yearorderrejected}}</span></td>
                                        <td><span>{{$yearorderrejected}}</span></td>
                                    </tr>

                                    <tr>
                                        <td><span>Out of Radius</span></td>
                                        <td><span>{{$dayorderdoutOfRadius}}</span></td>
                                        <td><span>{{$weekorderoutOfRadius}}</span></td>
                                        <td><span>{{$monthorderoutOfRadius}}</span></td>
                                        <td><span>{{$yearorderoutOfRadius}}</span></td>
                                        <td><span>{{$yearorderoutOfRadius}}</span></td>

                                    </tr>
                                   <tr>
                                        <td><span>Total Orders</span></td>
                                        <td><span id="dayorders">{{$totaldayorder}}</span></td>
                                        <td><span id="weakorders">{{$totalweekorder}}</span></td>
                                        <td><span  id="monthorders">{{$totalmonthorder}}</span></td>
                                        <td><span  id="yearlyorders">{{$totalyearorder}}</span></td>
                                        <td><span>{{$totalyearorder}}</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <div class="mb-20">
                                    <p>This section lists 10 best selling products for today, last week, last month, last year and all time.</p>
                                    <h3 class="font-normal">Top 10 Products</h3>
                                    <div class="selectors mt-20">
                                        <strong>For the Vendor:</strong>
                                        <input type="text" placeholder="Email or Company name">
                                        <a href="#">Reset</a>
                                        <div class="products mt-20">
                                            <strong class="mr-20">Products:</strong>
                                            <strong class="mr-20">All</strong>
                                            <a href="#" class="mr-20">Only available</a>
                                            <strong class="mr-20">For the Period:</strong>
                                            <a href="#" class="mr-20">Today</a>
                                            <a href="#" class="mr-20">This week</a>
                                            <a href="#" class="mr-20">This month</a>
                                            <a href="#" class="mr-20">This year</a>
                                        </div>
                                    </div>

                                </div>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>

                                        <th>Position</th>
                                        <th>Products</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><span>1.</span></td>
                                        <td><a href="#">Orange</a></td>
                                    </tr>
                                    <tr>
                                        <td><span>2.</span></td>
                                        <td><a href="#">Apples</a></td>
                                    </tr>
                                    <tr>
                                        <td><span>3.</span></td>
                                        <td><a href="#">Orange</a></td>
                                    </tr>
                                    <tr>
                                        <td><span>4.</span></td>
                                        <td><a href="#">Banana</a></td>
                                    </tr>
                                    <tr>
                                        <td><span>5.</span></td>
                                        <td><span>—</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>6.</span></td>
                                        <td><span>—</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>7.</span></td>
                                        <td><span>—</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>8.</span></td>
                                        <td><span>—</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div>
                        <hr>
                        {{--<button class="btn save_changesbtn">Save Changes</button>--}}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="sales_statictis">
                        <p class="p15"><i class="ti-bar-chart-alt mr-10"></i>sale Statistics <i class="ti-angle-down pull-right"></i></p>
                        <div class="content">
                            <div class="revenue">
                                <p class="display-ib">Revenue:</p><span class="mr-5">Rs0.00</span>
                            </div>


                            <div class="order">
                                <p class="display-ib">Orders:</p><span class="mr-5">0</span>
                            </div>

                            <small class="display_block">Yesterday Rs.0.00 / 0</small>
                        </div>
                    </div>

                    <div class="rss-feed-content">
                        <h2>Selly News</h2>
                        <h3>Nov 22, 2017</h3>
                        <a href="#">Why Website Color Schemes Matter and How to Use Them Right</a>
                        <h3>Nov 7, 2017</h3>
                        <a href="#">Shopping Cart Partners with Pitney Bowes to Help You Save on USPS Rates and Better Meet Customer’s Delivery Expectations</a>
                        <h3>Nov 1, 2017</h3>
                        <a href="#">Instagram Marketing For Ecommerce: The Complete Guide</a>
                        <hr>
                        <div class="display-ib">
                            <img src="assets/img/rss.png">
                            <a href="#">RSS feed</a>
                        </div>
                        <div class="display-ib pull-right">
                            <a href="#">Our Blog</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>


<!-- Javascript -->

<script type="javascript">

    $(function()
    {
        alert('loading');

        var departmentLabels = ['1', '2', '3', '4', '5', '6', '7' , '8' , '9' , '10' , '11' , '12' , '13' , '14' , '15' , '16' , '17' , '18' , '19' , '20' , '21' , '22' , '23' , '24' , '25' , '26' , '27' , '28' ,
            '29' , '30' , '31'];
        var departmentData = [9.9, 15, 20, 30, 100, 60, 130, 180, 90, 100 , 120 , 80 , 130, 145, 200, 30, 40, 20, 70, 120, 90, 100 , 120 , 80 , 130, 145, 200, 30, 15, 20, 70, 120];

        var designationLabels = ['Project Managers', 'Supervisors', 'Team Leaders', 'Executives' , 'Internees'];
        var designationData = [9.9, 20, 30, 40 , 50];

        var scalesOptions = {
            xAxes: [
                {
                    gridLines:
                        {
                            display: false
                        }
                }],
            yAxes: [
                {
                    gridLines:
                        {
                            color: '#eff3f6',
                            drawBorder: false,
                        },
                }]
        };


        // bar chart
        var ctxBarChart = document.getElementById("bar-chart").getContext("2d");
        var barChart = new Chart(ctxBarChart,
            {
                type: 'bar',
                data:
                    {
                        labels: departmentLabels,
                        datasets: [
                            {
                                data: departmentData,
                                label: 'Selly',
                                borderColor: 'transparent',
                                backgroundColor: 'rgba(1,153,248,.50)'
                            }]
                    },
                options:
                    {
                        responsive: true,
                        scales: scalesOptions,
                    }
            });

        $("#department").click(function(){

            var count = departmentLabels.length;
            barChart.data.labels = [];

            barChart.data.datasets.pop();

            var newBarDataset = {
                data: departmentData,
                label: 'Department',
                borderColor: 'transparent',
                backgroundColor: 'rgba(1,153,248,.50)',
            };
            for(var i=0; i<count; i++){
                barChart.data.labels.push(departmentLabels[i]);
            }

            barChart.data.datasets.push(newBarDataset);
            barChart.update();
        });

        $("#designation").click(function(){

            var count = designationLabels.length;
            barChart.data.labels = [];

            barChart.data.datasets.pop();

            var newBarDataset = {
                data: designationData,
                label: 'Designation',
                borderColor: 'transparent',
                backgroundColor: 'rgba(1,153,248,.50)',
            };
            for(var i=0; i<count; i++){
                barChart.data.labels.push(designationLabels[i]);
            }

            barChart.data.datasets.push(newBarDataset);

            barChart.update();
        });

        $("#salaries").click(function(){

            var count = salaryLabels.length;
            barChart.data.labels = [];

            barChart.data.datasets.pop();

            var newBarDataset = {
                data: salaryData,
                label: 'Salaries',
                borderColor: 'transparent',
                backgroundColor: 'rgba(1,153,248,.50)',
            };
            for(var i=0; i<count; i++){
                barChart.data.labels.push(salaryLabels[i]);
            }

            barChart.data.datasets.push(newBarDataset);
            barChart.update();
        });

        $("#status").click(function(){

            var count = statusLabels.length;
            barChart.data.labels = [];

            barChart.data.datasets.pop();

            var newBarDataset = {
                data: statusData,
                label: 'Status',
                borderColor: 'transparent',
                backgroundColor: 'rgba(1,153,248,.50)',
            };
            for(var i=0; i<count; i++){
                barChart.data.labels.push(statusLabels[i]);
            }

            barChart.data.datasets.push(newBarDataset);
            barChart.update();
        });

    });
</script>

@endsection