@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/jquery.dataTables.min.css') }}">

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-12">
                    <div class="report-page">
                        <h3 class="mt-35 font-normal mb-20 display-ib">{{$productTitle}}</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title">
                                    <p>Product Details</p>
                                </div>

                                <div class="row mb-20">
                                    <div class="col-md-6">
                                        <b>Total Items Purchased</b>:<span>{{$TotalItemsPurchased}}</span>
                                    </div>
                                    <div class="col-md-6">
                                        <b style="color: #008000;">Net Profit</b>:<span style="color: #008000;">{{$NetProfit}}</span>
                                    </div>
                                </div>
                                <div class="row mb-20">
                                    <div class="col-md-6">
                                        <b>Total Items Sold</b>:<span>{{$TotalItemsSold}}</span>
                                    </div>
                                    <div class="col-md-6">
                                        <b style=" color: #ff0000;">Items in Stock</b>:<span style="color: #ff0000">{{$ItemsInStock}}</span>
                                    </div>
                                </div>
                                <div class="row mb-20">
                                    <div class="col-md-12">
                                        <b>Total expense on purchase</b>:<span>{{$TotalExpenseOnPurchase}}</span>
                                    </div>
                                </div>
                                <div class="row mb-20">
                                    <div class="col-md-12">
                                        <b>Total income from Sale</b>:<span>{{$TotalIncomeFromSale}}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mt-35 col-md-12">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#purchase">Purchase</a></li>
                                            <li><a data-toggle="tab" href="#sale">Sale</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="purchase" class="tab-pane fade in active">
                                                <table class="table table-hover" id="datatable1">
                                                    <thead>
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Invoice#</th>
                                                        <th>Quantity</th>
                                                        <th>Unit</th>
                                                        <th>Unit Price</th>
                                                        <th>Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($purchaseHistory as $purchaseInfo)
                                                        @php
                                                            $purchaseTotal = $purchaseInfo->quantity*$purchaseInfo->unit_price;
                                                        @endphp
                                                        <tr>
                                                            <td>{{$purchaseInfo->purchase_date}}</td>
                                                            <td><a href="#">{{$purchaseInfo->invoice_no}}</a></td>
                                                            <td>{{$purchaseInfo->quantity}}</td>
                                                            <td>{{$purchaseInfo->unit}}</td>
                                                            <td>{{$purchaseInfo->unit_price}}</td>
                                                            <td>{{$purchaseTotal}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="sale" class="tab-pane fade">
                                                <table class="table table-hover" id="datatable2">
                                                    <thead>
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>Order#</th>
                                                            <th>Quantity</th>
                                                            <th>Unit</th>
                                                            <th>Unit Price</th>
                                                            <th>Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($saleHistory as $saleInfo)
                                                        @php
                                                            $saleTotal = $saleInfo->item_quantity*$saleInfo->price;
                                                        @endphp
                                                        <tr>
                                                            <td>{{$saleInfo->created_at}}</td>
                                                            <td><a href="#">{{$saleInfo->orderNumber}}</a></td>
                                                            <td>{{$saleInfo->item_quantity}}</td>
                                                            <td>{{$saleInfo->quantity_unit_id}}</td>
                                                            <td>{{$saleInfo->price}}</td>
                                                            <td>{{$saleTotal}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>

@endsection

@section('footer')
    <script>
        $(document).ready(function() {
            $('#datatable1').DataTable();
            $('#datatable2').DataTable();
        } );
    </script>

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/jquery.dataTables.min.js') }}"></script>

@endsection