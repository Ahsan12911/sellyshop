@extends('admin.layouts.master')

{{--<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">--}}
{{--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>--}}

@section('content')
    @php

        use App\Models\Admin\Users;
    @endphp
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main" class="stock-management-data">
            <div class="row">
                <form name="frmSearch" id="frmSearch" role="form" method="post"
                      action="{{ url('/admin/reports/ordersource/Search') }}">
                    {{csrf_field()}}
                    <div class="search-conditions-box clearfix">
                        <ul>
                            <li class="col-md-2">

                                <i class="ti-calendar"></i>
                                <input name="ordersource_date" id="daterange"
                                       class="relative_position width_100 input_padding border_blue border_radius3"
                                       type="text" placeholder="Enter Date Range">
                            </li>
                            <li class="col-md-1">
                                <button class="border_blue search_btn border_radius3">Search</button>
                            </li>


                        </ul>
                        <div class="clearfix"></div>

                    </div>

                </form>

                <div class="panel-body">
                    <div class="tab-content">
                        <form   method="post" action="{{url('/admin/reports/ordersourceReportExcel')}}" onsubmit="return confirm('Are you sure to delete these records?');">
                            {{csrf_field()}}
                        <div class="tab-pane fade in active" id="tab1web">
                            <table class="table order_table" id="orderSources">
                                <thead>
                                <tr>
                                    <script type="text/javascript">
                                        function selectAll(status) {
                                            $('.multiplerow').each(function () {
                                                $(this).prop('checked', status);
                                            });

                                        }
                                    </script>

                                    {{--<th>--}}
                                        {{--<input type="checkbox" onclick='selectAll(this.checked)' value="Select All"/>--}}
                                    {{--</th>--}}
                                    <th>SR.</th>
                                    <th>CSR</th>
                                    <th><i class="fa fa-mobile mobile-icon" aria-hidden="true"></i></th>
                                    <th><i class="fa fa-television television-icon" aria-hidden="true"></i></th>
                                    <th><i class="fa fa-phone mobile-icon" aria-hidden="true"></i></th>
                                    <th>Total Order</th>

                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $total= $weborders->count();
                               $web=$weborders->where('ordersource','=',1)->count();
                               $mobile=$weborders->where('ordersource','=',0)->count();
                               $cell=$weborders->where('ordersource','=',2)->count();
                                @endphp
                                @php
                                    $processCSR=  isset($processBy)?$processBy:'';
                                    $CSr=$processCSR->count();

                                     $processWeb=  isset($processByweb)?$processByweb:'';
                                    $web=$processWeb->count();

                                     $processMobil=  isset($processBymobil)?$processBymobil:'';
                                    $mobil=$processMobil->count();

                                $maxnum = max($web,$CSr,$mobil);
                                $counter=1;
                                $csrTotal='';
                                @endphp
                                @for($i=1; $i<$maxnum ; $i++)
                                    <tr>
                                        @php
                                        if($web == $maxnum){
                                           $username= $processWeb[$i]['ordersprocess']['username'];
                                            }
                                        elseif($CSr == $maxnum)
                                            {
                                          $username=  $processCSR[$i]['ordersprocess']['username'];
                                            }
                                        elseif($mobil == $maxnum)
                                            {
                                          $username=  $processMobil[$i]['ordersprocess']['username'];
                                        }
                                        @endphp
                                        {{--<td class="right_border_dotted"><input type="checkbox" class="multiplerow" id="multiplerow" name="delete[]" value=""></td>--}}
                                        <td>{{$counter}}</td>
                                        <td>
                                            {{$username}}
                                            {{--@if($web == $maxnum)--}}
                                                {{--{{$processWeb[$i]['ordersprocess']['username']}}--}}
                                            {{--@elseif($CSr == $maxnum)--}}
                                                {{--{{$processCSR[$i]['ordersprocess']['username']}}--}}
                                            {{--@elseif($mobil == $maxnum)--}}
                                                {{--{{$processMobil[$i]['ordersprocess']['username']}}--}}
                                            {{--@endif--}}
                                        </td>
                                        <td>
                                            @if($i<$mobil)
                                                {{$processMobil[$i]['count']}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($i<$web)
                                                {{$processWeb[$i]['count']}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($i<$CSr)
                                                {{$processCSR[$i]['count']}}
                                            @endif
                                        </td>
                                        <td>
                                            @php
                                                if($i<$mobil){
                                                  $coutMobille=$processMobil[$i]['count'];
                                              }if($i<$web){
                                                  $coutWeb=$processWeb[$i]['count'];
                                              }if($i<$CSr){
                                                  $coutCal=$processCSR[$i]['count'];
                                              }
                                              $csrTotal= $coutMobille + $coutWeb + $coutCal;

                                            @endphp
                                            {{$csrTotal}}
                                        </td>
                                    </tr>
                                    @php
                                        $counter ++;
                                    @endphp
                                @endfor


                                </tbody>
                            </table>

                        </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    </div>


@endsection

@section('footer')
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

    <script>

        $(document).ready(function () {

            $(function () {
                $('input[name="ordersource_date"]').daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                        cancelLabel: 'Clear'
                    }
                });

                $('input[name="ordersource_date"]').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                });

                $('input[name="ordersource_date"]').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });
                // $('input[name="order_date"]').daterangepicker();

            });
        })


    </script>
    <script>

        $(document).ready(function() {
            $('#orderSources').DataTable();
        } );
    </script>
@endsection