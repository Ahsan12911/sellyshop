@extends('admin.layouts.master')
@section('content')
    <div class="main">
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Stock Log</h3>
                        <br>
                        <form class="form-inline get_stock_log" action="">
                            <div class="form-group mr-20">
                                <input type="text" class="form-control" id="product_name" placeholder="Enter Product Name" required>
                            </div>
                            <button type="submit" class="btn btn-success">Get Log</button>
                        </form>
                    </div>
                    <div class="mt-35">
                        <div class="tab-content">
                            <div id="stock-log" class="tab-pane fade in active">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="clearfix"></div>
@endsection
@section('footer')
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script>
        $('.get_stock_log').submit(function (e) {
            e.preventDefault();
            var productName = $('#product_name').val();
            $.ajax({
                url: "{{ url('/admin/reports/getStockReport') }}",
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {'productName': productName},
                success: function (data) {
                    $('#stock-log').html(data);
                }
            });
        });
    </script>
@endsection