@extends('admin.layouts.master')
@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main" class="stock-management-data">
            <div class="row">
                <form name="frmPurchase" id="frmPurchase" role="form" method="post" action="{{ url('/admin/orders-management/place-order') }}">
                    <input type="hidden" name="customer_id" id="customer_id" value="">
                    <input type="hidden" name="orderSource" id="customer_id" value="csr">
                    <input type="hidden" name="fullfilmentStatus" id="customer_id" value="csr">
                    {{csrf_field()}}
                    <div class="col-md-12">
                        <h3 class="mt-35 font-normal mb-20">Take Order</h3>
                        <script type="text/javascript">
                            $(function() {
                                $("#hideaftermoment").delay(2000).fadeOut(0);
                            });
                        </script>
                        @if (session('status'))
                            <div id="hideaftermoment" class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="table_pager clearfix mt-20 product-savechanges-box mb-20">
                            <div class="col-md-12 p-0">
                                <h4 class="text-left mt-0">Customer Detail</h4>
                                <div class="col-md-3 pl-0 mb-10">
                                    <input type="text" name="search_keyword" id="search_keyword" placeholder="Name, Email, Phone" onkeyup="searchUserRecord(this.value)">
                                </div>
                                <div class="col-md-3 pl-0 mb-10" id="usersdata">
                                    <select class="form-control">
                                        <option value="">
                                            Select Customer
                                        </option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3 pl-0">
                                    <input type="text" name="customer_name" id="customer_name" placeholder="Name" required autofocus >
                                </div>
                                <div class="col-md-3 pl-0">
                                    <input type="text" name="customer_email" id="customer_email" placeholder="Email" required autofocus >
                                </div>
                                <div class="col-md-3 p-0">
                                    <input type="text" name="customer_number" id="customer_number" placeholder="Phone" required autofocus >
                                </div>

                                <div class="clearfix"></div>
                                <div class="mt-20">
                                    {{--<form class="form-horizontal" role="form">--}}
                                    <fieldset>
                                        <!-- Text input-->
                                        <div class="form-group mb-0">
                                            <div class="col-md-5 pr-0">
                                                <input type="text" name="Address" id="Address" placeholder="Address" class="form-control" >
                                            </div>
                                            <div class="col-md-2 pr-0">
                                                <select name="location" id="location" class="form-control">
                                                    <option value="">Select location</option>
                                                    @foreach($location as $locationid)
                                                        <option value="{{$locationid->id}}">{{$locationid->store_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <select class="form-control" required name="calltype">
                                                    <option value="">
                                                        Select call option
                                                    </option>
                                                    <option value="In bounded Call">In bounded Call</option>
                                                    <option value="Out bounded call">Out bounded call</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            Old Customer <input type="checkbox" name="update" id="update" >
                                        </div>
                                    </fieldset>

                                </div>



                            </div>
                        </div>
                        <br>
                        <div class="search-conditions-box">
                            <div class="col-md-2 pl-0 stock-arrival-date">
                                <label>Order Date</label>
                                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                    <input name="order_date" id="order_date" value="" type="text" class="form-control" required>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 pl-0 stock-arrival-date">
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-2 pl-0">
                                <button id="addRow" class="border_blue search_btn border_radius3 mr-12 add-record"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Product</button>
                            </div>
                            <div class="col-md-2 pl-0">
                                <button type="submit" id="placeOrder" class="border_blue search_btn border_radius3 mr-12">Place Order</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table" id="tbl_products">
                                <thead>
                                <tr>
                                    {{--<th width="3%" class="right_border_dotted text-center">Sr</th>--}}
                                    <th width="7%">Product</th>
                                    <th width="5%">Select Units</th>
                                    <th width="5%">Available Quantity</th>
                                    <th width="5%">Unit Price</th>
                                    <th width="5%">Quantity</th>
                                    <th width="5%">Total Price</th>
                                    <th width="1%">FOC</th>
                                    <th width="1%"  >Reason</th>
                                    <th width="1%"  >Reference</th>
                                    <th width="3%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tbl_products_body">
                                <tr>
                                    {{--<td class="right_border_dotted text-center">1</td>--}}
                                    <td>
                                        <select class="e1 form-control" name="products[]" id="product_1" onchange="getProductInfo(this.value,'1');" required>
                                            <option disabled="disabled" selected value="">Select Product</option>
                                            @foreach($productsData as $product)
                                                <option value="{{$product->id}}">{{$product->product_title}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="" id="unit1">
                                        {{--<select class="e1 form-control" name="units[]" onchange="setunitPrice()"  required>--}}

                                        {{--</select>--}}
                                    </td>
                                    <td><input type="text" name="openingStocks[]" id="opening_stock_1" readonly style="background: #ddd;" class="disabledControl"></td>
                                    <td><input type="text" name="costPrices[]" id="cost_price_1" readonly style="background: #ddd;"></td>
                                    <td><input type="text" name="orderQuantity[]" id="order_quantity_1" required onkeyup="updateSalePrice(this.value,'1');" class="quantity"></td>
                                    <td><input readonly style="background: #ddd;" type="text" name="totalPrices[]" id="total_price_1" class="price" required></td>
                                    <td><input onchange="changeTotal(1)" type="checkbox" name="focs[]" id="foc_1"></td>
                                    <td><select hidden id="focReason_1" onchange="showRefrence(1)" name="reason[]">
                                            <option>Select Reason</option>
                                            <option value="Gift">Gift</option>
                                            <option value="Sample">Sample</option>
                                            <option value="Replacement">Replacement</option>
                                            <option value="Other">Other</option>
                                        </select></td>
                                    <td><input type="text" name="orderReference[]" hidden id="orderReference_1" ></td>
                                    <td width="50" class="left_border_dotted text-center">{{--<a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>--}}</td>
                                </tr>
                                </tbody>
                                <tr id="getTotal" class="getTotal">
                                    <td colspan="4" class="text-right"><b>Total</b></td>
                                    <td><input type="text" name="OrderTotalQuantity" id="OrderTotalQuantity" readonly style="background: #ddd;"></td>
                                    <td><input type="text" name="OrderTotalPrice" id="OrderTotalPrice" readonly style="background: #ddd;"></td>
                                    <td></td>
                                </tr>
                            </table>

                            <input type="hidden" name="total_products" id="total_products" value="1">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">



        function updateSalePrice(val,divid)
        {
            var totalPrice;
            var totalnoofproductsselected = $('#total_products').val();
            var costPrice = parseInt($('#cost_price_'+divid).val());
            var is_foc = parseInt($('#foc_'+divid).val());
            var quantity = parseInt(val);
            var openingStock=$('#opening_stock_'+divid).val()

            if(openingStock < quantity ){
                alert('Please select available quantity');
                $('#placeOrder').hide()
                $('#addRow').hide()

            }
            else {
                $('#placeOrder').show()
                $('#addRow').show()
            }
            totalPrice = costPrice * quantity;

            if(costPrice !='' && !isNaN(costPrice) && quantity !='' && !isNaN(quantity))
            {
                $('#total_price_' + divid).val(totalPrice);
                //---------- Size -------------------
                var content = jQuery('#sample_table tr'),
                    size = jQuery('#tbl_products >tbody >tr').length + 1,
                    element = null,
                    element = content.clone();
                //-------------- Sum --------------------
                $('.quantity').blur(function () {
                    var qsum = 0;
                    $('.quantity').each(function() {
                        if($(this).val()!="")
                        {
                            qsum += parseFloat($(this).val());
                        }
                    });
                    $('#OrderTotalQuantity').val(qsum);
                    //-----------------------------------
                    var psum = 0;
                    $('.price').each(function() {
                        if($(this).val()!="")
                        {
                            psum += parseFloat($(this).val());
                        }
                    });
                    $('#OrderTotalPrice').val(psum);
                });
                //---------------------------------------
            }
        }

        function getProductInfo(val,divid)
        {
            var productid = val;

            $.ajax({
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/orders-management/getProductInfo')}}',
                data: 'pid=' + productid + '&divid=' + divid,
                dataType: 'json',
                success: function(data)
                {
                    if(data.status==="success")
                    {

                        $('#unit'+divid).html(data.unitData);

                    }
                    else if(data.status==="error")
                    {
                        //
                    }
                },
                error:function (error)
                {
                    $('#cost_price_'+divid).val('');
                    $('#opening_stock_'+divid).val('');
                    $('#order_quantity_'+divid).val('');
                }
            });
        }

        function searchUserRecord(srckeyword){

            $.ajax({
                type:'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:'{{url("admin/orders-management/searchUser")}}',
                data:{'search_keyword':srckeyword},
                success: function(data){

                    $('#usersdata').html(data);
                },
                error: function(){
                    //alert('failure');
                }
            });
        }

        function setcustomerdetail(){
            var customerId=$('#customerdata').val();

            $.ajax({
                type:'get',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:'{{url("admin/orders-management/customerdetail")}}',
                data:{'id':customerId},
                success: function(data){

                    if(data['full_name']){
                        var username=data['full_name'];
                    }
                    else{
                        var username=data['username'];
                    }

                    $('#customer_id').val(data['id']);
                    $('#customer_name').val(username);
                    $('#customer_email').val(data['email']);
                    $('#customer_number').val(data['phoneno']);
                    $('#Address').val(data['address']);
                },

                error: function(){
                    //alert('failure');
                }
                //alert('failure');
//                }ca
            });
        }

        $(document).ready(function(){

            jQuery(document).delegate('button.add-record', 'click', function(e) {

                e.preventDefault();

                var content = jQuery('#sample_table tr'),
                    size = jQuery('#tbl_products >tbody >tr').length;

                $('#total_products').val(size);



                var i = size;

                //$('#tbl_products_body').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
                $('#tbl_products_body').append('<tr id="rec-'+i+'">\n' +
                    '                <td>\n' +
                    '                    <select required class="e1 form-control" name="products[]" id="product_'+i+'" onchange="getProductInfo(this.value,'+i+');">\n' +
                    '                        <option value="">Select Product</option>\n' +
                    '                        @foreach($productsData as $product)\n' +
                    '                            <option value="{{$product->id}}">{{$product->product_title}}</option>\n' +
                    '                        @endforeach\n' +
                    '                    </select>\n' +
                    '                </td>\n' +
                    '                <td class="" id="unit'+i+'">\n' +
                    '                </td>\n' +
                    '                <td><input readonly style="background: #ddd;" type="text" name="openingStocks[]" id="opening_stock_'+i+'"></td>\n' +
                    '                <td><input readonly style="background: #ddd;" type="text" name="costPrices[]" id="cost_price_'+i+'"></td>\n' +
                    '                <td><input required type="text" name="orderQuantity[]" id="order_quantity_'+i+'" onkeyup="updateSalePrice(this.value,'+i+');" class="quantity"></td>\n' +
                    '                <td><input readonly style="background: #ddd;" type="text" name="totalPrices[]" id="total_price_'+i+'" class="price"></td>\n' +
                    '                <td><input type="checkbox" onchange="changeTotal('+i+')" name="focs[]" id="foc_'+i+'"></td>\n'+
                    '                 <td><select hidden id="focReason_'+i+'"  onchange="showRefrence('+i+')" name="reason[]">\n' +
                    '                 <option>Select Reason</option>\n' +
                    '                <option value="Gift">Gift</option>\n' +
                    '                <option value="Sample">Sample</option>\n' +
                    '                    <option value="Replacement">Replacement</option>\n' +
                    '                    <option value="Other">Other</option>\n' +
                    '                    </select></td>\n' +
                    '                   <td><input type="text" name="orderReference[]"  hidden id="orderReference_'+i+'" >\n' +
                    '                     </td>\n' +
                    '                <td width="50" class="left_border_dotted text-center"><a class="btn btn-xs delete-record" data-id="'+i+'"><i class="glyphicon glyphicon-trash"></i></a></td>\n' +
                    '            </tr>');
            });


            jQuery(document).delegate('a.delete-record', 'click', function(e)
            {

                e.preventDefault();



                var didConfirm = confirm("Are you sure You want to delete");
                if (didConfirm == true)
                {
                    var id = jQuery(this).attr('data-id');
                    var targetDiv = jQuery(this).attr('targetDiv');
                    var quantity=$('#order_quantity_'+id).val();
                    var totalPrice=$('#total_price_'+id).val();

                    var OrderTotalPrice=$('#OrderTotalPrice').val();
                    var OrderTotalQuantity=$('#OrderTotalQuantity').val();
                    jQuery('#rec-' + id).remove();
                    var newTotalPrice=OrderTotalPrice - totalPrice;
                    var newTotalQuantity=OrderTotalQuantity - quantity;
                    $('#OrderTotalQuantity').val(newTotalQuantity);
                    $('#OrderTotalPrice').val(newTotalPrice);




                    size = jQuery('#tbl_products >tbody >tr').length;
                    $('#total_products').val(size);

                    //regnerate index number on table
                    $('#tbl_products_body tr').each(function(index){
                        $(this).find('span.sn').html(index+1);
                    });


                    return true;
                }
                else
                {
                    return false;
                }
            });
        });

        function changeTotal(id) {

            var amount = $('#total_price_'+id).val();
            var total = $('#OrderTotalPrice').val();

            if ($('#foc_'+id).is(":checked"))
            {
                var newAmount = parseInt(total) - parseInt(amount);
                $('#focReason_'+id).show();
            }
            else {
                var newAmount = parseInt(total) + parseInt(amount);
                $('#focReason_'+id).hide();
            }
//            $('#OrderTotalPrice').val(newAmount);
        }

        function setunitPrice(divid,product) {
            var unit=$('#unit_'+divid).val();



            $.ajax({

                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/orders-management/getunitInfo')}}',
                data: 'pid=' + product + '&unit=' + unit,
                success: function(data)
                {
                    $('#cost_price_'+divid).val(data.product_price);
                    $('#opening_stock_'+divid).val(data.product_quantity);
                },
                error:function (error)
                {
                    alert('error');
                }
            });
        }
        function showRefrence(id)
        {
            if ($('#focReason_'+id).val() == 'Replacement') {
                $('#orderReference_'+id).show();
                document.getElementById("orderReference_"+id).required = true;

            }
            else {
                $('#orderReference_' + id).hide();
            }
}
    function setQuantityCheck(){

    }
    </script>

@endsection