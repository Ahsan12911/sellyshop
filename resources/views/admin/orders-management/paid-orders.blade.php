@extends('admin.layouts.master')

@php
    use App\Models\Admin\Orders;
    use App\Models\Admin\OrdersItems;
    use App\Models\Admin\Customers;
    use App\Models\Admin\Users;
@endphp

@section('content')

    <div id="wrapper">

        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            @include('admin.layouts.header')
            <div id="main">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <h3 class="mt-35 font-normal mb-20 display-ib"><a href="{{url('/admin/orders-management/paid-orders')}}">Paid Orders</a></h3>
                            <div class="filters">
                                <ul>
                                    <li><a href="{{url('/admin/orders-management/orders')}}">All Orders</a></li>
                                    <li><a href="{{url('/admin/dashboard')}}">Awaiting processing {{--(3)--}}</a></li>
                                    <li> <a href="{{url('/admin/orders-management/unpaid-orders')}}">Unpaid</a></li>
                                    <li><span>Paid but not Shipped</span></li>
                                </ul>
                            </div>
                        </div>

                        <form name="frmSearch" id="frmSearch" role="form" method="post" action="{{ url('/admin/orders-management/order/searchPaidOrders') }}">
                            {{csrf_field()}}
                            <div class="search-conditions-box clearfix">
                                <ul>
                                    <li class="col-md-3">
                                        <input class="width_100 input_padding border_blue border_radius3" type="text" name="search_keyword" id="search_keyword" value="@if(isset($search_keyword)){{$search_keyword}} @endif" placeholder="OrderID or email, ID1-ID2 for range">
                                    </li>
                                    <li class="col-md-3">
                                        <div class="form-group mb-0">
                                            <div class="width_100 border_blue border_radius3">
                                                <select name="shipping_status[]" id="dates-field2" class="multiselect-ui form-control" multiple="multiple">
                                                    @foreach ($fulfilmentStatus as $fStatus)
                                                        <option value="{{$fStatus['id']}}" @if(isset($shipping_status) && $shipping_status==$fStatus['id']){{"selected"}} @endif>{{$fStatus['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="form-group mb-0">
                                            <div class="width_100 border_blue border_radius3">
                                                <select name="payment_status[]" id="dates-field2" class="multiselect-ui form-control" multiple="multiple">
                                                    @foreach ($orderPaymentStatusesValues as $pStatus)
                                                        <option value="{{$pStatus['id']}}" @if(isset($payment_status) && $payment_status==$pStatus['id']){{"selected"}} @endif>{{$pStatus['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-md-2">
                                        <i class="ti-calendar"></i>
                                        <input name="order_date" id="daterange" value="@if(isset($order_date)){{$order_date}} @endif" class="relative_position width_100 input_padding border_blue border_radius3" type="text" placeholder="Enter Date Range">
                                    </li>
                                    <li class="col-md-1">
                                        <button class="border_blue search_btn border_radius3">Search</button>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                                <div class="col-md-12 text-center">
                                    <button type="button" class="absolute_position angle_downbtn border_radius3"><i class="ti-angle-down"></i></button>
                                </div>
                            </div>

                            <div class="search-conditions-hidden search-conditions-box">
                                <ul class="mt-10">
                                    <li class="mr-20">
                                        <p>Customer</p>
                                    </li>
                                    <li class="mr-20">
                                        <input name="customer_name" type="text" value="@if(isset($customer_name)){{$customer_name}} @endif" placeholder="Customer Name">
                                    </li>
                                    <li class="mr-20">
                                        <p>Telephone</p>
                                    </li>
                                    <li class="mr-20">
                                        <input type="text" name="tel" id="tel" value="@if(isset($tel)){{$tel}} @endif" placeholder="Telephone no">
                                    </li>
                                </ul>
                            </div>
                        </form>

                        <form method="post" action="{{url('/admin/orders-management/deleteOrExportorders')}}" >
                            {{--      <form method="post" action="{{url('/delete/vehicles')}}" >--}}
                            {{csrf_field()}}
                            <script type="text/javascript">
                                function selectAll(status){
                                    $('.multiplerow').each(function(){
                                        $(this).prop('checked',status);
                                    });

                                }
                            </script>
                        <div class="table-responsive">
                            <table class="table order_table" >
                                <thead>
                                <tr>
                                    {{--<th>
                                        <input type="checkbox">
                                    </th>--}}
                                    <th><input type="checkbox" onclick='selectAll(this.checked)' value="Select All" style="width: 20px"/> </th>
                                    <th>Sr#</th>
                                    <th>Order#</th>
                                    <th>Source</th>
                                    <th>Total Items</th>
                                    <th>Payment Type</th>
                                    <th>Date</th>
                                    <th>Customer</th>
                                    <th>Location</th>
                                    {{--<th>Move to</th>--}}
                                    <th>Order Count</th>
                                    <th>Fulfilment Status</th>
                                    <th>Payment Status</th>
                                    <th>Amount</th>
                                    <th>Processed by </th>
                                    <th>Delivered by </th>
                                    <th>Delivery Time</th>
                                    <th width="8%">Commission </th>
                                    {{--<th>Delivery Status</th>--}}
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $counterloo=0;
                                    $order_count=0;
                                    $totalAmount=array();
                                    $totalCommission=array();
                                @endphp

                                @foreach($result as $data)

                                    @php

                                        $counterloo++;

                                        $totalItems = OrdersItems::getOrderItemsCount($data['id']);

                                        $totalCommission[$counterloo] = $data['commission'];
                                        $totalAmount[$counterloo] = isset($data['total'])?$data['total']:'';

                                        if($data->user_id != '')
                                        {
                                            $cur_user_id = $data->user_id;

                                            $userObj = new Users();
                                            $userInfo = $userObj->getUserInfo($cur_user_id);

                                            $orderbyname = isset($userInfo[0]->username)?$userInfo[0]->username:'';
                                            $orderbylastname = isset($userInfo[0]->last_name)?$userInfo[0]->last_name:'';
                                            $orderbyemail = isset($userInfo[0]->email)?$userInfo[0]->email:'';

                                            $order_count = Orders::getOrdersCountByUID($cur_user_id);
                                        }
                                        else
                                        {
                                            $cur_customer_id = $data->customer_id;

                                            $customerObj = new Customers();
                                            $customerInfo = $customerObj->getCustomerInfo($cur_customer_id);

                                            $orderbyname = isset($customerInfo[0]->username)?$customerInfo[0]->username:'';
                                            $orderbyemail = isset($customerInfo[0]->email)?$customerInfo[0]->email:'';

                                            $order_count = Orders::getOrdersCountByCID($cur_customer_id);
                                        }
                                    @endphp
                                    <tr>
                                        {{--<td class="right_border_dotted text-center"><input type="checkbox"></td>--}}
                                        <td><input type="checkbox" class="multiplerow" id="multiplerow" name="delete[]" value="{{$data['id']}}"></td>
                                        <td>{{$counterloo}}.</td>
                                        <td><a href="{{url('/admin/orders-management/OrdersDetail',[$data->id])}}">{{$data['orderNumber']}}</a></td>
                                        <td>
                                            @php
                                                $source=isset($data->ordersource)?isset($data->ordersource):'';
                                            @endphp
                                            @if($data->ordersource==1)

                                                <i class="fa fa-television television-icon"  aria-hidden="true"></i>

                                            @elseif($data->ordersource==0)
                                                <i class="fa fa-mobile mobile-icon" aria-hidden="true"></i>

                                            @elseif($data->ordersource==2)
                                                <i class="fa fa-phone mobile-icon" aria-hidden="true"></i>
                                            @endif
                                        </td>
                                        <td><a href="#">{{$totalItems}}</a></td>
                                        <td><span>{{$data['order_payment_method']}}</span></td>
                                        <td class="cell-date"><span class="date">{{date('M d, Y',strtotime($data['created_at']))}} {{--Nov 21, 2017.--}}</span>
                                            <span class="time">{{date('g:i A',strtotime($data['created_at']))}} {{--11:57 AM--}}</span>
                                        </td>
                                        <td><a href="{{url('/admin/orders-management/users-orders',[$data['id']])}}">{{$orderbyname}}</a> <small class="display_block">{{$orderbyemail}}</small></td>
                                        <td>@php
                                                $storeName=$location->where('id','=',$data->location_id)->pluck('store_name')->first();
                                            @endphp
                                            {{$storeName}}
                                        </td>
                                        {{--<td><select>--}}
                                        {{--<option value="">All store location</option>--}}
                                        {{--</select></td>--}}
                                        {{--<td>--}}
                                            {{--<select onchange="updateOrderLocation('{{$data->id}}',this)" id="locationId">--}}
                                                {{--<option>Select Location</option>--}}
                                                {{--@foreach($location as $storelocation)--}}
                                                    {{--<option value="{{$storelocation->id}}">{{$storelocation->store_name}}</option>--}}
                                                {{--@endforeach--}}
                                            {{--</select></td>--}}

                                        <td><a href="{{url('/admin/orders-management/users-orders',[$data['id']])}}">{{$order_count}}</a></td>
                                        <?php
                                        $new = ['pro'];
                                        ?>
                                        <td>
                                            <select name="fulfil_status" id="fulfil_status"
                                                    @cannot('order_shipping_status')
                                                    disabled
                                                    @endcannot
                                                    onchange="updateFullfilmentStatus(this.value,'{{$data["id"]}}','{{$data["created_at"]}}')">
                                                <option value="" disabled>---Select One---</option>
                                                @foreach ($fulfilmentStatus as $fStatus)
                                                    <option value="{{$fStatus['id']}}"
                                                            @cannot('order_shipping_status_'.$fStatus['code'])
                                                            disabled
                                                            @endcannot
                                                            @if($data['shipping_status_id'] == $fStatus['id']) selected @endif>{{$fStatus['name']}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select name="payment_status" id="payment_status"
                                                    @cannot('order_payment_status')
                                                    disabled
                                                    @endcannot
                                                    onchange="updatePaymentStatus(this.value,'{{$data["id"]}}')">
                                                <option value="" disabled>---Select One---</option>
                                                @foreach ($orderPaymentStatusesValues as $pStatus)
                                                    <option value="{{$pStatus['id']}}"
                                                            @if($data['payment_status_id'] == $pStatus['id']) selected @endif>{{$pStatus['name']}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            @php
                                                $totalAmount[$counterloo] = isset($data['total'])?$data['total']:'';
                                            @endphp
                                            <span>RS.{{number_format((float)$data['total'], 2, '.', '')}}</span>
                                        </td>
                                        <td>
                                            <select name="processed_by" id="processed_by" onchange="updateProcessedBy(this.value,'{{$data["id"]}}')">
                                                <option value="" selected>---Select One---</option>
                                                @foreach ($csrusers as $user)
                                                    <option value="{{$user['id']}}" @if($data['processed_by'] == $user['id']) selected @endif>{{$user['username']}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select name="delivered_by" id="delivered_by" onchange="updateDeliveredBy(this.value,'{{$data["id"]}}')">
                                                <option value="" selected>---Select One---</option>
                                                @foreach ($riderusers as $user)
                                                    <option value="{{$user['id']}}" @if($data['delivered_by'] == $user['id']) selected @endif>{{$user['username']}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <span>{{$data['delivery_time']}}</span>
                                        </td>
                                        <td>
                                            <input type="text" class="add_statusbtn" value="<?php if($data['commission'])echo $data['commission'];else echo"0";$totalCommission[$counterloo] = $data['commission'];
                                            ?>" onkeyup="updateCommission(this.value,'{{$data["id"]}}')">
                                        </td>
                                        @can('OrderDelete')
                                        <td class="left_border_dotted text-center" onclick="return confirm('Are You sure to delete order?');">
                                            <a href="{{url('/admin/orders-management/delete',[$data['id']])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        </td>
                                        @endcan
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>

                            <div class="table_pager clearfix mt-20">
                                <div class="text-right col-lg-12 export-as-btn p-0">
                                    <div class="col-md-8 text-left pl-0">
                                        <div class="display-ib mr-10 select-store">
                                            <div class="dropdown">
                                                <select   name="locationId" class="form-control">
                                                    <option value="">Select Location</option>
                                                    @foreach($location as $storelocation)
                                                        <option value="{{$storelocation->id}}">{{$storelocation->store_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="display-ib">
                                            <input type="submit" value="Update" id="update" class="btn add_inputbtn btn-primary mr-10" name="submitbutton">
                                            @can('AccountReport')
                                                <button value="excel" class="btn btn-primary dropdown-toggle mr-10"
                                                        type="submit" name="submitbutton" aria-expanded="false">CSV Account
                                                </button>
                                            @endcan
                                            @can('CSRSupervioserReport')
                                                <button value="csrReport" class="btn btn-primary dropdown-toggle mr-10"
                                                        type="submit" name="submitbutton" aria-expanded="false">CSV CSR
                                                </button>
                                            @endcan
                                            @can('OrderDelete')
                                            <button class="btn btn-danger mr-10" type="submit" name="submitbutton" id="delete-order" value="delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </button>
                                            @endcan
                                        </div>
                                    </div>
                                    <div class="col-md-4 pr-0">
                                        <div class="pull-right">
                                            {{$links}}
                                        </div>
                                    </div>
                                </div>
                                <div class="tex-right col-md-12 text-right pr-0" >
                                    <ul class="mt-20">
                                        <li>
                                            <span class="mr-5">Orders Total:</span><strong>RS.{{array_sum($totalAmount)}}</strong>
                                        </li>
                                        <li>
                                            <span class="mr-5">Commission Total:</span><strong>RS.{{array_sum($totalCommission)}}</strong>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                        </div>

                    </div>

                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->
        <div class="clearfix"></div>
    </div>

@endsection

@section('footer')


    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>

@endsection

<script language="JavaScript">

    function updatePaymentStatus(value,order_id)
    {
        $.ajax({
            url: "{{ url('/admin/orders-management/order/setPayStatus') }}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{'orderId':order_id,'value':value},
            success: function (data)
            {
                if (data != "Error")
                {
                    /*var message_contaner = $('#direct-chat-messages_' + id);
                    message_contaner.append(data);*/
                }
            },
        });
    }

    function updateFullfilmentStatus(value,order_id,order_date)
    {
        $.ajax({
            url: "{{ url('/admin/orders-management/order/setFulfilStatus') }}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{'orderId':order_id,'value':value,'order_date':order_date},
            success: function (data)
            {
                if (data != "Error")
                {
                    /*var message_contaner = $('#direct-chat-messages_' + id);
                    message_contaner.append(data);*/
                }
            },
        });
    }

    function updateProcessedBy(value,order_id)
    {
        $.ajax({
            url: "{{ url('/admin/orders-management/order/setProcessor') }}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{'orderId':order_id,'value':value},
            success: function (data)
            {
                if (data != "Error")
                {
                    /*var message_contaner = $('#direct-chat-messages_' + id);
                    message_contaner.append(data);*/
                }
            },
        });
    }

    function updateDeliveredBy(value,order_id)
    {
        $.ajax({
            url: "{{ url('/admin/orders-management/order/setDeliverBoy') }}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{'orderId':order_id,'value':value},
            success: function (data)
            {
                if (data != "Error")
                {
                    /*var message_contaner = $('#direct-chat-messages_' + id);
                    message_contaner.append(data);*/
                }
            },
        });
    }

    function updateCommission(value,order_id)
    {
        $.ajax({
            url: "{{ url('/admin/orders-management/order/addCommission') }}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:{'orderId':order_id,'value':value},
            success: function (data)
            {
                if (data != "Error")
                {
                    /*var message_contaner = $('#direct-chat-messages_' + id);
                    message_contaner.append(data);*/
                }
            },
        });
    }

</script>
<script>
    function updateOrderLocation(orderId,val) {

        var location=val.options[val.selectedIndex].value;
//        alert(location);
        $.ajax({
            url:"{{url('/admin/updateOrderLocation')}}", //Cart\CartController@placeorder
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : { location:location, orderId:orderId},
            success:function (data) {
                if(data){
                    window.location.reload();
                }
            }
        });
    }
</script>