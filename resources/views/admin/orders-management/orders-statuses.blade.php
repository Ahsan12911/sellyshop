@extends('admin.layouts.master')

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Order statuses</h3>
                        <br>
                        <a href="{{url('/admin/orders-management/add-order-status')}}"><button class="add_statusbtn">Add Status</button></a>
                        {{--<button class="add_statusbtn" onclick="document.getElementById('id01').style.display='block'" style="width:auto; cursor: pointer;color: #333;">Track Delivery</button>
                        <button class="add_statusbtn">Track Shipment</button>--}}
                    </div>

                    <div class="container">
                        <div id="id01" class="modal">
                            <form class="modal-content animate" action="/action_page.php">
                                <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
                            </form>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function() {
                            $("#hideaftermoment").delay(2000).fadeOut(0);
                        });
                    </script>
                    @if (session('status'))
                        <div id="hideaftermoment" class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <script type="text/javascript">
                        $(function() {
                              $("#testdiv").delay(2000).fadeOut(0);
                        });
                    </script>
                    @if($errors->any())
                        <h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>
                    @endif
                    <div class="mt-35">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Payment Statuses</a></li>
                            <li><a data-toggle="tab" href="#menu1">Fulfilment Statuses</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">

                                <table class="table tables_ui" id="t_draggable2">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Order</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody class="t_sortable">
                                    @foreach($orderpaymentstatuses as $orderspaymentstatusInfo)
                                    <tr>
                                        <td class="right_border_dotted td_width10px"><i class="ti-move"></i></td>
                                        <td><a href="{{ URL::to('/admin/orders-management/update-order-status', [$orderspaymentstatusInfo->id]) }}">{{$orderspaymentstatusInfo->name}}</a></td>
                                        <td class="td_width100px"><span></span></td>
                                        <td class="left_border_dotted td_width10px" onclick="return confirm('Are You sure to delete this record?');"><a href="{{url('/admin/orders-management/deletepayment',[$orderspaymentstatusInfo->id])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <table class="table tables_ui" id="t_draggable2">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Order</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody class="t_sortable">
                                    @foreach($ordershipmentstatuses as $ordersshipmentstatusInfo)
                                    <tr>
                                        <td class="right_border_dotted td_width10px"><i class="ti-move"></i></td>
                                        <td><a href="{{url('/admin/orders-management/update-order-shipment-status',[$ordersshipmentstatusInfo->id])}}">{{$ordersshipmentstatusInfo->name}}</a></td>
                                        <td class="td_width100px"><span></span></td>
                                        <td class="left_border_dotted td_width10px" onclick="return confirm('Are You sure to delete this record?');"><a href="{{url('/admin/orders-management/deleteshipment',[$ordersshipmentstatusInfo->id])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    {{--<div>
                        <hr>
                        <button class="btn save_changesbtn">Save Changes</button>
                    </div>--}}
                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection
