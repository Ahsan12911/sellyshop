@extends('admin.layouts.master')

@section('content')

   <div class="main">
       <!-- MAIN CONTENT -->
       @include('admin.layouts.header')
       <div id="main">
           <div class="row">
               <div class="col-md-9">

                   <div>
                       <h3 class="mt-35 font-normal mb-20 display-ib">Orders</h3>
                       <div class="filters">
                           <ul>
                               <li><a href="{{url('/admin/orders-management/orders')}}">All Orders</a></li>
                               <li><span>Awaiting processing (3)</span></li>
                               <li> <a href="{{url('/admin/orders-management/unpaid-orders')}}">Unpaid</a></li>
                               <li><a href="{{url('/admin/orders-management/paid-orders')}}">Paid but not Shipped</a></li>
                           </ul>
                       </div>
                   </div>

                   <div class="search-conditions-hidden search-conditions-box">
                       <ul class="mt-10">
                           <li class="mr-20">
                               <p>Customer Name</p>
                           </li>
                           <li class="mr-20">
                               <input type="text" placeholder="Customer Name">
                           </li>
                           <li class="mr-20">
                               <p>Customer Access Level</p>
                           </li>
                           <li class="mr-20">
                               <select>
                                   <option>All Levels</option>
                                   <option>2</option>
                                   <option>3</option>
                                   <option>4</option>
                               </select>
                           </li>
                       </ul>
                       <ul class="mt-10">
                           <li class="mr-20">
                               <p>Customer Name</p>
                           </li>
                           <li class="mr-20">
                               <input type="text" placeholder="Customer Name">
                           </li>
                           <li class="mr-20">
                               <p>Customer Access Level</p>
                           </li>
                           <li class="mr-20">
                               <select>
                                   <option>All Levels</option>
                                   <option>2</option>
                                   <option>3</option>
                                   <option>4</option>
                               </select>
                           </li>
                       </ul>
                       <ul class="mt-10">
                           <li class="mr-20">
                               <p>Customer Name</p>
                           </li>
                           <li class="mr-20">
                               <input type="text" placeholder="Customer Name">
                           </li>
                           <li class="mr-20">
                               <p>Customer Access Level</p>
                           </li>
                           <li class="mr-20">
                               <select>
                                   <option>All Levels</option>
                                   <option>2</option>
                                   <option>3</option>
                                   <option>4</option>
                               </select>
                           </li>
                       </ul>
                       <ul class="mt-10">
                           <li class="mr-20">
                               <p>Customer Name</p>
                           </li>
                           <li class="mr-20">
                               <input type="text" placeholder="Customer Name">
                           </li>
                           <li class="mr-20">
                               <p>Customer Access Level</p>
                           </li>
                           <li class="mr-20">
                               <select>
                                   <option>All Levels</option>
                                   <option>2</option>
                                   <option>3</option>
                                   <option>4</option>
                               </select>
                           </li>
                       </ul>
                       <ul class="mt-10">
                           <li class="mr-20">
                               <p>Customer Name</p>
                           </li>
                           <li class="mr-20">
                               <input type="text" placeholder="Customer Name">
                           </li>
                           <li class="mr-20">
                               <p>Customer Access Level</p>
                           </li>
                           <li class="mr-20">
                               <select>
                                   <option>All Levels</option>
                                   <option>2</option>
                                   <option>3</option>
                                   <option>4</option>
                               </select>
                           </li>
                       </ul>
                   </div>

                   <table class="table order_table" >
                       <thead>
                       <tr>
                           <th>
                               <input type="checkbox">
                           </th>
                           <th>Order #</th>
                           {{--<th>Payment method</th>--}}
                           <th>Date</th>
                           <th>Customer</th>
                           <th>Payment Status</th>
                           <th>Fulfilment Status</th>
                           <th>Amount</th>
                           {{-- <th>Commission </th>
                           <th>Status</th>--}}
                           <th></th>
                       </tr>
                       </thead>
                       <tbody>
                       @foreach($orders as $ordersInfo)
                       <tr>
                           <td class="right_border_dotted text-center"><input type="checkbox"></td>
                           <td><a href="#">{{$ordersInfo->orderNumber}}</a></td>
                           <td><a href="#">{{$ordersInfo->date}}</a></td>
                           <td class="cell-date">
                               <span class="date">{{$ordersInfo->date}}{{--Nov 21, 2017.--}}</span>
                               <span class="time">{{$ordersInfo->date}}{{--11:57 AM--}}</span>
                           </td>
                           <td><i class="ti-user"></i> <a href="#">Customer Name</a> <small class="display_block">CustomerEmail@mail.com></td>
                           <td>
                               <select>
                                   <option>paid</option>
                                   <option>Awaiting Payment</option>
                                   <option>Partially Paid</option>
                                   <option>Declined</option>
                                   <option>Cancelled</option>
                               </select>
                           </td>
                           <td>
                               <select>
                                   <option>paid</option>
                                   <option>Awaiting Payment</option>
                                   <option>Partially Paid</option>
                                   <option>Declined</option>
                                   <option>Cancelled</option>
                               </select>
                           </td>
                           <td><span>RS.{{$ordersInfo->total}}</span>
                               <small class="display_block">Qty:2</small>
                           </td>
                           <td><input type="text"></td>
                           <td><input type="text"></td>
                           <td><input type="text"></td>
                           <td><button class="add_statusbtn">Track</button></td>
                           <td class="left_border_dotted text-center"><i class="fa fa-trash-o" aria-hidden="true"></i></td>
                       </tr>
                       @endforeach
                       </tbody>
                   </table>

                   <div class="table_pager">
                       <div class="col-lg-4 col-md-4 text-left">
                           <ul class="pagination">
                               <li><a href="#">&laquo;</a></li>
                               <li><a href="#">1</a></li>
                               <li><a href="#">2</a></li>
                               <li><a href="#">3</a></li>
                               <li><a href="#">4</a></li>
                               <li><a href="#">5</a></li>
                               <li><a href="#">&raquo;</a></li>
                           </ul>
                       </div>
                       <div class="col-lg-4 col-md-4 text-right">
                           <div class="items-per-page-wrapper">
                               <span><b>10</b> items</span>
                               <select>
                                   <option>10</option>
                                   <option>20</option>
                                   <option>30</option>
                                   <option>40</option>
                               </select>
                               <span>per page</span>
                           </div>
                       </div>
                       <div class="col-lg-4 col-md-4 text-right">
                           <ul>
                               <li>
                                   <span class="mr-5">Orders Total:</span><strong>RS.000.00</strong>
                               </li>
                           </ul>

                       </div>
                       <div class="clearfix"></div>
                   </div>

               </div>
               @include('admin.layouts.sidebar-right')
           </div>
       </div>
       <!-- END MAIN CONTENT -->
   </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/flot.tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-sparkline/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-tour/js/bootstrap-tour.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widget.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/data.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/scroll-parent.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/disable-selection.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/mouse.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/sortable.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-main/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-appear/jquery.appear.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection