@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->


        <form name="frmProducts" id="frmProducts" method="post" action="{{ url('/admin/marketing/updateSlider') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

<input type="hidden" value="{{$sliderData->id}}" name="sliderId">
            <div id="main">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Update Slider Images</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Product name Text input-->
                                        <div class="form-group {{ $errors->has('ImageTitle') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="Image_title">Image Title *</label>
                                            <div class="col-md-4">
                                                <input id="ImageTitle" name="ImageTitle"  type="text" value="{{$sliderData->title}}" @if($errors->has('ImageTitle'))style=" border-color:red ;" @endif>
                                            </div>
                                        </div>

                                        <!-- SKU Text input-->
                                        {{--<div class="form-group{{ $errors->has('category_icon') ? 'has-error' : '' }}">--}}
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Image">Image </label>
                                            <div class=" col-md-2">

                                                <input id="slider_image" type="file" name="slider_image"  class="input-md"  >

                                            </div>
                                            <div class="col-md-offset-1 col-md-2">
                                                <img src="{{asset('/public/images/SliderImages/')}}/{{$sliderData->image}}" height="50">

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="status">Enabled</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="status" id="status" value="1" @if($sliderData->status == '1') checked @endif>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Marketing section End-->
            </div>
            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="btn add_inputbtn" type="submit" name="" value="Update Slider">
                <a href="{{url('admin/marketing/new-launch-premier')}}" class="add_statusbtn" >Cancel</a>
            </div>
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection