@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->


        <form name="frmProducts" id="frmProducts" method="post" action="{{ url('/admin/marketing/update-images') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

<input type="hidden" name="imagesId" value="{{$imagesData->id}}">
            <div id="main">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Update Premier Images</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>


                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Image">Image 1 </label>
                                            <div class="col-md-2">
                                                <input id="image1" type="file" name="image1"  class="input-md"  >

                                            </div>
                                            <div class="col-md-offset-1 col-md-2">
                                                <img src="{{asset('/public/images/PremierImages/')}}/{{$imagesData->image_1}}" height="80">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="status">Enabled</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="status1" id="status" value="1" @if($imagesData->image_1_status == '1') checked @endif>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Image">Image 2 </label>
                                            <div class="col-md-2">
                                                <input id="image2" type="file" name="image2"  class="input-md"  >

                                            </div>
                                            <div class="col-md-offset-1 col-md-2">
                                                <img src="{{asset('/public/images/PremierImages/')}}/{{$imagesData->image_2}}" height="80">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="status2">Enabled</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="status2" id="status" value="1" @if($imagesData->image_2_status == '1') checked @endif>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Image">Image 3</label>
                                            <div class="col-md-2">
                                                <input id="image3" type="file" name="image3"  class="input-md"  >

                                            </div>
                                            <div class="col-md-offset-1 col-md-2">
                                                <img src="{{asset('/public/images/PremierImages/')}}/{{$imagesData->image_3}}" height="80">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="status">Enabled</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="status3" id="status" value="1" @if($imagesData->image_3_status == '1') checked @endif>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="Image">Image 4</label>
                                            <div class="col-md-2">
                                                <input id="image" type="file" name="image4"  class="input-md"  >

                                            </div>
                                            <div class="col-md-offset-1 col-md-2">
                                                <img src="{{asset('/public/images/PremierImages/')}}/{{$imagesData->image_4}}" height="80">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="status">Enabled</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="status4" id="status4" value="1" @if($imagesData->image_4_status == '1') checked @endif>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Marketing section End-->
            </div>
            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="btn add_inputbtn" type="submit" name="" value="Update Images">
                <a href="{{url('admin/marketing/new-launch-premier')}}" class="add_statusbtn" >Cancel </a>
            </div>
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection