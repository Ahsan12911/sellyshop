@extends('admin.layouts.master')

@section('content')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">New Launch/ Premier Sliders</h3>
                    </div>
                    <br>
                    <a href="{{url('/admin/marketing/add-slider')}}" class="btn add_statusbtn">Add Slider</a>
                    <br>
                    <br>

                    <form method="post" action="" >
                        {{csrf_field()}}
                        <script type="text/javascript">
                            $(function() {
                                $("#hideaftermoment").delay(2000).fadeOut(0);
                            });
                        </script>
                        @if (session('status'))
                            <div id="hideaftermoment" class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <script type="text/javascript">
                            $(function() {
                                $("#testdiv").delay(2000).fadeOut(0);
                            });
                        </script>
                        @if($errors->any())
                            <h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>
                        @endif

                        <table class="table order_table" >
                            <thead>
                            <tr>
                                <th width="5%">Image</th>
                                <th width="5%">Alt Title</th>
                                <th width="7%">Status</th>
                                <th width="5%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($newLaunchSlider as $slider)
                                    <tr>
                                        <td class="right_border_dotted text-center"><img src="{{asset('/public/images/SliderImages/')}}/{{$slider->image}}" height="80" width="150"></td>
                                        <td class="right_border_dotted text-center">{{$slider->title}}</td>
                                        @if($slider->status == '1')
                                            <td title="Enabled" class="right_border_dotted text-center">
                                                <a href="{{url('/admin/marketing/statusDisable',[$slider->id])}}" name="Enable"><i class="fa fa-power-off" aria-hidden="true" ></i></a>
                                            </td>
                                        @else
                                            <td title="Disabled" class="right_border_dotted text-center">
                                                <a href="{{url('/admin/marketing/statusEnable',[$slider->id])}}" name="Disable"><i class="fa fa-power-off" style="color:red" aria-hidden="true"></i></a>
                                            </td>
                                        @endif
                                        <td id="delete-category"  class="left_border_dotted text-center">
                                            <a class="edit" href="{{url('/admin/marketing/updateSlider' ,[$slider->id]) }}">
                                                <i class="fa fa-pencil"></i>
                                            </a><br>
                                            <a href="{{url('/admin/marketing/deleteSlider' ,[$slider->id]) }}" onclick="return confirm('Are You sure to delete this record?');">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="table_pager">
                            <div class="col-lg-6 col-md-6 text-left">



                            </div>
                            <div class="col-lg-6 col-md-6 text-right">
                                <div class="items-per-page-wrapper" style="float: right">


                                    {{--<span><b>{{  $categoriesData->count()}}</b> records</span>--}}
                                    {{--<input type="submit" name="submit" value="Delete Selected Value" >--}}
                                    {{--<span>found</span>--}}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div style="margin-top: 50px;">&nbsp;</div>
                        <div>
                            <h3 class="mt-35 font-normal mb-20 display-ib">New Launch/ Premier Images</h3>
                        </div>
                        <br>
                        <a href="{{url('/admin/marketing/add-images')}}" class="btn add_statusbtn">Add Images</a>
                        <br>
                        <br>
                        <table class="table order_table" >
                            <thead>
                                <tr>
                                    <th width="5%">Image-1</th>
                                    <th width="7%">Status</th>
                                    <th width="5%">Image-2</th>
                                    <th width="7%">Status</th>
                                    <th width="5%">Image-3</th>
                                    <th width="7%">Status</th>
                                    <th width="7%">Image-4</th>
                                    <th width="7%">Status</th>
                                    <th width="7%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            {{--<form method="post" action="{{url('/admin/marketing/PremierImageStatusUpdate')}}">--}}
                            @foreach($newLaunchImages as $newLaunchImageInfo)
                                <tr>
                                    <td class="right_border_dotted text-center"><img src="{{asset('/public/images/PremierImages/')}}/{{$newLaunchImageInfo->image_1}}" height="80"></td>
                                    @if($newLaunchImageInfo->image_1_status == '1')
                                        <td title="Enabled" class="right_border_dotted text-center">
                                            <a href="{{url('/admin/marketing/statusDisableImage1',[$newLaunchImageInfo->id])}}">
                                                <i class="fa fa-power-off" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    @else
                                        <td title="Disabled" class="right_border_dotted text-center">
                                            <a href="{{url('/admin/marketing/statusEnableImage1',[$newLaunchImageInfo->id])}}">
                                                <i class="fa fa-power-off" style="color:red" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    @endif
                                    <td class="right_border_dotted text-center">
                                        <img src="{{asset('/public/images/PremierImages/')}}/{{$newLaunchImageInfo->image_2}}" height="80">
                                    </td>
                                    @if($newLaunchImageInfo->image_2_status == '1')
                                        <td title="Enabled" class="right_border_dotted text-center">
                                            <a href="{{url('/admin/marketing/statusDisableImage2',[$newLaunchImageInfo->id])}}">
                                                <i class="fa fa-power-off" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    @else
                                        <td title="Disabled" class="right_border_dotted text-center">
                                            <a href="{{url('/admin/marketing/statusEnableImage2',[$newLaunchImageInfo->id])}}">
                                                <i class="fa fa-power-off" style="color:red" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    @endif
                                    <td class="right_border_dotted text-center">
                                        <img src="{{asset('/public/images/PremierImages/')}}/{{$newLaunchImageInfo->image_3}}" height="80">
                                    </td>
                                    @if($newLaunchImageInfo->image_3_status == '1')
                                        <td title="Enabled" class="right_border_dotted text-center">
                                            <a href="{{url('/admin/marketing/statusDisableImage3',[$newLaunchImageInfo->id])}}">
                                                <i class="fa fa-power-off" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    @else
                                        <td title="Disabled" class="right_border_dotted text-center">
                                            <a href="{{url('/admin/marketing/statusEnableImage3',[$newLaunchImageInfo->id])}}">
                                                <i class="fa fa-power-off" style="color:red" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    @endif
                                    <td class="right_border_dotted text-center">
                                        <img src="{{asset('/public/images/PremierImages/')}}/{{$newLaunchImageInfo->image_4}}" height="80">
                                    </td>
                                    @if($newLaunchImageInfo->image_4_status == '1')
                                        <td title="Enabled" class="right_border_dotted text-center">
                                            <a href="{{url('/admin/marketing/statusDisableImage4',[$newLaunchImageInfo->id])}}">
                                                <i class="fa fa-power-off" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    @else
                                        <td title="Disabled" class="right_border_dotted text-center">
                                            <a href="{{url('/admin/marketing/statusEnableImage4',[$newLaunchImageInfo->id])}}">
                                                <i class="fa fa-power-off" style="color:red" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    @endif
                                    <td id="delete-category"  class="left_border_dotted text-center">
                                        <a class="edit" href="{{url('/admin/marketing/update-images' ,[$newLaunchImageInfo->id]) }}">
                                            <i class="fa fa-pencil"></i>
                                        </a><br>
                                        <a href="{{url('/admin/marketing/delete-images' ,[$newLaunchImageInfo->id]) }}" onclick="return confirm('Are You sure to delete this record?');">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </form>
                            </tbody>
                        </table>
                        <div class="table_pager">
                            <div class="col-lg-6 col-md-6 text-left">

                                {{--<ul class="pagination">--}}
                                {{--<li><a href="#">&laquo;</a></li>--}}
                                {{--<li><a href="#">1</a></li>--}}
                                {{--<li><a href="#">2</a></li>--}}
                                {{--<li><a href="#">3</a></li>--}}
                                {{--<li><a href="#">4</a></li>--}}
                                {{--<li><a href="#">5</a></li>--}}
                                {{--<li><a href="#">&raquo;</a></li>--}}
                                {{--</ul>--}}

                            </div>
                            <div class="col-lg-6 col-md-6 text-right">
                                <div class="items-per-page-wrapper" style="float: right">


                                    {{--<span><b>{{  $categoriesData->count()}}</b> records</span>--}}
                                    {{--<input type="submit" name="submit" value="Delete Selected Value" >--}}
                                    {{--<span>found</span>--}}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </form>
                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/flot.tooltip/jquery.flot.tooltip.js') }}"></script>
{{--    <script src="{{ URL::asset('public/admin/assets/vendor/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>--}}
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-sparkline/js/jquery.sparkline.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-tour/js/bootstrap-tour.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widget.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/data.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/scroll-parent.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/disable-selection.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/mouse.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/sortable.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-main/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery-appear/jquery.appear.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection