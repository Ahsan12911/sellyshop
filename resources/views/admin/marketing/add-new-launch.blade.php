@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<style>
    @if($errors->has('category_description'))
        #cke_1_contents{
        border: 1px solid red;
    }
    @endif
        @if($errors->has('meta_descriptions'))
           #cke_2_contents{
        border: 1px solid red;
    }
    @endif

</style>
@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->


        <form name="frmProducts" id="frmProducts" method="post" action="{{ url('/admin/categories-management/addCategories') }}" enctype="multipart/form-data">
            {{ csrf_field() }}


            <div id="main">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Add Categories</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Product name Text input-->
                                        <div class="form-group{{ $errors->has('categoryTitle') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="category_title">Category name *</label>
                                            <div class="col-md-4">
                                                <input id="category_title" name="categoryTitle" value="{{ old('categoryTitle') }}" @if($errors->has('categoryTitle'))style=" border-color:red ;" @endif class="form-control input-md"  type="text">
                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('categoryTitle') }}</span>
                                        <!-- SKU Text input-->
                                        {{--<div class="form-group{{ $errors->has('category_icon') ? 'has-error' : '' }}">--}}
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name_fr">Category icon</label>
                                            <div class="col-md-4">
                                                <input id="category_icon" type="file" name="category_icon" value="{{ old('category_icon') }}" class="input-md" @if($errors->has('category_icon'))style=" border:1px solid red ;" @endif  >
                                            </div>
                                        </div>
{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('category_icon') }}</span>--}}
                                        <!--Full Description Text area-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="category_description">Description</label>
                                            <div class="col-md-8">
                                                <div action="#" method="POST" class='form-wysiwyg'>
                                                    <textarea name="category_description" id="category_description" class='ckeditor span12' rows="5">{{ old('category_description') }}</textarea>
                                                </div>
                                            </div>
                                            {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('category_description') }}</span>--}}
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Product section End-->

                <!-- Marketing section Start-->
                <div class="marketing-sec">
                    <div class="row">
                        <div class="col-md-9">

                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Meta description input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Meta Description</label>
                                            <div class="col-md-8">
                                                <div action="#" method="POST" class='form-wysiwyg'>
                                                    <textarea name="meta_descriptions" id="meta_descriptions"  class='ckeditor span12' rows="5">{{ old('meta_descriptions') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('meta_descriptions') }}</span>--}}
                                        <!-- Meta keywords input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Meta keywords</label>
                                            <div class="col-md-4">
                                                <input id="meta_keywords" name="meta_keywords" @if($errors->has('meta_keywords'))style=" border-color:red ;" @endif value="{{ old('meta_keywords') }}" class="form-control input-md"  type="text">
                                            </div>
                                        </div>
{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('meta_keywords') }}</span>--}}
                                        <!-- Product page title input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Category page title</label>
                                            <div class="col-md-4">
                                                <input id="category_page_title" name="category_page_title" value="{{ old('category_page_title') }}"  @if($errors->has('category_page_title'))style=" border-color:red ;" @endif class="form-control input-md"  type="text">
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('category_page_title') }}</span>--}}
                                        <!-- Clean URL input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Clean URL</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="text" name="cleanURL" value="{{ old('cleanURL') }}" id="cleanURL" @if($errors->has('cleanURL'))style=" border-color:red ;" @endif class="form-control" aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">.html</span>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('cleanURL') }}</span>--}}
                                        <!-- Add to Facebook input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="status">Enabled</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="status" id="status" value="1">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Marketing section End-->
            </div>
            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="add_statusbtn" type="submit" name="" value="Add Category">
            </div>
        </form>
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

@endsection