@extends('admin.layouts.master')

@php
    use \App\Models\Admin\OrdersItems;
    use \App\Models\Admin\InventoryItems;
@endphp

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Manage Stock</h3>
                    </div>
                    <form   method="post" action="{{url('/admin/stock-management/stockReport')}}" >
                        {{csrf_field()}}

                    <a href="{{url('/admin/products-management/add-products')}}" class="btn add_statusbtn">Add Product</a>&nbsp;
                    <a href="{{url('/admin/stock-management/add-purchase')}}" class="btn add_statusbtn">Add Purchase</a><br>
                    <br>
                    <table class="table order_table"  id="myTable">
                        <thead>
                        <tr>
                            <script type="text/javascript">
                                function selectAll(status){
                                    $('.multiplerow').each(function(){
                                        $(this).prop('checked',status);
                                    });

                                }
                            </script>

                            <th>
                                <input type="checkbox" onclick='selectAll(this.checked)' value="Select All"/>
                            </th>
                            <th>Product</th>
                            <th>Closing Stock</th>
                            {{--<th>New Purchase</th>--}}
                            {{--<th>New Sale</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($productsData as $productInfo)
                            @php
                                $OrderItems = new OrdersItems();
                                $InventoryItems = new InventoryItems();

                                $cur_product_id = $productInfo->id;
                                $openingStock = isset($productInfo->quantity_in_stock)?$productInfo->quantity_in_stock:'0';

                                $newSale = $OrderItems->sumItemsSold($cur_product_id);
                                $newPurchase = $InventoryItems->sumItemsPurchased($cur_product_id);

                                $closingStock = ($openingStock + $newPurchase)-($newSale);

                            @endphp
                            <tr>
                                <td class="right_border_dotted"><input type="checkbox" class="multiplerow" id="multiplerow" name="excel[]" value="{{$productInfo->id}}"></td>

                                <td><a class="ml-20" href="{{url('/admin/products-management/updateProducts' , [$productInfo->id])}}">{{$productInfo->product_title}}</a></td>
                                <td>{{$productInfo->quantity_in_stock}}</td>
{{--                                <td>{{$newPurchase}}</td>--}}
                                {{--<td>{{$newSale}}</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="table_pager">
                        <div class="col-lg-6 col-md-6 text-left"></div>

                        {{--<div class="col-lg-6 col-md-6 text-right">--}}
                            {{--<div class="items-per-page-wrapper">--}}
                                {{--{{$productsData->links()}}--}}

                                <button  value="excel" class="btn btn-primary dropdown-toggle mr-10" type="submit"  name="submitbutton" aria-expanded="false">Export As CSV
                                </button>
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="clearfix"></div>
                    </div>

                        </form>

                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')

    <script>



 function searchProducts(){
            var keyword =  $('#searchkeyword').val();
            var category =  $('#category').val();
            var stock =  $('#stock').val();

            $.ajax({
                type:'get',
                url:'{{url("admin/products-management/searchProducts")}}',
                data:{'keyword':keyword,'category':category,'stock':stock},
                success:function(data) {
                    $(' tbody').html(data);


                },
                error:function(){

                },
            });

        }

      function  confirmdelete(a){

          var txt=a;
          if (!confirm("Are You sure to delete this record?")) {
              return false;

          } else {
              window.location = "{{url('/admin/products-management/delProducts')}}/"+txt+"!";

          }

      }
    </script>
    <script>

        $(document).ready(function() {
            $('#myTable').DataTable();
        } );
    </script>
@endsection