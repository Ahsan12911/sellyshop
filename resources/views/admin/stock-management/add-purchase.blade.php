@extends('admin.layouts.master')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main" class="stock-management-data">
            <div class="row">
                <form name="frmPurchase" id="frmPurchase" role="form" method="post" action="{{ url('/admin/stock-management/addPurchase') }}">
                    {{csrf_field()}}
                    <div class="col-md-12">
                        <h3 class="mt-35 font-normal mb-20">Add Purchase</h3>
                        <div class="">
                            @if (session('status'))
                                <div class="alert alert-{{ session('statusClass') }}">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>


                        <script type="text/javascript">
                            $(function() {
                                $("#testdiv").delay(2000).fadeOut(0);
                            });
                        </script>

                        <div id="testdiv" class="alert alert-danger" hidden="hidden">Stock added</div>

                        <div class="search-conditions-box">
                            {{--<div class="col-md-2 pl-0">
                                <label>Product catagories</label>
                                <select class="form-control">
                                    <option>Fruits</option>
                                </select>
                            </div>--}}
                            <div class="col-md-2 pl-0 stock-arrival-date">
                                <label>Stock Arrival Date</label>
                                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                    <input name="purchase_date" id="purchase_date" value="" type="text" class="form-control" required>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-2 pl-0 stock-arrival-date">
                                <label>Invoice#</label>
                                <input name="invoice_no" id="invoice_no" value="" type="text" class="form-control" required>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-2 pl-0">
                                <button class="border_blue search_btn border_radius3 mr-12 add-record"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Product</button>
                            </div>
                            <div class="col-md-2 pl-0">
                                <button type="submit" class="border_blue search_btn border_radius3 mr-12">Save</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table" id="tbl_products">
                                <thead>
                                <tr>
                                    {{--<th width="3%" class="right_border_dotted text-center">Sr</th>--}}
                                    <th width="10%">Product</th>
                                    <th width="5%">Units</th>
                                    <th width="5%">Opening Stock</th>
                                    <th width="5%">New Quantity</th>
                                    <th width="5%">Total Stock</th>
                                    <th width="5%">Cost Price</th>
                                    <th width="5%">Sale Price</th>
                                    <th width="5%">Profit </th>
                                    <th width="3%">FOC</th>
                                    <th width="3%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tbl_products_body">
                                <tr>
                                    {{--<td class="right_border_dotted text-center">1</td>--}}
                                    <td>
                                        <select class="e1 form-control" name="products[]" id="product_1" onchange="getProductInfo(this.value,'1');" required>
                                            <option value="">Select Product</option>
                                            @foreach($productsData as $product)
                                                <option value="{{$product->id}}">{{$product->product_title}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td class="" id="unit1">

                                    </td>
                                    <td><input readonly style="background: #ddd;" type="text" name="openingStocks[]" id="opening_stock_1" class="disabledControl"></td>
                                    <td><input type="text" name="quantities[]" id="quantitiy_1" onchange="updateTotalStock(this.value,'1');" required></td>
                                    <td><input readonly style="background: #ddd;" type="text" name="totalStocks[]" id="total_stock_1"></td>
                                    <td><input type="text" name="costPrices[]" id="cost_price_1" onchange="getSalePrice('1');" required></td>
                                    <td><input   type="text" name="salePrices[]" id="sale_price_1" onchange="getSalePrice('1');" required></td>
                                    <td><input type="text" placeholder="0.00 " name="profits[]" id="profit_1"  required></td>
                                    <td><input type="checkbox" name="focs[]" id="foc_1"></td>
                                {{--    <td width="50" class="left_border_dotted text-center"> <a class="btn btn-primary" data-toggle="modal" data-target="#StoreModel" >Store</a></td>--}}
                                </tr>
                                </tbody>
                            </table>
                            <input type="hidden" name="total_products" id="total_products" value="1">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

    <div class="modal fade" id="StoreModel" role="dialog">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h2>Customer Information</h2>
                    <input type="hidden" id="productIdStore">
                    <p class="fname_error error text-center alert alert-danger hidden"></p>
                    <div class="form-group">
                        <div class="row"><span class="error-class" id="name"></span></div>
                        <label class="control-label col-sm-2" for="Name">Store:</label>
                        {{--<div class="col-sm-10">--}}
                        {{--<select id="storeLocation1" name="rows[0][storeLocation]" class="form-control" >--}}
                        {{--<option>Store</option>--}}
                        {{--@foreach($locationData as $location)--}}
                        {{--<option value="{{$location->id}}">{{$location->store_name}}</option>--}}

                        {{--@endforeach--}}
                        {{--</select>--}}
                        {{--</div>--}}
                    </div>
                    <div class="form-group">
                        <div class="row"><span class="error-class" id="name"></span></div>
                        <label class="control-label col-sm-2" for="Name">Quantity:</label>
                        <div class="col-sm-10">

                            <input type="text" class="form-control col-sm-5" name="rows[0][quantity]" id="quantity1" ></div>
                    </div>
                    <div class="form-group">
                        <div class="row"><span class="error-class" id="name"></span></div>
                        <label class="control-label col-sm-2" for="Name">Store:</label>
                        {{--<div class="col-sm-10">--}}
                        {{--<select id="storeLocation2" name="rows[0][storeLocation]" class="form-control col-sm-5" >--}}
                        {{--<option>Store</option>--}}
                        {{--@foreach($locationData as $location)--}}
                        {{--<option  value="{{$location->id}}">{{$location->store_name}}</option>--}}
                        {{--@endforeach--}}
                        {{--</select>--}}
                        {{--</div>--}}
                    </div>

                    <div class="form-group">
                        <div class="row"><span class="error-class" id="name"></span></div>
                        <label class="control-label col-sm-2" for="Name">Quantity:</label>
                        <div class="col-sm-10">

                            <input type="text" class="form-control col-sm-5" name="rows[0][quantity]" id="quantity2" ></div>
                    </div>

                    {{--<div class="form-group">--}}
                    {{--<div class="row"><span class="error-class" id="name"></span></div>--}}
                    {{--<label class="control-label col-sm-2" for="Name">Store:</label>--}}
                    {{--<div class="col-sm-10">--}}
                    {{--<select id="storeLocation3" name="rows[0][storeLocation]" class="form-control col-sm-5" >--}}
                    {{--<option>Store</option>--}}
                    {{--@foreach($locationData as $location)--}}
                    {{--<option value="{{$location->id}}">{{$location->store_name}}</option>--}}

                    {{--@endforeach--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <div class="row"><span class="error-class" id="name"></span></div>
                        <label class="control-label col-sm-2" for="Name">Quantity:</label>
                        <div class="col-sm-10">

                            <input type="text" class="form-control col-sm-5" name="rows[0][quantity]" id="quantity3" ></div>
                    </div>


                    <p class="email_error error text-center alert alert-danger hidden"></p>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success " style="height: 35px;"  data-dismiss="modal">
                                <span id="footer_action_button" class="glyphicon glyphicon-remove"
                                      onclick="storeStock()"  style="margin-bottom: 15px;">Save Change</span>
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> Close
                        </button>
                    </div>

                    </form>

                </div>
            </div>
        </div>



        {{--<div class="modal-dialog">--}}

        {{--<!-- Modal content-->--}}
        {{--<div class="modal-content">--}}

        {{--<form method="post" action="{{ url('/admin/orders-management/setAddress') }}">--}}

        {{--<input type="hidden" name="id" value="{{$userId}}">--}}
        {{--<input type="text" name="email" value="{{$email}}">--}}
        {{--<input type="text" name="address" value="{{$address}}">--}}
        {{--<input type="text" name="phoneno" value="{{$phoneno}}">--}}
        {{--<input type="submit" value="Save Change" class="btn add_inputbtn">--}}

        {{--</form>--}}
        {{--<div class="modal-footer">--}}
        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--</div>--}}
    </div>


@endsection

@section('footer')
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>

    <script type="text/javascript">
        function updateTotalStock(val,divid)
        {
            var totalStock;
            var openingStock = parseInt($('#opening_stock_'+divid).val());
            var newQuantity = parseInt(val);

            if(newQuantity !='' && !isNaN(newQuantity))
            {
                totalStock = openingStock + newQuantity;
                $('#total_stock_'+divid).val(totalStock);
            }
        }

        function getSalePrice(divid)
        {
            var productSaleprice;
            var costPrice = parseInt($('#cost_price_'+divid).val());


            var saleprice=parseInt($('#sale_price_'+divid).val()) ;
            var profitAmount = saleprice - costPrice;
            var profitPercent = (profitAmount/saleprice)*100;
            var profitPercent = Math.round(profitPercent * 100) / 100;

//        productSaleprice = costPrice + profitAmount;

            if(costPrice !='' && !isNaN(costPrice) && profitAmount !='' && !isNaN(profitAmount))
            {
                $('#profit_'+divid).val(profitPercent);
            }
        }

        function getProductInfo(val,divid)
        {
            var productid = val;


            $.ajax({
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/stock-management/getProductInfo')}}',
                data: 'pid=' + productid + '&divid=' + divid,
                dataType: 'json',
                success: function(data)
                {
                    if(data.status==="success")
                    {
//                        $('#opening_stock_'+divid).val(data.openingStock);
//                        $('#productIdStore').val(val);
//                        $('#total_stock_'+divid).val(data.openingStock);
                        $('#unit'+divid).html(data.unitData);

                    }
                    else if(data.status==="error")

                    {
                        //
                    }
                },
                error:function (error)
                {
                    //
                }
            });
        }

        $(document).ready(function(){

            jQuery(document).delegate('button.add-record', 'click', function(e) {

                e.preventDefault();

                var content = jQuery('#sample_table tr'),
                    size = jQuery('#tbl_products >tbody >tr').length + 1,
                    element = null,
                    element = content.clone();

                $('#total_products').val(size);

                /*element.attr('id', 'rec-'+size);
                element.find('.delete-record').attr('data-id', size);
                element.appendTo('#tbl_products_body');
                element.find('.sn').html(size);*/

                var i = size;

                //$('#tbl_products_body').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
                $('#tbl_products_body').append('<tr id="rec-'+i+'">\n' +
                    '                <td>\n' +
                    '                    <select required class="e1 form-control" name="products[]" id="product_'+i+'" onchange="getProductInfo(this.value,'+i+');">\n' +
                    '                        <option value="">Select Product</option>\n' +
                    '                        @foreach($productsData as $product)\n' +
                    '                            <option value="{{$product->id}}">{{$product->product_title}}</option>\n' +
                    '                        @endforeach\n' +
                    '                    </select>\n' +
                    '                </td>\n' +
                    '                <td class="" id="unit'+i+'">\n' +
                    '                </td>\n' +
                    '                <td><input readonly style="background: #ddd;" type="text" name="openingStocks[]" id="opening_stock_'+i+'"></td>\n' +
                    '                <td><input required type="text" name="quantities[]" id="quantitiy_'+i+'" onchange="updateTotalStock(this.value,'+i+');"></td>\n' +
                    '                <td><input readonly style="background: #ddd;" type="text" name="totalStocks[]" id="total_stock_'+i+'"></td>\n' +
                    '                <td><input required type="text" name="costPrices[]" id="cost_price_'+i+'" onchange="getSalePrice('+i+');"></td>\n' +
                    '                <td><input required  type="text" name="salePrices[]" id="sale_price_'+i+'" onchange="getSalePrice('+i+');"></td>\n' +
                    '                <td><input required type="text" placeholder="0.00 " name="profits[]" id="profit_'+i+'" ></td>\n' +
                    '                <td><input type="checkbox" name="focs[]" id="foc_'+i+'"></td>\n' +
                    '                <td width="50" class="left_border_dotted text-center"><a class="btn btn-xs delete-record" data-id="'+i+'"><i class="glyphicon glyphicon-trash"></i></a></td>\n' +
                    '            </tr>');
            });

            jQuery(document).delegate('a.delete-record', 'click', function(e) {

                e.preventDefault();
                var didConfirm = confirm("Are you sure You want to delete");
                if (didConfirm == true)
                {
                    var id = jQuery(this).attr('data-id');
                    var targetDiv = jQuery(this).attr('targetDiv');
                    jQuery('#rec-' + id).remove();

                    size = jQuery('#tbl_products >tbody >tr').length;
                    $('#total_products').val(size);

                    //regnerate index number on table
                    $('#tbl_products_body tr').each(function(index){
                        $(this).find('span.sn').html(index+1);
                    });

                    return true;
                }
                else
                {
                    return false;
                }
            });

        });


    </script>
    <script>
        function storeStock() {
            var productid=$('#productIdStore').val();
            var storeLocation1=$('#storeLocation1').val();
            var quantity1=$('#quantity1').val();
            var storeLocation2=$('#storeLocation2').val();
            var quantity2=$('#quantity2').val();
            var storeLocation3=$('#storeLocation3').val();
            var quantity3=$('#quantity3').val();

            $.ajax({
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/stock-management/addStoreStock')}}',
                data: 'pid=' + productid + '&rows[0][storeLocation]=' + storeLocation1 + '&rows[0][quantity]=' + quantity1  +
                '&rows[1][storeLocation]=' + storeLocation2 + '&rows[1][quantity]=' + quantity2 +
                '&rows[2][storeLocation]=' + storeLocation3  + '&rows[2][quantity]=' + quantity3,
                dataType: 'json',
                success: function(data)
                {
                    $('#testdiv').html(data).fadeIn('slow');
                    //$('#msg').html("data insert successfully").fadeIn('slow') //also show a success message
                    $('#testdiv').delay(5000).fadeOut('slow');

                    $("#StoreModel").hide('hide');
                },
                error:function (error)
                {
                    //
                }
            });
        }
        function setunitPrice(divid , productid) {
            var unit = $('#productUnits_'+divid).val();
            $.ajax({
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{url('/admin/stock-management/getProductUnitPrice')}}',
                data: 'pid=' + productid + '&divid=' + divid + '&unit=' + unit,
                dataType: 'json',
                success: function(data)
                {
                    if(data.status==="success")
                    {
                        $('#opening_stock_'+divid).val(data.openingStock);
                        $('#productIdStore').val(val);
                        $('#total_stock_'+divid).val(data.openingStock);
                        $('#unit'+divid).html(data.unitData);

                    }
                    else if(data.status==="error")

                    {
                        //
                    }
                },
                error:function (error)
                {
                    //
                }
            });
        }

    </script>
@endsection