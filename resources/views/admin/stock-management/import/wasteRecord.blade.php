

    <thead>
    <tr>
        <script type="text/javascript">
            function selectAll(status){
                $('.multiplerow').each(function(){
                    $(this).prop('checked',status);
                });

            }
        </script>

        <th>
            <input type="checkbox" onclick='selectAll(this.checked)' value="Select All" style="width: 20px"/>
        </th>
        <th>Date</th>
        <th>Product</th>
        <th>Invoice</th>
        <th>Unit</th>
        <th>Opening Stock</th>
        <th>Quantity Waste</th>
        <th>Remaining Stick </th>
        <th></th>
    </tr>
    </thead>
    @foreach($data as $wasteInfo)
        <tr>
            <td class="right_border_dotted"><input type="checkbox" class="multiplerow" id="multiplerow" name="excel[]" value="{{$wasteInfo->id}}"></td>
            @php
                $product=  \App\Models\Admin\Products::all();
                      $productinfo=$product->where('id','=',$wasteInfo->product_id)->pluck('product_title')->first();
            @endphp

            <td>{{date('M d, Y',strtotime($wasteInfo['created_at']))}}</td>
            <td><a class="ml-20"  target="_blank">{{$productinfo}}</a></td>
            <td>{{$wasteInfo->invoice}}</td>
            <td>{{$wasteInfo->unit}}</td>
            <td>{{$wasteInfo->opening_stock}}</td>
            <td>{{$wasteInfo->quantity}}</td>
            <td>{{$wasteInfo->total_stock}}</td>

            <td>{{--<a class="ml-20" href="#">Edit</a>--}}</td>
        </tr>
        @endforeach
    </tbody>
    {{--@endif--}}
