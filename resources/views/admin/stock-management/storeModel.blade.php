<div class="modal-dialog" style="width: 90%">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <h2>Customer Information</h2>
            <form method="post" action="{{ url('/admin/orders-management/setAddress') }}" class="form-horizontal" role="form">
                {{csrf_field()}}
                <p class="fname_error error text-center alert alert-danger hidden"></p>
                <div class="form-group">
                    <div class="row"><span class="error-class" id="name"></span></div>
                    <label class="control-label col-sm-2" for="Name">Store:</label>
                    <div class="col-sm-10">
                        <select id="storeLocation1" name="storeLocation1" class="form-control" >
                            <option>Store</option>
                            @foreach($locationData as $location)
                                <option value="{{$location->id}}">{{$location->store_name}}</option>

                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row"><span class="error-class" id="name"></span></div>
                    <label class="control-label col-sm-2" for="Name">Quantity:</label>
                    <div class="col-sm-10">

                        <input type="text" class="form-control col-sm-5" name="name" id="name" ></div>
                </div>
                <div class="form-group">
                    <div class="row"><span class="error-class" id="name"></span></div>
                    <label class="control-label col-sm-2" for="Name">Store:</label>
                    <div class="col-sm-10">
                        <select id="storeLocation2" name="storeLocation1" class="form-control col-sm-5" >
                            <option>Store</option>
                            @foreach($locationData as $location)
                                <option value="{{$location->id}}">{{$location->store_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row"><span class="error-class" id="name"></span></div>
                    <label class="control-label col-sm-2" for="Name">Quantity:</label>
                    <div class="col-sm-10">

                        <input type="text" class="form-control col-sm-5" name="name" id="name" ></div>
                </div>

                <div class="form-group">
                    <div class="row"><span class="error-class" id="name"></span></div>
                    <label class="control-label col-sm-2" for="Name">Store:</label>
                    <div class="col-sm-10">
                        <select id="storeLocation2" name="storeLocation1" class="form-control col-sm-5" >
                            <option>Store</option>
                            @foreach($locationData as $location)
                                <option value="{{$location->id}}">{{$location->store_name}}</option>

                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row"><span class="error-class" id="name"></span></div>
                    <label class="control-label col-sm-2" for="Name">Quantity:</label>
                    <div class="col-sm-10">

                        <input type="text" class="form-control col-sm-5" name="name" id="name" ></div>
                </div>


                <p class="email_error error text-center alert alert-danger hidden"></p>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success " style="height: 35px;" >
                        <span id="footer_action_button" class="glyphicon glyphicon-check"  style="margin-bottom: 15px;">Save Change</span>
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span> Close
                    </button>
                </div>

            </form>

        </div>
    </div>
</div>