@extends('admin.layouts.master')

@section('content')
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/jquery.dataTables.min.css') }}">
    &nbsp;
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-9">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Waste history</h3>
                    </div>
                    {{--@php($name='newReport')--}}
                    <a href="{{url('/admin/stock-management/waste-loss-stock')}}" class="btn add_statusbtn">Add Waste Loss</a>
                    {{--@if($wasteName=='oldReport')--}}
                    {{--<a href="{{ url('/admin/stock-management/wasteHistory',[$name]) }}" class="btn add_statusbtn">New Waste Loss Report</a>--}}
                    {{--@else--}}
                        {{--@php($name='oldReport')<a href="{{ url('/admin/stock-management/wasteHistory',[$name]) }}" class="btn add_statusbtn">Old Waste and Loss</a>--}}
                        {{--@endif--}}
                    <input name="order_date" id="daterange"
                           class="relative_position width_100 input_padding border_blue border_radius3"
                           type="text" placeholder="Enter Date Range" >
                    <button id="wasteDate" class="btn add_statusbtn" onclick="getWasteRecord()">Get Record</button>

                    <br>
                    <div id="searchwastedata">
                    <form   method="post" action="{{url('/admin/stock-management/WasteExcel')}}" >
                        {{csrf_field()}}

                        <table class="table order_table" id="purchase-data2">
                            <thead>
                            <tr>
                                <script type="text/javascript">
                                    function selectAll(status){
                                        $('.multiplerow').each(function(){
                                            $(this).prop('checked',status);
                                        });

                                    }
                                </script>

                                <th>
                                    <input type="checkbox" onclick='selectAll(this.checked)' value="Select All" style="width: 20px"/>
                                </th>
                                <th>Date</th>
                                <th>Product</th>
                                <th>Invoice</th>
                                <th>Unit</th>
                                <th>Opening Stock</th>
                                <th>Unit Price</th>
                                <th>Quantity Waste</th>
                                <th>Total Waste Price</th>
                                <th>Remaining Stick </th>
                                <th></th>
                            </tr>
                            </thead>

                                <tbody>
                                @php
                                        @endphp
                                @foreach($wasteHistory as $wasteInfo)
                                    <tr>
                                        <td class="right_border_dotted"><input type="checkbox" class="multiplerow" id="multiplerow" name="excel[]" value="{{$wasteInfo->id}}"></td>
                                        @php
                                         $productinfo=$product->where('id','=',$wasteInfo->product_id)->pluck('product_title')->first();
                                        @endphp

                                        <td>{{date('M d, Y',strtotime($wasteInfo['created_at']))}}</td>
                                        <td><a class="ml-20"  target="_blank">{{$productinfo}}</a></td>
                                        <td>{{$wasteInfo->invoice}}</td>
                                        <td>{{$wasteInfo->unit}}</td>
                                        <td>{{$wasteInfo->opening_stock}}</td>
                                        <td>{{$wasteInfo->waste_opening_price}}</td>
                                        <td>{{$wasteInfo->quantity}}</td>
                                        <td>{{$wasteInfo->total_waste_price}}</td>
                                        <td>{{$wasteInfo->total_stock}}</td>

                                        <td>{{--<a class="ml-20" href="#">Edit</a>--}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            {{--@endif--}}
                        </table>
                        <div class="table_pager">
                            <div class="col-lg-6 col-md-6 text-left"></div>
                            <div class="col-lg-6 col-md-6 text-right">
                                <div class="items-per-page-wrapper">

                                    <button  value="excel" class="btn btn-primary dropdown-toggle mr-10" type="submit"  name="submitbutton" aria-expanded="false">Export As CSV
                                    </button>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                    </div>
                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')

    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>


    <script>


        function searchProducts(){
            var keyword =  $('#searchkeyword').val();
            var category =  $('#category').val();
            var stock =  $('#stock').val();

            $.ajax({
                type:'get',
                url:'{{url("admin/products-management/searchProducts")}}',
                data:{'keyword':keyword,'category':category,'stock':stock},
                success:function(data) {
                    $(' tbody').html(data);


                },
                error:function(){

                },
            });

        }

        function  confirmdelete(a){

            var txt=a;
            if (!confirm("Are You sure to delete this record?")) {
                return false;

            } else {
                window.location = "{{url('/admin/products-management/delProducts')}}/"+txt+"!";

            }

        }

    </script>'
    <script>

        $(document).ready(function() {
            $('#purchase-data2').DataTable();
        } );
    </script>
    <script>
            function getWasteRecord() {
               var date= $('#daterange').val();
                $.ajax({
                    type:'get',
                    url:'{{url("/admin/stock-management/wastByDate")}}',
                    data:{'date':date,},
                    success:function(data) {
                        $('#purchase-data2').html(data);


                    },
                    error:function(){

                    },
                });

            }
    </script>


@endsection