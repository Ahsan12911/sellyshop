@extends('admin.layouts.master')
@php
    use App\Models\Admin\Users;
    use App\Models\Admin\Customers;
@endphp

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <form   method="post" action="{{url('/admin/store-management/updatlocation')}}" >
{{csrf_field()}}
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mt-35 font-normal mb-20">New Orders</h3>

                   <table class="dashboard table-striped text-center mailbox-messages" id="dashboard" >
                        <thead>
                        <tr> 
                            <!-- <script type="text/javascript">
                                function selectAll(status){
                                    $('.multiplerow').each(function(){
                                        $(this).prop('checked',status);
                                    });

                                }
                            </script> -->

                            <th class="text-center check_list" width="37">
                                <label class="checkbox_label">
                                    <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                                    </button>
                                    <!-- <span class="marg20">All</span> -->
                                </label>
                                
                                <!-- <input type="checkbox" class="minimal checkbox-toggle" id="check-all" value="Select All"/> -->
                            </th>
                            <th>Order #</th>
                            <th>Source</th>
                            <th>Order Count</th>
                            <th>Customer</th>
                            <th>Location</th>
                            <th>Amount</th>
                            <th>Method</th>
                            <th>Fraud Indicator</th>
                            <th>Fulfilment status</th>
                            <th>Payment status</th>

                            {{--<th>Move to </th>--}}

                            {{--<th>Commission</th>--}}
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($awaitingorders as $awordersInfo)
                            @php


                                    if($awordersInfo->user_id != '')
                                    {
                                        $cur_user_id = $awordersInfo->user_id;

                                        $userObj = new Users();
                                        $userInfo = $userObj->getUserInfo($cur_user_id);

                                        $orderbyname = isset($userInfo[0]->username)?$userInfo[0]->username:'';
                                        $orderbylastname = isset($userInfo[0]->last_name)?$userInfo[0]->last_name:'';
                                        $orderbyemail = isset($userInfo[0]->email)?$userInfo[0]->email:'';
                                         $ordercount=\App\Models\Admin\Orders::getOrdersCountByUID($cur_user_id);
                                    }
                                    else
                                    {
                                        $cur_customer_id = $awordersInfo->customer_id;

                                        $customerObj = new Customers();
                                        $customerInfo = $customerObj->getCustomerInfo($cur_customer_id);

                                        $orderbyname = isset($customerInfo[0]->username)?$customerInfo[0]->username:'';
                                        $orderbyemail = isset($customerInfo[0]->email)?$customerInfo[0]->email:'';

                                        $ordercount=\App\Models\Admin\Orders::getOrdersCountByCID($cur_customer_id);

                                    }
                            @endphp
                        <tr>
                            <td class="right_border_dotted text-center">
                            <input type="checkbox" class="check multiplerow minimal" id="multiplerow" name="update[]" value="{{$awordersInfo->id}}">
                            
                            </td>

                            <td><a href="#">{{$awordersInfo->orderNumber}}</a>
                                <small class="display_block">
                                    {{date('d M , Y',strtotime($awordersInfo->created_at))}} {{--Nov 21, 2017.--}}
                                    {{date('g:i A',strtotime($awordersInfo->created_at))}} {{--11:57 AM--}}


                                    {{--Nov 20, 2017. 03:58 PM--}}</small></td>
                            <td class="text-center">
                                @php
                                    $source=isset($awordersInfo->ordersource)?isset($awordersInfo->ordersource):'';
                                        @endphp
                                @if($awordersInfo->ordersource==1)

                                    <i class="fa fa-television television-icon" aria-hidden="true"></i>

                            @elseif($awordersInfo->ordersource==0)
                                    <i class="fa fa-mobile mobile-icon"  aria-hidden="true"></i>

                                @elseif($awordersInfo->ordersource==2)
                                    <i class="fa fa-phone mobile-icon" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td class="text-center">{{$ordercount}}</td>
                            <td><i class="ti-user"></i> <a href="#">{{$orderbyname}}</a> <small class="display_block">{{$orderbyemail}}</small></td>

                            <td>@php
                                    $storeName=$location->where('id','=',$awordersInfo->location_id)->pluck('store_name')->first();
                                @endphp
                                {{$storeName}}
                            </td>
                            <td>RS.{{$awordersInfo->total}}</td>
                            <td class="text-center">{{$awordersInfo->order_payment_method}}</td>
                            <td class="text-center"><i class="fa fa-shield shield-icon " aria-hidden="true"></i></td>
                            <td class="text-center">{{$awordersInfo->order_shipment_status}}</td>
                            <td class="text-center">{{$awordersInfo->order_payment_status}}</td>

                            {{--<td><select>--}}
                                    {{--<option value="">All store location</option>--}}
                                {{--</select></td>--}}
                            {{--<td>--}}
                                {{--<select onchange="updateOrderLocation('{{$awordersInfo->id}}',this)" id="locationId">--}}
                                    {{--<option >Select Location</option>--}}
                                    {{--@foreach($location as $storelocation)--}}
                                    {{--<option value="{{$storelocation->id}}">{{$storelocation->store_name}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select></td>--}}

                            {{--<td></td>--}}
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div style="float: left">   <select   name="locationId">
                            <option value="">Select Location</option>
                            @foreach($location as $storelocation)
                                <option value="{{$storelocation->id}}">{{$storelocation->store_name}}</option>
                            @endforeach
                        </select>
                        </div>
                    <div><input type="submit" value="Update" id="update" class="btn add_inputbtn"></div>
                    <div style="float: right">  {{$awaitingorders->links()}}

                    </div>
                    <br>
                    <br>
                    <div class="mt-35 col-lg-6 col-md-6">

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Low inventory products ({{$lowinvtproductsCount}})</a></li>
                            <li><a data-toggle="tab" href="#menu1">Best sellers</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">

                                @if($lowinvtproductsCount > 0)
                                    <table class="table products-outofStock table-striped">
                                        <thead>
                                        <tr>
                                            <th>SKU (Stock Keeping Unit ID)</th>
                                            <th>Name</th>
                                            <th>In Stock</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($lowinvproducts as $liproductInfo)
                                            <tr>
                                                <td>{{$liproductInfo->sku}}</td>
                                                <td><a href="#">{{$liproductInfo->product_title}}</a></td>
                                                <td>{{$liproductInfo->quantity_in_stock}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                <p>All the products are in sufficient quantities</p>
                                @endif
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <table class="table products-outofStock table-striped">
                                    <thead>
                                    <tr>
                                        <th>SKU (Stock Keeping Unit ID)</th>
                                        <th>Name</th>
                                        <th>Sold</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($bestsellersproducts as $bestselInfo)
                                        <tr>
                                            <td>{{$bestselInfo->sku}}</td>
                                            <td><a href="#">{{$bestselInfo->product_title}}</a></td>
                                            <td>{{$bestselInfo->quantity_in_stock}}</td>
                                        </tr>

                                    @endforeach

                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>
                @include('admin.layouts.sidebar-right')
            </div>
            </form>
        </div>
        <!-- END MAIN CONTENT -->
    </div>


@endsection

@section('footer')



        <script>
        $(function () {
            //Enable iCheck plugin for checkboxes
            //iCheck for checkbox and radio inputs
            $('.mailbox-messages input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
    
            //Enable check and uncheck all functionality
            $(".checkbox-toggle").click(function () {
                var clicks = $(this).data('clicks');
                if (clicks) {
                    //Uncheck all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
                    $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
                } else {
                    //Check all checkboxes
                    $(".mailbox-messages input[type='checkbox']").iCheck("check");
                    $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
                }
                $(this).data("clicks", !clicks);
            });
    
    
        });
    </script>
    <script>
        function updateOrderLocation(orderId,val) {

            var location=val.options[val.selectedIndex].value;
//        alert(location);
            $.ajax({
                url:"{{url('/admin/updateOrderLocation')}}", //Cart\CartController@placeorder
                type: 'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data : { location:location, orderId:orderId},
                success:function (data) {
                    if(data){
                        window.location.reload();
                    }
                }
            });
        }


        $(document).ready(function() {
    $('#dashboard').DataTable();
    $('.products-outofStock').DataTable();
} );

  //iCheck for checkbox and radio inputs
      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
      });


    </script>
    
@endsection