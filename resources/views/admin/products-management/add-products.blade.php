@extends('admin.layouts.master')

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<style>
    @if($errors->has('product_description'))
        #cke_1_contents{
        border: 1px solid red;
    }
    @endif
        @if($errors->has('product_detail'))
           #cke_2_contents{
        border: 1px solid red;
    }
    @endif
        @if($errors->has('meta_descriptions'))
           #cke_3_contents{
        border: 1px solid red;
    }
    @endif
</style>

@section('content')

    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')

        <form name="frmProducts" id="frmProducts" method="post" action="{{ url('/admin/products-management/addProducts') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div id="main">
                <!-- Add Product section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Add Products</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Product name Text input-->
                                        <div class="form-group{{ $errors->has('productTitle') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="product_name">Product name *</label>
                                            <div class="col-md-4">
                                                <input id="product_title" name="productTitle" class="form-control input-md" @if($errors->has('productTitle'))style=" border-color:red ;" @endif  type="text" value="{{ old('productTitle') }}">
                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('productTitle') }}</span>

                                        <!-- SKU Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name_fr">SKU</label>
                                            <div class="col-md-4">
                                                <input id="sku" name="sku" class="form-control input-md"  type="text" value="{{ old('sku') }}" @if($errors->has('sku'))style=" border-color:red ;" @endif  type="text">
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('sku') }}</span>--}}

                                        <!-- UPC/ISBN Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="available_quantity">UPC/ISBN</label>
                                            <div class="col-md-4">
                                                <input id="upc_isbn" name="upc_isbn" class="form-control input-md"  type="text" value="{{ old('upc_isbn') }}" @if($errors->has('upc_isbn'))style=" border-color:red ;" @endif  type="text">
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('upc_isbn') }}</span>--}}

                                        <!-- Mnf#/Vendor# Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_weight">Mnf#/Vendor#</label>
                                            <div class="col-md-4">
                                                <input id="mnf_vendor" name="mnf_vendor" class="form-control input-md"  type="text" value="{{ old('mnf_vendor') }}" @if($errors->has('mnf_vendor'))style=" border-color:red ;" @endif  type="text">
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('mnf_vendor') }}</span>--}}
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_description">Image</label>
                                            <div class="col-md-4">
                                                <input type="file" name="product_image" id="product_image" class="custom-file-input"  value="{{ old('product_image') }}" @if($errors->has('product_image')) style="  border:1px solid red ;" @endif >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_description">Multiple Images</label>
                                            <div class="col-md-4">
                                                <input type="file" name="multiple_images[]" id="product_image" class="custom-file-input" multiple>
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('product_image') }}</span>--}}

                                        <!-- Category Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name_fr">Category</label>
                                            <div class="col-md-4">
                                                <select class="form-control" id="category_id" name="category_id" @if($errors->has('category_id'))style=" border-color:red ;" @endif  type="text" >
                                                    <option value="" selected="selected">Select Category</option>
                                                    @foreach($categories as $pcat)
                                                        <option value="{{$pcat->id}}">{{$pcat->category_title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Shop  Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name_fr">Shop</label>
                                            <div class="col-md-4">
                                                <select class="form-control" id="shop_id" name="shop_id" @if($errors->has('shop_id'))style=" border-color:red ;" @endif  type="text" >
                                                    <option value="" selected="selected">Select Shop</option>
                                                    @foreach($shops as $shop)
                                                        <option value="{{$shop->id}}">{{$shop->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    {{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('category_id') }}</span>--}}
                                        <!-- Description Text area-->
                                        <div class="form-group" >
                                            <label class="col-md-4 control-label" for="product_weight">Description</label>
                                            <div class="col-md-8">
                                                <div action="#" method="POST" class='form-wysiwyg' >
                                                    <textarea name="product_description" id="product_description" class='ckeditor span12' rows="5"  type="text"> {{ old('product_description') }}</textarea>
                                                </div>
                                            </div>
                                        </div>

{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('product_description') }}</span>--}}
                                        <!--Full Description Text area-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_weight">Full Description</label>
                                            <div class="col-md-8">
                                                <div action="#" method="POST" class='form-wysiwyg'>
                                                    <textarea name="product_detail" id="product_detail" class='ckeditor span12' rows="5" >{{ old('product_detail') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Health Benifits Text area-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_weight">Health Benefits</label>
                                            <div class="col-md-8">
                                                <div action="#" method="POST" class='form-wysiwyg'>
                                                    <textarea name="product_benefits" id="product_detail" class='ckeditor span12' rows="5" >{{ old('product_benefits') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('product_detail') }}</span>--}}
                                        <!--Available for sale yes/no-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_weight">Available for sale</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="available_for_sales" id="available_for_sales" value="1">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="new_arrival">Premium Product</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="premiumProducts" id="new_arrival" value="1">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="popular_items">Popular Item</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="popular_items" id="popular_items" value="1">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="best_sellers">Best Sellers</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="best_sellers" id="best_sellers" value="1">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="featured_product">Featured Product</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="featured_product" id="featured_product" value="1">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>


                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Product section End-->

                <!-- Prices & Inventory section Start-->
                <div class="add-product-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Prices & Inventory</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!--Price  Text input-->
                                        {{--<div class="form-group">--}}
                                            {{--<label class="col-md-4 control-label" for="">Offer Price</label>--}}
                                            {{--<div class="col-md-3">--}}
                                                {{--<div class="input-group">--}}
                                                    {{--<span class="input-group-addon" id="basic-addon1">Rs</span>--}}
                                                    {{--<input type="text" name="marketPrice"  value="{{ old('marketPrice') }}" @if($errors->has('marketPrice'))style=" border-color:red ;" @endif id="marketPrice" class="form-control" placeholder="0.00" aria-describedby="basic-addon1">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('marketPrice') }}</span>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label class="col-md-4 control-label" for="">Sale Price</label>--}}
                                                {{--<div class="col-md-3">--}}
                                                    {{--<div class="input-group">--}}
                                                        {{--<span class="input-group-addon" id="basic-addon1">Rs</span>--}}
                                                        {{--<input type="text" name="salePrice"   id="salePrice" class="form-control" placeholder="0.00" aria-describedby="basic-addon1">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                <div class="col-md-2 text-center">
                                                <div id="sale-checkbox">
                                                    <label>
                                                        <input type="checkbox" name="on_sales" id="on_sales" value="1">
                                                        <span>Sale</span>
                                                    </label>
                                                </div>
                                            </div>

                                            {{--<div class="col-md-3">--}}
                                                {{--<div class="input-widget">--}}
                                                    {{--<div class="input-group">--}}
                                                        {{--<span class="input-group-addon btn dropdown-toggle" data-toggle="dropdown" id="basic-addon1">$ <span class="caret ml-5"></span></span>--}}
                                                        {{--<ul class="dropdown-menu">--}}
                                                            {{--<li><a href="">%</a></li>--}}
                                                            {{--<li><a href="">$</a></li>--}}
                                                        {{--</ul>--}}
                                                        {{--<input type="text" name="salePriceValue" id="salePriceValue" class="form-control" placeholder="0.00" aria-describedby="basic-addon1">--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}

                                            <!-- Arrival date Calendar-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Arrival date</label>
                                            <div class="col-md-4">
                                                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                                    <input type="text" name="arrivalDate" id="arrivalDate"   value="{{ old('arrivalDate') }}"   @if($errors->has('arrivalDate'))style=" border-color:red ;" @endif class="form-control">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>

                                            </div>
                                        </div>

{{--                                        <span class="text-danger col-md-8" style="margin-left: 32%">{{ $errors->first('arrivalDate') }}</span>--}}
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Expire date</label>
                                            <div class="col-md-4">
                                                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                                                    <input type="text" name="expireDate" value="{{ old('expireDate') }}"  id="arrivalDate" class="form-control"  @if($errors->has('expireDate'))style=" border-color:red ;" @endif>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('expireDate') }}</span>--}}

                                        <!-- Inventory tracking-->
                                        <div class="form-group{{ $errors->has('productTitle') ? 'has-error' : '' }}">
                                            <label class="col-md-4 control-label" for="">Inventory tracking
                                            </label>
                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle" name="inventoryEnabled" id="inventoryEnabled" value="1">
                                                    </label>
                                                </div>
                                            </div>

                                            {{--<div class="col-md-2">--}}
                                                {{--<label class="control-label" for="">Quantity in stock--}}
                                                {{--</label>--}}
                                            {{--</div>--}}

                                            {{--<div class="col-md-2">--}}
                                                {{--<input type="text"  name="quantity_in_stock" value="{{ old('quantity_in_stock') }}"   @if($errors->has('quantity_in_stock'))style=" border-color:red ;" @endif id="quantity_in_stock" class="form-control" placeholder="1000" aria-describedby="basic-addon1">--}}
                                            {{--</div>--}}
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('quantity_in_stock') }}</span>

                                        <!-- length Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Length of fractional part of quantity</label>
                                            <div class="col-md-4">
                                                <input id="product_length_fractional_part" name="product_length_fractional_part"   @if($errors->has('product_length_fractional_part'))style=" border-color:red ;" @endif  value="{{ old('product_length_fractional_part') }}" class="form-control input-md" type="text" placeholder="0">
                                            </div>
                                        </div>
{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('product_length_fractional_part') }}</span>--}}

                                        <!--Quantity units Text input-->
                                        {{--<div class="form-group">--}}
                                            {{--<label class="col-md-4 control-label" for="">Quantity units</label>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<input id="unit" name="unit"   value="{{ old('unit') }}" class="form-control input-md" type="text" placeholder=""  @if($errors->has('unit'))style=" border-color:red ;" @endif>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="product_name_fr">Quantity units</label>
                                            <div class="col-md-2">
                                                {{--<button type="button" class="create-inline add_statusbtn" id="addproductunit" value="Add Row" >Add quantity unit</button>--}}
                                            </div>
                                        </div>
                                        <!--Add Multiplier Table-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for=""></label>
                                            <div class="col-md-8">
                                                <table class="table tables_ui order-list" id="unittable">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        {{--<th>Multiplier</th>--}}
                                                        <th>Unit</th>
                                                        <th>Price</th>
                                                        <th>Discount Price</th>
                                                        <th>Quantity</th>
                                                        <th></th>

                                                    </tr>
                                                    </thead>
                                                    <tbody class="t_sortable">
                                                    <tr>
                                                        <td></td>

                                                        <td class=""> <input type="text" name="rows[0][unit]" required   class="form-control" ></td>
                                                        <td class=""> <input type="text" name="rows[0][salePrice]" required   class="form-control" ></td>
                                                        <td class=""> <input type="text" name="rows[0][discountPrice]" class="form-control" ></td>
                                                        <td class=""> <input type="text" name="rows[0][quantity_in_stock]" class="form-control" ></td>
                                                        {{--<td class="left_border_dotted td_width10px">--}}
                                                            {{--<i class="fa fa-trash-o" aria-hidden="true"></i>--}}
                                                        {{--</td>--}}
                                                        <td class="left_border_dotted td_width10px">  <button type="button" class="create-inline add_statusbtn" id="addproductunit" value="Add Row" ><i class="fa fa-plus-square-o" aria-hidden="true"></i></button></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--Quantity units-->
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('unit') }}</span>--}}

                                        <!--Add Multiplier Table-->

                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Prices & Inventory section End-->

                <!-- Shipping section Start-->
                <div class="shipping-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Shipping</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!--Weight Text input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Weight</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="basic-addon1">Ibs</span>
                                                    <input type="text" name="weight" id="weight"  value="{{ old('weight') }}" @if($errors->has('weight'))style=" border-color:red ;" @endif class="form-control" placeholder="0.0000" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                        </div>
                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('weight') }}</span>

                                        <!--Requires shipping Text input-->
                                        <div class="form-group}">
                                            <label class="col-md-4 control-label" for="">Requires shipping
                                            </label>
                                            <div class="col-md-2">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="require_shipping" id="require_shipping" data-toggle="toggle" value="1">
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="shipping-checkbox">
                                                    <label>
                                                        <input type="checkbox" name="free_shipping" id="free_shipping" value="1">
                                                        <span>Free shipping</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="free-shipping-tax">
                                                <div class="col-md-2 text-right">
                                                    <label class="control-label" for="">Freight
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic-addon1">Rs</span>
                                                        <input type="text" name="freight"  value="{{ old('freight') }}"  @if($errors->has('freight'))style=" border-color:red ;" @endif id="freight" class="form-control" placeholder="0.00" aria-describedby="basic-addon1">
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('freight') }}</span>

                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Shipping section End-->

                <!-- Marketing section Start-->
                <div class="marketing-sec">
                    <div class="row">
                        <div class="col-md-9">
                            <div>
                                <h3 class="mt-35 font-normal mb-20 display-ib">Marketing</h3>
                            </div>
                            <div class="add-product-box">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <!-- Meta description input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Meta Description</label>
                                            <div class="col-md-8" >
                                                <div action="#" method="POST" class='form-wysiwyg'>
                                                    <textarea name="meta_descriptions" id="meta_descriptions" class='ckeditor span12' rows="5"> {{ old('meta_descriptions') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('meta_descriptions') }}</span>--}}

                                        <!-- Meta keywords input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Meta keywords</label>
                                            <div class="col-md-4">
                                                <input id="meta_keywords" name="meta_keywords" value="{{ old('meta_keywords') }}" @if($errors->has('meta_keywords'))style=" border-color:red ;" @endif class="form-control input-md"  type="text">
                                            </div>
                                        </div>
                                        {{--<span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('meta_keywords') }}</span>--}}
                                        <!-- Product page title input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Product page title</label>
                                            <div class="col-md-4">
                                                <input id="product_page_title" name="product_page_title" value="{{ old('product_page_title') }}" @if($errors->has('product_page_title'))style=" border-color:red ;" @endif class="form-control input-md"  type="text">
                                            </div>
                                        </div>
{{--                                        <span class="text-danger col-md-9" style="margin-left: 32%">{{ $errors->first('product_page_title') }}</span>--}}
                                        <!-- Clean URL input-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="">Clean URL</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="text" name="cleanURL" id="cleanURL"  value="{{ old('cleanURL') }}" @if($errors->has('cleanURL'))style=" border-color:red ;" @endif class="form-control" aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">.html</span>
                                                </div>
                                                {{--<span class="text-danger col-md-9" >{{ $errors->first('cleanURL') }}</span>--}}
                                                <div class="help-block">
                                                    <ul class="list-unstyled">
                                                        <li>The clean URL will be generated automatically.
                                                            .
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        @can('Product Enable')
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="status">Enabled</label>
                                            <div class="col-md-8">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-toggle="toggle"  name="status" id="status" value="1">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                            @endcan
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Marketing section End-->
            </div>
            <!-- END MAIN CONTENT -->

            <!-- Add Product button fixed at bottom -->
            <div class="add-productbtn-fixed">
                <input class="add_statusbtn" type="submit" name="" value="Add Product">
            </div>
        </form>
    </div>

@endsection


@section('footer')

    <script src="{{ URL::asset('public/admin/assets/scripts/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    {{--<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>--}}

    <script>
        var i =1;
        $("#addproductunit").on("click", function () {

            var newRow = $("<tr>");
            var cols = "";

            cols += '<td></td>';
            cols += '<td><input type="text" class="form-control" name="rows['+i+'][unit]"/></td>';
            cols += '<td class=""> <input type="text" name="rows['+i+'][salePrice]" required   class="form-control" ></td>';
            cols += '<td class=""> <input type="text" name="rows['+i+'][discountPrice]" class="form-control" ></td>';
            cols += '<td class=""> <input type="text" name="rows['+i+'][quantity_in_stock]" class="form-control" ></td>';
            cols += '<td class="ibtnDel left_border_dotted td_width10px"  value="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></td>';

            newRow.append(cols);
            $("table.order-list").append(newRow);
            i ++;
        });



        $("table.unittable").on("click", ".ibtnDel", function (event) {
            $(this).closest("tr").remove();

        });




    </script>
@endsection