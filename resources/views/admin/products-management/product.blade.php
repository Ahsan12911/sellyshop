@extends('admin.layouts.master')

@section('content')
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <style>
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #2caae1;;
            border-right: 16px solid white;;
            border-bottom: 16px solid #2caae1;;
            border-left: 16px solid white;    ;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
            position: fixed;
            display: none;
            width: 70px;
            height:70px;
            top: 60%;
            left: 40%;
            right: 20%;
            bottom: 40%;
            cursor: pointer;

        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

    </style>
    <script language="javascript" type="text/javascript">
        $(window).load(function() {
            $('#loader').hide();
        });
    </script>
    <div class="main">
        <!-- MAIN CONTENT -->
        @include('admin.layouts.header')
        <div id="main">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <h3 class="mt-35 font-normal mb-20 display-ib">Products</h3>
                    </div>
                    <form method="post" action="{{url('admin/products-management/products/searchProducts')}}">
                        {{csrf_field()}}
                    <div class="search-conditions-box clearfix products_box">
                        <div class="col-md-3">
                            <input type="text" placeholder="Search Keywords" id="searchkeyword" name="keyword">
                        </div>
                        <div class="col-md-3">
                            <select name="category" id="category" >
                                <option value="">Any Category</option>
                                @foreach($category as $cat)
                                    <option value="{{$cat->id}}">{{$cat->category_title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select id="stock" name="stock">
                                <option value="">Any Stock Status</option>
                                {{--<option>In Stock</option>--}}
                                <option value="lowstock">Low Stock</option>
                                <option value="outofstock">Out Of Stock</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="border_blue search_btn border_radius3 btn-block">Search</button>
                        </div>
                        <div class="col-md-1">
                            @php
                                $search=isset($clearsearch)?$clearsearch:'empty';

                            @endphp
                            @if($search == 'clearsearch')
                                <a href="{{url('/admin/products-management/products')}}" class="border_blue search_btn border_radius3 btn-block">Clear</a>
                            @endif

                        </div>
                    </div>
                    </form>
                    <br>


                    <form method="post" action="{{url('/admin/products-management/delmultipleProduct')}}" onsubmit="return confirm('Are you sure to delete these records?');">
                        {{csrf_field()}}
                        <a href="{{url('/admin/products-management/add-products')}}" class="btn add_statusbtn">Add Product</a>

                        @if($productsData->count())

                            <input type="submit" name="submit" id="delete-order" class="btn add_inputbtn"     value="Delete Selected Value" >
                        @endif             <br>
                        <br>
                        <div class="search-conditions-hidden search-conditions-box products-hidden-box clearfix">
                            <div class="col-md-3">
                                <div>
                                    <p>Search In:</p>
                                    <ul>
                                        <li>
                                            <label>
                                                <input type="checkbox" name="" value="">
                                                Name</label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="checkbox" name="" value="">
                                                SKU</label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="checkbox" name="" value="">
                                                Description</label>
                                        </li>
                                        <li>
                                            <label>
                                                <input type="checkbox" name="" value="">
                                                Tag</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div>
                                    <span>Availability</span>
                                    <select>
                                        <option>any availability status</option>
                                        <option>Only Enabled</option>
                                        <option>Only Disabled</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(function() {
                                $("#hideaftermoment").delay(2000).fadeOut(0);
                            });
                        </script>
                        @if (session('status'))
                            <div id="hideaftermoment" class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <script type="text/javascript">
                            $(function() {
                                $("#testdiv").delay(2000).fadeOut(0);
                            });
                        </script>
                        @if($errors->any())
                            <h4 id="testdiv" class="alert alert-danger">{{$errors->first()}}</h4>
                        @endif
                        <table class="table order_table"  id="myTable">
                            <thead>
                            <tr>
                                <script type="text/javascript">
                                    function selectAll(status){
                                        $('.multiplerow').each(function(){
                                            $(this).prop('checked',status);
                                        });

                                    }
                                </script>

                                <th>
                                    <input type="checkbox" onclick='selectAll(this.checked)' value="Select All"/>
                                </th>

                                @can('Product Enable')
                                <th></th>
                                @endcan
                                <th>SKU</th>
                                <th></th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>In Stock</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            <div class="loader" id="loader" ></div>
                            @foreach($productsData as $productInfo)
                                @php
                                    //------------ Category Detail ----------
                                    $product_category = '';
                                    //---------------------------------------
                                @endphp
                                <tr>
                                    <td class="right_border_dotted"><input type="checkbox" class="multiplerow" id="multiplerow" name="delete[]" value="{{$productInfo->id}}"></td>

                                    @can('Product Enable')
                                        @if($productInfo->status == '1')
                                        <td title="Enabled" class="right_border_dotted text-center"><a href="{{url('/admin/products-management/statusupdate',[$productInfo->id])}}"><i class="fa fa-power-off" aria-hidden="true"></i></a></td>
                                    @else
                                        <td title="Disabled" class="right_border_dotted text-center"><a href="{{url('/admin/products-management/updatestatusone',[$productInfo->id])}}"><i class="fa fa-power-off" style="color:red" aria-hidden="true"></i></a></td>
                                    @endif
                                    @endcan
                                    <td><span>{{$productInfo->sku}}</span></td>
                                    <td>
                                        <div class="file-uploader">
                                            <img src="{{ URL::asset('public/images/products') }}/{{$productInfo->product_image}}" alt="" class="img-responsive">
                                        </div>
                                    </td>
                                    <td>
                                        <a class="ml-20" href="{{url('/admin/products-management/updateProducts' , [$productInfo->id])}}">{{$productInfo->product_title}}</a>
                                    </td>
                                    <td>

                                        @foreach($category as $cat)

                                            @if($cat->id == $productInfo->category_id )
                                                <span>{{$cat->category_title}}</span>
                                            @endif

                                        @endforeach
                                    </td>
                                    <td>
                                        <div class="editable">
                                            <span class="symbol">Rs</span>
                                            
                                            <span class="value">{{$productInfo->salePriceValue}}</span>
                                        </div>
                                    </td>
                                    <td>{{$productInfo->quantity_in_stock}}</td>
                                    {{--                            <td><a href="{{url('/admin/products-management/update-stock' , [$productInfo->id])}}">Add stock</a></td>--}}
                                    <td onclick="return confirm('Are You sure to delete this record? ');" id="delete-product" class="left_border_dotted text-center"><a href="{{url('/admin/products-management/delProducts', [$productInfo->id])}}"> <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="table_pager">
                            <div class="col-lg-4 col-md-4 text-left">
                            </div>

                            <div class="col-lg-4 col-md-offset-4 col-md-4 text-right" style="float: right">
                                <div class="items-per-page-wrapper" id="page-link">
                                    {{$productsData->links()}}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>


                </div>
                @include('admin.layouts.sidebar-right')
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

@endsection

@section('footer')




    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.resize.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/Flot/jquery.flot.time.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/flot.tooltip/jquery.flot.tooltip.js') }}"></script>--}}
{{--    <script src="{{ URL::asset('public/admin/assets/vendor/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>--}}
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-sparkline/js/jquery.sparkline.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/bootstrap-tour/js/bootstrap-tour.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widget.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/data.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/scroll-parent.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/disable-selection.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/mouse.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-ui/ui/widgets/sortable.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-main/jquery.dataTables.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jquery-appear/jquery.appear.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/jquery.vmap.min.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('public/admin/assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>--}}
    <script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>


    <script>



        function searchProducts(){
            var keyword =  $('#searchkeyword').val();
            var category =  $('#category').val();
            var stock =  $('#stock').val();
            $('#loader').show();
            $.ajax({
                type:'get',
                url:'{{url("admin/products-management/searchProducts")}}',
                data:{'keyword':keyword,'category':category,'stock':stock},
                success:function(data) {
                    $(' tbody').html(data);
                    $('#page-link').hide();
                    $('#loader').hide();

                },
                error:function(){
                    $('#page-link').show();
                },
            });

        }

        function  confirmdelete(a){

            var txt=a;
            if (!confirm("Are You sure to delete this record?")) {
                return false;

            } else {
                window.location = "{{url('/admin/products-management/delProducts')}}/"+txt+"!";

            }

        }

    </script>


