@extends('admin.layouts.master')
@section('content')

    {{----}}
    {{--<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">--}}
    {{----}}
    {{--<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>--}}

    <section class="content">
        <!-- MAILBOX BEGIN -->
        <div class="mailbox row">
            <div class="col-xs-12">
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="">
                            <div class="col-md-12">
                                <!-- BOXES are complex enough to move the .box-header around.
                                     This is an example of having the box header within the box body -->
                                {{--<div class="box-header pl-0">--}}
                                    {{--<i class="icon-gear"></i>--}}
                                    {{--<span>Module Access Permissions</span>--}}
                                    {{--<h3 class="box-title ml-10">()</h3>--}}
                                {{--</div>--}}
                                <!-- Navigation - folders-->
                                {{--<div style="margin-top: 15px;" class="module-access-tabs">--}}
                                    {{--<ul class="nav nav-pills">--}}
                                        {{--<li class="li-custom  "><a class="a-custom" href="">Roles</a></li>--}}
                                        {{--<li class="li-custom "><a class="a-custom" href="">Permissions</a></li>--}}
                                        {{--<li class="li-custom  "><a class="a-custom" href="">Assign Role to Users</a></li>--}}
                                        {{--<li class="li-custom @if ($CLT === '') active @endif "><a class="a-custom" href="{{url('/hr-management/general-sops')}}"></a></li>--}}
                                        {{--<li class="li-custom @if ($CLT === '') active @endif "><a class="a-custom" href="{{url('/hr-management/approvals')}}"></a></li>--}}
                                        {{--<li class="li-custom @if ($CLT === '') active @endif "><a class="a-custom" href="{{url('/hr-management/leave-balance')}}"></a></li>--}}
                                        {{--<li class="li-custom @if ($CLT === '') active @endif "><a class="a-custom" href="{{url('/hr-management/holidays')}}"></a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div><!-- /.col (LEFT) -->--}}
                            <!-- /.col (RIGHT) -->
                            <div>&nbsp;</div>
                            <section class="">
                                @yield('module_access_management_content')
                            </section>

                        </div><!-- /.row -->
                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">

                    </div><!-- box-footer -->
                </div><!-- /.box -->
            </div><!-- /.col (MAIN) -->
        </div>
        <!-- MAILBOX END -->
        </div>
    </section>

    <style>
        .li-custom{
            height: 60px;
            border: 1px solid #367fa9;
        }
        .a-custom{
            height: 100%;
        }
    </style>

@endsection


{{--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>--}}
