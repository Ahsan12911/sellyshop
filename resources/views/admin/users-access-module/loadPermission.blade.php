
            <legend>Permissions for <strong>{{$roleName->name}}</strong></legend>
@php
$counter=1;
@endphp
            @foreach($all_permissions as $permission)
                <div class="col-md-5"> <strong>{{ $permission->description }}</strong></div>

                {{--<div class="checkbox">--}}
                    {{--<label>--}}
                        {{--<input type="checkbox" data-toggle="toggle" >--}}
                    {{--</label>--}}
                {{--</div>--}}
                <input  data-toggle="toggle"  title="{{$permission->name}}" id="checkbox{{$counter}}" onchange="togglePermission('{{$permission->id}}','{{$roleName->id}}','{{$counter}}')"  type="checkbox" value="yes" name="checkbox" class="toggle-one"   data-size="small"  data-onstyle="success" data-offstyle="danger"
                        @foreach($role as $roles)
                        @if($permission->id == $roles->permission_id)
                        checked
                        @endif
                        @endforeach
                >

                <br>
                <br>

                @php
                    $counter ++;
                @endphp
            @endforeach


            <script>
                $(function() {
                    $('.toggle-one').bootstrapToggle();
                })
            </script>