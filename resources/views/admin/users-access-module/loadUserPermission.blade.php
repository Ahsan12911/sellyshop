@php
    $counter=1;
@endphp
@foreach($all_permissions as $permission)
    <div class="col-md-5">
        <strong>{{ $permission->description }}</strong>
    </div>
    <input data-toggle="toggle" title="{{$permission->name}}" id="checkbox{{$counter}}"
           onchange="toggleUsersPermission('{{$permission->id}}','{{$user->id}}','{{$counter}}')" type="checkbox"
           value="yes" name="checkbox" class="toggle-one" data-size="small" data-onstyle="success"
           data-offstyle="danger"
           @if(in_array($permission->id,$userPermissions))
           checked
            @endif
    >

    <br>
    <br>

    @php
        $counter ++;
    @endphp
@endforeach


<script>
    $(function () {
        $('.toggle-one').bootstrapToggle();
    })
</script>