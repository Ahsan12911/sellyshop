<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <form method="post" action="{{url('/admin/users-access-module/updatePermissions')}}" class="form-horizontal" role="form">
                {{csrf_field()}}
                <p class="fname_error error text-center alert alert-danger hidden"></p>
                <div class="form-group">
                    <div class="row"><span class="error-class" id="name"></span></div>
                    <label class="control-label col-sm-2" for="Name">Name:</label>
                    <div class="col-sm-10">
                        <input type="hidden" class="form-control" name="Id" id="EditId" value="{{$permission->id}}">
                        <input type="text" class="form-control" name="Name" id="EditName" value="{{$permission->name}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="row"><span class="error-class" id="title"></span></div>
                    <label class="control-label col-sm-2" for="Name">Title:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="Title" id="EditTitle" value="{{$permission->title}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="Name">Description:</label>
                    <div class="col-sm-10">
                        <textarea rows="7" class="form-control" name="Description" id="EditDescription">{{$permission->description}}</textarea>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row"><span class="error-class" id="roles"></span></div>
                    <label class="control-label col-sm-2" for="role">Roles:</label>
                    <div class="col-sm-10">
                        <select name="role[]" class="form-control Multi_Select" multiple="multiple" data-placeholder="Select Roles" style="width: 100%;" >
                            {{--@if(isset($rolesSelected->id)?$rolesSelected->id:'')--}}
                          {{--<option>  {{$rolesSelected->id}}</option>--}}
                            @foreach($roles as $role)
                                    <option value="{{$role->id}}" @foreach($rolesSelected as $select) @if($select->role_id ==$role->id ) selected @endif @endforeach >
                                        {{$role->name}}</option>
                                @endforeach

                        </select>
                    </div>
                </div>

                <p class="email_error error text-center alert alert-danger hidden"></p>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success " style="height: 35px;" >
                        <span id="footer_action_button" class="glyphicon glyphicon-check"  style="margin-bottom: 15px;"> Update</span>
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span> Close
                    </button>
                </div>

            </form>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.Multi_Select').multiselect();
    });

</script>