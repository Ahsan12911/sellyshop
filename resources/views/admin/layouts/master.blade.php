<!doctype html>
<html lang="en">
<head>
    <title>Pakistan’s No 1 Online Grocery E-Store | Selly.pk</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->

    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/themify-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/vendor/animate/animate.min.css') }}">
    {{--<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/x-editable/bootstrap3-editable/css/bootstrap-editable.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/dropzone/dropzone.min.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/bootstrap-tour/css/bootstrap-tour.min.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/jqvmap/jqvmap.min.css') }}">--}}
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/daterangepicker.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css">

    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/selectric.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/bootstrap-multiselect.css') }}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/demo.css') }}">

    <!-- <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/demo-panel/style-switcher.css') }}"> -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/jquery.dataTables.min.css') }}">

    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('public/admin/assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ URL::asset('public/admin/assets/img/selly-favicon.png') }}">

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/moment/min/moment.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

</head>
<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- LEFT SIDEBAR -->
    @include('admin.layouts.sidebar-left')
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    @yield('content')

    @include('admin.layouts.orderjs')
    <!-- END MAIN -->
    <div class="clearfix"></div>
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
    @yield('footer')
<script src="{{ URL::asset('public/admin/assets/scripts/klorofilpro-common.js') }}"></script>
</body>
</html>