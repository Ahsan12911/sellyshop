@if(isset($subFulfilmentStatus))
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Select Reason</h4>
            </div>
            <div class="modal-body">
                <form id="formSubFullfilmentStatus" method="post">
                    <div class="form-group">
                        <label for="sub_shipping_status">Select Reason For Order Not Delivered:</label>
                        <select class="form-control" id="sub_shipping_status">
                            <option selected disabled="disabled">---Select One---</option>
                            @foreach($subFulfilmentStatus as $item)
                                <option value="{{$item->status_number}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" id="sub_shipping_status_orderId" value="">
                    <button type="submit" class="btn btn-success">Save</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endif
<script>
    function updatePaymentStatus(value, order_id) {
        $.ajax({
            url: "{{ url('/admin/orders-management/order/setPayStatus') }}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {'orderId': order_id, 'value': value},
            success: function (data) {
                if (data != "Error") {
                    /*var message_contaner = $('#direct-chat-messages_' + id);
                    message_contaner.append(data);*/
                }
            },
        });
    }


    function updateFullfilmentStatus(value, order_id, order_date) {

        $('#sub_shipping_status_orderId').val('');
        $.ajax({
            url: "{{ url('/admin/orders-management/order/setFulfilStatus') }}",
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {'orderId': order_id, 'value': value, 'order_date': order_date},
            success: function (data) {
                if (data != "Error") {
                    $('#fulfil_status_'+order_id).prop('disabled',true);
                    var statusCode = data.statusCode;
                    if(statusCode === "pro")
                    {
                        $('#processed_by_'+order_id+' option[value="'+'{{\Illuminate\Support\Facades\Auth::user()->id}}'+'"]').prop('selected', true)
                    }
                    if(statusCode === "ond")
                    {
                        $('#sub_shipping_status_orderId').val(order_id);
                        $('#myModal').modal('show');
                    }
                    /*var message_contaner = $('#direct-chat-messages_' + id);
                    message_contaner.append(data);*/
                }
            },
            error: function (error) {
                alert('Selected Quantity is not available');
                                location.reload();


            }
        });
    }

    $('#formSubFullfilmentStatus').submit(function(e) {

        var formData = {
            'orderId' : $('#sub_shipping_status_orderId').val(),
            'status'  : $('#sub_shipping_status').val()
        };
        var url = "{{url('admin/orders-management/order/setSubFulfilStatus')}}"; // th0e script where you handle the form input.
        $.ajax({
            type: "POST",
            url: url,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: formData, // serializes the form's elements.
            success: function(data)
            {
                $('#shipping_sub_status_message_'+$('#sub_shipping_status_orderId').val()).text(data.statusMessage);
                $('#myModal').modal('hide');
            },
            error:function (e) {
                $('#myModal').modal('hide');
                alert("Something went Wrong...!!! Please Report the error to the admin");
            }
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

</script>