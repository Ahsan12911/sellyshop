<tr>
    <th>Product</th>
    <th>Closing Stock</th>
</tr>
@foreach($productsData as $productDetail)
@foreach($productDetail as $productInfo)
    @php
        $OrderItems = new \App\Models\Admin\OrdersItems();
        $InventoryItems = new \App\Models\Admin\InventoryItems();

        $cur_product_id = $productInfo->id;
        $openingStock = isset($productInfo->quantity_in_stock)?$productInfo->quantity_in_stock:'0';

        $newSale = $OrderItems->sumItemsSold($cur_product_id);
        $newPurchase = $InventoryItems->sumItemsPurchased($cur_product_id);

        $closingStock = ($openingStock + $newPurchase)-($newSale);

    @endphp
    <tr>

        <td><a class="ml-20" href="{{url('/admin/products-management/updateProducts' , [$productInfo->id])}}">{{$productInfo->product_title}}</a></td>
        <td>{{$productInfo->quantity_in_stock}}</td>
    </tr>
@endforeach
@endforeach