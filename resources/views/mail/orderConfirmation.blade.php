<!DOCTYPE html>
<html dir="ltr" lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="My Store" />
    <title>Selly.pk</title>
    <link href="image/catalog/cart.png" rel="icon" />
    <!-- Fonts - Start -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:500,700,900,300,400' rel='stylesheet' type='text/css'>
    <!-- Fonts - End -->
    <!-- CSS - Start -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- CSS - End -->
    <!-- JavaScript - Start -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- JavaScript - End -->
    <style>
        body {
            color: #808080;
            font-family: "Roboto", sans-serif;
            font-size: 14px;
            font-weight: 400;
            letter-spacing: 0.7px;
            line-height: 24px;
            margin:0;
        }
        .container h1{
            color: #000;
            font-weight: 700;
            text-transform: uppercase;
        }
        .checkout_payment .p-0 {
            padding: 0 !important;
        }
        .checkout_form_invoice {
            padding: 40px 0 0 0;
        }
        .invoice_box h2 {
            font-size: 20px;
            margin-bottom: 15px;
            text-transform: capitalize;
            color:#000;
        }
        .mb-0 {
            margin-bottom: 0 !important;
        }
        .mt-20 {
            margin-top: 20px;
        }
        .billing_address_box h3 {
            color: #000;
            font-size: 18px;
            margin-top: 40px;
        }
        .checkout_payment .billing_address_box span {
            display: inline-block;
            margin-bottom: 0;
        }
        .payment_method_box h3 {
            color: #000;
            font-size: 18px;
            margin-top: 40px;
        }
        .invoice_box_right {
            padding: 40px 0 0 0;
        }
        .invoice_box_right img {
            width: 200px;
        }
        .place_order_table {
            margin-top: 50px;
        }
        .place_order_table .table>thead>tr>th {
            border-bottom: 1px solid #ddd;
            padding: 10px 0;
        }
        .place_order_table .table>thead>tr>th {
            width: 30% !important;
        }
        .place_order_table .table>tbody>tr>td {
            border-top: 1px solid #ddd;
            padding:5px;width:0;
        }
        .grand-total{font-weight:600;margin-top:10px;}
        .content-top{margin-top:50px;}
        .page-title{font-size:20px;}
        .place_order_table .grand-total{color:#000;font-size:20px;}
    </style>
</head>
<body class="checkout-cart">
<div class="container checkout_payment">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="col-md-12">
                <div class="content-top">
                    <h1 class="page-title">Thank you for your order
                    </h1>
                </div>
                <p>Congratulations! Your order has been placed.
                    You will receive your order confirmation shortly.
                </p>
            </div>
            <div class="col-md-12">
                <button type="button" class="btn btn-default">Print Invoice</button>
                <hr>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-5 col-sm-6 col-xs-6">
                <div class="checkout_form_invoice invoice_box clearfix">
                    <h2 class="mb-10">Invoice #905</h2>
                    <p class="order-date mb-0">Feb 22, 2018, 04:36 PM</p>
                    <p class="grand-total">Grand total: Rs 35.00</p>
                    <div class="billing_address_box mt-20">
                        <h3>Billing Address</h3>
                        <div class="billing_firstName">Name : Atique Yousaf</div>
                        <div class="billing_StreetNum"><span>Address : </span>House No. 123 St No. 1, Makkah Colony, <br> Gulberg III. Lahore </div>
                        <div class="billing_PhoneNum"><span>Phone No.: </span><b> 0315-7696461</b></div>
                        <div class="payment_method_box mt-20">
                            <h3>Payment Method</h3>
                            <span>Cash on Delivery</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-6">
                <div class="invoice_box_right clearfix">
                    <div class="">
                        <img src="{{Utility::getImage('template/selly-logo.png')}}" title="Your Store" alt="Your Store" class="img-responsive">
                        <p class="url">
                            <a href="#">www.selly.pk</a>
                        </p>
                        <p>
                            405, Al- Hafeez Shopping Mall, Main Boulevard <br> Gulberg III. Lahore, Punjab, 54670
                            Pakistan
                        </p>
                        <p>Phone: 0304 - 111 - 7355</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="table-responsive place_order_table">
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="30%">Item list</th>
                            <th width="30%">Price</th>
                            <th width="30%">Qty</th>
                            <th width="5%">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <a href="#" class="item-name">Potatoes  آلو</a>
                            </td>
                            <td class="price">Rs35.00</td>
                            <td class="qty">1 / KG</td>
                            <td class="total">Rs 35.00</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-right">
                                Subtotal:	Rs 35.00
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-right">
                                <h2 class="grand-total"> Grand Total:	Rs35.00</h2>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>