<?php



Route::get('search','Product\ProductsController@index');
//inner-pages
Route::get('about-us','Misc\Misc@getAboutUs');
Route::get('privacy-policy','Misc\Misc@getPrivacyPolicy');
Route::get('return-policy','Misc\Misc@getReturnPolicy');
Route::get('terms-conditions','Misc\Misc@getTermsConditions');
//Categories Block

Route::get('send_test_email','OrderController@index');
Route::get('/getCategories',function (){
    return Utility::getCategoriesHtml();
});

//Auth Routes For Customer

Route::post('registerCustomer','Auth\RegisterController@registerCustomer')->name('registerCustomer');
Route::post('loginCustomer','Auth\LoginController@customerLogin')->name('loginCustomer');
Route::post('forgotPasword','Auth\ForgotPasswordController@forgotPasword')->name('forgotPasword');


Route::post('resetuserPasword','UsersController@resetuserPasword')->name('resetuserPasword');
Route::get('resetPasword/{id}','Auth\ResetPasswordController@resetPasword')->name('resetPasword');
Route::post('Paswordreset','Auth\ResetPasswordController@Paswordreset')->name('Paswordreset');



//Route For Each Product
Route::get('{product}.html', 'SingleProduct\SingleProductController@index');
Route::get('{category}/{product}.html', 'SingleProduct\SingleProductController@index');
Route::get('{product}.html', 'SingleProduct\SingleProductController@indexWithCategory');

Route::get('cart','Cart\CartController@index');

//Checkout
Route::get('checkout','Cart\CartController@goToCheckout');
Route::get('CheckedOutSuccessfully-ThankYou','Cart\CartController@checkOutSuccess');

Route::get('quickview/{slug}', 'Misc\Misc@getQuickView');


foreach (\App\Models\Categories::select('cleanURL')->get() as $category)
{
    Route::get('{'.$category->cleanURL.'}', 'Product\ProductsController@index');;
}


//-----add reviews

Route::post('cart/addreviews','Cart\CartController@addreviews');



//$categories = \App\Models\Categories::all();
//$categories = Utility::getCategories();
////Routes for Categories
//foreach (\App\Models\Categories::all() as $category)
//{
//    Route::get('/'.strtolower($category->cleanURL).'','Product\ProductsController@index');
//}


Route::get('/redirect/google', 'SocialAuthGoogleController@redirect');
Route::get('/callback/google', 'SocialAuthGoogleController@callback');


Route::get('/redirect/facebook', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');
//Cart Routes

Route::post('cart/add','Cart\CartController@addToCart');
Route::post('cart/update','Cart\CartController@updateCart');
Route::post('cart/delete','Cart\CartController@deleteCart');
Route::post('cart/deleteall','Cart\CartController@deleteallCart');
Route::get('cart/getCart','Cart\CartController@makeCart')->name('getCart');
Route::get('cart/getTotalItemsAndPrice','Cart\CartController@getTotalItemsAndPrice')->name('getCart');
Route::get('cart/getCartDropDown','Cart\CartController@makeCartDropDown')->name('getCartDropDown');
Route::post('cart/placeOrder','Cart\CartController@placeOrder')->name('placeOrder');

Route::post('cart/addtowishlist','Cart\CartController@addtowishlist');




