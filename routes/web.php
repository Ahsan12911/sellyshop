<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('run-artisan-command/{command}',function($command){
    Artisan::call($command);
});

Route::get('/Storeview',function (){
    return view('Storefront');
});
Route::get('/demostore', function () {
    return view('templates.front');
});
include ('admin/admin-routes.php');

Route::get('/setprice', 'Home\Home@setprice');
Route::group(['middleware' => ['websiteClose']], function () {

    include('front-end/front-end-routes.php');
    include('api/api-routes.php');

    Route::get('/', function () {
        return view('templates.home');
    });


    include ('front-end/front-end-routes.php');
    include ('api/api-routes.php');


    Auth::routes();

    Route::get('/test', function () {
        foreach (Selly::getProductsByCaregory() as $item) {
            echo $item;
        }
    });


    $this->get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('/admin/login', 'Auth\LoginController@doLogin');

    Route::get('order/list/{id}', 'Home\Home@orderlist');

    Route::get('order/Items', 'Home\Home@orderItems');
    
     Route::get('/', 'Home\Home@index')->name('home');


// Authentication Routes...

//Auth Middleware to Check if Users is login or Not
//Route::group(['middleware' => 'auth'], function () {
//    Route::get('/', function () {
//        return redirect('/admin/dashboard');
//    });
//});


//Route::get('/test','Test\Test@index');

    Auth::routes();

     Route::get('/home', 'HomeController@index')->name('home');

});


