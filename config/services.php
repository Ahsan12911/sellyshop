<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mandrill' => [
        'secret' => '0747761b05cf584d92b6a9cdd10e0563-us17',
    ],

    'mailgun' => [
        'domain' => env('test.selly.pk'),
        'secret' => env('key-8070425456bf6177712c3350a417472d'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Users::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '1754245054663102',
        'client_secret' => '9c3a7aa71c2defbfac5892a8e8d28d50',
        'redirect' => 'https://selly.pk/callback/toHome',
    ],
    'google' => [
        'client_id' => '5904946914-ene6su1ieb7h6b2eau3uis4njnnov6gd.apps.googleusercontent.com',
        'client_secret' => 'noV4Es8IMgxcW-kbp3w03g9M',
        'redirect' => 'https://selly.pk/callback/google'
    ],
];
