/**
 *get Categories From Server
 *
 * @param url
 * @return Categories
 */
function getCategories(url,target) {
    var categories = "";
    $.ajax({
        type: "GET",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: url,
        processData: false, // tell jQuery not to process the data
        contentType: false, // tell jQuery not to set contentType
        async : false,
        success: function (data) {
            categories =  data;
        },
        error:function (jqXHR, exception) {

        }
    });
    return categories;
}

//show loader
function showLoader(toggle) {
    if(toggle===true)
    {
        $('#preloader').css('display','block');
    }
    if(toggle===false)
    {
        $('#preloader').css('display','none');
    }
}